/*
 * Extends Easyui tree and makes use of a surrounding panel to save the newly ordered tree.
 * Minimum tree options are url, saveUrl (this one doesn't exist in easyui tree) and dnd:true
 * The surrouding panel must have save and cancel tool ('.icon-save', '.icon-cancel')
 *  @flo
  */
$.extend(jQuery.fn.tree.methods, {
    getDataTree: function (jq) {
        var definitions = [], methods = this;
        this.getRoots(jq).forEach(function (root) {
            definitions.push(methods.getData(jq, root.target));
        });
        var mapperFunc = function (element) { // can customize here the data format you want
            var result = { id: element.id };
            if (element.children) {
                result.children = element.children.map(mapperFunc);
            }
            return result;
        };
        return definitions.map(mapperFunc);
    },
    activatePanelControls: function (jq) {
        var $panel = jq.closest('.panel'), methods = this, opts = jQuery.data(jq[0]).tree.options;
        $panel.find('.icon-save, .icon-cancel').css('opacity', 0.6).hover(function () {
            $(this).css('opacity', 1);
        }, function () {
            $(this).css('opacity', 0.6);
        });
        $panel.find('.icon-save').click(function () {
            $.ajax({
                method: 'POST',
                url: opts.saveUrl, // saveUrl needed option
                data: { tree: methods.getDataTree(jq) },
                success: function () {
                    alert('Category definitions saved');
                    jq.tree('reload');
                }
            });
        });
        $panel.find('.icon-cancel').click(function () {
            jq.tree('reload');
        });
    },
    deActivatePanelControls: function (jq) {
        var $panel = jq.closest('.panel');
        $panel.find('.icon-save, .icon-cancel').css('opacity', 0.3).off();
    },
    dataTreeHasChanged: function (jq) {
        var dataTreeEquals = function (defs1, defs2) {
            if (defs1.length !== defs2.length) {
                return false;
            }
            return defs1.every(function (element, index) {
                if (element.id !== defs2[index].id) {
                    return false;
                }
                if (element.children && !defs2[index].children) {
                    return false;
                }
                if (element.children) {
                    return dataTreeEquals(element.children, defs2[index].children);
                }
                return true;
            });
        }, methods = this;
        return !dataTreeEquals(jQuery.data(jq[0]).dataTree, methods.getDataTree(jq));
    }
});

$.extend(jQuery.fn.tree.defaults, {
    onLoadSuccess: function (node, data) {
        var $this = $(this), methods = $this.tree.methods;
        // hack, because onLoadSuccess is triggered on node drops too!
        if (data.length !== 1) {
            jQuery.data(this, {
                dataTree: methods.getDataTree($this)
            });
            methods.deActivatePanelControls($this);
        }
    },
    onDrop: function () {
        var $this = $(this), methods = $this.tree.methods;
        if (!methods.dataTreeHasChanged($this)) {
            methods.deActivatePanelControls($this);
        } else {
            methods.activatePanelControls($this);
        }
    }
});