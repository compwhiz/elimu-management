function collapse(mnu){
			var node = $('#'+mnu).tree('getSelected');
			$('#'+mnu).tree('collapse',node.target);
		}
		
		function expand(mnu){
			var node = $('#'+mnu).tree('getSelected');
			$('#'+mnu).tree('expand',node.target);
		}
		
		function collapseAll(mnu){
			$('#'+mnu).tree('collapseAll');
		}
		
		function expandAll(mnu){
			$('#'+mnu).tree('expandAll');
		}
		
		function getSelected(mnu){
			var node = $('#'+mnu).tree('getSelected');
			if (node){
				var s = node.text;
				if (node.attributes){
					s += ","+node.attributes.p1+","+node.attributes.p2;
				}
				alert(s);
			}
		}
		
		function isLeaf(mnu){
			var node = $('#'+mnu).tree('getSelected');
			var b = $('#'+mnu).tree('isLeaf', node.target);
			alert(b)
		}
		
		function open_module(id, title, url){
			if(url!==''){
			 $('#fr_module').attr('src', url);
			 $('.layout-panel-center > div.panel-header > div.panel-title').html(title);
			 ui.call('./endpoints/helpinfo/','mnuid='+id+'&title='+title,'DivHelpInfo');
			}
		}
		
	
	function handleMinimize(winid,title){
     $('#parent').append('<a href="javascript:void(0)" class="easyui-linkbutton" onclick="$(\'#'+winid+'\').window(\'open\')">'+title+'</a>');
    }
    
     function openWindow(id,title,modkey,modtype,modvars,win,hei,minm,maxm,clo,col,res,bta,bte,btd,bti,btx){
		 
		var winid  = 'w'+id;
		var gridid = 'dg'+id;
		
	  if( $('#'+winid).length==0 ){
        $("body").append( $("<div>", {id: winid}) );

	    $('#'+winid).window({
	    href:'./endpoints/win/?modkey='+modkey,
	    method:'post',
	    title: title,
	    width: win,
	    height:hei,
	    fit:false,
	    border:false,
	    cache:true,
	    minimizable: (minm==1?true:false),
	    maximizable:(maxm==1?true:false),
	    closable: (clo==1?true:false),
	    collapsible:(col==1?true:false),
	    resizable:(res==1?true:false),
	    queryParams:{id:id, modkey:modkey, modtype:modtype, modvars:modvars, win:win, hei:hei, bta:bta, bte:bte, btd:btd, bti:bti, btx:btx},
	    onBeforeOpen:function(){},
	    tools:[{
                 iconCls:'icon-reload',
                 handler:function(){
                 $('#'+winid).window('refresh');
                 }
               }],
	    onLoad:function(){ 
		  
		 if(modtype==='dga') {
		  var dg = $('#'+gridid).datagrid({
		     url:'./endpoints/grid/',
             pagination:'true',
             rownumbers:'true',
             fitColumns:'true',
             singleSelect:'true',
             idField:'id',
             queryParams:{ modvars:modvars,function:'data' },
             multiSort:true,
             remoteFilter:true,
             fit:true,
             type:'post',
		     filterBtnIconCls:'icon-filter',
		 });
		dg.datagrid('enableFilter');
	  }
	},
	onLoadError:function(){},
	onBeforeClose:function(){},
	onMinimize:function(){
	     var title = $(this).window('options').title;
	     var s = $('<div class="ws"></div>').appendTo($('div.wb')).html(title);
	     
	     s.hover(
		  function(){$(this).addClass('ws-over')},
		  function(){$(this).removeClass('ws-over')}
	     );
	
	     s.bind('click', {w:this}, function(e){
		  $(e.data.w).window('open');
		  s.remove();
	     });
	     	
	} 	
		
    });
   }else{
	  $('#'+winid).window('open');
   }
  }
   
    function hideFilter(dgID){
	   var dg = $('#'+dgID).datagrid();
       $('#'+dgID).datagrid('disableFilter');
       $('#btnFilterHide').hide();
       $('#btnFilterShow').show();
    }
   
    function showFilter(dgID){
	   var dg = $('#'+dgID).datagrid();
	   dg.datagrid('enableFilter');
	   $('#btnFilterHide').show();
       $('#btnFilterShow').hide();
    }
    
      
        function dateYmdFormatter(date){
			var y = date.getFullYear();
			var m = date.getMonth()+1;
			var d = date.getDate();
			return y+'-'+(m<10?('0'+m):m)+'-'+(d<10?('0'+d):d);
		}
		
		function dateYmdParser(s){
			if (!s) return new Date();
			var ss = (s.split('-'));
			var y = parseInt(ss[0],10);
			var m = parseInt(ss[1],10);
			var d = parseInt(ss[2],10);
			if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
				return new Date(y,m-1,d);
			} else {
				return new Date();
			}
		}
		
	   function dateTimeYmdFormatter(date){
		   
			var y = date.getFullYear();
			var m = date.getMonth()+1;
			var d = date.getDate();
			var h = date.getHours();
			var i = date.getMinutes();
			
			return y+'-'+(m<10?('0'+m):m)+'-'+(d<10?('0'+d):d)+' '+h+':'+(i<10?('0'+i):i);
		}
		
		function dateTimeYmdParser(s){
			
			if (!s) return new Date();
			var ss = (s.split('-'));
			var tt = (s.split(' '));
			
			var y  = parseInt(ss[0],10);
			var m  = parseInt(ss[1],10);
			var d  = parseInt(ss[2],10);
			var t  = (tt[1]);
			
			if(typeof t !='undefined'){
			 var hm = (t.split(':'));
			 var h  = hm[0];
			 var mm = hm[1];
		    }else{
			 var date =  new Date();
			 var h    =  date.getHours();
			 var mm   =  date.getMinutes();
			}
			
			if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
				return new Date(y,m-1,d,h,mm);
			} else {
				return new Date();
			}
		}
		
		
    var crud = {
		  add :function(id,title){
            $('#dlg'+id).dialog({ title: 'New '+title, closed: false, resizable: true, modal: true});
			$('#fm'+id).form('clear');
		},
		edit:function (id,title){
			var row = $('#dg'+id).datagrid('getSelected');
			if (row){
			 $('#dlg'+id).dialog({ title: 'Edit '+title, closed: false, resizable: true, modal: true});
			 $('#fm'+id).form('load',row);
			}
		},
		save:function (id,module_packed){
			var validate =  $('#fm'+id).form('validate');
			var fdata = $('#fm'+id).serialize() + '&module='+module_packed+'&function=save';	
		    $.post('./endpoints/crud/', fdata, function(data) {

            if (data.success === 1) {
                $('#dlg'+id).dialog('close'); 
			    $('#dg'+id).datagrid('reload');
            } else {
            	$.messager.alert('Error',data.message,'error');
            }
           }, "json");
           
		},
		remove:function(id,title,module_packed){
			var row = $('#dg'+id).datagrid('getSelected');
			
			if (row){
				$.messager.confirm('Confirm','Are you sure you want to remove this record?',function(r){
					if (r){
						
			        var rdata = 'id='+row.id+'&module='+module_packed+'&function=remove';	
		            $.post('./endpoints/crud/', rdata, function(data) {

                    if (data.success === 1) {
                     $('#dlg'+id).dialog('close');
			         $('#dg'+id).datagrid('reload');
                    } else {
                     $.messager.alert('Error',data.message,'error');
                    }
                  }, "json");
					}
				});
			}
	   },
	  data_import:function(id)	{
		$('#fileupload'+id).val('');
	  	$('#dlg_import'+id).dialog('open');
	   },
	  data_export:function(id,module_packed)	{
	  	$('#dlg_export'+id).dialog('open');
      },
	  do_export:function(id,module_packed)	{
			        var export_type =$('#export_type'+id).val() ,rdata = 'module='+module_packed+'&function=export&export_type='+export_type;	
			        //$.messager.progress();
		            $.post('./endpoints/crud/', rdata, function(data) {

                    if (data.success === 1) {
                       
                    	//$.messager.progress('close');
                        $('#dlg_export'+id).dialog('close');
                        	
                      if(typeof data.export === 'string'){
                     	top.location = './' + data.export;
                      }else{
                      	 $.messager.alert('Error','unable to get export file location','error');	
                      }
                      
                    } else {
                      var err;
                      if(typeof data.message === 'undefined'){
                     	err  =  'An error occured';
                      }else{
                     	err  =  data.message;
                      }
                     
                      $.messager.alert('Error',err,'error');	
	                }
           
	             }, "json");
	   },
	   do_import:function(id,module_packed){
		var data = new FormData($('#fmi'+id)[0]);
		var filename = $('#fileupload'+id).val();
		if(filename!==''){
			$.messager.progress({text:'Uploading...'});
			$.ajax({
			url: './endpoints/crud/?module='+module_packed+'&function=import',
			type: 'POST',
			data: data,
			cache: false,
			async:'false',
			dataType: 'json',
			processData: false,
			contentType: false,
			success: function(data, textStatus, jqXHR){
				$.messager.progress('close');
				if(data.success==1){
				 if(typeof data.message ==='string'){
				  $.messager.alert('File Successfully Imported',data.message);
				  $('#dlg_import'+id).dialog('close');
			      $('#dg'+id).datagrid('reload');
				 }
				}else{
					
				 if(typeof data.message ==='string'){
        	 	   $.messager.alert('Error',data.message,'error');
        	 	 }
        	 	 
				 if(typeof data.export ==='string'){
        	 	   if(data.export!=''){
        	 	    $('#ifr_main').attr('src',data.export);
			       }
        	 	 }
        	 	 
        	 	 $('#fileupload'+id).val('');
        	 	 
				}
			},
			error: function(jqXHR, textStatus, errorThrown){
				console.log('ERRORS: ' + textStatus);
			}
		  });
       }
      },
    }
