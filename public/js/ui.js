var ui = {
	cc:function(  _function , _params , update_element){/*normal ajax call*/
	
        $.ajax({
          type: 'POST',    
          url:'./endpoints/crud/',
          data: 'function='+_function+'&'+_params,
          success: function(responseText){
            $('#'+update_element+'').html(responseText);
         }
        });
 	 },
	fc:function( _params , update_element){/*fast call*/
	
        $.ajax({
          type: 'POST',    
          url:'./endpoints/crud/',
          data: _params,
          success: function(responseText){
            $('#'+update_element+'').html(responseText);
         }
        });
 	 },
	mc:function( url , _params,update_element){/*mobile call*/
	
        $.ajax({
          type: 'POST',    
          url: './endpoints/mobile/',
          data: _params,
          success: function(responseText){
            $('#'+update_element+'').html(responseText);
         }
        });
 	 },
 	call:function(  url , _params , update_element){/*normal ajax call*/
	
        $.ajax({
          type: 'POST',    
          url:url,
          data: 'direct=1&'+_params,
          success: function(responseText){
            $('#'+update_element+'').html(responseText);
         }
        });
 	 },
 	 populate_fields:function(url, Params) {
	 	       	 $.post(url,   Params , function(data){
	 	       	 	
	 	       	         $.each(data, function(field,value) {
	 	       	         	
	 	       	          var elm         = $('input#'+field);
	 	       	          var is_input    = $(elm).is("input");
	 	       	          var is_select   = $(elm).is("select");
	 	       	          var is_textarea = $('#'+field).is("textarea");
                          var elementType = $('input#'+field).prop('tagName') ? 1 : 0;
                          
                             if(is_textarea===true){
                             	$('#'+field).val(value);
                             }else if( (elementType ==1) ){
								if ( $('#'+field).hasClass('easyui-datebox')){
							     $('#'+field).datebox('setValue', value);
						        }else{
                             	 $('input#'+field).val(value);
							    }
                             }else{
                             	jQuery("select#"+field+" option[value='"+value+"']").attr("selected", "selected");
                             }
	 	       	          });
	 	       	          
                   }, "json" );
	}
}
