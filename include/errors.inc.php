<?php

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('log_errors',true);
ini_set('error_log',ROOT.'logs/errors.log');

//set_error_handler('custom_error_handler');

 function custom_error_handler($errno, $errstr, $errfile, $errline, $context) {

      switch ($errno){
      	case E_NOTICE:
      		$desc = 'E_NOTICE';
      	break;
      	case E_USER_NOTICE:
      		$desc = 'E_USER_NOTICE';
      	break;
      	case E_USER_WARNING:
      		$desc = 'E_USER_WARNING';
      	break;
      	case E_STRICT:
      		$desc = 'E_STRICT';
      	break;
      	case E_WARNING:
      		$desc = 'E_WARNING';
      	break;
      	case E_ERROR:
      		$desc = 'E_ERROR';
      	break;
      	case E_CORE_ERROR:
      		$desc = 'E_CORE_ERROR';
      	break;
      	case E_PARSE:
      		$desc = 'E_PARSE';
      	break;
      	case E_COMPILE_ERROR:
      		$desc = 'E_COMPILE_ERROR';
      	break;
      	case E_COMPILE_WARNING:
      		$desc = 'E_COMPILE_WARNING';
      	break;
      	default:
      		$desc = $errno;
       }

          if($errno != E_STRICT && $errno!= E_WARNING ){

            $error_file  = ROOT.'logs/'.md5($errfile.$errno).'.php';
            $context     = base64_encode((serialize($context)));
            $content     =  "<?php ".PHP_EOL;
            $content    .=  "\$vars = \"{$context}\";".PHP_EOL;
            $content    .=  "echo '<pre>';".PHP_EOL;
            $content    .=  "var_dump( unserialize((base64_decode(\$vars))));".PHP_EOL;
            $content    .=  "echo '</pre>';".PHP_EOL;
            $content    .=  "?> ".PHP_EOL;

            $fp = @fopen( $error_file , 'w' );

            if(is_readable($error_file)){
             @flock($fp,3);
             @fwrite( $fp , $content );
             @fclose( $fp);
		   }

        }

}
