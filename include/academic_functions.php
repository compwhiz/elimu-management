<?php


function form_summary($yearcode, $termcode, $formcode, $exam, $show_column = 'M', $save = false)
{
    global $db;

    $summary_streams = array();
    $summary_points = array();
    $summary = array();
    $show_column = strtoupper($show_column);

    $student_means_all = $db->GetAssoc("SELECT ADMNO, FULLNAME, FORMCODE, STREAMCODE,TOTAL,NUMSUBJ,TOTALP,TOTALM,GRADECODEP,GRADECODEM,
 	 POSTREAM_MARKS,POSFORM_MARKS,POSTREAM_POINTS,POSFORM_POINTS
 	  FROM VIEWSTUDENTSMEAN
 	  WHERE YEARCODE='{$yearcode}'
 	  AND TERMCODE='{$termcode}'
 	  AND FORMCODE='{$formcode}'
 	  AND EXAM='{$exam}'
 	  ORDER BY POSFORM_MARKS ASC
 	  ");

    if (sizeof($student_means_all) > 0) {

        foreach ($student_means_all as $admno => $my_mean) {

            $streamcode = valueof($my_mean, 'STREAMCODE');
            $total = valueof($my_mean, 'TOTAL', '', 'round');
            $totalm = valueof($my_mean, 'TOTALM', '', 'round');
            $totalp = valueof($my_mean, 'TOTALP');
            $gradecode_col = 'GRADECODE' . $show_column;
            $gradecode = valueof($my_mean, $gradecode_col);

            if (isset($summary[$streamcode][$gradecode])) {
                ++$summary[$streamcode][$gradecode];
            } else {
                $summary[$streamcode][$gradecode] = 1;
            }

            if (array_key_exists($streamcode, $summary_points)) {
                $summary_points[$streamcode] += $totalp;
            } else {
                $summary_points[$streamcode] = $totalp;
            }

            if (!array_key_exists($streamcode, $summary_streams)) {
                $summary_streams[$streamcode] = $streamcode;
            }

        }

        $grades_points = $db->GetAssoc('select GRADECODE,ROUND(MAX) POINTS  from SAMGRD');

        $summary_grades = $db->GetAssoc('SELECT DISTINCT GRADECODE,GRADECODE GRADENAME FROM SAXGRD ORDER BY POINTS DESC');
        $summary_grades['Y'] = 'Y';
        $summary_grades['Z'] = 'Z';
        $summary_grades['X'] = 'X';

        $totals = array();
        $sort = array();
        $totals_form = array();
        $totals_form['grade_points'] = array();
        $totals_form['num_grade_students'] = array();
        $totals_form['avg_points_sum'] = 0;
        $totals_form['sum_points'] = 0;
        $totals_form['avg_points'] = 0;
        $totals_form['num_students'] = 0;

        $summary_td_colspan = 1 + count($summary_grades) + 3;

        if (count($summary_streams) > 0) {
            $overall_students = 0;
            $summary_points_form = 0;
            arsort($summary_points);

            foreach ($summary_points as $summary_streamcode => $stream_points) {
                $overall = array();
                $num_students = array_sum($summary[$summary_streamcode]);

                if (isset($overall_students)) {
                    $overall_students += $num_students;
                } else {
                    $overall_students = $num_students;
                }

                $totals[$summary_streamcode]['num_students'] = $num_students;

                if (count($summary_grades) > 0) {
                    $stream_sum_points = 0;

                    foreach ($summary_grades as $x => $summary_gradecode) {

                        $num_grade_students = isset($summary[$summary_streamcode][$summary_gradecode]) ? ($summary[$summary_streamcode][$summary_gradecode]) : null;

                        if (isset($grades_points[$summary_gradecode]) && $num_grade_students >= 1) {
                            $stream_sum_points += ($grades_points[$summary_gradecode] * $num_grade_students);
                        }

                        if (isset($overall[$summary_gradecode])) {
                            $overall[$summary_gradecode] += $num_grade_students;
                        } else {
                            $overall[$summary_gradecode] = $num_grade_students;
                        }

                        $totals[$summary_streamcode]['num_grade_students'][$summary_gradecode] = $num_grade_students;
                        if ($num_grade_students > 0 && isset($grades_points[$summary_gradecode])) {
                            $totals[$summary_streamcode]['stream_grade_points'][$summary_gradecode] = $grades_points[$summary_gradecode] * $num_grade_students;
                        } else {
                            $totals[$summary_streamcode]['stream_grade_points'][$summary_gradecode] = 0;
                        }


                        if (isset($totals_form['num_grade_students'][$summary_gradecode])) {
                            $totals_form['num_grade_students'][$summary_gradecode] += $totals[$summary_streamcode]['num_grade_students'][$summary_gradecode];
                        } else {
                            $totals_form['num_grade_students'][$summary_gradecode] = $totals[$summary_streamcode]['num_grade_students'][$summary_gradecode];
                        }

                        if (isset($totals_form['grade_points'][$summary_gradecode])) {
                            $totals_form['grade_points'][$summary_gradecode] += $totals[$summary_streamcode]['stream_grade_points'][$summary_gradecode];
                        } else {
                            $totals_form['grade_points'][$summary_gradecode] = $totals[$summary_streamcode]['stream_grade_points'][$summary_gradecode];
                        }

                    }
                }


                $num_x = valueof($overall, 'X', 0);
                $num_y = valueof($overall, 'Y', 0);
                $num_z = valueof($overall, 'Z', 0);

                $num_X_Y_Z = ($num_x + $num_y + $num_z);


                $totals[$summary_streamcode]['stream_sum_points'] = array_sum($totals[$summary_streamcode]['stream_grade_points']);
                $totals[$summary_streamcode]['stream_avg_points'] = round($totals[$summary_streamcode]['stream_sum_points'] / ($totals[$summary_streamcode]['num_students'] - $num_X_Y_Z), 3);
                $totals[$summary_streamcode]['stream_gradecode'] = get_grade_code($totals[$summary_streamcode]['stream_avg_points']);

                $totals_form['sum_points'] += $totals[$summary_streamcode]['stream_sum_points'];
                $totals_form['avg_points_sum'] += $totals[$summary_streamcode]['stream_avg_points'];
                $totals_form['num_students'] += $num_students;

                $sort[$summary_streamcode] = $totals[$summary_streamcode]['stream_avg_points'];

            }
        }

        $totals_form['avg_points'] = $totals_form['avg_points_sum'] / count($totals);
        $totals_form['gradecode'] = get_grade_code($totals_form['avg_points']);

        arsort($sort);

        if ($save) {
            foreach ($sort as $streamcode => $mean) {
                $record = new ADODB_Active_Record('SAFSMN', array('FORMCODE', 'STREAMCODE', 'YEARCODE', 'TERMCODE', 'EXAM'));

                if (empty($record->_original)) {
                    $record->formcode = $formcode;
                    $record->streamcode = $streamcode;
                    $record->yearcode = $yearcode;
                    $record->termcode = $termcode;
                    $record->exam = $exam;
                }
                $record->meanscore = $mean;
                $record->gradecode = get_grade_code($record->meanscore);

                @$record->Save();
            }
            return;
        }


        echo " \r\n<div class=\"page-break\"></div>  \r\n";

        echo "<table cellpadding=\"2\" cellspacing=\"0\" width=\"100%\" class=\"data\">";

        echo "<tr  nobr=\"true\">";
        echo "<td colspan=\"{$summary_td_colspan}\" align=\"center\"><b>MEAN GRADE ANALYSIS</b></td>";
        echo "<tr>";

        echo "<tr>";
        echo "<td class=\"header\" >CLASS</td>";

        if (count($summary_grades) > 0) {
            foreach ($summary_grades as $x => $summary_gradecode) {
                echo "<td class=\"header\" >{$summary_gradecode}</td>";
            }
        }

        echo "<td class=\"header\" >ENTRY</td>";
        echo "<td class=\"header\" >MEAN POINT</td>";
        echo "<td class=\"header\" >M.G</td>";
        echo "</tr>";

        foreach ($sort as $streamcode => $stream_avg) {
            $num_students = isset($totals[$streamcode]['num_students']) ? $totals[$streamcode]['num_students'] : 0;
            $stream_gradecode = isset($totals[$streamcode]['stream_gradecode']) ? $totals[$streamcode]['stream_gradecode'] : 0;
            echo "<tr>";
            echo "<td>{$streamcode}</td>";

            if (count($summary_grades) > 0) {
                foreach ($summary_grades as $x => $summary_gradecode) {
                    $gradecode_num_students = isset($totals[$streamcode]['num_grade_students'][$summary_gradecode]) ? $totals[$streamcode]['num_grade_students'][$summary_gradecode] : null;
                    echo "<td>{$gradecode_num_students}</td>";
                }
            }

            echo "<td>{$num_students}</td>";
            echo "<td>{$stream_avg}</td>";
            echo "<td>{$stream_gradecode}</td>";
            echo "</tr>";
        }

        echo "<tr>";
        echo "<td>&nbsp;</td>";

        if (count($summary_grades) > 0) {
            foreach ($summary_grades as $x => $summary_gradecode) {
                echo "<td>&nbsp;</td>";
            }
        }

        echo "<td>&nbsp;</td>";
        echo "<td>&nbsp;</td>";
        echo "<td>&nbsp;</td>";
        echo "</tr>";

        echo "<tr>";
        echo "<td><b>OVERALL FORM</b></td>";

        if (count($summary_grades) > 0) {
            foreach ($summary_grades as $x => $summary_gradecode) {
                $num_grade_students = isset($totals_form['num_grade_students'][$summary_gradecode]) ? $totals_form['num_grade_students'][$summary_gradecode] : null;
                echo "<td><b>{$num_grade_students}</b></td>";
            }
        }
        $points = $totals_form['avg_points'] ? round($totals_form['avg_points'], 1) : '';

        echo "<td><b>{$totals_form['num_students']}</b></td>";
        echo "<td><b>{$points}</b></td>";
        echo "<td><b>{$totals_form['gradecode']}</b></td>";
        echo "</tr>";
        echo "</table>";

    }
}

function get_grade_code($points)
{
    global $db;
    return $db->CacheGetOne(1200, "select GRADECODE from SAMGRD WHERE MIN<={$points} AND MAX>={$points}");
}
