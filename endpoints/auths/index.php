<?php

require_once('../../init.php');
require_once(ROOT.'config/db.php');

 $vars          =  filter_input(INPUT_GET , 'vars');
 $vars_unpack   =  unpackvars($vars);
 $appid         =  valueof($vars_unpack, 'appid');
 $modid         =  valueof($vars_unpack, 'modid');
 
 header('Content-type: text/json');
 
 $auths = new auths($appid,$modid);
 
 echo $auths->list_menu();
 