<?php

require_once('../../init.php');
require_once(ROOT.'config/db.php');

 $modtype       =  filter_input(INPUT_POST , 'modtype');
 $vars          =  filter_input(INPUT_POST , 'modvars');
 $vars_unpack   =  unpackvars($vars);
 
 $appid         =  valueof($vars_unpack, 'appid');
 $modid         =  valueof($vars_unpack, 'modid');
 $mnuid         =  valueof($vars_unpack, 'mnuid');
 $modpath       =  valueof($vars_unpack, 'modpath');
 
 if(empty($modpath)){
 	die("cant find module");
 }
 
define('MNUID',$mnuid);

 $modpath_arr  = explode('/', $modpath);
 $modpath_arrv = array_reverse($modpath_arr);
 $dir          = isset($modpath_arrv[0]) ? $modpath_arrv[0] : '';
 $modpath_prnt = str_replace($dir.'/', '', $modpath);
 $file_config  = ROOT. $modpath. '/cfg.php';
 $file_index   = ROOT. $modpath. '/index.php';
 $file_class   = ROOT. $modpath. '/'. $dir .'.php';
 
 if(is_readable($file_config) && is_file($file_config)){
 	require_once($file_config);
 }else{
 	die("the module config file is not viewable");
 }

 switch($modtype){
 case 'dga':
  $grid = new grid();
  $grid->draw_grid( $mnuid, $vars );
  $grid->draw_form_simple( $mnuid, $vars );
 break;
 default:
   if(is_readable($file_index) && is_file($file_index)){
 	require_once($file_index);
   }else{
 	die("module not viewable");
   }
}
