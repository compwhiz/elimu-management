<?php

require_once('../../init.php');
require_once(ROOT.'config/db.php');

 $q             =  filter_input(INPUT_POST , 'q');
 $q             =  trim($q);
 $vars          =  filter_input(INPUT_POST , 'vars');
 $vars_unpack   =  unpackvars($vars);
 
 $skip          =  array('IdField','TextField','source');
 $select_cols   =  array();
 $json_cols     =  array();
 
 $IdField       =  valueof($vars_unpack, 'IdField');
 $TextField     =  valueof($vars_unpack, 'TextField');
 $source        =  valueof($vars_unpack, 'source');
 $showMax       =  !empty($q) ? 10 : 5;
 
 foreach ($vars_unpack as $db_col => $mapped_field){
 	if(!in_array($db_col,$skip)){
 	 $select_cols[]    = " {$db_col} {$mapped_field}";
 	 $json_cols[]      = $mapped_field;
		
 	 if($IdField == $mapped_field){
	  $keycol  = $db_col;
	 }else if($TextField == $mapped_field){
	  $textcol = $db_col; 
	 }
	 
 	}else{
		
	}
 }
 
 $select_cols_str = implode("," ,$select_cols);
 $rs  = $db->SelectLimit("select {$select_cols_str} from {$source} where {$IdField} like '%{$q}%' or {$TextField} like '%{$q}%'", $showMax);
 
$str = '';
if($rs){
	$numRecs = $rs->RecordCount();
	$numCols = count($json_cols);
	if($numRecs>0){
		$count = 1;
		$count_cols = 0;

		$str = '';
		while (!$rs->EOF){
			
			 $str .= "{";
			 
			 $count_cols = 1;
			 foreach ($json_cols as $json_col ){
			 	$json_col_value = isset($rs->fields[$json_col]) ? $rs->fields[$json_col] : '***';
			 	$str .= "\"{$json_col}\":\"{$json_col_value}\"";
			 	$str .=  $count_cols<$numCols ? ',' : '';
			 	
			 	++$count_cols;
			 }
		 
			 $str .= "}";
			 
            $str .=  $count <$numRecs ? ",\r\n" : '';			 
			
		 ++$count;	
		 $rs->MoveNext();
		}
	}
}
 
 header('Content-type: text/json');
 
 echo '['.$str.']';
