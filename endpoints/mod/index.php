<?php

require_once('../../init.php');
require_once(ROOT.'config/db.php');

//print_pre($_POST);
//print_pre($_GET);

 $vars          =  filter_input(INPUT_GET , 'vars');
 $vars_unpack   =  unpackvars($vars);
 
// print_pre($vars_unpack);
 
 $appid         =  valueof($vars_unpack, 'appid');
 $modid         =  valueof($vars_unpack, 'modid');
 $mnuid         =  valueof($vars_unpack, 'mnuid');
 $modpath       =  valueof($vars_unpack, 'modpath');
 
 if(empty($modpath)){
 	die("cant find module");
 }
 
 $modpath_arr  = explode('/', $modpath);
 $modpath_arrv = array_reverse($modpath_arr);
 $dir          = isset($modpath_arrv[1]) ? $modpath_arrv[1] : '';
 $modpath_prnt = str_replace($dir.'/', '', $modpath);
 $file_config  = ROOT. $modpath. '/cfg.php';
 $file_index   = ROOT. $modpath. '/index.php';
 $file_class   = ROOT. $modpath. '/'. $dir .'.php';
 
// echo "\$modpath_prnt={$modpath_prnt} <br>";
// echo "\$file_config={$file_config} <br>";
// echo "\$file_index={$file_index} <br>";
// echo "\$file_class={$file_class} <br>";
 
 if(is_readable($file_index) && is_file($file_index)){
 	require_once($file_index);
 }else{
 	die("module not viewable");
 }
 