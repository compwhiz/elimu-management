<?php

/**
 *requires $_GET['to'] = 'script.php'
 */
 
require_once('../../init.php');
require_once(ROOT.'config/db.php');

if(isset($_POST['module']) && isset($_POST['function'])){
 $modulestr      =  filter_input(INPUT_POST , 'module');
 $function       =  filter_input(INPUT_POST , 'function');
}elseif(isset($_POST['modvars']) && isset($_POST['function'])){
 $modulestr      =  filter_input(INPUT_POST , 'modvars');
 $function       =  filter_input(INPUT_POST , 'function');
}elseif(isset($_GET['modvars']) && isset($_GET['function'])){
 $modulestr      =  filter_input(INPUT_GET , 'modvars');
 $function       =  filter_input(INPUT_GET , 'function');
}elseif(isset($_GET['module']) && isset($_GET['function'])){
 $modulestr      =  filter_input(INPUT_GET , 'module');
 $function       =  filter_input(INPUT_GET , 'function');
}elseif(isset($_GET['module']) && ( !isset($_GET['function']) || !isset($_POST['function']))){ 
 $modulestr      =  filter_input(INPUT_GET , 'module');
 $function       =  '';
}elseif(isset($_GET['modvars']) && ( !isset($_GET['function']) || !isset($_POST['function']))){ 
 $modulestr      =  filter_input(INPUT_GET , 'modvars');
 $function       =  '';
}else{
  header('Content-type : text/json');
  echo json_response( 0 , 'invalid end point');
  die;
}

 $goto           =  filter_input(INPUT_GET , 'to', FILTER_SANITIZE_STRING);
 $module_unpack  =  unpackvars($modulestr);
 $modpath        =  valueof($module_unpack, 'modpath');
 $mnuid          =  valueof($module_unpack, 'mnuid');
 $module_index   =  ROOT .$modpath .'/print/index.php';
 $file_config    =  ROOT. $modpath. '/cfg.php';
 $goto_file      =  ROOT. $modpath. '/'.$goto;

 define('MNUID',$mnuid);

 if(is_readable($file_config) && is_file($file_config)){
 	require_once($file_config);
 }else{
 	die("the module config file is not viewable");
 }
 
 if(is_readable($goto_file) && is_file($goto_file)){
 	require_once($goto_file);
 }else{
 	die("the Goto File is not viewable");
 }
 
