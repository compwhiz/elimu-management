<?php

 $db->debug=0;
 
 
 $tables_valid = array();
 $xml_files    = Datadict_FileList('ac');
 
 if(count($xml_files)>0){
	
 $schema = new adoSchema( $db );
 $schema->debug = false;

  foreach ($xml_files as $xml_file_path => $xml_file){
  	
  echo 'loading '.$xml_file_path.'<br>';
  
  $sql = array();
  $schema->continueOnError = true;
  $schema->SetUpgradeMethod('REPLACE');
  $sql = $schema->ParseSchema($xml_file_path, false);

  if(count($sql)>0) {
  foreach($sql as $sql_statement){
	if(strstr($sql_statement, 'CREATE TABLE')){
		 $sql2array  = explode(' ',$sql_statement);
         
         if(isset($sql2array[2]) && !empty($sql2array[2])){
          $tables_valid[$sql2array[2]]   =  $sql2array[2];
         }
	}//if
   }//
   }//foreach xml file
  }//if sql
 }//if xml files

$tables_db     = $db->MetaTables('TABLES');
$tables_aliens = array();

if(count($tables_db)){
	foreach ($tables_db as $table_db){
		if(!array_search($table_db, $tables_valid)){
//			if( !strstr($table_db, '_LOG') && !strstr($table_db, 'VIEW') ){
			if( !strstr($table_db, '_LOG')){
			 $tables_aliens[] = $table_db;
			}
		}
	}
}
$db->debug=1;
asort($tables_aliens);

$tables_aliens_sorted = array_values($tables_aliens);

if(count($tables_aliens_sorted)>0){
	     $time     = date('YmdHi');
	     $log_File = ROOT ."logs/drop-tables.{$time}.log";
	     $fh       = fopen($log_File, 'a') or die("can't open file");
	foreach ($tables_aliens_sorted as $alien_table){
		 fwrite($fh, "DROP TABLE {$alien_table}". "\r\n");
		$db->Execute("DROP TABLE {$alien_table}");
		$db->Execute("DROP VIEW {$alien_table}");
	}
	fclose($fh);
}

 function Datadict_FileList($appid) {
   global  $root_path;

  $fileDirectory  = ROOT .'config/dd/';

  if(!is_readable($fileDirectory)) exit("Directory <b>'{$fileDirectory}'</b> does not exist or is not readable!");

  $Datadict_files       = array();

  try {
	$it = new RecursiveIteratorIterator(new IgnorantRecursiveDirectoryIterator($fileDirectory), RecursiveIteratorIterator::SELF_FIRST );
	foreach( $it as $fullFileName => $fileSPLObject )
         if(!is_dir($fullFileName))
         {
		  if (@eregi("^\.{1,2}$|\.(xml)$", $fullFileName)) {
		  	 $pathinfo                 =  pathinfo($fullFileName);
		  	 $filename	               =  $pathinfo['basename'];
	 		 $Datadict_files[$fullFileName] = $filename;
	 	  }
         }
   }
    catch (UnexpectedValueException $e) {
	 printf("Directory [%s] contained a directory we can not recurse into", $dir);
   }

   asort($Datadict_files);
   return $Datadict_files;

 }

 class IgnorantRecursiveDirectoryIterator extends RecursiveDirectoryIterator { 
    function getChildren() { 
        try { 
            return new IgnorantRecursiveDirectoryIterator($this->getPathname()); 
        } catch(UnexpectedValueException $e) { 
            return new RecursiveArrayIterator(array()); 
        } 
    } 
} 
