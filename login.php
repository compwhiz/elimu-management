<?php
 global $schoolname; 
?>
<!DOCTYPE html>
<html lang="en">
    <head>
		<meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <title>Elimu</title>
        <meta name="description" content="Elimu" />
        <meta name="keywords" content="Elimu" />
        <link rel="shortcut icon" href="../favicon.ico"> 
        <link rel="stylesheet" type="text/css" href="./public/css/login.css" />
        <link rel="stylesheet" type="text/css" href="./public/css/themes/default/easyui.css">
		<script src="./public/js/modernizr.custom.63321.js"></script>
		<!--[if lte IE 7]><style>.main{display:none;} .support-note .note-ie{display:block;}</style><![endif]-->
		<style>	
body {
	background: #7f9b4e url(./public/logo.png) no-repeat center top;
	-webkit-background-size: cover;
	-moz-background-size: cover;
	background-size: cover;
	font-family: 'Lato', Calibri, Arial, sans-serif;
    font-weight: 300;
    font-size: 15px;
    color: #333;
    -webkit-font-smoothing: antialiased;
    overflow-y: scroll;
    overflow-x: hidden;
    max-width: 100%;
    height: auto;
    width: auto\9; /* ie8 */
}
.container > header h1,
.container > header h2 {
	color: #fff;
	text-shadow: 0 1px 1px rgba(0,0,0,0.7);
}
			
html { height: 100%; }

a {
	color: #555;
	text-decoration: none;
}

.container {
	width: 100%;
	position: relative;
}

.clr {
	clear: both;
	padding: 0;
	height: 0;
	margin: 0;
}

.main {
	width: 90%;
	margin: 0 auto;
	position: relative;
}

.container > header {
	margin: 10px;
	padding: 20px 10px 10px 10px;
	position: relative;
	display: block;
	text-shadow: 1px 1px 1px rgba(0,0,0,0.2);
    text-align: center;
}

.container > header h1 {
	font-size: 30px;
	line-height: 38px;
	margin: 0;
	position: relative;
	font-weight: 300;
	color: #666;
	text-shadow: 0 1px 1px rgba(255,255,255,0.6);
}

.container > header h2 {
	font-size: 14px;
	font-weight: 300;
	margin: 0;
	padding: 15px 0 5px 0;
	color: #666;
	font-family: Cambria, Georgia, serif;
	font-style: italic;
	text-shadow: 0 1px 1px rgba(255,255,255,0.6);
}

/* Header Style */
.codrops-top {
	line-height: 24px;
	font-size: 11px;
	background: #fff;
	background: rgba(255, 255, 255, 0.8);
	text-transform: uppercase;
	z-index: 9999;
	position: relative;
	font-family: Cambria, Georgia, serif;
	box-shadow: 1px 0px 2px rgba(0,0,0,0.2);
}

.codrops-top:before,
.codrops-top:after {
    content: " "; /* 1 */
    display: table; /* 2 */
}

.codrops-top:after {
    clear: both
}

.codrops-top a {
	padding: 0px 10px;
	letter-spacing: 1px;
	color: #333;
	display: inline-block;
}

.codrops-top a:hover {
	background: rgba(255,255,255,0.6);
}

.codrops-top span.right {
	float: right;
}

.codrops-top span.right a {
	float: left;
	display: block;
}



.support-note span{
	color: #ac375d;
	font-size: 16px;
	display: none;
	font-weight: bold;
	text-align: center;
	padding: 5px 0;
}
		</style>
    </head>
    <body>
       <div class="container">
		
            <div class="codrops-top">
                <a href="./"><strong>ElimuSwift</strong></a>
                <span class="right">
                    <a href="https://compwhiz.co.ke/knowledgebase">
                        <strong>Help</strong>
                    </a>
                    <a href="https://compwhiz.co.ke/elimubugs">
                        <strong>Submit Bug</strong>
                    </a>
                </span>
            </div>
			
			<header>

                <h1><strong>&nbsp;</strong></h1>
				<h2>&nbsp;</h2>

				<div class="support-note">
					<span class="note-ie">Sorry, only modern browsers.</span>
				</div>
				
			</header>
			
			<section class="main">
				<form class="form_login" id="form_login" method="POST" action="./" onsubmit="return false;">
					<h1><span class="log-in">Log in</span> to <span class="sign-up">Elimu</span></h1>
					<p class="float">
						<label for="login"><i class="icon-user"></i>Username</label>
						<input type="text" id="login" name="login" placeholder="Username or email" autocomplete="off" >
					</p>
					<p class="float">
						<label for="password"><i class="icon-lock"></i>Password</label>
						<input type="password" id="password" name="password" placeholder="Password" class="showpassword" autocomplete="off" >
					</p>
					<p class="clearfix"> 
						<input type="submit" name="submit" value="Log in" onclick="ui.login();return false;">
						<a href="#" class="forgotpass">Forgot Password</a>
					</p>
					
				</form>​​
			</section>
			
			
        </div>
        
       <script type="text/javascript" src="./public/js/jquery.min.js"></script>
	   <script type="text/javascript" src="./public/js/jquery.easyui.min.js"></script>
	   
		<script type="text/javascript">
		
			$(function(){
			    $(".showpassword").each(function(index,input) {
			        var $input = $(input);
			        $("<p class='opt'/>").append(
			            $("<input type='checkbox' class='showpasswordcheckbox' id='showPassword' />").click(function() {
			                var change = $(this).is(":checked") ? "text" : "password";
			                var rep = $("<input placeholder='Password' type='" + change + "' />")
			                    .attr("id", $input.attr("id"))
			                    .attr("name", $input.attr("name"))
			                    .attr('class', $input.attr('class'))
			                    .val($input.val())
			                    .insertBefore($input);
			                $input.remove();
			                $input = rep;
			             })
			        ).append($("<label for='showPassword'/>").text("Show password")).insertAfter($input.parent());
			    });

			    $('#showPassword').click(function(){
					if($("#showPassword").is(":checked")) {
						$('.icon-lock').addClass('icon-unlock');
						$('.icon-unlock').removeClass('icon-lock');    
					} else {
						$('.icon-unlock').addClass('icon-lock');
						$('.icon-lock').removeClass('icon-unlock');
					}
			    });
			});
		
	  var ui = {	
			
		login:function (){
			$.messager.progress();
		    $.post('./endpoints/login/',  $('#form_login').serialize() , function(data) {
		    $.messager.progress('close');
            if (data.success === 1) {
                $.messager.show({ title: 'Success', msg: data.message });
				top.location = './';
            } else {
            	$.messager.alert('Log In Error',data.message,'error');
            }
           }, "json");
           
		}
		};
		</script>
    </body>
</html>
