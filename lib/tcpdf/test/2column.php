<?php

require_once('../config/lang/eng.php');
require_once('../tcpdf.php');

$PDF_HEADER_TITLE = 'My Test Report';

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Uni-Plus');
$pdf->SetTitle('My Fisrt Report');
$pdf->SetSubject(PDF_AUTHOR. 'Report');
$pdf->SetKeywords('uniplus ,report,pdf');

$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
//$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
//$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

$pdf->SetFont('dejavusans', '', 14, '', true);

$pdf->AddPage();

//$html = "<br><br><br>Hi, iam <b color=\"red\">Erax</b>";

$left_column   = " 
  <table>
    <tr>
     <td>r1 cell 1</td>
     <td>r1 cell 2</td>
     <td>r1 cell 3</td>
    </tr>
    <tr>
     <td>r2 cell 1</td>
     <td>r2 cell 2</td>
     <td>r2 cell 3</td>
    </tr>
    
  </table>
 ";
$right_column  = "right right right right right right right right right right right right right right right";


// get current vertical position
$y = $pdf->getY();

// set color for background
$pdf->SetFillColor(255, 255, 200);

// set color for text
$pdf->SetTextColor(0, 0, 0,'','','red');

// write the first column
$pdf->writeHTMLCell(80, 100, '', $y, $left_column, 1, 0, 0, true, 'J', true);

// set color for background
$pdf->SetFillColor(215, 235, 255);

// set color for text
$pdf->SetTextColor(127, 31, 0);

// write the second column
$pdf->writeHTMLCell(80, 100, '', '', $right_column, 1, 1, 0, true, 'J', true);

//test another set
$y = $pdf->getY();
$pdf->writeHTMLCell(80, 100, '', $y, $right_column, 1, 0, 0, true, 'J', true);
$pdf->writeHTMLCell(80, 100, '', '', $left_column, 1, 1, 0, true, 'J', true);

// reset pointer to the last page
$pdf->lastPage();

$pdf->Output('erax2column.pdf','I');