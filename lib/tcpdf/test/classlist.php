<?php
//============================================================+
// File name   : example_048.php
// Begin       : 2009-03-20
// Last Update : 2010-08-08
//
// Description : Example 048 for TCPDF class
//               HTML tables and table headers
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com s.r.l.
//               Via Della Pace, 11
//               09044 Quartucciu (CA)
//               ITALY
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: HTML tables and table headers
 * @author Nicola Asuni
 * @since 2009-03-20
 */

require_once('../config/lang/eng.php');
require_once('../tcpdf.php');
require_once('db.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Uni-Plus');
$pdf->SetTitle('StudentList');
$pdf->SetSubject('Students List');
$pdf->SetKeywords('Students, Students List, classlist');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', 'B', 12);

// add a page
$pdf->AddPage();

$pdf->Write(0, ' ', '', 0, 'L', true, 0, false, false, 0);

$pdf->SetFont('helvetica', '', 8);

// -----------------------------------------------------------------------------

// NON-BREAKING ROWS (nobr="true")


$data  = $db->Execute("SELECT * FROM VIEWMARKS");
//print_pre($data->fields);
//exit();

$table = <<<EOD
<table border="1" cellpadding="2" cellspacing="0" align="left" style=\"border:1px solid #f00;color:red;\">
 <tr nobr="true">
  <th colspan="6">Class List</th>
 </tr>
EOD;

$bgcolor = '#EEEEEE';	

$table .= <<<EOD
 <tr nobr="true">
  <th width=\"5%\" bgcolor="{$bgcolor}">No.</th>
  <th bgcolor="{$bgcolor}">Student Number</th>
  <th bgcolor="{$bgcolor}">Student Name</th>
  <th bgcolor="{$bgcolor}">Unit Code</th>
  <th bgcolor="{$bgcolor}">Year</th>
  <th bgcolor="{$bgcolor}">Semester</th>
 </tr>
EOD;

if($data->RecordCount()>0){
	$count=1;
while (!$data->EOF){
	
$bgcolor = $count%2>0 ? '#FFFFFF' : '#F1F1F1';	

$table .= <<<EOD
 <tr nobr="true">
  <td bgcolor="{$bgcolor}">{$count}</td>
  <td bgcolor="{$bgcolor}">{$data->fields['STUDENTNUMBER']}</td>
  <td bgcolor="{$bgcolor}">{$data->fields['FULLNAME']}</td>
  <td bgcolor="{$bgcolor}">{$data->fields['UNITNAME']}</td>
  <td bgcolor="{$bgcolor}">{$data->fields['ACADEMICYEAR']}</td>
  <td bgcolor="{$bgcolor}">{$data->fields['SEMSESSIONCODE']}</td>
 </tr>
EOD;
++$count;
$data->MoveNext();
}
}

$table .= <<<EOD
</table>
EOD;

$pdf->writeHTML($table, true, false, false, false, '');

// -----------------------------------------------------------------------------

//Close and output PDF document
$pdf->Output('report.pdf', 'I');

//============================================================+
// END OF FILE                                                
//============================================================+
