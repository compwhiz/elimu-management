<?php

require_once('./init.php');
require_once('./config/db.php');

$smsd_params  =  $db->CacheGetOne(3000,"select PARAMS from SASMSCFG WHERE GATEWAY='class.ModemGateway.php' ");

if(!empty($smsd_params))
{
 $saved_params =   unserialize( base64_decode($smsd_params) );
}

$db_host         = valueof($saved_params, 'mysql_host'); 
$mysql_user      = valueof($saved_params, 'mysql_user'); 
$mysql_password  = valueof($saved_params, 'mysql_password'); 
$mysql_db        = valueof($saved_params, 'mysql_db'); 

$db_smsd = ADONewConnection( 'mysqli');

if(!$db_smsd->Connect( $db_host, $mysql_user, $mysql_password, $mysql_db)){
 die('Connection failed');
}

$db_smsd->SetFetchMode(ADODB_FETCH_ASSOC);

$modems    = $db_smsd->GetRow("select * from phones ");

$IMEI      = valueof($modems, 'IMEI'); 
$Battery   = valueof($modems, 'Battery'); 
$Signal    = valueof($modems, 'Signal'); 
$Sent      = valueof($modems, 'Sent'); 
$Received  = valueof($modems, 'Received'); 
$Send      = valueof($modems, 'Send'); 
$Receive   = valueof($modems, 'Receive'); 
 
echo '<i class="fa fa-mobile-phone" ></i>' .$IMEI .'<br>';
echo '<i class="fa fa-signal" ></i>' .$Signal .'<br>';
echo '<i class="fa fa-battery-full" ></i>' .$Battery .'<br>';
echo '<i class="fa fa-arrow-circle-o-left" ></i>' .$Sent .'<br>';
echo '<i class="fa fa-arrow-circle-o-right" ></i>' .$Received .'<br>';



