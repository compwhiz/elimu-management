<?php

//import from excel 

@ignore_user_abort(true);
@set_time_limit(0);
@ini_set("max_execution_time", 600000000);

require_once 'init.php';
require_once ROOT.'config/db.php';
require_once ROOT.'lib/PHPExcel/PHPExcel.php';

$auditdate  =  date('Y-m-d');
$audittime  =  date('H:i:s');
					
$subjects       = $db->GetAssoc("select SUBJECTCODE,SUBJECTNAME from SASUBJECTS ORDER BY  CODEOFFC");
$subjects_cols  = array();

print_pre($subjects);//remove 

//exit();//remove 

$files = array();
//$files[] = 'data-2014-F1.xls';
$files[] = 'data-2015-f1.xls';
//$files[] = 'data-2015-f3.xls';
//$files[] = 'data-2015-F2.xls';

//$db->debug=1;//remove 

foreach($files as $file){
	
$filename = ROOT.'tmp_files/'.$file;

//  Read your Excel workbook
try {

$inputFileType  = PHPExcel_IOFactory::identify( $filename );
$objReader      = PHPExcel_IOFactory::createReader( $inputFileType );
$objPHPExcel    = $objReader->load( $filename );

} catch(Exception $e) {
 echo 'Error loading file "'.pathinfo($filename,PATHINFO_BASENAME).'": '.$e->getMessage();
}

//  Get worksheet dimensions
$sheet         = $objPHPExcel->getSheet(0); 
$highestRow    = $sheet->getHighestRow(); 
$highestColumn = $sheet->getHighestColumn();

$max_id        = generateID('SASTUDSUB', 'ID', $db);

       for ($row = 1; $row <= $highestRow; $row++) { 
       	
       //  Read a row of data into an array
        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                    NULL,
                                    TRUE,
                                    FALSE);
                                    
         
       	 if($row==1){
       	 	if(isset($rowData[0])){
       	 	 //export err file header
       	 		//print_pre($rowData[0]);
       	 		//exit();//remove 
					
             foreach ($rowData[0] as $xls_col_pos=>$xls_title){
             	$xls_title     = trim($xls_title);
             	if(isset($subjects[$xls_title])){
             	  $subjects_cols[$xls_col_pos] = $xls_title;	
             	}
             }
             //print_pre($subjects_cols);
       	 	}
       	   }elseif($row>=2) { 
			   
             if(isset($rowData[0])) {
				 
				 //print_pre($rowData[0]);
				 //exit();//remove 
				 
				 $data       = array();
				 $subjects   = array();
				 	
				 $admno          = $rowData[0][0];
				 $year           = $rowData[0][17];
				 $termcode_raw   = $rowData[0][18];
				 $termcode       = str_replace('T','',$termcode_raw);
				 $termcode       = trim($termcode);
				 $streamcode     = $rowData[0][19];
				 $formcode       = $rowData[0][20];
				 
				  foreach ($rowData[0] as $xls_col_pos=>$xls_value){
					if(array_key_exists( $xls_col_pos,$subjects_cols)){
					 $subjectcode = $subjects_cols[$xls_col_pos];
					 if(!is_null($xls_value)){
					  $xls_value               =   ($xls_value);
					  
					  if($xls_value=='--'){
					    //$xls_value_array                  =   array(null,null);
				      }else{
						$xls_value_array                  = explode(' ',$xls_value);
						$subjects[$subjectcode]['score']  =	valueof($xls_value_array, 0); 
					    $subjects[$subjectcode]['grade']  =	valueof($xls_value_array, 1); 
				      }
				     }
					}
				  }

				   if(sizeof($subjects)>0){
				    foreach($subjects as $subjectcode=> $student_subject ) {

					/*
					$subject = new ADODB_Active_Record(
					   'SASTUDSUB',
					    array('ADMNO','SUBJECTCODE','FORMCODE','STREAMCODE','YEARCODE','TERMCODE')
					);
					
					$subject->Load("
					ADMNO='{$admno}'
					AND SUBJECTCODE='{$subjectcode}'
					AND FORMCODE='{$formcode}'
					AND STREAMCODE='{$streamcode}'
					AND YEARCODE='{$year}'
					AND TERMCODE='{$termcode}'
					");
					
					if(empty($subject->_original)){
					 $subject->id           = $max_id;
					 $subject->admno        = $admno;
					 $subject->subjectcode  = $subjectcode;
					 $subject->formcode     = $formcode;
					 $subject->streamcode   = $streamcode;
					 $subject->yearcode     = $year;
					 $subject->termcode     = $termcode;
					}
					
					$subject->total         = valueof($student_subject, 'score'); 
					$subject->totalp        = valueof($student_subject, 'score'); 
					$subject->paper1        = valueof($student_subject, 'score');
					$subject->paper1r       = valueof($student_subject, 'score');
					$subject->paper1p       = valueof($student_subject, 'score');
					$subject->avgexam       = valueof($student_subject, 'score');
					
					$subject->gradecode        = valueof($student_subject, 'grade'); 
					$subject->paper1gradecode  = valueof($student_subject, 'grade');
					$subject->examgradecode    = valueof($student_subject, 'grade');
					$subject->audituser  =  'import';
					$subject->auditdate  =  $auditdate;
					$subject->audittime  =  $audittime;
					
					$subject->picked        = 1;
					*/
					//$db->debug=1;//remove 
					
                    $total         = valueof($student_subject, 'score');
                    $gradecode     = valueof($student_subject, 'grade'); 
					$del = $db->Execute("
						delete  from SASTUDSUB where
						ADMNO='{$admno}'
						AND SUBJECTCODE='{$subjectcode}'
						AND FORMCODE!='{$formcode}'
						AND YEARCODE='{$year}'
						AND TOTAL='{$total}'
						AND GRADECODE='{$gradecode}'
						");
						//exit();//remove
						
					 //if(!$subject->Save()){
					 if(!$del){
						
					   $db->debug=1;
					    //$subject->Save();
					   $db->debug=0;
					   
					   die( $db->ErrorMsg() );
					   
					 }else{
                      ++$max_id;
					 }
					 //exit();
             }//foreach($subjects
             //exit();//remove 
	  }//if(!empty($subjectcode)){
     }//if(isset($rowData[0])){
    }//each data row
   }//loop rows
 }//each file
