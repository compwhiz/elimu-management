<?php


class sms
{
	public $gateway;
	public $error;
	
	public function __construct()
	{
		global $db;

	$sms_gateway  =  $db->GetRow("select * from SASMSCFG WHERE ISACTIVE=1 ");

	if(empty($sms_gateway)){
	  $this->error = ("No default Gateway");
	  return;
	}

	$gateway      = valueof($sms_gateway, 'GATEWAY'); 
	$params       = valueof($sms_gateway, 'PARAMS');
	$replace      = array('.php','class.');
	$className    = str_replace($replace,'',$gateway);
	$saved_params = array(); 

	if(!empty($params))
	{
	 $saved_params =   unserialize( base64_decode($params) );
	}

	$_class_params = range(0,10);
	$_class_params = array();

	foreach($saved_params as $k=>$v){
	 $_class_params[] = $v;
	}

	require_once( ROOT .'classes/sms/'.$gateway);

	$this->gateway  = new $className(
		   valueof($_class_params,0,''),
		   valueof($_class_params,1,''),
		   valueof($_class_params,2,''),
		   valueof($_class_params,3,''),
		   valueof($_class_params,4,''),
		   valueof($_class_params,5,''),
		   valueof($_class_params,6,''),
		   valueof($_class_params,7,''),
		   valueof($_class_params,8,''),
		   valueof($_class_params,9,''),
		   valueof($_class_params,10,'')
		   );

	}

	public function sendMessage( $recipients, $message ){
	  try{
       $this->gateway->sendMessage($recipients, $message );
      }catch (Exception $e){
         echo $e->getMessage();
	  }
	}
	
}
