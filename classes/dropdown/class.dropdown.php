<?php

  final class dropdown {
  
   static public function form_select($select_name,$table,$value_column,$text_column,$sql_where='',$default_value='',$options='',$show_value_column='',$ordercolumn='',$forcedb=false , $class='easyui-comboboxx' ){
    global $db;
    
    if(defined('MAKE_FIELDS_UNIQUE') && defined('MNUID')){
	 if(MAKE_FIELDS_UNIQUE && MNUID>0 ){
	  $select_name = "{$select_name}_".MNUID; 
     }
    }

   $sql_where =  trim($sql_where);

   $cachesecs  = !empty($cachesecs) && $cachesecs> 0 ? $cachesecs : null;

   $select_columns = array();

   if(!empty($value_column))
   {
    $select_columns[] = $value_column;
   }

   if(!empty($text_column))
   {
    $select_columns[] = $text_column;
   }

   if(!empty($ordercolumn) && $ordercolumn!=$text_column && $ordercolumn!=$value_column){
    $select_columns[] = $ordercolumn;
   }

   $select_columns = array_unique($select_columns);

   $sql_columns = join(',', $select_columns);

    $select_sql  = "SELECT DISTINCT {$sql_columns} FROM {$table} ";

   $select_sql .= isset($sql_where) && !empty($sql_where)?" WHERE ".$sql_where:'';

   if(!empty($ordercolumn)){
     $select_sql .= " ORDER BY ".$ordercolumn;
   }else{
	   if(!empty($value_column)){
	   	$select_sql .= " ORDER BY ".$value_column;
	   }else if(!empty($text_column)){
	    $select_sql .= " ORDER BY ".$text_column;
	   }
   }

   try  {

   if(!$forcedb){
    $select_record_set = $db->CacheGetArray(0,$select_sql);
   }else{
    $select_record_set = $db->GetArray($select_sql);
   }
   
   if(empty($options)){
   $options           = ' data-options="editable:false,width:150" ';
   }
   
   
   $select  = "<select  class=\"".$class."\"  name=\"".$select_name."\" id=\"".$select_name."\" ".$options." >";
 
   $select .= "<option value=\"\"></option>";
  
   
    if($select_record_set && sizeof($select_record_set)>0){

     foreach ($select_record_set as $select_record){

      $value = $select_record[$value_column];
      
      if (isset($text_column) && !empty($text_column)) {
      $text  = $select_record[$text_column];
      }

      $select .= "<option value=\"".$value."\"";
      $select .= isset($default_value) && trim($default_value)==trim($value)?' selected':'';
      $select .= " >";
      $select .= isset($show_value_column)&&$show_value_column==true?$value:'';
      $select .= isset($show_value_column)&&$show_value_column==true?' : ':'';
      $select .= !empty($text) ? $text : $value;
      $select .= "</option>";

     } 
    }
    
   $return = $select .= "</select>";
  }catch (Exception $e){
   $return = $e->getMessage();
  }
  
  return ($return);

 }

  static public function form_select_fromArray($select_name,$array= array(),$default_value='',$options='',$show_value_column='' , $width='100', $use_filter_after_records=50 , $force_use_filter=false) {
	
    if(defined('MAKE_FIELDS_UNIQUE') && defined('MNUID')){
	 if(MAKE_FIELDS_UNIQUE && MNUID>0 ){
	  $select_name = "{$select_name}_".MNUID; 
     }
    }

     $width = str_replace('px','', $width);

	 $numOptions = count($array);
	 $select   = "<select class=\"easyui-combobox\"  name=\"".$select_name."\" id=\"".$select_name."\"  style=\"width:{$width}px\" ".$options.">";
     $select_options = "<option value=\"\">... </option>";

     if($numOptions>0){

       foreach ($array as $k=>$v){

       	 $value =  !is_array($k) ? trim($k) : null;
       	 $text  =  !is_array($v) ? trim($v) : null;

          $select_options .= "<option value=\"".$value."\"";
          $select_options .= isset($default_value) && trim($default_value)==trim($value)?' selected':'';
          $select_options .= " >";
          $select_options .= isset($show_value_column)&&$show_value_column==true? $value :'';
          $select_options .= isset($show_value_column)&&$show_value_column==true?' : ':'';
          $select_options .= !is_null($text) ? $text : $value;
          $select_options .= "</option>";

       }
     }
     
      $select .= "{$select_options}</select>";
      return self::compress_html($select);
     
 }
 
  public static function academic_year ($name,$selected_year='',$options='', $forcedb=false , $only_active= false){

 	@$institution_calendar = new institution_calendar(1);
 	
 	$sql_where = " ";

 	if($only_active){
 	 
       if(isset($institution_calendar->academic_year) && strlen($institution_calendar->academic_year)>=2)
       {
 		$sql_where .= "  ACADEMICYEAR>='{$institution_calendar->academic_year}' ";
       }
 	}
 	
 	if(empty($selected_year)){
 		$selected_year = $institution_calendar->academic_year;
 	}

	return self::form_select ($name,'SAYEARS','ACADEMICYEAR','',$sql_where,$selected_year,$options,false,'ACADEMICYEAR', $forcedb );

}

  public static function admission_year ($name, $selected_year='', $options=''){
	$years = range( date('Y'),  (date('Y')-10) );
  	$select_options = array();
  	foreach ($years as $year ) {
  		$select_options[$year] = $year;
  	}
 	return self::form_select_fromArray($name, $select_options, $selected_year , $options );
}

  public static function session ($name, $selected_code='', $options='', $show_value_column= false , $forcedb = false ){
	return self::form_select ($name,'SASESSIONS','SESSCODE','SESSNAME','',$selected_code,$options,$show_value_column,'SESSCODE', $forcedb, 'easyui-combobox' );
}

  public static function paymode ($name, $selected_code='', $options='', $show_value_column= false , $forcedb = false ){
	return self::form_select ($name,'FINPYMD','PYMDCODE','PYMDDESC','',$selected_code,$options,$show_value_column,'PYMDCODE', $forcedb, 'easyui-combobox' );
}

  public static function bank ($name, $selected_code='', $options='', $show_value_column= false , $forcedb = false ){
	return self::form_select ($name,'FINBK','BKCODE','BKNAME','',$selected_code,$options,$show_value_column,'BKCODE', $forcedb, 'easyui-combobox' );
}

  public static function transtype ($name,$selected_code='',$options='', $forcedb=false){
	return self::form_select ($name,'PTTRANSACTIONTYPES','TRANSTYPECODE','TRANSTYPENAME','',$selected_code,$options,false,'', $forcedb );
}

  public static function institution ($name,$selected_code='',$options='', $forcedb=false){
	return self::form_select ($name,'SAINSTITUTION','INSTITUTIONCODE','INSTITUTIONNAME','',$selected_code,$options,false,'', $forcedb );
 }

  public static function stdcat ($name,$selected_code='',$options='', $show_value_column = false, $forcedb=false){
	return self::form_select ($name,'SASTUDCAT','SCATCODE','SCATNAME','',$selected_code,$options, $show_value_column,'SCATCODE', $forcedb  );
}

  public static function stdstatus ($name,$selected_code='',$options='', $forcedb = false){
	return self::form_select ($name,'SASTSCODES','STSCODE','STSNAME','',$selected_code,$options,false,'STSCODE',  $forcedb);
}

  public static function student_statustype ($name,$selected_code='',$options='', $forcedb = false){
	return self::form_select ($name,'SASTATUSTYPES','STATUSTYPECODE','STATUSTYPENAME','',$selected_code,$options,false,'STATUSTYPECODE', $forcedb );
}

 public static function academic_status ($name,$selected_statuscode='',$options='', $forcedb=false){
	return self::form_select ($name,'SASTSCODES','STATUSCODE','STATUSNAME','',$selected_statuscode,$options,false,'STATUSNAME', $forcedb  );
}

 public static function gender ($name,$selected_code='',$options='' ,$forcedb = false){
	return self::form_select ($name,'SAGND','GNDCODE','GNDNAME','',$selected_code,$options,0,'', $forcedb  );
}

 
 public static function ethnicities ($name,$selected_code='',$options='',$forcedb = false){
   return self::form_select ($name,'SAETC','ETCCODE','ETCNAME','',$selected_code,$options,false,'', $forcedb );
 }
 
 public static function studentstatus ($name,$selected_code='',$options='',$forcedb = false){
   return self::form_select ($name,'SASTS','STATUSCODE','STATUSNAME','',$selected_code,$options,false,'', $forcedb );
 }

 public static function town ($name,$selected_code='',$options='', $show_value_column=false, $forcedb = false){
	return self::form_select ($name,'SATOWN','TOWNCODE','TOWNNAME','',$selected_code,$options, $show_value_column ,'', $forcedb );
}

 public static function salutation ($name,$selected_salutation='',$options='', $forcedb=false){
	return self::form_select ($name,'SASLT','SLTCODE','SLTNAME','',$selected_salutation,$options,0,'', $forcedb );
}

 public static function country ($name,$selected_country='',$options='', $forcedb = false){
	return self::form_select ($name,'SACOUNTRIES','COUNTRYCODE','COUNTRYNAME','',$selected_country,$options,0,'COUNTRYNAME', $forcedb  );
}

 public static function dept ($name,$selected_code='',$options=''){
	return self::form_select ($name, 'SADEPT', 'DEPTCODE', 'DEPTNAME', '', $selected_code, $options);
}

 public static function kcpegrades ($name,$selected_code='',$options=''){
	return self::form_select ($name, 'SAKCPEGRADES', 'GRADECODE', 'GRADENAME', '', $selected_code, $options,0,'GRADECODE');
}

 public static function kcpesubjects ($name,$selected_code='',$options=''){
	return self::form_select ($name, 'SAKCPESUBJECT', 'KCPESUBCODE', 'KCPESUBNAME', '', $selected_code, $options);
}
  
 public static function dorm ($name,$selected_code='',$options='',$forcedb = false){
   return self::form_select ($name,'SADORMS','DORMCODE','DORMNAME','',$selected_code,$options,false,'', $forcedb );
 }

 public static function house ($name,$selected_code='',$options=''){
	return self::form_select ($name, 'SAHOUSES', 'HOUSECODE', 'HOUSENAME', '', $selected_code, $options);
}

 public static function form ($name,$selected_code='',$options=''){
	return self::form_select ($name, 'SAFORMS', 'FORMCODE', 'FORMNAME', '', $selected_code, $options);
}

 public static function stream ($name,$selected_code='',$options=''){
	return self::form_select ($name, 'SASTREAMS', 'STREAMCODE', 'STREAMNAME', '', $selected_code, $options);
}

 public static function year ($name,$selected_code='',$options=''){
 	if(empty($selected_code)){
 	 $selected_code = date('Y');
 	}
	return self::form_select ($name, 'SAYEAR', 'YEARCODE', 'YEARNAME', '', $selected_code, $options);
}

 public static function term ($name,$selected_code='',$options=''){
	return self::form_select ($name, 'SATERMS', 'TERMCODE', 'TERMNAME', '', $selected_code, $options);
}

 public static function grading ($name,$selected_code='',$options=''){
	return self::form_select ($name, 'SAXGSYS', 'GSYSCODE', 'GSYSNAME', '', $selected_code, $options);
 }

 public static function cluster ($name,$selected_code='',$options='' , $show_value_column = false, $forcedb = false){
	return self::form_select ($name,'SASBCLS','CLSTCODE','CLSTNAME','',$selected_code, $options, $show_value_column,'', $forcedb );
 }

 public static function subjectcombination ($name,$selected_code='',$options='' , $show_value_column = false, $forcedb = false){
	return self::form_select ($name,'SASBC','SBCCODE','SBCNAME','',$selected_code, $options, $show_value_column,'', $forcedb );
 }

 public static function subject ($name,$selected_code='',$options='' , $show_value_column = false, $forcedb = false){
	return self::form_select ($name,'SASUBJECTS','SUBJECTCODE','SUBJECTNAME','',$selected_code, $options, $show_value_column,'', $forcedb );
 }

 public static function intake ($name,$selected_intake_code='',$options='' , $forcedb = false){
	return self::form_select ($name,'AAINTAKE','INTAKECODE','INTAKEDESC','',$selected_intake_code,$options,true,'', $forcedb );
}
 
 public static function yearlist ($select_name,$selected_year,$options=''){
 $current_year  = date('Y');
 $pos           = $current_year-15;
 $year  = "<select name=\"{$select_name}\" id=\"{$select_name}\" {$options}> \n";
 $year .= empty($selected_year)?'<option><?option>':'';
  while ($pos<=$current_year){
   $year .= "<option value=\"{$pos}\" ".(!empty($selected_year)&&$selected_year==$pos?'selected':'').">{$pos}</option> \n";
   ++$pos;
  }
 $year .= "</select> \n";
 return $year;
}

 public static function maritalstate ($name,$selected_code='',$options='', $forcedb = false){
	return self::form_select ($name,'SAMST','MSTCODE','MSTNAME','',$selected_code,$options,'','', $forcedb );
}

 public static function religion ($name,$selected_religion='',$options='',$forcedb = false){
	return self::form_select ($name,'SARLG','RLGCODE','RLGNAME','',$selected_religion,$options,false,'', $forcedb );
}

 public static function bookcategory ($name,$selected_code='',$options='',$forcedb = false){
   return self::form_select ($name,'SLBCT','BCTCODE','BCTNAME','',$selected_code,$options,false,'', $forcedb );
 }
 
 public static function book_publisher ($name,$selected_code='',$options='',$forcedb = false){
   return self::form_select ($name,'SLPBL','PBLCODE','PBLNAME','',$selected_code,$options,false,'', $forcedb );
 }
 
 public static function book_shelves ($name,$selected_code='',$options='',$forcedb = false){
   return self::form_select ($name,'SLSLV','SLVCODE','SLVNAME','',$selected_code,$options,false,'', $forcedb );
 }
 
 public static function book_category ($name,$selected_code='',$options='',$forcedb = false){
   return self::form_select ($name,'SLBCT','BCTCODE','BCTNAME','',$selected_code,$options,false,'', $forcedb );
 }
 
 public static function stationery_category ($name,$selected_code='',$options='',$forcedb = false){
   return self::form_select ($name,'SLSCT','SCTCODE','SCTNAME','',$selected_code,$options,false,'', $forcedb );
 }
 
 public static function lib_member_type ($name,$selected_code='',$options='',$forcedb = false){
   return self::form_select ($name,'SLMTP','MTPCODE','MTPNAME','',$selected_code,$options,false,'', $forcedb );
 }

 public static function month ($name,$selected_month,$javascipt_options){
 $month = 1;
 $end   = 12;
 $select ="<select name=\"{$name}\"  id=\"{$name}\" {$javascipt_options}>";
 while ($month<=$end){
	$short_month_name = date('M',strtotime(date('Y').'-'.date('n'+$month).'-'.date('d')));
	$long_month_name  = date('F',strtotime(date('Y').'-'.date('n'+$month).'-'.date('d')));

	$select .="<option ";
	$select .= "value=\"".strtolower($short_month_name)."\"";
	$select .= strtolower($selected_month)==strtolower($short_month_name)?'selected':'';
	$select .= " >";
	$select .= $long_month_name;
	$select .= "</option>";
 ++$month;
 }
 $select .="</select>";
 return $select;
 }

 public function view ($select_name,$selected_view='',$options=''){
 	global $db;

	$database_views  = $db->MetaTables('VIEWS');
	sort($database_views,SORT_STRING);
	$return  ="<select name=\"{$select_name}\" id=\"{$select_name}\" {$options}>";
	$return .="<option></option>";
  foreach ($database_views as $view){
  	$return .="<option value=\"{$view}\" ";
  	$return .= !empty($selected_view) && $selected_view==$view?' selected ':'';
  	$return .=">{$view}</option>";
  }
  $return .="</select>";
 return $return;
}

 public static function  table ($select_name,$selected_table='',$options=''){
 	global $db;
	$tables  = $db->MetaTables('TABLES');
	sort($tables,SORT_STRING);
	$return  ="<select name=\"{$select_name}\" id=\"{$select_name}\" {$options}>";
	$return .="<option></option>";
  foreach ($tables as $table){
  	$return .="<option value=\"{$table}\" ";
  	$return .= !empty($selected_table)&&$selected_table==$table?' selected ':'';
  	$return .=">{$table}</option>";
  }
  $return .="</select>";
 return $return;
}

 public static function  column ($select_name,$table,$selected_column='',$options=''){
 	global $db;
	$columns = $db->MetaColumns($table,true);
	$return  ="<select name=\"{$select_name}\" id=\"{$select_name}\" {$options}>";
	$return .="<option></option>";
  foreach ($columns as $column){
  	$return .="<option value=\"{$column->name}\" ";
  	$return .= !empty($selected_column)&&$selected_column==$column->name?' selected ':'';
  	$return .=">{$column->name}</option>";

  }
  $return .="</select>";
 return $return;
}

  }
