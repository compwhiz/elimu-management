<?php

 class user {
	public $id;
	public $userid;
	public $username;
	public $email;
	public $groupcode;
	public $groupname;
	public $deptcode;
	public $deptname;
	public $isAdmin;
	public $auditdate;
	public $audittime;
	public $auditip;

 public function __construct( )
 {
	
	$this->id         = isset($_SESSION['user']['id']) ? $_SESSION['user']['id'] : null;
	$this->userid     = isset($_SESSION['user']['userid']) ? $_SESSION['user']['userid'] : null;
	$this->username   = isset($_SESSION['user']['username']) ? $_SESSION['user']['username'] : null;
	$this->email      = isset($_SESSION['user']['email']) ? $_SESSION['user']['email'] : null;
	$this->groupcode  = isset($_SESSION['user']['groupcode']) ? $_SESSION['user']['groupcode'] : null;
	$this->groupname  = isset($_SESSION['user']['groupname']) ? $_SESSION['user']['groupname'] : null;
	$this->deptcode   = isset($_SESSION['user']['deptcode']) ? $_SESSION['user']['deptcode'] : null;
	$this->deptname   = isset($_SESSION['user']['deptname']) ? $_SESSION['user']['deptname'] : null;
	$this->isAdmin    = strtolower($this->userid) == 'admin' ? true : false;
	$this->auditdate  = date('Y-m-d');
	$this->audittime  = time();
	$this->auditip    = get_user_ip2lng();
  	
	
}

 public static function createNew($userid,  $username, $deptcode, $email, $password='', $groupcode=''){
            global $db;
            
            if(empty($groupcode)){
 	        $groupcode   = $db->CacheGetOne(9000,"SELECT GROUPCODE FROM USERGROUPS WHERE IST=1");
		    }
 	        
            $record_user = new ADOdb_Active_Record('USERS',array('USERID'));
			$record_user->Load(" USERID='{$userid}' ");
			
			if(empty($record_user->_original))
			{
				$record_user->id                =  generateID('USERS','ID');
				$record_user->userid            =  $userid;
				$new =  true;
			}else{
				$new = false;
			}
			
			     
              if( !empty($password) ){
               $record_user->password        = md5( strtolower($userid).$password.strtolower($userid));
              }
              
			  $record_user->groupcode         =  $groupcode;
			  $record_user->deptcode          =  $deptcode;
			  $record_user->username          =  Camelize($username);
              $record_user->email             =  $email;
              $record_user->audituser         =  'system';
              $record_user->auditdate         =  date('Y-m-d');
              $record_user->audittime         =  time();
                
			   if(empty($record_user->password)){
				$record_user->password = md5($record_user->userid);
			   }
                                      
			  if($record_user->Save()){
			  	return  true;
			  }
			  
			  return  false;
    }
 
} 
