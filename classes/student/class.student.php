<?php

 class student {
	public $id;
	public $admno;
	public $accountno;
	public $fullname;
	public $dob;
	public $gndcode;
	public $rlgcode;
	public $housecode;
	public $housename;
	public $dormcode;
	public $dormname;
	public $streamcode;
	public $streamname;
	public $formcode;
	public $formname;
	public $yearcode;
	public $sbccode;
	public $kcpegrade;
	public $kcpemean;
	public $kcpepoint;
	public $kcpemarks;
	public $indexno;
	public $admdate;
	public $mobilephone;
	public $residence;
	public $postalcode;
	public $address;
	public $email;
	public $medstatus;
	public $relationship;

 public function __construct( $admno, $secs2cache=1200 )
 {
	global $db;
	
	$record   = $db->CacheGetRow($secs2cache,"select * from VIEWSP WHERE ADMNO='{$admno}' ");

	foreach ($record as $k=>$v){
	 $ks = strtolower($k);
//	 echo "\$this->{$ks} = valueof(\$record, '{$k}');<br>";	
	}
	

	foreach ($record as $k=>$v){
	 $ks = strtolower($k);
	}
	
	$this->id         = valueof($record, 'ID');
	$this->admno      = valueof($record, 'ADMNO');
	$this->accountno  = valueof($record, 'ACCOUNTNO');
	$this->fullname   = valueof($record, 'FULLNAME');
	$this->dob        = valueof($record, 'DOB');
	$this->gndcode    = valueof($record, 'GNDCODE');
	$this->rlgcode    = valueof($record, 'RLGCODE');
	$this->housecode  = valueof($record, 'HOUSECODE');
	$this->housename  = valueof($record, 'HOUSENAME');
	$this->dormcode   = valueof($record, 'DORMCODE');
	$this->dormname   = valueof($record, 'DORMNAME');
	$this->streamcode = valueof($record, 'STREAMCODE');
	$this->streamname = valueof($record, 'STREAMNAME');
	$this->formcode   = valueof($record, 'FORMCODE');
	$this->formname   = valueof($record, 'FORMNAME');
	$this->yearcode   = valueof($record, 'YEARCODE');
	$this->sbccode    = valueof($record, 'SBCCODE');
	$this->kcpegrade  = valueof($record, 'KCPEGRADE');
	$this->kcpemean   = valueof($record, 'KCPEMEAN');
	$this->kcpepoint  = valueof($record, 'KCPEPOINT');
	$this->kcpemarks  = valueof($record, 'KCPEMARKS');
	$this->indexno    = valueof($record, 'INDEXNO');
	$this->admdate    = valueof($record, 'ADMDATE');
	$this->mobilephone  = valueof($record, 'MOBILEPHONE');
	$this->residence    = valueof($record, 'RESIDENCE');
	$this->postalcode   = valueof($record, 'POSTALCODE');
	$this->address      = valueof($record, 'ADDRESS');
	$this->email        = valueof($record, 'EMAIL');
	$this->medstatus    = valueof($record, 'MEDSTATUS');
	$this->relationship = ( $this->gndcode == 'M') ? 'son' : 'daughter';
	
	
}
} 
