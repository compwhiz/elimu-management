<?php

 class school{
	public $name;
	public $address;
	public $telephone;
	public $vision;
	public $mision;
	public $county;
	public $email;
	public $website;
	public $motto;
	public $logo_path;
	
	public $active_yearcode;
	public $active_termcode;
	public $active_termname;
	public $active_term_date_start;
	public $active_term_date_end;
	
    public function __construct($cacheSecs=200){
     global $db;
//	   $db->debug=1;
	   
	     
	 $school           =  $db->CacheGetRow($cacheSecs,"SELECT * FROM viewschool");
//	 print_pre($school);

	 $this->address    =  valueof($school, 'ADDRESSPOST');
	 $this->code       =  valueof($school, 'SCHOOLCODE');
	 $this->name       =  valueof($school, 'SCHOOLNAME');
	 $this->telephone  =  valueof($school, 'MOBILEPHONE');
	 $this->email      =  valueof($school, 'EMAIL');
	 $this->website    =  valueof($school, 'WEBSITE');
	 $this->motto      =  valueof($school, 'MOTTO');
	 $this->vision     =  valueof($school, 'VISION');
	 $this->mision     =  valueof($school, 'MISSION');
	 $this->motto      =  valueof($school, 'MOTTO');
	 $logo             =  valueof($school, 'LOGO');
//	 $this->logo_path  =  valueof($school, 'SCHLOGO','../../public/images/logo.gif');
	 $this->logo_path  =  '../../public/images/'.$logo;
	 
	 $active           =  $db->CacheGetRow($cacheSecs,"SELECT C.YEARCODE, C.TERMCODE, T.TERMNAME, C.DATESTART, C.DATEEND FROM satcal C  INNER JOIN SATERMS T ON T.TERMCODE=C.TERMCODE WHERE C.ACTIVE=1");
     $this->active_yearcode         =  valueof($active, 'YEARCODE');
     $this->active_termcode         =  valueof($active, 'TERMCODE');
     $this->active_termname         =  valueof($active, 'TERMNAME');
     $this->active_term_date_start  =  valueof($active, 'DATESTART');
     $this->active_term_date_end    =  valueof($active, 'DATEEND');
			
    }
    
	
}