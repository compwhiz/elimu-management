<?php

/**
 * @author kenmsh@gmail.com
 * @since v5
 */
 
 class grid{
    private $id;
    private $page;
    private $rows;
	private $datasrc;
	private $datatbl;
	
  public function __construct(){
		global $cfg;
		
		$this->page         = filter_input(INPUT_POST , 'page' , FILTER_VALIDATE_INT);
		$this->rows         = filter_input(INPUT_POST , 'rows' , FILTER_VALIDATE_INT);
		$this->datasrc      = valueof($cfg,'datasrc');
		$this->datatbl      = valueof($cfg,'datatbl');
		$this->primary_key  = valueof($cfg,'pkcol');
		$this->id           = filter_input(INPUT_POST , 'id');
		
	}
  
  public function data(){
	global $db,$cfg;
			
	$page  =  $this->page>0 ? $this->page : 1;
	$rows  =  $this->rows>0 ? $this->rows : 10;
    
    $sortbys = array();
    
    if( isset($_POST['sort']) && !empty($_POST['sort']) ) {
	 $sort_cols_array  = explode(',', $_POST['sort']);
	 $sort_order_array = explode(',', $_POST['order']);
	 
	 if(sizeof($sort_cols_array)>0){
		foreach($sort_cols_array as $sort_col_index => $sort_col_name){
		  
		 $sort_col_name   = isset($cfg['columns'][$sort_col_name]['dbcol']) ? strtoupper($cfg['columns'][$sort_col_name]['dbcol']) : $sort_col_name;
		 $sort_col_order  = valueof($sort_order_array, $sort_col_index, 'asc');
		 $sortbys[]       = "{$sort_col_name} {$sort_col_order}";
		}
	 }
	 
	 if(sizeof($sortbys)>0){
	  $sortby = ' order by ' .implode(',', $sortbys);
	 }
	 
   }else{
	  $sortby =  ' order by ID asc';  
   }
   
    $filters_conditions_str =  '';
    
    /**
     * @ekariz 22OCT15 
     */
     
    if(isset($_POST['filterRules'])) {
	 $replace = array("'");
	 $filterRules     = $_POST['filterRules'];
	 $filterRules_obj = json_decode($filterRules);
	 $filters_conditions = array();
	 
	 foreach($filterRules_obj as $filterRule){
	  $filterRuleCol = @$filterRule->field;
	  $filterRuleOpr = @$filterRule->op;
	  $filterRuleVal = str_replace( $replace, "\'", @$filterRule->value );
	  
	  $filterRuleDBCol = isset($cfg['columns'][$filterRuleCol]['dbcol']) ? $cfg['columns'][$filterRuleCol]['dbcol'] : null;
	  
	  if(!is_null($filterRuleDBCol)){
	  
	   switch($filterRuleOpr){
         case 'none' : $filters_conditions[]="{$filterRuleDBCol} is not null";break;
         case 'contains' : $filters_conditions[]="{$filterRuleDBCol} like '%{$filterRuleVal}%'";break;
         case 'equal' : $filters_conditions[]="{$filterRuleDBCol} = '{$filterRuleVal}'";break;
         case 'notequal' : $filters_conditions[]="{$filterRuleDBCol} != '{$filterRuleVal}'";break;
         case 'beginwith' : $filters_conditions[]="{$filterRuleDBCol} like '{$filterRuleVal}%'";break;
         case 'endwith' : $filters_conditions[]="{$filterRuleDBCol} like '%{$filterRuleVal}'";break;
         case 'less' : $filters_conditions[]="{$filterRuleDBCol} < '{$filterRuleVal}'";break;
         case 'lessorequal' : $filters_conditions[]="{$filterRuleDBCol} <= '{$filterRuleVal}'";break;
         case 'greater' : $filters_conditions[]="{$filterRuleDBCol} > '{$filterRuleVal}'";break;
         case 'greaterorequal' : $filters_conditions[]="{$filterRuleDBCol} >= '{$filterRuleVal}'";break;
        }
        
	  }
        
	 }
	 
	 if(sizeof($filters_conditions)>0){
		$filters_conditions_str = 'and ('. implode(' and ', $filters_conditions). ')';
	 }
	}
	
    
	$offset = ($page-1)*$rows;
	$result = array();
	
	////////////////////////////////////////////////////////////////////////
 	$selects    = array();
 	$fields     = array();
 	
    if(isset($cfg['columns'])){
	  if(sizeof($cfg['columns'])>0){
		foreach ($cfg['columns'] as $col => $col_properties){
			
			$dbcol          =  isset($col_properties['dbcol']) ? $col_properties['dbcol'] : $col;
			$selects[]      =  "{$dbcol} {$col}";
			
		}
	 }
    }

    $sql_where = ' where 1=1 '. $filters_conditions_str;
    
    $selects_str  =  implode(',', $selects);
	
	$numRecords = $db->GetOne("select count(*) from {$this->datasrc} {$sql_where} ");

	$result["total"] = $numRecords;
	
	$rs = $db->SelectLimit("select ID id, {$selects_str} from {$this->datasrc}  {$sql_where}  {$sortby}",$rows, $offset);
		
	$items = array();
	
    if($rs){	
	 while(!$rs->EOF){
		 
	  if(isset($rs->fields['ID'])){
	   $rs->fields['id'] = $rs->fields['ID'];
      }elseif(isset($rs->fields['id'])){
	   //$rs->fields['id'] = $rs->fields['ID'];
      }
      
	  array_push($items, $rs->fields);
	  
	  $rs->MoveNext();
	 }
	}
	
	$result["rows"] = $items;

	echo header('Content-type : text/json');
	echo json_encode($result);
	
	}
	
  public function draw_grid( $mnuid, $module_packed ){
		global $cfg;
		
		$grid_with = 0;
		$Headers = array();
		if(isset($cfg['columns'])){
	     if(sizeof($cfg['columns'])>0){
		  foreach ($cfg['columns'] as $col => $col_properties){
		  	
		  	$visible        =  valueof($col_properties,'visible',1);
		  	$sortable       =  valueof($col_properties,'sortable',1);
		  	$title          =  valueof($col_properties,'title',$col);
			$width          =  valueof($col_properties,'width',50);
			$width          =  is_numeric($width) ? $width : 50;
			
			if($visible==1 || $visible==2 ){
			 $Headers[$col]  =  array(
			                  'title'=>$title, 
			                  'width'=>$width, 
			                  'sortable'=> ($sortable==1) ? 'true' : 'false'
			                  );
			                  
		  	 $grid_with     += $width;
			}
			
		  }
		 }
		}
		
	  $form_width    = isset($cfg['form_width']) && $cfg['form_width']>=450 ? $cfg['form_width'] : 500;
	  $form_height   = isset($cfg['form_height']) && $cfg['form_height']>=200 ? $cfg['form_height'] : 200;
		
	  $grid_width    = isset($cfg['grid_width']) ? $cfg['grid_width'] : 500;
	  $grid_height   = isset($cfg['grid_height']) ? $cfg['grid_height'] : 200;
		
	  $allow_add     = isset($cfg['tblbns']['chk_button_add']) && $cfg['tblbns']['chk_button_add']== 1 ? true : false;
	  $allow_edit    = isset($cfg['tblbns']['chk_button_edit']) && $cfg['tblbns']['chk_button_edit']== 1 ? true : false;
	  $allow_delete  = isset($cfg['tblbns']['chk_button_delete']) && $cfg['tblbns']['chk_button_delete']== 1 ? true : false;
	  $allow_import  = isset($cfg['tblbns']['chk_button_import']) && $cfg['tblbns']['chk_button_import']== 1 ? true : false;
	  $allow_export  = isset($cfg['tblbns']['chk_button_export']) && $cfg['tblbns']['chk_button_export']== 1 ? true : false;
	  
	  $apptitle      = valueof($cfg, 'apptitle');
	 ?>	
	 
		<table 
		  id="dg<?php echo $mnuid; ?>" 
		  title="" class="easyui-datagrid" 
		  style="width:<?php echo $grid_width; ?>px;height:<?php echo $grid_height; ?>px"
		  toolbar="#toolbar<?php echo $mnuid; ?>" 
		  pagination="true"
		  rownumbers="true" 
		  fitColumns="true" 
		  singleSelect="true"
		 >
		<thead>
			<tr>
			   <?php
			    if(sizeof($Headers)>0){
			    	foreach ($Headers as $code => $properties){

			    		$width = valueof($properties, 'width');
			    		$title = valueof($properties, 'title');
			    		$sortable = valueof($properties, 'sortable','false');
			    		
			    		echo "<th field=\"{$code}\" width=\"{$width}\" sortable=\"{$sortable}\">{$title}</th>\r\n";
			    	}
			    }
			   ?>
			</tr>
		</thead>
	</table>
		
	<div id="toolbar<?php echo $mnuid; ?>">
		<?php if($allow_add){ ?><a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="crud.add(<?php echo $mnuid; ?>,'<?php echo $apptitle; ?>')">New </a> <?php } ?>
		<?php if($allow_edit){ ?><a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="crud.edit(<?php echo $mnuid; ?>,'<?php echo $apptitle; ?>')">Edit</a> <?php } ?>
		<?php if($allow_delete){ ?><a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="crud.remove(<?php echo $mnuid; ?>,'<?php echo $apptitle; ?>','<?php echo $module_packed; ?>')">Remove</a><?php } ?>
		<?php if($allow_import){ ?><a href="#" class="easyui-linkbutton" iconCls="icon-undo" plain="true" onclick="crud.data_import(<?php echo $mnuid; ?>,'<?php echo $module_packed; ?>')">import</a><?php } ?>
		<?php if($allow_export){ ?><a href="#" class="easyui-linkbutton" iconCls="icon-redo" plain="true" onclick="crud.data_export(<?php echo $mnuid; ?>,'<?php echo $module_packed; ?>')">export</a><?php } ?>
	</div>
	

 <?php if($allow_import){ ?>
 <div id="dlg_import<?php echo $mnuid; ?>" class="easyui-dialog" title="Data File Import" style="width:350px;height:200px;padding:5px" closed="true" >
 <form id="fmi<?php echo $mnuid; ?>" method="post" novalidate>
 <input id="fileupload<?php echo $mnuid; ?>" type="file" name="import_file<?php echo $mnuid; ?>" onchange="crud.do_import(<?php echo $mnuid; ?>,'<?php echo $module_packed; ?>');" >
    <div id="progress<?php echo $mnuid; ?>" class="progress">
      <div class="progress-bar progress-bar-success"></div>
    </div>
    <div id="files<?php echo $mnuid; ?>" class="files"></div>
 </form>
 </div>
 <?php } ?>
 
 <?php if($allow_export){ ?>
 <div id="dlg_export<?php echo $mnuid; ?>" class="easyui-dialog" title="Data Export" style="width:350px;height:200px;padding:5px" closed="true" >
 <div class="fitem">
 <label>Export Type</label>
  <select id="export_type<?php echo $mnuid; ?>" name="export_type<?php echo $mnuid; ?>">
   <option value="1">First Row Only</a>
   <option value="5">First 5 Rows</a>
   <option value="65000">All Rows</a>
  </select>
</div>
<div class="fitem">
<label>&nbsp;</label>
  <a href="#" class="easyui-linkbutton" iconCls="" plain="false" onclick="crud.do_export(<?php echo $mnuid; ?>,'<?php echo $module_packed; ?>')">Export</a>
</div>
 </div>
 <?php 
 }
 ?>

 <?php 
}
	
  public function draw_form_simple( $mnuid, $module_packed ){
		global $cfg, $combogrid_array;
		$module = unpackvars($module_packed);
		
		$form_width        = isset($cfg['form_width'])  ? $cfg['form_width'] : 400;
		$form_height       = isset($cfg['form_height'])  ? $cfg['form_height'] : 200;
         
        if( isset($cfg['tblbns']['chk_button_save']) ){
        $allow_save         =  $cfg['tblbns']['chk_button_save']== 1 ? true : false;
	    }else{
		 $allow_save        =  true;
	    }
	?>
   <div id="dlg<?php echo $mnuid; ?>" class="easyui-dialog" style="width:<?php echo $form_width; ?>px;height:<?php echo $form_height; ?>px;padding:10px 20px" closed="true" buttons="#dlg-buttons<?php echo $mnuid; ?>">
		<form id="fm<?php echo $mnuid; ?>" method="post" novalidate>
		<?php
		
    if(isset($cfg['columns'])){
	  if(sizeof($cfg['columns'])>0){
		foreach ($cfg['columns'] as $col => $col_properties){
			
			$visible           =  valueof($col_properties,'visible',1);
			$title             =  isset($col_properties['title']) ? $col_properties['title'] : $col;
			$validation_class  =  isset($col_properties['validation_class']) ? $col_properties['validation_class'] : '';
			$required          =  isset($col_properties['required']) && $col_properties['required']==1 ? "required=\"true\" " : '';
			$validType         =  isset($col_properties['validType']) ? $col_properties['validType'] : '';
			$inputType         =  isset($col_properties['inputType']) ? $col_properties['inputType'] : 'text';
			
			if($visible==1  || $visible==3){
			switch($inputType) {
				case 'text':
				default:
				 $input  = "<input type=\"text\" id=\"{$col}\" name=\"{$col}\" class=\"{$validation_class}\" {$required} validType=\"{$validType}\">";
				 break;	
				case 'numberbox':
				 $input  = "<input id=\"{$col}\" name=\"{$col}\" class=\"easyui-numberbox\"  {$required} validType=\"{$validType}\">";
				 break;	
				case 'numberspinner':
				 $input  = "<input type=\"text\" id=\"{$col}\" name=\"{$col}\" class=\"easyui-numberspinner\"  style=\"width:80px;\" data-options=\"min:0,precision:2\" >";
				 break;	
				case 'timespinner':
				 $input  = "<input type=\"text\" id=\"{$col}\" name=\"{$col}\" class=\"easyui-timespinner\"  style=\"width:80px;\" >";
				 break;	
				case 'monthspinner':
				 $input  = "<input type=\"text\" id=\"{$col}\" name=\"{$col}\" class=\"easyui-numberspinner\"  style=\"width:80px;\" data-options=\"min:1,max:12,precision:0\" >";
				 break;	
				case 'email':
				 $input  = "<input type=\"email\" id=\"{$col}\" name=\"{$col}\" class=\"{$validation_class}\" {$required} validType=\"{$validType}\">";
				 break;	
				case 'textarea':
				 $input  = "<textarea  id=\"{$col}\" name=\"{$col}\" class=\"{$validation_class}\" {$required} validType=\"{$validType}\"></textarea>";
				 break;	
				case 'password':
				 $input  = "<input type=\"password\" id=\"{$col}\" name=\"{$col}\" class=\"{$validation_class}\" {$required} validType=\"{$validType}\">";
				 break;	
				case 'date':
				 $input  = "<input type=\"text\" id=\"{$col}\" name=\"{$col}\" class=\"easyui-datebox {$validation_class}\" {$required} validType=\"{$validType}\" data-options=\"formatter:dateYmdFormatter,parser:dateYmdParser\" >";
				 break;	
				case 'checkbox':
				 $input  = "<input type=\"checkbox\" id=\"{$col}\" name=\"{$col}\" value=\"1\" class=\"{$validation_class}\" {$required} validType=\"{$validType}\">";
				break;
				case 'select':
			       $selectsrc         =  isset($col_properties['selectsrc']) ? $col_properties['selectsrc'] : 'text';
			 
			        if(strstr($selectsrc,'|')){
					  $selectsrc_array            = explode('|', $selectsrc);
					  $table_col_datasrc          = valueof($selectsrc_array, 0);
	                  $linked_tbl_col_code        = valueof($selectsrc_array, 1);
	                  $linked_tbl_col_name        = valueof($selectsrc_array, 2);
	                  
	                  if(!empty($table_col_datasrc) && !empty($linked_tbl_col_code) && !empty($linked_tbl_col_name) ){
	                  	 $input    = ui::form_select($col , $table_col_datasrc , $linked_tbl_col_code , $linked_tbl_col_name , '', '');
	                  }
			        }
			   break;
			   case 'combogrid':
			   $input  = "<input type=\"text\" id=\"{$col}\" name=\"{$col}\" class=\"{$validation_class}\" {$required} validType=\"{$validType}\">";
			    echo '<script>';
                 echo ui::ComboGrid($combogrid_array,$col);
                echo '</script>';
			   break;
			   case 'yearselect':
			    $year     = date("Y")-2;
			    $end_year = date("Y")+2;
			    
			    $input = "<select name=\"{$col}\"  id=\"{$col}\" >";
  	            $input .= "<option value=\"\">[Year]</option>";
  	            while ($year < $end_year) {
  	 	         $input .= "<option value=\"{$year}\" >{$year}</option>";
  	             ++$year;
  	            }
  	           $input .= "</select>";
			   break;
			 }
			
			
			echo  <<<FORM_ITEM
<div class="fitem">
 <label>{$title}:</label>
 {$input}
</div>
FORM_ITEM;

         }//if visible
	   }
	 }
    }
		?>
		</form>
	</div>
	<div id="dlg-buttons<?php echo $mnuid; ?>">
		<?php if($allow_save){ ?><a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="crud.save(<?php echo $mnuid; ?>,'<?php echo $module_packed; ?>')">Save</a><?php } ?>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg<?php echo $mnuid; ?>').dialog('close')">Cancel</a>
	</div>
	<?php
	}
		
  public function grid_save_row_simple(){
		global $db,$cfg;
		
		$pk_col     =  isset($cfg['pkcol']) ? $cfg['pkcol'] : $this->primary_key;
		$user       =  new user();
		$pk_cols    =  array();
		$pk_values  =  array();
		$keycolums  =  $db->MetaPrimaryKeys( $this->datatbl );
		
	    if( sizeof($keycolums)>0 && isset($cfg['columns']) ) {
	     if(sizeof($cfg['columns'])>0){
		  foreach ($keycolums as $keycolum) {
		   if($keycolum=='ID'){
			 $pk_value    = 0;
			 $dbcol       = 'ID';
			 $pk_values[]  = "{$keycolum}='{$this->id}'";
			 $pk_cols[]    = "{$dbcol}";
		    }else{
		    foreach ($cfg['columns'] as $col => $col_properties){
		  	 $dbcol      = ($col_properties['dbcol']);
		  	 if($dbcol == $keycolum){
		  	 $pk_value     = filter_input(INPUT_POST , $col, FILTER_SANITIZE_STRING);
		  	 $pk_values[]  = "{$keycolum}='{$pk_value}'";
		  	 $pk_cols[]    = $dbcol;
		  	 }
		  	}
		   }
		  }
	     }
	    
	    if( sizeof($pk_values)>0 && sizeof($pk_cols)>0 ){
		 $record        = new ADODB_Active_Record( $this->datatbl, $pk_cols );
		 $pk_values_str = implode(' and ', $pk_values);
		 $record->Load("{$pk_values_str}");
	    }
	    
	    }else{
		 $record  = new ADODB_Active_Record( $this->datatbl, array( ID ));
		 $record->Load("ID={$this->id}");	
		}
		
		if(empty($record->_original)){
		 $record->id      = generateID($this->datatbl);
		 $record->$pk_col = $pk_value;
		}
		
	    if(isset($cfg['columns'])){
	     if(sizeof($cfg['columns'])>0){
		  foreach ($cfg['columns'] as $col => $col_properties){
		  	 $dbcol           = strtolower($col_properties['dbcol']);
		  	 $value           = filter_input(INPUT_POST , $col ,FILTER_SANITIZE_STRING);
		  	 $value           = centerTrim($value);
		  	 
		  	 if($dbcol=='password') { 
			  $value = md5($value);
			 }
			 
		  	 $record->$dbcol  = $value;
		  }
	     }
	    }	
		
		@$record->audituser  = $user->userid;
		@$record->auditdate  = $user->auditdate;
		@$record->audittime  = $user->audittime;
		@$record->auditip    = $user->auditip;
		
		if($record->Save()){
			echo json_response(1,'ok',array('id'=>$record->id));
		}else{
			echo json_response(0,'save failed '.$db->ErrorMsg());
		}
		
	}
	
  public  function grid_remove_row(){
		global $db, $cfg;
		
		$sql = "DELETE FROM {$this->datatbl} WHERE ID={$this->id}";
		
		if($db->Execute($sql)){
			echo json_response(1,'Record Deleted');
		}else{
			echo json_response(0,'Delete Failed');
		}
		
	}
	
  public function export(){
  	global $db, $cfg;
  	$appname     =  isset($cfg['appname']) ? $cfg['appname'] : 'export_file' . date('YmdHis');
  	$export_type = filter_input(INPUT_POST , 'export_type');
  	
    require_once ROOT.'lib/PHPExcel/PHPExcel.php';
    
    $objPHPExcel  =  new PHPExcel();
    $objWorksheet = $objPHPExcel->createSheet(0);
	
  	$grid_with = 0;
  	$selects = array();
  	$items = array();
		if(isset($cfg['columns'])){
	     if(sizeof($cfg['columns'])>0){
	     
	      $objWorksheet->setCellValueByColumnAndRow( 0, 1, 'ID' );
	     		
	      $count_col = 1;
	      
		  foreach ($cfg['columns'] as $col => $col_properties){
		  	
		  	$visible        =  valueof($col_properties,'visible',1);
		  	$title          =  valueof($col_properties,'title',$col);
			
			$dbcol          =  isset($col_properties['dbcol']) ? $col_properties['dbcol'] : $col;
			$selects[]      =  $dbcol;
			
			$objWorksheet->setCellValueByColumnAndRow( $count_col, 1, $title );
            $objWorksheet->getColumnDimensionByColumn($count_col)->setAutoSize( true ); 
           ++$count_col;
           
		  }
		 }
		}
		
	 $selects_str  =  implode(',', $selects);
	 
	 switch ($export_type){
	 	case 0:	
	 	case 1:	
	 	 $rs           =  $db->SelectLimit("select ID, {$selects_str} from {$this->datasrc}  order by ID asc",1);
	 	break;
	 	case 5:	
	 	 $rs           =  $db->SelectLimit("select ID, {$selects_str} from {$this->datasrc}  order by ID asc",5);
	 	break;
	 	default:
	 	$rs            =  $db->Execute("select ID, {$selects_str} from {$this->datasrc} order by ID asc");
     }
	 
		
     if($rs){	
     $rownum = 2;
	 while(!$rs->EOF){
	   $col_index = 0;
	   
	   foreach ($rs->fields as $rs_field_key => $rs_field_val ){
	     $objWorksheet->setCellValueByColumnAndRow( $col_index, $rownum, $rs_field_val );	
	     ++$col_index;
	   }
      ++$rownum; 
	  $rs->MoveNext();
	  }
	 }
	 
	  $objWorksheet->setTitle($appname);
      $objPHPExcel->setActiveSheetIndex(0);
     
      $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
      $writeTo   =  "tmp_files/{$appname}.xls";
      
      try {
       $objWriter->save( ROOT.$writeTo);
       return json_response(1,'ok',array('export'=>$writeTo));
      }catch (Exception $e){
      	$err = str_replace(ROOT, './',$e->getMessage());
      	return json_response(0, $err );
      }
		
  }
	
  public function import(){
  	global $db, $cfg, $mnuid;
  	
  	
  	$appname       =  isset($cfg['appname']) ? $cfg['appname'] : 'export_file' . date('YmdHis');
  	$datatbl       =  isset($cfg['datatbl']) ? $cfg['datatbl'] : null;
  	$import_file   =  "import_file{$mnuid}";
  	
  	if(empty($datatbl)){
  	 return json_response(0, 'destination database table not defined');	
  	}
  	
  	$keycolums     = $db->MetaPrimaryKeys( $datatbl );
  	
    require_once ROOT.'lib/PHPExcel/PHPExcel.php';
      
   if(isset($_FILES[$import_file])){
   	
   	 $c = array();
   	 $has_err = false;
   	
   	  if(isset($cfg['columns'])){
	     if(sizeof($cfg['columns'])>0){
	     
	      $count_col = 1;
	      
		  foreach ($cfg['columns'] as $col => $col_properties){
		  	
		  	$title          =  valueof($col_properties,'title',$col);
		  	$title          =  trim($title);
			$dbcol          =  isset($col_properties['dbcol']) ? $col_properties['dbcol'] : $col;
			
			$title_key = md5($title);
			
			$c[$title_key]      =  $dbcol;
			
           ++$count_col;
           
		  }
		 }
		}
		
     	$name      = isset($_FILES[$import_file]['name']) ? $_FILES[$import_file]['name']  : null;
     	$type      = isset($_FILES[$import_file]['type']) ? $_FILES[$import_file]['type']  : null;
     	$tmp_name  = isset($_FILES[$import_file]['tmp_name']) ? $_FILES[$import_file]['tmp_name']  : null;
     	$error     = isset($_FILES[$import_file]['error']) ? $_FILES[$import_file]['error']  : null;
     	$size      = isset($_FILES[$import_file]['size']) ? $_FILES[$import_file]['size']  : null;
     	
     	//move to tmp
     	$new_location  = ROOT . "tmp_files/{$name}";
     	
     	if(!move_uploaded_file($tmp_name,  $new_location )){
     		return json_response(0, 'unable to move uploaded to temporaly location' );
     	}
     	
     	
       //  Read your Excel workbook
      try {
	
        $inputFileType  = PHPExcel_IOFactory::identify( $new_location );
        $objReader      = PHPExcel_IOFactory::createReader( $inputFileType );
        $objPHPExcel    = $objReader->load( $new_location );
    
       } catch(Exception $e) {
         return json_response(0, 'Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
       }

      //  Get worksheet dimensions
       $sheet         = $objPHPExcel->getSheet(0); 
       $highestRow    = $sheet->getHighestRow(); 
       $highestColumn = $sheet->getHighestColumn();

       $dbcol_saveTo = array();
       
       for ($row = 1; $row <= $highestRow; $row++){ 
       	
       //  Read a row of data into an array
        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                    NULL,
                                    TRUE,
                                    FALSE);
                                    
         
       	 if($row==1){
       	 	if(isset($rowData[0])){
       	 	 //export err file header
       	 	 $err    = array();
       	 	 $err[0] = $rowData[0];	
             foreach ($rowData[0] as $xls_col_pos=>$xls_title){
             	
             	$xls_title     = trim($xls_title);
             	$xls_title_key = md5($xls_title);
             	
             	if(isset($c[$xls_title_key])){
             	  $dbcol_saveTo[$xls_col_pos] = $c[$xls_title_key];	
             	}
             }
       	 	}
       	   }	
             
            if($row>=2) { 
             if(isset($rowData[0])){
             	$data = array();
             	$data['ID'] = generateID($datatbl);
              foreach ($rowData[0] as $xls_col_pos=>$xls_value){
             	$destination_col = isset($dbcol_saveTo[$xls_col_pos]) ? $dbcol_saveTo[$xls_col_pos] : null;
             	if(!is_null($destination_col)){
             	 $data[$destination_col]  =	centerTrim($xls_value);
                }
              }
              
              if(sizeof($data)>0){
              	
              	//load
              	 $record_orm_saved = false;
              	 
              	 if(sizeof($keycolums)>0){
              	 	
              	  $load_sql = array();
              	  $record_orm = new ADODB_Active_Record($datatbl, $keycolums);
              	   //map key col vals
              	   
              	   if($keycolums && sizeof($keycolums)>=1){
              	    foreach ($keycolums as $keycolumn){
              	      if(isset($data[$keycolumn])){	
              	       $value       = $data[$keycolumn]; 	
              	       $load_sql[]  = "{$keycolumn}='{$value}'";
              	      }
              	    }
              	   }
              	    
              	    $load_sql_where = implode('AND ', $load_sql);
              	    
              	    if(!empty($record_orm)){
              	    	$record_orm->Load( $load_sql_where );
              	    	
              	    	if(!empty($record_orm->_lasterr)){
              	    		$record_orm_saved = false;
              	    	}else{
              	    	  	foreach ($data as $data_col => $data_col_val){
              	    	  		$data_col_small = strtolower($data_col);
              	    	  		$record_orm->$data_col_small  = $data_col_val;
              	    	  	}
              	    	  	
              	    	  	$insert = $record_orm->Save();
              	    	  	
              	    	  	if($insert){
              	    	  	 $record_orm_saved = true;	
              	    	  	}else{ 
              		         $record_orm_saved = false;
              		         $has_err =  true;
              	            }
              	            
              	    	}
              	    }
              	 }
              	
              	if(!$record_orm_saved){  
              	  $insert = $db->AutoExecute($datatbl,$data, 'INSERT');
              	}
              	
              	if(!$insert){
              		$data['e']  = $db->ErrorMsg();
              		$err[]  = $data;
              		$has_err =  true;
              	}
              }
              
             }
       	 	}//each data row
        }
        
        if(!$has_err){
         return json_response(1,'file ' .$name .' uploaded');
        }else{
        	
         if(sizeof($err)<=1){
         	return json_response(0,'failed to import ' .$name , array('export'=>''));
         }
        	
         
           $objPHPExcel  =  new PHPExcel();
           $objWorksheet = $objPHPExcel->createSheet(0);
	
  	      $items = array();
  	      
		 if(isset($err[0])){
	     if(sizeof($err[0])>0){
	      $count_col = 0;
		  foreach ($err[0] as $col_title){
			
		  	if($col_title!='ID'){
			$objWorksheet->setCellValueByColumnAndRow( $count_col, 1, $col_title );
            $objWorksheet->getColumnDimensionByColumn($count_col)->setAutoSize( true ); 
            ++$count_col;
		  	}
           
		  }
		  
		  $objWorksheet->setCellValueByColumnAndRow( $count_col, 1, 'technical desc' );
          $objWorksheet->getColumnDimensionByColumn($count_col)->setAutoSize( true ); 
          
		 }
		}
		
		 if($err[1]){	
		  $rownum = 2;
		 foreach ($err as $err_index => $err_row_array){
		  if($err_index>0){	
		   $col_index = 0;
		   
		   foreach ($err_row_array as $key => $val ){
			 if($key!='ID'){
			  $objWorksheet->setCellValueByColumnAndRow( $col_index, $rownum, $val );	
			   ++$col_index;
			 }
		   }
		  
		   ++$rownum; 
		   }
		  }
		 }
	 
	  $appname   = $appname . '_import_errors' ;
	  
	  $objWorksheet->setTitle($appname);
      $objPHPExcel->setActiveSheetIndex(0);
     
      $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
      $writeTo   =  "tmp_files/{$appname}.xls";
      
       try {
       $objWriter->save( ROOT.$writeTo);
        return json_response(0, 'failed to import ' .$name,array('export'=>$writeTo));
       }catch (Exception $e){
      	$err = str_replace(ROOT, './',$e->getMessage());
      	return json_response(0, $err );
       }
		
      return json_response(0,'failed to import ' .$name , array('export'=>$writeTo));
     
     }
    }
  }
	
}
