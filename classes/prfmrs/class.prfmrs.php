<?php

 class prfmrs {
 	private $tax_rates  = array();
 	private $nhifrates  = array();
 	private $activeyear;
 	private $activemonth;
 	
 	public function __construct(){
 		global $db;
 		
 	    $active_period      = $db->GetRow("SELECT PRYEAR,PRMONTH FROM PRPRD WHERE ISACTIVE=1");
 	    $this->activeyear   = valueof($active_period,'PRYEAR');
 	    $this->activemonth  = valueof($active_period,'PRMONTH');
 	    
 	    $this->tax_rates   = $db->GetArray("SELECT * FROM PRRTTX WHERE  PRYEAR={$this->activeyear}  ORDER BY FROMAMT ");
 	    
 	    $this->nhifrates   = $db->GetArray("SELECT * FROM PRRTNH WHERE PRYEAR={$this->activeyear} ORDER BY FROMAMT");

 	}
 	
    public function calculate_TaxableTotal($xx){

    }
    
 	public function calculate_TaxRate($taxable_salary){
 	 
 	 	 	 	 
 $diff          = array();
 $my_tax_array  = array();
 $tax_charged   = 0;
 $total_tax     = 0;
 
 foreach ($this->tax_rates as $index=>$taxrate){
	if( ($taxable_salary >= $taxrate['FROMAMT']) && ($taxable_salary<=$taxrate['TOAMT'])  ) {
	 $diff[$index] = $taxable_salary- $taxrate['TOAMT'];
	}
 }

 asort($diff,SORT_ASC);
 $diff_keys           = array_keys($diff);
 $my_tax_range        = current($diff_keys);
 

 foreach ($this->tax_rates as $index=>$taxrate){
  if($index<=$my_tax_range){	
 	$my_tax_array[$index] = $taxrate;
	if( ($taxable_salary >= $taxrate['FROMAMT']) && ($taxable_salary<=$taxrate['TOAMT'])  ) {
		$my_tax_array[$index]['BALANCE']  = $taxable_salary-$tax_charged;
	}
	
 	$tax_charged  += $taxrate['TAXED'];
 	
 	if(isset($my_tax_array[$index]['BALANCE'])){
 		$my_tax_array[$index]['TAXED'] = $my_tax_array[$index]['BALANCE'];
 	}
 	
  }//if $my_tax_range
 }//foreach rates
 
 foreach ($my_tax_array as $my_tax){
	 $total_tax += ($my_tax['RATE']/100) * $my_tax['TAXED'];
 }
 return $total_tax;
 
}//fn getRate
 	
 	public function calculate_NHIFRate($gross_pay){
 	 
     $diff          = array();
     
     foreach ($this->nhifrates as $index=>$nhifrate){
	  if( ( $gross_pay >= $nhifrate['FROMAMT'] ) && ( $gross_pay <= $nhifrate['TOAMT'] )  ) {
	   $diff[$index] = $gross_pay- $nhifrate['TOAMT'];
	  }
     }

     asort($diff,SORT_ASC);
     $diff_keys           = array_keys($diff);
     $my_pay_range        = current($diff_keys);

     if(isset($this->nhifrates[$my_pay_range]['RATE']) && is_numeric($this->nhifrates[$my_pay_range]['RATE'])) {
  	  $deduction = $this->nhifrates[$my_pay_range]['RATE'];
     }else{
  	  $deduction = 0;
     }
  
     return $deduction;
 
    }//fn getRate
 	
    public function calculate_BankLoan(){
    	
    }
    
    public function calculate_InsuranceRelief($premium){
    
      
      $max = 5000;
      if(is_numeric($premium)){
       $relief   = (0.15 * $premium);
      }else{
      	$relief  = 0;
      }
      if($relief>$max){
      	$relief = $max;
      }
      return $relief;
    }
    
    public function Constant_Value($value){
    	return isset($value) ? $value: 0;
    }
    
   public static function list_fmrs($select_name,$selected_method='',$options=''){
 	$custom_methods = get_class_methods( __class__ );
 	
 	unset($custom_methods[0]);
 	
 	$custom_methods[0]='User_Defined';
 	
 	asort($custom_methods);
 	
 	$selected_method = isset($selected_method) && !empty($selected_method) ? $selected_method : 'User_Defined';
 	
 	$return  ="<select name=\"{$select_name}\" id=\"{$select_name}\" {$options}>";
	$return .="<option></option>";
    foreach ($custom_methods as $custom_method){
	 if($custom_method !='list_fmrs'){
  	  $return .="<option value=\"{$custom_method}\" ";
  	  $return .= !empty($selected_method)&&$selected_method==$custom_method?' selected ':'';
  	  $return .=">{$custom_method}</option>";
     }
    }
    $return .="</select>";
    return $return;
 }
    
 }
 
