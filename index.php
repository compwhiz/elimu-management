<?php
// Enable CORS by
// wampmanager -> Apache -> Apache modules -> headers_module : VERY IMPORTANT
// Proof its working
// Grade analysis
//
//include_once('./include/Mobile_Detect.php');
//$Mobile_Detect   = new Mobile_Detect;
//
//if( $Mobile_Detect->isMobile() ){
//header("Location: ./mobile/");
//}
// TODO: On new installations, remember that contct column on SATSSG and SATSS -- Very Important
// TODO: Edit Exam global setup: No. of Subjects and Minimum subjects are interchange.
// TODO: Check on the Z problem. Mean grades affects the students with less than required subjects
// TODO: Add Class Teachers to Grade Analysis
// TODO: Add Printout for per class and stream in student perfomance
// TODO: Add Printout for per exam and stream in student perfomance
// TODO: Student perfomance as a merit list from form one or as the user wishes with dropwdown menus
// TODO: Automatically import data
// TODO: Grading of imported data with previous exam settings
// TODO: Ranking subjects in report form to be automatic
// TODO: Ranking KCSE and Position...and Ranking in general
// TODO: Classlist and Data Cleanup is not consisent. Brings up all the students on subsequent forms.
// If this installation has not been paid for by a certain date. I will show a page to prevent the user from
// using this application
$endDate = json_decode(file_get_contents('enddate.json'), true);
if (date('Y-m-d') >= $endDate['date']) {
    die("Contact KENNEDY MUTISYA | 0713 642 175");
}

require_once('./init.php');
require_once('./config/db.php');
include_once('./include/combine.php');

if (!isset($_SESSION['user'])) {
    require_once('login.php');
    die;
}
//$db->debug=1;//remove
$ip = get_user_ip();
$date = date('Y-m-d H:i:s');
$time = time();


$user = new user();

$track = $db->Execute("insert into visits(IP,DATEV,TIMEV)VALUES('{$ip}','{$date}',{$time})");
$apps = $db->GetAssoc("SELECT DISTINCT APPID,APPNAME FROM SYSAPPS ORDER BY APPID   ");

if (isset($_GET['appid'])) {
    $appid = filter_input(INPUT_GET, 'appid', FILTER_SANITIZE_STRING);
} else {
    $appid = 'ST';
}

if (strtolower($user->userid) == 'admin') {
    $mods = $db->GetAssoc("SELECT DISTINCT MODID,MODNAME,MODICON FROM SYSMODS WHERE APPID='{$appid}' ORDER BY MODPOS");
} else {
    $mods = $db->GetAssoc("
                         SELECT DISTINCT MODID,MODNAME,MODICON FROM sysmods
                         WHERE APPID='{$appid}'
                         AND MODID IN (SELECT DISTINCT MODID FROM sysmnuauth
                         WHERE GROUPCODE='{$user->groupcode}' AND APPID='{$appid}')
                         ORDER BY MODPOS
                         ");
}

$school = new school();
$school_name = Camelize($school->name);
$app_title = 'Elimu - ' . $school_name;
$app_footer_text = 'School Information Management System';

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?php print $app_title; ?></title>
    <link rel="stylesheet" type="text/css" href="./public/css/themes/gray/easyui.css">
    <!--    <link rel="stylesheet" type="text/css" href="./public/css/themes/mobile.css">-->
    <link rel="stylesheet" type="text/css" href="./public/css/themes/icon.css">
    <link rel="stylesheet" type="text/css" href="./public/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="./public/css/main.css">
    <link rel="stylesheet" type="text/css" href="./public/css/pace.css">
    <script src="./public/js/chartist.js"></script>
    <script src="./public/js/axios.js"></script>
    <link rel="stylesheet" type="text/css" href="./public/css/chartist.css">
    <script>
        paceOptions = {
            restartOnPushState: true
        }
    </script>
    <script type="text/javascript" src="./public/js/pace.min.js"></script>

    <?php
    combine_js(
        array(
            './public/js/jquery.min',
            './public/js/jquery.easyui.min',
        ),
        false
    );
    ?>
    <!--    <script type="text/javascript" src="./public/js/jquery.easyui.mobile.js"></script>-->
    <script type="text/javascript" src="./public/js/highcharts/highcharts.js"></script>

</head>

<body id="easyui_layout" class="easyui-layout">

<div id="mm1" style="width:150px;">
    <!--    <div onclick="top.location='./?appid=ST"><i class="fa fa-home fa-fw"></i>&nbsp;Academic</div>-->
    <!--    <div><i class="fa fa-edit"></i>&nbsp;My Profile</div>-->
    <!--    <div><i class="fa fa-gear"></i>&nbsp;Account Settings</div>-->
    <div onclick="window.open('password.php', 'update', 'width=800, height=600');"><i class="fa fa-eye-slash"></i>&nbsp;Change
        Password
    </div>
    <!--    <div onclick="window.open('update.php', 'update', 'width=800, height=600');"><i class="fa fa-refresh"></i>&nbsp;System-->
    <!--        Update-->
    <!--    </div>-->
    <div class="menu-sep"></div>
    <div onclick="top.location='./endpoints/logout/';"><i class="fa fa-power-off"></i>&nbsp;Log Off</div>
</div>

<div title="" data-options="region:'north',border:false" style="height:30px;padding:0;background:#fff;">
    <div class="easyui-panel" data-options="fit:true,border:false" style="padding:0;background:#E6EEF8">

        <a href="./" class="easyui-linkbutton" data-options="plain:true" onclick="top.location=top.location"><i
                    class="fa fa-graduation-cap"></i>&nbsp; <?php echo $app_title; ?></a>

        <div style="float:right;text-align:right;padding-right:150px;">
            <a href="#" class="easyui-menubutton" data-options="menu:'#mm1'"><i
                        class="fa fa-user"></i>&nbsp;<?php print ucwords($user->username); ?></a>
        </div>

    </div>
</div>


<div data-options="region:'west',split:true,splitImg:'true',iconCls:''" title="Navigation"
     style="width:220px;padding:0;">

    <div class="easyui-accordion" data-options="fit:true,border:false,animate:false" style="padding:0;">

        <?php
        if ($mods) {
            if (sizeof($mods) > 0) {
                foreach ($mods as $modid => $mod) {

                    $modicon = valueof($mod, 'MODICON');
                    $modname = valueof($mod, 'MODNAME');

                    $vars['appid'] = $appid;
                    $vars['modid'] = $modid;
                    $vars_str = packvars($vars);

                    echo "
				 	<div title=\"{$modname}\" iconCls=\"{$modicon}\" style=\"overflow-x:hidden;overflow-y:auto;padding:2px;\">

				 	<ul id=\"mnu_{$modid}\" class=\"easyui-tree\" data-options=\"
				    url: './endpoints/menu/?vars={$vars_str}',
				    method: 'post',
				    animate: true,
				    onClick: function(node){
				    var isLeaf ,url='',modkey='',width=700,height=500;
				     isLeaf = true;
				      if(typeof node.children==='object'){
				       isLeaf = false;
				       if(typeof node.state==='string'){
				        if(node.state==='closed'){
				         expand('mnu_{$modid}');
				        }else{
				         collapse('mnu_{$modid}');
				        }
				       }
				      }
				      if(isLeaf) {
				        if (node.attributes) {

		                openWindow( node.id, node.text,
		                          node.attributes.modkey,
		                          node.attributes.modtype,
		                          node.attributes.modvars,
		                          node.attributes.win,
		                          node.attributes.hei,
		                          node.attributes.min,
		                          node.attributes.max,
		                          node.attributes.clo,
		                          node.attributes.col,
		                          node.attributes.res,
		                          node.attributes.bta,
		                          node.attributes.bte,
		                          node.attributes.btd,
		                          node.attributes.bti,
		                          node.attributes.btx
		                );
				      }
				    }
				  }

			     \"></ul>

				</div>
				 	";

                }
            }
        }
        ?>

    </div>
</div>

<div data-options="region:'center',iconCls:'icon-ok'" title="Dashboard" style="padding:5px">
    <div class="easyui-layout" data-options="fit:true">
        <div data-options="region:'center',title:'Home',href:'home.php?appid=<?php echo $appid; ?>' ">

        </div>
    </div>
</div>


<div data-options="region:'south',border:false" style="text-align:right;height:30px;padding:10px;background:#B3DFDA;">
    &copy; <?php echo $app_footer_text; ?> <?php echo date('Y'); ?>
</div>
<iframe name="ifr_main" id="ifr_main" frameborder="0" height="0" width="0" src="" scrolling="no"></iframe>
<script type="text/javascript">
    if (window.navigator.onLine) {
        axios.defaults.headers.post['Content-Type'] ='application/json;charset=utf-8';
        axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*';
        axios.post('http://compwhiz.co.ke/school/makindu').then(resp=>{
            console.log(resp.data);
        }).catch(err=>{
            console.log(err)
        });
    }
</script>


<?php
combine_js(
    array(
        './public/js/jquery.portal',
        './public/js/datagrid-filter',
        './public/js/ui',
        './public/js/main',
        './public/js/moment.min',
        './public/js/jquery.tablenav.min',
        './public/js/jquery.countTo',
    ),
    false
);
?>

</body>
</html>
