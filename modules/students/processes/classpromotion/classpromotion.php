<?php

  class classpromotion {
	private $id;
	private $datasrc;
	private $primary_key;
	
	public function __construct(){
		global $cfg;
		
		$this->id           = filter_input(INPUT_POST , 'id');
		$this->datasrc      = valueof($cfg,'datasrc');
		$this->primary_key  = valueof($cfg,'pkcol');
	}
	
	public function pre_run() {
		global $db;
		
       $yearcode          = filter_input(INPUT_POST , ui::fi('year'));
       
       if(empty($yearcode)){
        return json_response(0,"Please Select Year ");
       }
       
       $forms  = $db->GetAssoc('SELECT FORMCODE,FORMNAME FROM SAFORMS ORDER BY FORMCODE');
       
	  if( sizeof($forms)>0){
	   return json_response(1,'OK');
	  }else{ 	
	   return json_response(0,"No Records Found for {$yearcode} ");
	  }

	}
    
 }
