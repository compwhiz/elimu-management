<?php

$yearcode    = filter_input(INPUT_GET , ui::fi('year'));
$termcode    = filter_input(INPUT_GET , ui::fi('term'));
$exam        = filter_input(INPUT_GET , ui::fi('exam'));
$exam_name   = filter_input(INPUT_GET , ui::fi('exam_name'));
$formstream  = filter_input(INPUT_GET , ui::fi('form'));
$positioning = filter_input(INPUT_GET , ui::fi('positioning'));
$disp_score  = filter_input(INPUT_GET , ui::fi('subjscore'));

 if(strlen($formstream)>1) {
  $formstream_code_col = "STREAMCODE";	
  $positioning_col     = "POSTREAM";	
  $formcode            = substr($formstream,0,1);
  
   switch($positioning){
	case 'p':	
     $positioning_col        = 'POSTREAM_POINTS';
     $display_col_name       = 'TOTALP';
     $display_col_title      = 'PNTS';
     $display_col_gradecode  = 'GRADECODEP';
     $positioning_by_descr   = ' POINTS ';	
    break;
    case 'm':
    default:
     $positioning_col        = 'POSTREAM_MARKS';	
     $display_col_name       = 'TOTALM';
     $display_col_title      = 'AVE';	
     $display_col_gradecode  = 'GRADECODEM';
     $positioning_by_descr   = ' MARKS ';	
    break;
  }

 }else{
   $formstream_code_col = "FORMCODE";
   $positioning_col     = "POSFORM";			
   $formcode            = $formstream;
 
   switch($positioning){
	case 'p':	
     $positioning_col        = 'POSFORM_POINTS';
     $display_col_name       = 'TOTALP';
     $display_col_title      = 'PNTS';
     $display_col_gradecode  = 'GRADECODEP';
     $positioning_by_descr   = ' POINTS ';
    break;
    case 'm':
    default:
     $positioning_col        = 'POSFORM_MARKS';
     $display_col_name       = 'TOTALM';
     $display_col_title      = 'AVE';	
     $display_col_gradecode  = 'GRADECODEM';
     $positioning_by_descr   = ' MARKS ';	
    break;
  }
  
 }

switch ($disp_score){
	case 'r':
	  $score_column_postfix = '';	
	 break;
	 case 'r':
	 default:	
	  $score_column_postfix = 'P';	
	 break;
}


$examdesc_joined = "FORM {$formcode} {$exam_name} TERM {$termcode} {$yearcode}";
$examdesc        = strtoupper($examdesc_joined);

$data  = $db->Execute("
        SELECT * FROM VIEWSTUDENTSUBJECTS 
        WHERE YEARCODE='{$yearcode}'
        AND TERMCODE='{$termcode}' 
        AND {$formstream_code_col}='{$formstream}' 
        ORDER BY STREAMCODE, SORTPOS,CODEOFFC
        ");

 $classTeachers  =  $db->CacheGetAssoc(1200,"
 SELECT C.STREAMCODE,T.FULLNAME 
 FROM SACT C 
 INNER JOIN  SATEACHERS T ON T.TCHNO=C.TCHNO 
left join SASTREAMS R ON R.STREAMCODE = C.STREAMCODE
left join SAFORMS F ON F.FORMCODE = R.FORMCODE
 WHERE C.YEARCODE='{$yearcode}' AND R.FORMCODE='{$formstream}'
 ");
 
 
 $school         =  new school();
 $schooladdr     =  $school->address;
 $schoolname     =  $school->name;
 $schooltel      =  $school->telephone;
 $schoolvision   =  $school->vision;
 $schoollogo     =  valueof($school, 'logo_path','./public/images/logo.gif');


 ?>
 <html>
  <head>
   <title>Exam List</title>
  <script language="JavaScript" type="text/javascript">
    /*setTimeout("window.print();", 10000);*/
</script>
<style>
 body {
 padding : 0px;
 margin : 0px;
 font-size:9px;
 }
    table.data {
        font-family:Verdana;
        font-size:9px;
        empty-cells: show;
        border:1px solid #000;
        border-collapse:collapse;
        border-spacing: 0.5rem;
        empty-cells:show;
    }

    table.data td {
        border:1px solid #ccc;
    }
    
    table.data td.header {
        background-color:#EDECEB;
        font-size:bold;
    }

    table.data td.abottom {
        vertical-align:bottom;
        font-size:10px;
    }

    span.title{
     font-size:14px;
     font-weight:bold;
    }
    
    @media all {
        .page-break  { display: none; }
    }

    @media print {
        .page-break  { 
            display: block; 
            page-break-before: always;
            margin:0px;
            padding:0px;
        }
    }

    @media screen {
        .page-break  { 
            display: block; 
            page-break-before: always;
            margin:5px;
            padding:5px;
        }
    }

</style>

  </head>
 <body>
 <?php
  if($data) {
 	
     if(strstr($exam,'CAT') || strstr($exam,'PAPER')){
        $pointscol    = "{$exam}POINTS";
        $gradecodecol = "{$exam}GRADECODE";
       }else{
       	$pointscol    = "POINTS";
       	$gradecodecol = "GRADECODE";
       }
       
    $gradecodecol  = strtoupper($gradecodecol);
 	$subjects_data = array();
 	$distinct_subjects = array();
 	$summary_grades = array();
 	$summary_streams = array();
 	$summary_points = array();
 	$summary = array();
 	
 	if($data->RecordCount()>0) {
 		while (!$data->EOF) {
       	 
       	 	$admno        = valueof($data->fields, 'ADMNO');	
       	 	$fullname     = valueof($data->fields, 'FULLNAME');	
       	 	$subjectcode  = valueof($data->fields, 'SUBJECTCODE');	
       	 	$catcode      = valueof($data->fields, 'CATCODE');	
       	 	$formcode     = valueof($data->fields, 'FORMCODE');
       	 	$streamcode   = valueof($data->fields, 'STREAMCODE');
       	 	$sbccode      = valueof($data->fields, 'SBCCODE');
       	 	
       	 	$cat1         = valueof($data->fields, 'CAT1'.$score_column_postfix);
       	 	$cat2         = valueof($data->fields, 'CAT2'.$score_column_postfix);
       	 	$cat3         = valueof($data->fields, 'CAT3'.$score_column_postfix);
       	 	$cat4         = valueof($data->fields, 'CAT4'.$score_column_postfix);
       	 	$cat5         = valueof($data->fields, 'CAT5'.$score_column_postfix);
       	 	
       	 	$cat1gradecode  = valueof($data->fields, 'CAT1GRADECODE');
       	 	$cat2gradecode  = valueof($data->fields, 'CAT2GRADECODE');
       	 	$cat3gradecode  = valueof($data->fields, 'CAT3GRADECODE');
       	 	$cat4gradecode  = valueof($data->fields, 'CAT4GRADECODE');
       	 	$cat5gradecode  = valueof($data->fields, 'CAT5GRADECODE');

       	 	$paper1gradecode  = valueof($data->fields, 'PAPER1GRADECODE');
       	 	$paper2gradecode  = valueof($data->fields, 'PAPER2GRADECODE');
       	 	$paper3gradecode  = valueof($data->fields, 'PAPER3GRADECODE');
       	 	$paper4gradecode  = valueof($data->fields, 'PAPER4GRADECODE');
       	 	$paper5gradecode  = valueof($data->fields, 'PAPER5GRADECODE');
       	 	
       	 	$avgcat       = valueof($data->fields, 'AVGCAT');
       	 	
       	 	$paper1       = valueof($data->fields, 'PAPER1'.$score_column_postfix);
       	 	$paper2       = valueof($data->fields, 'PAPER2'.$score_column_postfix);
       	 	$paper3       = valueof($data->fields, 'PAPER3'.$score_column_postfix);
       	 	$paper4       = valueof($data->fields, 'PAPER4'.$score_column_postfix);
       	 	$paper5       = valueof($data->fields, 'PAPER5'.$score_column_postfix);
       	 	
       	 	$avgexam      = valueof($data->fields, 'AVGEXAM');
       	 	
       	 	$total        = valueof($data->fields, 'TOTAL');
       	 	$gradecode    = valueof($data->fields, 'GRADECODE');
       	 	$points       = valueof($data->fields, 'POINTS');
       	 	$picked       = valueof($data->fields, 'PICKED');
       	 	
       	 	if(!array_key_exists($subjectcode, $distinct_subjects)){
       	 		$distinct_subjects[$subjectcode] = $subjectcode;
       	 	}
       	 	
       	 	$exam_column = $exam.$score_column_postfix;
       	 	$exam_column_value = valueof($data->fields, $exam_column);
       	 	
       	 	$subjects_data[$admno][$subjectcode]['score']      = $exam_column_value;
       	 	$subjects_data[$admno][$subjectcode]['gradecode']  = valueof($data->fields, $gradecodecol);
       	 	$subjects_data[$admno][$subjectcode]['picked']     = valueof($data->fields, 'PICKED');
       	 	
       	  $data->MoveNext();
       	 }
 	}
 	
 	 $student_means = $db->GetAssoc("SELECT ADMNO, FULLNAME, FORMCODE, STREAMCODE,TOTAL,NUMSUBJ,TOTALP,TOTALM,GRADECODEP,GRADECODEM,
 	 POSTREAM_MARKS,POSFORM_MARKS,POSTREAM_POINTS,POSFORM_POINTS
 	  FROM VIEWSTUDENTSMEAN 
 	  WHERE YEARCODE='{$yearcode}' 
 	  AND {$formstream_code_col}='{$formstream}' 
 	  AND TERMCODE='{$termcode}' 
 	  AND EXAM='{$exam}'
 	  order by {$positioning_col} asc
 	  ");
 	 
 	 if(sizeof($student_means)>0) {
 	 	$page_rows    = 1;
 	 	$pageno       = 1;
 	 	$page_num_lines       = 30;
 	 	$num_subjects = count($distinct_subjects);
 	 	$td_colspan = 10 + $num_subjects;
 	 	
 	 	$page1_header = "<table cellpadding=\"2\" cellspacing=\"0\" width=\"100%\" class=\"data\">";
 	 	
 	 	$page1_header .= "<tr>";
 	 	 $page1_header .= "<td colspan=\"{$td_colspan}\">
 	 	 
 	 	   <table width=\"100%\" border=0 cellspacing=\"0\" cellpadding=\"1\" class=\"data\">
 	 	   
 	 	    <tr>
 	 	     <td rowspan=\"6\"><img src=\"{$schoollogo}\" ></td>
 	 	    </tr>
 	 	    
 	 	    <tr>
 	 	     <td valign=\"top\"  colspan=\"2\"><span class=\"title\">{$schoolname}</span></td>
 	 	    </tr>
 	 	    
 	 	    <tr>
 	 	     <td  colspan=\"2\"><b>Address : {$schooladdr}</b></td>
 	 	    </tr>
 	 	    
 	 	    <tr>
 	 	     <td colspan=\"2\"><b>Tel :{$schooltel}</b></td>
 	 	    </tr>
 	 	    
 	 	    <tr>
 	 	     <td colspan=\"2\"><b>Vision :{$schoolvision}</b></td>
 	 	    </tr>
 	 	    
 	 	    <tr>
 	 	     <td colspan=\"2\">&nbsp;</td>
 	 	    </tr>
 	 	    
 	 	    <tr>
 	 	     <td><b>{$examdesc}</b></td><td><b>RE GRADED EXAM MARKS</b></td>
 	 	    </tr>
 	 	    
 	 	   </table>
 	 	   
 	 	 </td>";
 	 	$page1_header .= "</tr>";
 	 	
 	 	echo $page1_header;
 	 	
 	 	$header = "<table cellpadding=\"2\" cellspacing=\"0\" width=\"100%\" class=\"data\">";
 	 	
 	 	$header .= "<tr  nobr=\"true\">";
 	 	 $header .="<td class=\"header\" >NO</td>";
 	 	 $header .="<td nowrap class=\"header\" >ADM</td>";
 	 	 $header .="<td nowrap class=\"header\" nowrap>NAME</td>";
 	 	 $header .="<td nowrap class=\"header\" >CLS</td>";
 	 	 
 	 	 if(count($distinct_subjects)){
 	 	 	foreach ($distinct_subjects as $subjectcode){
 	 	 		$header .="<td class=\"header\" >{$subjectcode}</td>";
 	 	 	}
 	 	 }
 	 	 
 	 	 $header .="<td nowrap class=\"header\" >SUBJ ENTRY</td>";
 	 	 $header .="<td class=\"header\" >TOT</td>";
 	 	 $header .="<td nowrap class=\"header\" >{$display_col_title}</td>";
 	 	 $header .="<td nowrap class=\"header\" >MG</td>";
 	 	 $header .="<td nowrap class=\"header\" >POS. CLASS</td>";
 	 	 $header .="<td nowrap class=\"header\" >POS. FORM</td>";
 	 	$header .="</tr>";
 	 	
 	 	echo $header;
 	 	
 	 	$count = 1;
 	 	foreach ($student_means as $admno=>$my_mean){
 	 		
       	 	$fullname         = valueof($my_mean, 'FULLNAME');
       	 	$numsubj          = valueof($my_mean, 'NUMSUBJ');
       	 	$streamcode       = valueof($my_mean, 'STREAMCODE');
       	 	$total            = valueof($my_mean, 'TOTAL','','round');
       	 	$totalm           = valueof($my_mean, 'TOTALM','','round');
       	 	$totalp           = valueof($my_mean, 'TOTALP');
       	 	$gradecodep       = valueof($my_mean, 'GRADECODEP');
       	 	$gradecodem       = valueof($my_mean, 'GRADECODEM');
       	 	$postream_points  = valueof($my_mean, 'POSTREAM_POINTS');
       	 	$posform_points   = valueof($my_mean, 'POSFORM_POINTS');
       	 	$posform_marks    = valueof($my_mean, 'POSFORM_MARKS');
       	 	$postream_marks   = valueof($my_mean, 'POSTREAM_MARKS');	
       	 	
       	 	
            if(isset($summary[$streamcode][$gradecodem])){
            	++$summary[$streamcode][$gradecodem];
            }else{
            	$summary[$streamcode][$gradecodem] =1;
            }
                   	 	
            if(array_key_exists($streamcode, $summary_points)){
            	$summary_points[$streamcode] += $totalp;
            }else{
            	$summary_points[$streamcode] = $totalp;
            }
                 	 	
            if(!array_key_exists($streamcode, $summary_streams)){
            	$summary_streams[$streamcode] = $streamcode;
            }
            
            
 	 	echo "<tr>";
 	 	 echo "<td>{$count}</td>";
 	 	 echo "<td>{$admno}</td>";
 	 	 echo "<td nowrap>{$fullname}</td>";
 	 	 echo "<td>{$streamcode}</td>";
 	 	 
 	 	 if(count($distinct_subjects)){
 	 	 	foreach ($distinct_subjects as $subjectcode){
 	 	 		
 	 	 		$subject_score     = isset($subjects_data[$admno][$subjectcode]['score']) ? round($subjects_data[$admno][$subjectcode]['score'],0) : null;
 	 	 		$subject_gradecode = isset($subjects_data[$admno][$subjectcode]['gradecode']) ? $subjects_data[$admno][$subjectcode]['gradecode'] : null;
 	 	 		$subject_picked    = isset($subjects_data[$admno][$subjectcode]['picked']) && $subjects_data[$admno][$subjectcode]['picked']==1 ? '' : '*';
 	 	 		
 	 	 		$cell_vars = $subject_score>0 ? "{$subject_picked}{$subject_score} {$subject_gradecode}" : '&nbsp;';
 	 	 		echo "<td>{$cell_vars}</td>";
 	 	 	}
 	 	 }
 	 	 
 	 	 $display_col_name_value  = valueof($my_mean, $display_col_name ,'','floor' );
 	 	 
 	 	 if($disp_score=='p'){
// 	 	  $display_col_name_value = $display_col_name_value *100;	
 	 	 }
 	 	 
 	 	 echo "<td>{$numsubj}</td>";
 	 	 echo "<td>{$total}</td>";
 	 	 echo "<td>{$display_col_name_value}</td>";
 	 	 echo "<td>" . valueof($my_mean, $display_col_gradecode). "</td>";

        if($positioning=='p'){
 	 	 echo "<td>{$postream_points}</td>";
 	 	 echo "<td>{$posform_points}</td>";
        }else{
 	 	 echo "<td>{$postream_marks}</td>";
 	 	 echo "<td>{$posform_marks}</td>";
        }
        
 	 	echo "</tr>";
 	 	
 	 	
     if ($page_rows == $page_num_lines ) {

        echo "</table>";
        
         echo $header;
        echo " \r\n<div class=\"page-break\"></div>  \r\n";

        $pageno++;
        $page_rows = 0;
     }
    
 	 	++$count;
 	 	++$page_rows;
 	}
 	 	
 	 	echo "</table>";
 	 }else{
 	 	echo ("First Grade Form {$formstream} then refresh this report");
 	 }
 }
 
 //summary
//
// echo form_summary($yearcode, $termcode, $formcode, $exam);
//  	
 echo "<table cellpadding=\"2\" cellspacing=\"0\" width=\"100%\" class=\"data\">";
 
  echo "<tr>";
   echo "<td colspan=\"2\">&nbsp;</td>";
  echo "</tr>";
  
  echo "<tr>";
   echo "<td valign=\"top\">";
   
      echo "<table cellpadding=\"2\" cellspacing=\"0\" width=\"100%\" class=\"data\">";
 
       echo "<tr>";
        echo "<td colspan=\"2\"><b>CLASS TEACHERS</b></td>";
       echo "</tr>";
  
      if($classTeachers && is_array($classTeachers) && sizeof($classTeachers)>0){
       foreach ($classTeachers as $teacher_streamcode => $teachername){
         echo "<tr>";
         echo "<td width=\"30px\">{$teacher_streamcode}</td>";
          echo "<td>{$teachername}</td>";
         echo "</tr>";
       }
      }
      
     echo "</table>";
   echo "</td>";
   
   echo "<td  valign=\"top\">";
  
    echo "<table cellpadding=\"2\" cellspacing=\"0\" width=\"100%\" class=\"data\">";
 
     echo "<tr>";
      echo "<td colspan=\"2\"><b>KEY</b>:</td>";
     echo "</tr>";
  
     echo "<tr>";
      echo "<td>X</td>";
      echo "<td>ABSENTEE</td>";
     echo "</tr>";
  
     echo "<tr>";
      echo "<td>Y</td>";
      echo "<td>EXAM IRREGULARITIES</td>";
     echo "</tr>";
  
     echo "<tr>";
      echo "<td>Z</td>";
      echo "<td>MISSING MARKS</td>";
     echo "</tr>";
  
     echo "</table>";
    echo "</td>";
   echo "</tr>";
  echo "</table>";
 
  function get_grade_code($points){
  	global $db;
  	return $db->CacheGetOne(1200,"select GRADECODE from SAMGRD WHERE MIN<={$points} AND MAX>={$points}");
  }
  
 ?>
 </body>
  </html>
 
