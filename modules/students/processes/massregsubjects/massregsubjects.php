<?php

  class massregsubjects {
	private $id;
	private $datasrc;
	private $primary_key;
	
	public function __construct(){
		global $cfg;
		
		$this->id           = filter_input(INPUT_POST , 'id');
		$this->datasrc      = valueof($cfg,'datasrc');
		$this->primary_key  = valueof($cfg,'pkcol');
	}
	
	public function pre_run() {
		global $db;
		
        $yearcode     = filter_input(INPUT_POST , ui::fi('year'));
        $formstream   = filter_input(INPUT_POST , ui::fi('form'));

		if(strlen($formstream)>1){
		 $formcode            = $formstream;
		 $formstream_code_col = "STREAMCODE";
		}else{
		 $formcode            = substr($formstream,0,1);
		 $formstream_code_col = "FORMCODE";
		}
		
       if(empty($yearcode)){
        return json_response(0,"Please Select Year ");
       }
       
       if(empty($formcode)){
        return json_response(0,"Please Select Class ");
       }
       
      $students  = $db->GetOne("SELECT COUNT(ADMNO) FROM VIEWSP WHERE {$formstream_code_col}='{$formcode}' AND YEARCODE='{$yearcode}'");
       
	  if($students>0){
	   return json_response(1,'OK');
	  }else{ 	
	   return json_response(0,"No Students Found in Form {$formcode} Year {$yearcode} ");
	  }

	}
    
 }
