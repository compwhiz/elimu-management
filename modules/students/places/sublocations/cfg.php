<?php
/**
* auto created config file for modules/students/places/sublocations
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-01-06 17:54:27
*/

$scriptname  = @end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'Sub Locations';//user-readable formart
 $cfg['appname']       = 'sublocations';//lower cased one word
 $cfg['datasrc']       = 'ADSUBLOCATIONS';//where to get data
 $cfg['datatbl']       = 'ADSUBLOCATIONS';//base data src [for updates & deletes]
 $cfg['form_width']    = 540;
 $cfg['form_height']   = 230;
 $cfg['window_width']    = 500;
 $cfg['window_height']   = 410;
 
 $cfg['pkcol']         = 'SUBLOCATIONCODE';//the primary key
 
 $cfg['tblbns']['chk_button_add']    = 1;
 $cfg['tblbns']['chk_button_edit']   = 1;
 $cfg['tblbns']['chk_button_delete'] = 1;
 $cfg['tblbns']['chk_button_import'] = 1;
 $cfg['tblbns']['chk_button_export'] = 1;
 

$cfg['columns']['wsnz7'] = array(
                      'dbcol'=>'LOCATIONCODE',
                      'title'=>'Location',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'ADLOCATIONS|LOCATIONCODE|LOCATIONNAME|LOCATIONCODE',
                      );
                      



                      
$cfg['columns']['2hvih'] = array(
                      'dbcol'=>'SUBLOCATIONCODE',
                      'title'=>'Code',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['jsnav'] = array(
                      'dbcol'=>'SUBLOCATIONNAME',
                      'title'=>'Name',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      $combogrid_array   = array();
	 
$combogrid_array['wsnz7']['columns']['LOCATIONCODE']  = array( 'field'=>'LOCATIONCODE', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['wsnz7']['columns']['LOCATIONNAME']  = array( 'field'=>'LOCATIONNAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['wsnz7']['source'] ='ADLOCATIONS';
  