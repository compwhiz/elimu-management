<?php
/**
* auto created config file for /modules/students/setup/houses
* @author kenmsh@gmail.com
* 
* @version 2.0
* @since 2016-01-29 09:34:48
*/

$scriptname  = @end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'Houses';//user-readable formart
 $cfg['appname']       = 'houses';//lower cased one word
 $cfg['datasrc']       = 'SAHOUSES';//where to get data
 $cfg['datatbl']       = 'SAHOUSES';//base data src [for updates & deletes]
 $cfg['form_width']    = 350;
 $cfg['form_height']   = 170;
 $cfg['window_width']    = 510;
 $cfg['window_height']   = 400;
 
 $cfg['pkcol']         = 'HOUSECODE';//the primary key
 
 $cfg['tblbns']['chk_button_add']    = 1;
 $cfg['tblbns']['chk_button_edit']   = 1;
 $cfg['tblbns']['chk_button_delete'] = 1;
 $cfg['tblbns']['chk_button_import'] = 1;
 $cfg['tblbns']['chk_button_export'] = 1;
 

$cfg['columns']['cwvgn'] = array(
                      'dbcol'=>'HOUSECODE',
                      'title'=>'House Code',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['a8bt7'] = array(
                      'dbcol'=>'HOUSENAME',
                      'title'=>'House Name',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      $combogrid_array   = array();
  