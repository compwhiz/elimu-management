<?php
/**
* auto created config file for modules/students/setup/forms
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-01-06 15:01:17
*/

$scriptname  = @end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'Forms';//user-readable formart
 $cfg['appname']       = 'forms';//lower cased one word
 $cfg['datasrc']       = 'SAFORMS';//where to get data
 $cfg['datatbl']       = 'SAFORMS';//base data src [for updates & deletes]
 $cfg['form_width']    = 400;
 $cfg['form_height']   = 250;
 $cfg['window_width']    = 500;
 $cfg['window_height']   = 400;
 
 $cfg['pkcol']         = 'FORMCODE';//the primary key
 
 $cfg['tblbns']['chk_button_add']    = 1;
 $cfg['tblbns']['chk_button_edit']   = 1;
 $cfg['tblbns']['chk_button_delete'] = 1;
 $cfg['tblbns']['chk_button_import'] = 0;
 $cfg['tblbns']['chk_button_export'] = 0;
 

$cfg['columns']['aywve'] = array(
                      'dbcol'=>'FORMCODE',
                      'title'=>'Code',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['ziu3e'] = array(
                      'dbcol'=>'FORMNAME',
                      'title'=>'Name',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['b1alh'] = array(
                      'dbcol'=>'STREAMS',
                      'title'=>'Streams',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'numberspinner',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['5ois9'] = array(
                      'dbcol'=>'SBCCODE',
                      'title'=>'Code',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>4,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'SASBC|SBCCODE|SBCNAME|SBCCODE',
                      );
                      



                      $combogrid_array   = array();
	 
$combogrid_array['5ois9']['columns']['SBCCODE']  = array( 'field'=>'SBCCODE', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['5ois9']['columns']['SBCNAME']  = array( 'field'=>'SBCNAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['5ois9']['source'] ='SASBC';
  