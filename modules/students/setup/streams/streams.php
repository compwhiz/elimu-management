<?php

/**
* auto created config file for modules/students/setup/streams
* @author kenmsh@gmail.com
* 
* @version 2.0
* @since 2016-01-16 14:50:10
*/

 final class streams {
 private $id;
 private $datasrc;
 private $primary_key;
	
 public function __construct(){
  global $cfg;
		
     $this->id           = filter_input(INPUT_POST , 'id');
     $this->datasrc      = valueof($cfg,'datasrc');
     $this->primary_key  = valueof($cfg,'pkcol');
 }
	
 public function save(){
  global $db,$cfg;
  
   $grid = new grid();
   return $grid->grid_save_row_simple();
 }
	
 public function remove(){
  global $db, $cfg;
  
   $grid = new grid();
  return $grid->grid_remove_row();
 }
	
 	
 public function export(){
  global $db, $cfg;
  
   $grid = new grid();
  return $grid->export();
 }
 	
 public function import(){
  global $db, $cfg;
  
   $grid = new grid();
  return $grid->import();
 }
 
}
