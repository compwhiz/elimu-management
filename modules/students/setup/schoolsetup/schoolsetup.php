<?php

  class schoolsetup {
	private $id;
	private $datasrc;
	private $primary_key;
	private $admno;
	
	public function __construct(){
		global $cfg;
		
		$this->id           = filter_input(INPUT_POST , 'id');
		$this->datasrc      = valueof($cfg,'datasrc');
		$this->primary_key  = valueof($cfg,'pkcol');
	}
	
	public function data(){
		global $db;
		
		$data = array();
		
        $school = $db->GetRow("SELECT * FROM VIEWSCHOOL WHERE ID=1");
        
		$data['id']    = valueof( $school, 'ID'); 
		$data['schoolcode']    = valueof( $school, 'SCHOOLCODE'); 
		$data['schoolname']    = valueof( $school, 'SCHOOLNAME'); 
		$data['moecode']    = valueof( $school, 'MOECODE'); 
		$data['tsccode']    = valueof( $school, 'TSCCODE'); 
		$data['kneccode']    = valueof( $school, 'KNECCODE'); 
		$data['telephone']    = valueof( $school, 'TELEPHONE'); 
		$data['mobilephone']    = valueof( $school, 'MOBILEPHONE'); 
		$data['postalcode']    = valueof( $school, 'POSTALCODE'); 
		$data['addresspost']    = valueof( $school, 'ADDRESSPOST'); 
		$data['addressphys']    = valueof( $school, 'ADDRESSPHYS'); 
		$data['email']    = valueof( $school, 'EMAIL'); 
		$data['website']    = valueof( $school, 'WEBSITE'); 
		$data['countycode']    = valueof( $school, 'COUNTYCODE'); 
		$data['countyname']    = valueof( $school, 'COUNTYNAME'); 
		$data['constituencycode']    = valueof( $school, 'CONSTITUENCYCODE'); 
		$data['constituencyname']    = valueof( $school, 'CONSTITUENCYNAME'); 
		$data['districtcode']    = valueof( $school, 'DISTRICTCODE'); 
		$data['districtname']    = valueof( $school, 'DISTRICTNAME'); 
		$data['divisioncode']    = valueof( $school, 'DIVISIONCODE'); 
		$data['divisionname']    = valueof( $school, 'DIVISIONNAME'); 
		$data['zonecode']    = valueof( $school, 'ZONECODE'); 
		$data['zonename']    = valueof( $school, 'ZONENAME'); 
		$data['locationcode']    = valueof( $school, 'LOCATIONCODE'); 
		$data['locationname']    = valueof( $school, 'LOCATIONNAME'); 
		$data['sublocationcode']    = valueof( $school, 'SUBLOCATIONCODE'); 
		$data['sublocationname']    = valueof( $school, 'SUBLOCATIONNAME'); 
		$data['enrolltypecode']    = valueof( $school, 'ENROLLTYPECODE'); 
		$data['enrolltypename']    = valueof( $school, 'ENROLLTYPENAME'); 
		$data['gendertypecode']    = valueof( $school, 'GENDERTYPECODE'); 
		$data['gendertypename']    = valueof( $school, 'GENDERTYPENAME'); 
		$data['catgcode']    = valueof( $school, 'CATGCODE'); 
		$data['catgname']    = valueof( $school, 'CATGNAME'); 
		$data['levelcode']    = valueof( $school, 'LEVELCODE'); 
		$data['levelname']    = valueof( $school, 'LEVELNAME'); 
		$data['sstatuscode']    = valueof( $school, 'SSTATUSCODE'); 
		$data['sstatusname']    = valueof( $school, 'SSTATUSNAME'); 

		$data['logo']          = valueof( $school, 'LOGO');
		$data['motto']         = valueof( $school, 'MOTTO');
		$data['vision']        = valueof( $school, 'VISION');
		$data['mission']       = valueof( $school, 'MISSION');

	    if(defined('MAKE_FIELDS_UNIQUE') && defined('MNUID')) {
		foreach($data as $k=>$v){
		  $field_id = "{$k}_".MNUID; 
		  unset($data[$k]);
		  $data[$field_id] = $v;
		}
	   }

		return json_encode($data);
		
	}
	
	public function upload_logo(){
		global $db;
		
		$fileid  = ui::fi('school_logo');
		
		if(!isset($_FILES[$fileid])){
		 return  json_response(0,'Logo not Uploaded');	
		}
		
		$name      = $_FILES[$fileid]['name'];
		$tmp_name  = $_FILES[$fileid]['tmp_name'];
		$name      = $_FILES[$fileid]['name'];
		$type      = $_FILES[$fileid]['type'];
		$info      = pathinfo($tmp_name);
		
		$logo    = ROOT.'public/images/school_logo.jpg';
		
		if(file_exists($logo)){
			if(is_readable($logo)){
				@unlink($logo);
			}
		}
		
		if(!move_uploaded_file($tmp_name, $logo)){
		 return  json_response(0,'Upload Failed');		
		}
		
        $photo_small = ROOT.'public/images/school_logo_resized.jpg';
        
        $image = new simpleimage();
        $image->load($logo);
        $image->resize(160,110);
        $image->save($photo_small);
        $image->save($photo_small);
        
        rename($photo_small, $logo);
        
        $logo_info  = pathinfo($logo);
        $basename   = valueof($logo_info,'basename');
        
        $db->Execute("UPDATE SASCHOOL SET LOGO='{$basename}' WHERE ID=1");
        
		return  json_response(1,'Logo Uploaded');	
		
	}
	
	public function logo(){
		global $db;
		
		$logo_name       = $db->GetOne("SELECT LOGO FROM SASCHOOL WHERE ID=1");
		$logo            = ROOT.'public/images/'.$logo_name;
		
		if(file_exists($logo) && is_readable($logo)){
		 $logo_info = pathinfo($logo);
		 $image_name =  valueof($logo_info,'basename');	
		 $rand = mt_rand();	
		 $web_img = '<img src="./public/images/'.$image_name.'?cache_reload='.$rand.'" border="0">';
		 $message = "OK";
		}else{
		 $web_img = '<img src="./public/images/logo-default.gif" border="0">';
		 $message = "NO PHOTO";
		}
		
		return json_encode( array(
	     'success' => 1,
	     'message' => $message,
	     'logo' => $web_img,
	    ));
	 
	}
	
	public function save(){
		global $db,$cfg;

    $user              = new user();		
    
    $school = new ADODB_Active_Record('SASCHOOL', array('ID'));
    $school->Load("ID=1");
    
    if(empty($school->_original)){
     $school->id           = generateID('SASCHOOL');
    }
      
    $school->schoolcode        =  filter_input(INPUT_POST, ui::fi('schoolcode'));
	$school->schoolname        =  filter_input(INPUT_POST, ui::fi('schoolname'));	
 	$school->moecode           =  filter_input(INPUT_POST, ui::fi('moecode'));
	$school->tsccode           =  filter_input(INPUT_POST, ui::fi('tsccode'));
	$school->kneccode          =  filter_input(INPUT_POST, ui::fi('kneccode'));
	$school->countycode        =  filter_input(INPUT_POST, ui::fi('countycode'));
	$school->constituencycode  =  filter_input(INPUT_POST, ui::fi('constituencycode'));
	$school->districtcode      =  filter_input(INPUT_POST, ui::fi('districtcode'));
	$school->divisioncode      =  filter_input(INPUT_POST, ui::fi('divisioncode'));
	$school->locationcode      =  filter_input(INPUT_POST, ui::fi('locationcode'));
	$school->sublocationcode   =  filter_input(INPUT_POST, ui::fi('sublocationcode'));
	$school->addresspost       =  filter_input(INPUT_POST, ui::fi('addresspost'));
	$school->addressphys       =  filter_input(INPUT_POST, ui::fi('addressphys'));
	$school->postalcode        =  filter_input(INPUT_POST, ui::fi('postalcode'));
	$school->telephone         =  filter_input(INPUT_POST, ui::fi('mobilephone'));
	$school->mobilephone       =  filter_input(INPUT_POST, ui::fi('mobilephone'));
	$school->email             =  filter_input(INPUT_POST, ui::fi('email' , FILTER_VALIDATE_EMAIL));
	$school->website           =  filter_input(INPUT_POST, ui::fi('website'));
	$school->motto             =  filter_input(INPUT_POST, ui::fi('motto'));
	$school->vision            =  filter_input(INPUT_POST, ui::fi('vision'));
	$school->mission           =  filter_input(INPUT_POST, ui::fi('mission'));
	$school->sstatuscode       =  filter_input(INPUT_POST, ui::fi('sstatuscode'));
	$school->enrolltypecode    =  filter_input(INPUT_POST, ui::fi('enrolltypecode'));
	$school->levelcode         =  filter_input(INPUT_POST, ui::fi('levelcode'));
	$school->gendertypecode    =  filter_input(INPUT_POST, ui::fi('gendertypecode'));
	$school->catgcode          =  filter_input(INPUT_POST, ui::fi('catgcode'));
	$school->audituser         =  $user->userid;
	$school->auditdate         =  date('Y-m-d'); 
	$school->audittime         =  time();
    
    if($school->Save()){
     return json_response(1,'record saved');
    }else{
     $error = $db->ErrorMsg();	
     return json_response(0,"save failed : {$error}");
    }

	}
	
	public function remove(){
		global $db, $cfg;
		json_response(0,'failed');
	}
	
}
