<?php

?>
<style>
td.label {   font-size: 12px; }
div#<?php echo ui::fi('logo'); ?> { cursor:pointer; border:1px solid #DEDEDE;min-width: 162px; min-height: 112px; max-width: 162px; max-height: 112px; }
</style>

	<form id="<?php echo ui::fi('ff'); ?>" method="post" novalidate>
	<div class="easyui-tabs" data-options="tabWidth:123" style="width:606px;height:350px">
		
		<div title="Identification" style="padding:5px">
	   
	    	<table cellpadding="2" cellspacing="0" width="100%">
	    		
	    		<tr>
	    		 <td width="125px">Registration Code:</td>
	    		 <td><?php echo ui::form_input( 'text', 'schoolcode', 10, '', '', '', 'data-options="required:true, validType:\'minLength[3]\'"', '', 'easyui-textbox' , ""); ?>
	    		 <td rowspan="7">
					 <div id="<?php echo ui::fi('logo'); ?>" title="click to replace logo" onclick="<?php echo $cfg['appname']; ?>.initiate_upload_logo();"><img src="./public/images/logo-default.gif" border="0"></div>
				 </td>
	    		</tr>
	    		
	    		<tr>
	    		 <td>School Name:</td>
	    		 <td><?php echo ui::form_input( 'text', 'schoolname', 20, '', '', '', 'data-options="required:true, validType:\'minLength[3]\'"', '', 'easyui-textbox' , ""); ?></td>
	    		</tr>
	    		
	    		<tr>
	    		 <td>MOE Code:</td>
	    		 <td><?php echo ui::form_input( 'text', 'moecode', 10, '', '', '', 'data-options="required:true, validType:\'minLength[3]\'"', '', 'easyui-textbox' , ""); ?></td>
	    		</tr>
	    		
	    		<tr>
	    		 <td>TSC Code:</td>
	    		 <td><?php echo ui::form_input( 'text', 'tsccode', 10, '', '', '', 'data-options="required:true, validType:\'minLength[3]\'"', '', 'easyui-textbox' , ""); ?></td>
	    		</tr>
	    		
	    		<tr>
	    		 <td>KNEC Code:</td>
	    		 <td><?php echo ui::form_input( 'text', 'kneccode', 10, '', '', '', 'data-options="required:true, validType:\'minLength[3]\'"', '', 'easyui-textbox' , ""); ?></td>
	    		</tr>
	    		
	    		<tr>
	    			<td>Mission:</td>
	    			<td><textarea id="<?php echo ui::fi('mission'); ?>" name="<?php echo ui::fi('mission'); ?>" cols="30" rows="2"></textarea></td>
	    		</tr>
	    		
	    		<tr>
	    			<td>Vision:</td>
	    			<td><textarea id="<?php echo ui::fi('vision'); ?>" name="<?php echo ui::fi('vision'); ?>"   cols="30" rows="2"></textarea></td>
	    		</tr>
	    		
	    		<tr>
	    			<td>Motto:</td>
	    			<td><textarea id="<?php echo ui::fi('motto'); ?>" name="<?php echo ui::fi('motto'); ?>"    cols="30" rows="2"></textarea></td>
	    		</tr>
	    		
	    	</table>
	
		</div>
		<div title="Location" style="padding:5px">
			
	    	<table cellpadding="2" cellspacing="0" width="100%">
	    	
	    	    <tr>
	    			<td>County:</td>
	    			<td>
	    				<?php 
//	    				 echo dropdown::form_select ('county','ADCOUNTIES','COUNTYCODE','COUNTYNAME','');  
	    				 echo ui::form_input('text','countycode','30','','','','style="width:250px;" ');  
	    				?>
	    			</td>
	    		</tr>
	    		
	    		<tr>
	    			<td>Constituency:</td>
	    			<td>
	    				<?php
//	    				 echo dropdown::form_select ('constituency','ADCONSTITUENCIES','CONSTITUENCYCODE','CONSTITUENCYNAME','','','','','CONSTITUENCYNAME');  
	    				 echo ui::form_input('text','constituencycode','30','','','','style="width:250px;" ');  
	    				 ?>
	    			</td>
	    		</tr>
	    		
	    		<tr>
	    			<td>District:</td>
	    			<td>
	    			 <?php 
//	    			 echo dropdown::form_select ('district','ADDISTRICTS','DISTRICTCODE','DISTRICTNAME','','','','','DISTRICTNAME');  
	    			 echo ui::form_input('text','districtcode','30','','','','style="width:250px;" ');  
	    			 ?>
	    			</td>
	    		</tr>
	    		
	    		<tr>
	    			<td width="180px">Division</td>
	    			<td>
	    			<?php 
//	    			echo dropdown::form_select ('division','ADDIVISIONS','DIVISIONCODE','DIVISIONNAME','','','','','DIVISIONNAME');  
	    			echo ui::form_input('text','divisioncode','30','','','','style="width:250px;" ');  
	    			?>
	    			</td>
	    		</tr>
	    		
	    		<tr>
	    			<td  width="180px">Location:</td>
	    			<td>
	    				<?php 
//	    				echo dropdown::form_select ('location','ADLOCATIONS','LOCATIONCODE','LOCATIONNAME','','','','','LOCATIONNAME');  
                         echo ui::form_input('text','locationcode','30','','','','style="width:250px;" ');  
	    				?>
	    			</td>
	    		</tr>
	    		
	    		<tr>
	    			<td>Sub Location:</td>
	    			<td>
	    				<?php
//	    				  echo dropdown::form_select ('sublocation','ADSUBLOCATIONS','SUBLOCATIONCODE','SUBLOCATIONNAME','','','','','SUBLOCATIONNAME');  
	    				  echo ui::form_input('text','sublocationcode','30','','','','style="width:250px;" ');  
	    				 ?>
	    			</td>
	    		</tr>
	    		
	    	</table>
	    	
		</div>
		
		<div title="Contacts" style="padding:5px">
			
	    	<table cellpadding="2" cellspacing="0" width="100%">
	    		
	    		<tr>
	    			<td>Physical Address:</td>
	    			<td><?php echo ui::form_input( 'text', 'addressphys', 20, '', '', '', 'data-options="required:true, validType:\'minLength[3]\'"', '', 'easyui-textbox' , ""); ?></td>
	    		</tr>
	    		
	    		<tr>
	    			<td>Telephone:</td>
	    			<td><?php echo ui::form_input( 'text', 'telephone', 15, '', '', '', 'data-options="required:true, validType:\'minLength[3]\'"', '', 'easyui-textbox' , ""); ?></td>
	    		</tr>
	    		
	    		<tr>
	    			<td>Mobile Phone:</td>
	    			<td><?php echo ui::form_input( 'text', 'mobilephone', 15, '', '', '', 'data-options="required:true, validType:\'minLength[3]\'"', '', 'easyui-textbox' , ""); ?></td>
	    		</tr>
	    		
	    		<tr>
	    			<td>Postal Code:</td>
	    			<td><?php echo ui::form_input( 'text', 'postalcode', 15, '', '', '', 'data-options="required:true, validType:\'minLength[3]\'"', '', 'easyui-textbox' , ""); ?></td>
	    		</tr>
	    		
	    		<tr>
	    			<td>Postal Address:</td>
	    			<td><?php echo ui::form_input( 'text', 'addresspost', 15, '', '', '', 'data-options="required:true, validType:\'minLength[3]\'"', '', 'easyui-textbox' , ""); ?></td>
	    		</tr>
	    		
	    		<tr>
	    			<td>Website:</td> 
	    			<td><?php echo ui::form_input( 'text', 'website', 15, '', '', '', 'data-options="required:true, validType:\'url\'" ', '', 'easyui-textbox' , ""); ?></td>
	    		</tr>
	    		
	    		<tr>
	    			<td>Email:</td>
	    			<td><?php echo ui::form_input( 'text', 'email', 15, '', '', '', ' data-options="required:true, validType:[\'email\',\'length[0,20]\']" ', '', 'easyui-textbox' , ""); ?></td>
	    		</tr>
	    		
	    		
	    	</table>
	    	
		</div>
		
		<div title="Characteristics" style="padding:5px">
		
		<table cellpadding="2" cellspacing="0" width="100%">
		
	     <tr>
	      <td>Status:</td>
	      <td>
	       <?php echo dropdown::form_select ('sstatuscode','ADSTATUSES','SSTATUSCODE','SSTATUSNAME','','','','','SSTATUSNAME');  ?>
	      </td>
	     </tr>
	    	
	     <tr>
	      <td>Enrollment Type:</td>
	      <td>
	       <?php echo dropdown::form_select ('enrolltypecode','ADENROLLTYPES','ENROLLTYPECODE','ENROLLTYPENAME','','','','','ENROLLTYPENAME');  ?>
	      </td>
	     </tr>
	    		
	     <tr>
	      <td>Level:</td>
	      <td>
	       <?php echo dropdown::form_select ('levelcode','ADLEVELS','LEVELCODE','LEVELNAME','','','','','LEVELNAME');  ?>
	      </td>
	     </tr>
	    		
	     <tr>
	      <td>Type:</td>
	      <td>
	       <?php echo dropdown::form_select ('gendertypecode','ADGENDERTYPES','GENDERTYPECODE','GENDERTYPENAME','','','','','GENDERTYPENAME');  ?>
	      </td>
	     </tr>
	    		
	     <tr>
	      <td>Category:</td>
	      <td>
	       <?php echo dropdown::form_select ('catgcode','ADCATEGORIES','CATGCODE','CATGNAME','','','','','CATGNAME');  ?>
	      </td>
	     </tr>
	    	
	    </table> 
		</div>
		
		
	</div>
   </form>
   
	<div style="margin:10px 0;"></div>
	<table cellpadding="2" cellspacing="0" width="300px">
	 <tr>
	 <td><a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-save'" onclick="<?php echo $cfg['appname']; ?>.saveSchool();">Save</a></td>
	 <td><a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-reload'" onclick="<?php echo $cfg['appname']; ?>.clearForm()">Reset</a></td>
	 </tr>
	</table>
	
 
 <div id="<?php echo ui::fi('dlg_upload'); ?>" class="easyui-dialog" title="Logo Upload" style="width:300px;height:120px;padding:5px" closed="true" >
 <form id="ff_logo<?php echo MNUID; ?>" class="form-horizontal" action="./" method="POST" enctype="multipart/form-data">
  <input  type="file" name="<?php echo ui::fi('school_logo'); ?>" id="<?php echo ui::fi('school_logo'); ?>" onchange="<?php echo $cfg['appname']; ?>.do_upload_logo()">
 </form>
 </div>
 
	<script>
		
	  $( document ).ready(function() {
       setTimeout("<?php echo $cfg['appname']; ?>.loadData();",1000);
      });

	  
       var <?php echo $cfg['appname']; ?> = {
		clearForm:function (){
			$('#<?php echo ui::fi('ff'); ?>').form('clear');
			$('#<?php echo ui::fi('logo'); ?>').html('<img src="./public/images/logo-default.gif" border="0">');
			<?php echo $cfg['appname']; ?>.loadData();
		},
		loadData:function (){
		 $('#<?php echo ui::fi('ff'); ?>').form('load', './endpoints/crud/?modvars=<?php echo $vars; ?>&function=data&id=1&'+$('#<?php echo ui::fi('ff'); ?>').serialize());
		  <?php echo $cfg['appname']; ?>.load_logo();
		},
		load_logo :function (){
		   var rdata = 'modvars=<?php echo $vars; ?>&function=logo';	
		   $.post('./endpoints/crud/', rdata, function(data) {
		   if (data.success === 1 && (typeof data.logo!=undefined)) {
			$('#<?php echo ui::fi('logo'); ?>').html(data.logo);
		   }else{
			$.messager.alert('Error',data.message,'error');
		   }
		  }, "json");
		},
		initiate_upload_logo:function (){
	   	$.messager.confirm('Confirm','Do you want to upload a logo Now?',function(r){
         if (r){
	  	  $('#<?php echo ui::fi('dlg_upload'); ?>').dialog('open');
         }
	   	});
	   },
	   do_upload_logo:function(){
		var data = new FormData($('#ff_logo<?php echo MNUID; ?>')[0]);
		$.ajax({
        url : './endpoints/crud/?modvars=<?php echo $vars; ?>&function=upload_logo',
        type: 'POST',
        data: data,
        cache: false,
        async:'false',
        dataType: 'json',
        processData: false,
        contentType: false,
        success: function(data, textStatus, jqXHR){
         <?php echo $cfg['appname']; ?>.load_logo();
         $('#<?php echo ui::fi('dlg_upload'); ?>').dialog('close');
        },
        error: function(jqXHR, textStatus, errorThrown){

        }
        });
       },
	   saveSchool:function (){
			var validate =  $('#<?php echo ui::fi('ff'); ?>').form('validate');
			if(!validate){
			  $.messager.alert('Error','Fill In All Required Fields','error');
			 return;	
			}else{
			 $.messager.progress();
			 var fdata = $('#<?php echo ui::fi('ff'); ?>').serialize() +'&modvars=<?php echo $vars; ?>&function=save';	
		     $.post('./endpoints/crud/', fdata, function(data) {
		     $.messager.progress('close');
             if (data.success === 1) {
                $.messager.show({title: 'Success',msg: data.message});
             } else {
				$.messager.alert('Error',data.message,'error');
             }
           }, "json");
		 }
		},
	  }
		
	<?php
       
       if(isset($combogrid_array)){
       	if(sizeof($combogrid_array)>0){
       		foreach ($combogrid_array as $ac_source=>$details){
       		  echo ui::ComboGrid($combogrid_array,$ac_source);	
       		}
       	}
       }
	?>
		
	
	</script>
	
