<?php

/**
* auto created config file for modules/students/setup/school
* @author coderX
* @todo by-me-beer
* @version 1.0
* @since 2014-09-29 23:35:58
*/

if(!defined('MAKE_FIELDS_UNIQUE')){
 define('MAKE_FIELDS_UNIQUE' , true);
}

$scriptname  = @end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'School';//user-readable formart
 $cfg['appname']       = 'school';//lower cased one word
 $cfg['datasrc']       = 'SASCHOOL';//where to get data
 $cfg['datatbl']       = 'SASCHOOL';//base data src [for updates & deletes]
 $cfg['form_width']    = 400;
 $cfg['form_height']   = 200;
 
 $cfg['pkcol']         = 'ID';//the primary key
 
 $cfg['tblbns']['chk_button_add']    = 1;
 $cfg['tblbns']['chk_button_edit']   = 1;
 $cfg['tblbns']['chk_button_delete'] = 1;
 $cfg['tblbns']['chk_button_import'] = 0;
 $cfg['tblbns']['chk_button_export'] = 0;
 

$cfg['columns']['jrroz'] = array(
                      'dbcol'=>'SCHCODE',
                      'title'=>'Code',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      
$cfg['columns']['qdarw'] = array(
                      'dbcol'=>'SCHNAME',
                      'title'=>'Name',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      
$cfg['columns']['ry7qo'] = array(
                      'dbcol'=>'SCHADDRESS',
                      'title'=>'Address',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      
$cfg['columns']['ysee7'] = array(
                      'dbcol'=>'SCHCOUNTY',
                      'title'=>'County',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>0,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      
$cfg['columns']['xwsj'] = array(
                      'dbcol'=>'SCHTEL',
                      'title'=>'Telephone',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>0,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      
$cfg['columns']['ntzur'] = array(
                      'dbcol'=>'SCHEMAIL',
                      'title'=>'Email',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>0,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      
$cfg['columns']['sby4s'] = array(
                      'dbcol'=>'SCHWEBSITE',
                      'title'=>'Website',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>0,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      
$cfg['columns']['xatrz'] = array(
                      'dbcol'=>'SCHMOTTO',
                      'title'=>'Motto',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>0,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      
$cfg['columns']['xphu0'] = array(
                      'dbcol'=>'SCHVISION',
                      'title'=>'Vision',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>0,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      
$cfg['columns']['ilqrk'] = array(
                      'dbcol'=>'SCHMISION',
                      'title'=>'Mision',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>0,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      
$cfg['columns']['ermph'] = array(
                      'dbcol'=>'SCHLOGO',
                      'title'=>'Logo',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>0,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      
                      
$combogrid_array   = array();

$combogrid_array['countycode']['columns']['COUNTYCODE']      = array( 'field'=>'COUNTYCODE', 'title'=>'Code', 'width'=> 100 , 'isIdField' => true );
$combogrid_array['countycode']['columns']['COUNTYNAME']        = array( 'field'=>'COUNTYNAME', 'title'=>'Name', 'width'=> 100, 'isTextField'=>true);
$combogrid_array['countycode']['source'] ='ADCOUNTIES';

$combogrid_array['constituencycode']['columns']['CONSTITUENCYCODE']      = array( 'field'=>'CONSTITUENCYCODE', 'title'=>'Code', 'width'=> 100 , 'isIdField' => true );
$combogrid_array['constituencycode']['columns']['CONSTITUENCYNAME']    = array( 'field'=>'CONSTITUENCYNAME', 'title'=>'Name', 'width'=> 200, 'isTextField'=>true);
$combogrid_array['constituencycode']['source'] ='ADCONSTITUENCIES';

$combogrid_array['districtcode']['columns']['DISTRICTCODE']      = array( 'field'=>'DISTRICTCODE', 'title'=>'Code', 'width'=> 100 , 'isIdField' => true );
$combogrid_array['districtcode']['columns']['DISTRICTNAME']    = array( 'field'=>'DISTRICTNAME', 'title'=>'Name', 'width'=> 200, 'isTextField'=>true);
$combogrid_array['districtcode']['source'] ='ADDISTRICTS';

$combogrid_array['divisioncode']['columns']['DIVISIONCODE']      = array( 'field'=>'DISTRICTCODE', 'title'=>'Code', 'width'=> 100 , 'isIdField' => true );
$combogrid_array['divisioncode']['columns']['DIVISIONNAME']    = array( 'field'=>'DIVISIONNAME', 'title'=>'Name', 'width'=> 200, 'isTextField'=>true);
$combogrid_array['divisioncode']['source'] ='ADDIVISIONS';

$combogrid_array['locationcode']['columns']['LOCATIONCODE']      = array( 'field'=>'LOCATIONCODE', 'title'=>'Code', 'width'=> 100 , 'isIdField' => true );
$combogrid_array['locationcode']['columns']['LOCATIONNAME']    = array( 'field'=>'LOCATIONNAME', 'title'=>'Name', 'width'=> 200, 'isTextField'=>true);
$combogrid_array['locationcode']['source'] ='ADLOCATIONS';

$combogrid_array['sublocationcode']['columns']['SUBLOCATIONCODE']      = array( 'field'=>'SUBLOCATIONCODE', 'title'=>'Code', 'width'=> 100 , 'isIdField' => true );
$combogrid_array['sublocationcode']['columns']['SUBLOCATIONNAME']    = array( 'field'=>'SUBLOCATIONNAME', 'title'=>'Name', 'width'=> 200, 'isTextField'=>true);
$combogrid_array['sublocationcode']['source'] ='ADSUBLOCATIONS';

                      
