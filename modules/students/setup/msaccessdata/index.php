<?php
require_once('cfg.php');

$class     = CLASSFILE;
$classfile = CLASSFILE.'.php';

require_once("{$classfile}");	

 if(!class_exists($class)){
 	die('class--' .$classfile. 'not found');
 }
 
 $_class = new $class();
 $grid   = new grid();
 
 $module = array();
 $module['url']    =  DIR;
 $module['class']  =  $class;
 $module_packed    =  packvars($module);
 
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta charset="UTF-8">
	<title>DataImport</title>
	
	<link rel="stylesheet" type="text/css" href="../../public/css/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="../../public/css/themes/icon.css">
	<script type="text/javascript" src="../../public/js/jquery.min.js"></script>
	<script type="text/javascript" src="../../public/js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="../../public/js/ui.js"></script>
	
</head>
<body>
	<script>
	
	</script>
   <div class="easyui-panel" title="Data Import from MS Access Database" style="width:600px;height:450px">
    <div style="padding:10px">
	<form id="frmData" method="post" novalidate>
		
	    	<table cellpadding="2" cellspacing="0" width="100%">
	    	
				<tr>
	    			<td>Access Db Path</td>
	    			<td>
	    			  <input type="text" id="db_path" name="db_path" value="c:\school.db" size="50"> 
	    			</td>
	    		</tr>
	    		
	    	
	    		<tr>
	    			<td><input type="checkbox" id="copy_classes" name="copy_classes" value="1"> <label for="copy_classes">copy classes</label></td>
	    			<td>
	    			  <div id="prg_copy_classes" class="easyui-progressbar" data-options="value:0" style="width:400px;"></div>
	    			</td>
	    		</tr>
	    		
	    		<tr>
	    			<td><input type="checkbox" id="copy_teachers" name="copy_teachers" value="1"> <label for="copy_teachers">copy teachers</label></td>
	    			<td>
	    			  <div id="prg_copy_teachers" class="easyui-progressbar" data-options="value:0" style="width:400px;"></div>
	    			</td>
	    		</tr>
	    		
	    		<tr>
	    			<td><input type="checkbox" id="copy_students" name="copy_students" value="1"> <label for="copy_students">copy students</label></td>
	    			<td>
	    			  <div id="prg_copy_students" class="easyui-progressbar" data-options="value:0" style="width:400px;"></div>
	    			</td>
	    		</tr>
	    		
	    		<tr>
	    			<td><input type="checkbox" id="copy_student_houses" name="copy_student_houses" value="1"> <label for="copy_student_houses">student houses</label></td>
	    			<td>
	    			  <div id="prg_copy_student_houses" class="easyui-progressbar" data-options="value:0" style="width:400px;"></div>
	    			</td>
	    		</tr>
	    		
	    		<tr>
	    			<td><input type="checkbox" id="copy_exam" name="copy_exam" value="1"> <label for="copy_exam">copy exam</label></td>
	    			<td>
	    			  <div id="prg_copy_exam" class="easyui-progressbar" data-options="value:0" style="width:400px;"></div>
	    			</td>
	    		</tr>
	    		
	    		<tr>
	    			<td><input type="checkbox" id="copy_cats" name="copy_cats" value="1"> <label for="copy_cats">copy cats</label></td>
	    			<td>
	    			  <div id="prg_copy_cats" class="easyui-progressbar" data-options="value:0" style="width:400px;"></div>
	    			</td>
	    		</tr>
	    		
	    		<tr>
	    			<td>&nbsp;</td>
	    			<td>
	    			 <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-save'" onclick="import_data();">Import Data</a>
	    			</td>
	    		</tr>
	    		
	    		<tr>
	    			<td colspan="2"><div id="div_grade"></div></td>
	    		</tr>
	    		
	    	</table>
 
     </form>
     <iframe name="i" id="i" frameborder="0" height="0px" width="100%" src="" scrolling="no"></iframe>
    </div>
   </div>
	
	<style scoped="scoped">
		.textbox{
			height:20px;
			margin:0;
			padding:0 2px;
			box-sizing:content-box;
		}
	</style>
	<script>
		
		function clearForm(){
			$('#frmFP').form('clear');
		}
			
		function import_data(){
		    $('#prg_copy_classes').progressbar({ value: 0 });
			$('#prg_copy_teachers').progressbar({ value: 0 });
			$('#prg_copy_students').progressbar({ value: 0 });
			$('#prg_copy_student_houses').progressbar({ value: 0 });
			$('#prg_copy_exam').progressbar({ value: 0 });
			$('#prg_copy_cats').progressbar({ value: 0 });
			
            var fdata = $('#frmData').serialize()  + '&module=<?php echo $module_packed; ?>&function=init';
            $('#i').attr('src', '../../modules/students/setup/msaccessdata/import.php?module=<?php echo $module_packed; ?>&'+fdata);
		}
		
			
	</script>
	
</body>
</html>
