<?php

require_once('../../../../init.php');
require_once('../../../../config/db.php');

/*
echo '<pre>';
 print_r($_GET);
echo '</pre>';
*/

$function = filter_input(INPUT_GET , 'function');
$db_path = filter_input(INPUT_GET , 'db_path');
$copy_classes = filter_input(INPUT_GET , 'copy_classes');
$copy_teachers = filter_input(INPUT_GET , 'copy_teachers');
$copy_students = filter_input(INPUT_GET , 'copy_students');
$copy_student_houses = filter_input(INPUT_GET , 'copy_student_houses');
$copy_student_subjects = filter_input(INPUT_GET , 'copy_student_subjects');
$copy_exam = filter_input(INPUT_GET , 'copy_exam');
$copy_cats = filter_input(INPUT_GET , 'copy_cats');

$db_path = str_replace('\\','\\\\',$db_path);

if( (!is_file($db_path))){
echo "
<script>
 parent.$.messager.alert('Error','{$db_path} is not a valid MS-Access Database','error');
</script>
		";	
}


if( (is_file($db_path) && !is_readable($db_path))){
echo "
<script>
 parent.$.messager.alert('Error','{$db_path} is not readable or does not exist','error');
</script>
		";	
}

try {
$source = ADONewConnection('access');
$dsn = "Driver={Microsoft Access Driver (*.mdb)};Dbq={$db_path};Uid=Admin;Pwd=1111985;";

 if(!$source->Connect($dsn)){
	echo "
	<script>
	 parent.$.messager.alert('Error IMP00001:','{$db->ErrorMsg()}','error');
	</script>
	";	
 }

}catch(Exemption $e){
	echo "
	<script>
	 parent.$.messager.alert('Error IMP00002:','{$e->getMessage()}','error');
	</script>
	";	
 }

 //var_dump($source);

if(!$source){
	echo "
	<script>
	 parent.$.messager.alert('Error IMP00003:','Unable to connect to access db using dsn={$dsn}','error');
	</script>
	";	
}

$source->SetFetchMode(ADODB_FETCH_ASSOC);	
ADODB_Active_Record::SetDatabaseAdapter( $db );

$source->debug=0;
$db->debug=0;
$null = null;

if($copy_classes==1){
	copy_classes();
}

if($copy_teachers==1){
	copy_teachers();
}

if($copy_students==1){
copy_students();
}

if($copy_student_houses==1){
copy_student_houses();
}

if($copy_student_subjects==1){
//copy_student_subjects();
}

if($copy_exam==1){
copy_exam();
}

if($copy_cats==1){
copy_cats();
}
  //classes
 function copy_classes() {
 global $source,$db;
 
 $db->Execute("DELETE FROM SASTREAMS");
 $data = $source->Execute("SELECT FORM,CLASS FROM CLASSES ORDER BY FORM");
  if(($data)){
  	
  	 $RecordCount = $data->RecordCount();
  	 $count=1;
  
  	  if($RecordCount>0){
        while (!$data->EOF){
        
            $formcode   = isset($data->fields['FORM']) ? $data->fields['FORM'] : '';
            $streamcode = isset($data->fields['CLASS']) ? $data->fields['CLASS'] : '';
            
            $record = new ADODB_Active_Record('SASTREAMS',array('STREAMCODE'));
        	$record->Load("STREAMCODE='{$streamcode}'");
        	
			if(empty($record->_original)){
			 $record->id          = generateID($record->_tableat, 'ID', $db);;
			 $record->formcode    = $formcode;
			 $record->streamcode  = $streamcode;
			 $record->streamname  = $streamcode;
			}
		
			 $record->Save();
        	
        ++$count;
            
		$data->MoveNext();
		
		$progress= ($count/$RecordCount)*100;
	
		sleep(.7);
		echo "
		<script>
          parent.$('#prg_copy_classes').progressbar({
            value: {$progress}
            });
</script>
		";
        }
  	  }
  	}
  }	

 //teachers
 function copy_teachers(){
 global $source,$db;
 
 $db->Execute("DELETE FROM TEACHERS ");
 $db->Execute("DELETE FROM USERS WHERE UPPER(USERID)!='ADMIN' ");
 $data = $source->Execute("SELECT * FROM TEACHERS where TEACHERNO is not null order by TEACHERNO ");
  if(($data)){
  	  
  	 $RecordCount = $data->RecordCount();
  	 $count=1;
  
      //s echo "\$RecordCount={$RecordCount}<br>";
  	  if($RecordCount>0){
        while (!$data->EOF) {
      
        $id          = isset($data->fields['ID']) ? $data->fields['ID'] : '';
        $teacherno   = isset($data->fields['TEACHERNO']) ? $data->fields['TEACHERNO'] : '';
        $name        = isset($data->fields['NAME']) ? $data->fields['NAME'] : '';
        $initials    = isset($data->fields['INITIALS']) ? $data->fields['INITIALS'] : '';
       
        if(!empty($teacherno)){
            $teacher = new ADODB_Active_Record('SATEACHERS',array('TCHNO'));
        	$teacher->Load("TCHNO='{$teacherno}'");
			if(empty($teacher->_original)){
			 $teacher->id       = $id;
			 $teacher->tchno    = $teacherno;
			 $teacher->username = $name;
			 $teacher->password = md5($teacherno);
			 $teacher->dob = date('Y-m-d');		
			 $teacher->doe = date('Y-m-d');		
			 $teacher->address = 'PO BOX';		
			 $teacher->postalcode = '0';		
			 $teacher->deptcode = 'NA';	
			 $teacher->sltcode = 'NA';	
			 $teacher->deptcode = 'NA';	
			 $teacher->gndcode = 'NA';	
			 $teacher->mstcode = 'NA';	
			 $teacher->residence = 'NA';	
			 $teacher->email = 'teacher@school.ac.ke';	
			 $teacher->tqfcode = 'NA';	
			 $teacher->rlgcode = 'NA';	
			 $teacher->mobilephone = '07';	
			 $teacher->nationalid = '0';	
			 $teacher->audituser   = 'import';
			 $teacher->auditdate   = date("Y-m-d");
			 $teacher->audittime   = time();
			
			}
			
			 $teacher->fullname  = $name;		
			 $teacher->initials  = $initials;		
				
			 if($teacher->Save()){
			  $sysuser   = createNew($teacher->tchno, $teacher->fullname,  $teacher->deptcode,$teacher->email);
			  ++$count;
			 }
			
        }
        	
		$data->MoveNext();
		
		$progress= ($count/$RecordCount)*100;
	
	    //echo "\$progress= {$count} {$progress}<br>";
		
		sleep(.7);
		echo "
		<script>
          parent.$('#prg_copy_teachers').progressbar({
            value: {$progress}
            });
</script>
		";
		
        }
  	  }
  	}
  }	

//students
 function copy_students(){
 global $source,$db;
 
 $data = $source->Execute("SELECT * FROM STUDENTS  ORDER BY ADMISSIONNO");
  if(isset($data)){
  	 
  	 $RecordCount = $data->RecordCount();
  	 $count=1;
  
  	  if($RecordCount>0){
        while (!$data->EOF){
        	
        	//print_pre($data->fields);
        	
        	$name          = isset($data->fields['NAME']) ? $data->fields['NAME'] : '';
        	$admissionno   = isset($data->fields['ADMISSIONNO']) ? $data->fields['ADMISSIONNO'] : '';
        	$class         = isset($data->fields['CLASS']) ? $data->fields['CLASS'] : '';
        	$form          = isset($data->fields['FORM']) ? $data->fields['FORM'] : '';
        	$kcpe          = isset($data->fields['KCPE']) ? $data->fields['KCPE'] : '';
        	$kcpemean      = isset($data->fields['KCPEMEAN']) ? $data->fields['KCPEMEAN'] : '';
        	$kcpegrade     = isset($data->fields['KCPEGRADE']) ? $data->fields['KCPEGRADE'] : '';
        	$kcpepoint     = isset($data->fields['KCPEPOINT']) ? $data->fields['KCPEPOINT'] : '';
        	$gender        = isset($data->fields['GENDER']) ? $data->fields['GENDER'] : '';
        	$house         = isset($data->fields['HOUSE']) ? $data->fields['HOUSE'] : '';
        	$mobileno      = isset($data->fields['MOBILENO']) ? $data->fields['MOBILENO'] : '';
        	$fee           = isset($data->fields['FEE']) ? $data->fields['FEE'] : '';
        	$mumno         = isset($data->fields['MUMNO']) ? $data->fields['MUMNO'] : '';
        	$indexno       = 0;
        	
        	$sastudents = new ADODB_Active_Record('SASTUDENTS',array('ADMNO'));
        	$sastudents->Load("ADMNO='{$admissionno}'");
			if(empty($sastudents->_original)){
				$sastudents->id     = generateID($sastudents->_tableat, 'ID', $db);
				$sastudents->admno  = $admissionno;
				$sastudents->password    = md5($admissionno);
				$sastudents->rlgcode     = 'C';
				$sastudents->address     = 'P.O. BOX';
				$sastudents->admdate     = date('Y-m-d');
			}
			
			$sastudents->fullname    = $name;
			$sastudents->housecode   = $house;
			$sastudents->streamcode  = $class;
			
			$sastudents->kcpegrade   = $kcpegrade;
			$sastudents->kcpemean    = $kcpemean;
			$sastudents->kcpepoint   = $kcpepoint;
			
			$sastudents->indexno     = $indexno;
			$sastudents->username    = $admissionno;
			$sastudents->mobilephone = $mobileno;
			$sastudents->gndcode     = $gender;
			
			$sastudents->residence   = null;
			$sastudents->postalcode  = 0;
			$sastudents->email       = 'student@school.ac.ke';
			$sastudents->audituser   = 'import';
			$sastudents->auditdate   = date("Y-m-d");
			$sastudents->audittime   = time();
			
            //print_pre($sastudents);exit();
            if($sastudents->Save()){
			  ++$count;
			}
//exit();
		$data->MoveNext();
		
		$progress = round(($count/$RecordCount)*100,0);
	
		sleep(.7);
		echo "
		<script>
          parent.$('#prg_copy_students').progressbar({
            value: {$progress}
            });
</script>
		";
		
        }
  	  }
  	}
}	

 function copy_student_houses(){
 global $source,$db;
 	
 //houses
 //$source->debug=1;
 $data = $source->Execute("SELECT ADMISSIONNO, HOUSES FROM HOUSES ORDER BY ADMISSIONNO");
  if($data) {
  	  $RecordCount = $data->RecordCount();
  	  $count=1;
  
  	  if($RecordCount>0){
  	  	$distinct_houses = array();
        while (!$data->EOF){
        	
        $admissionno  = isset($data->fields['ADMISSIONNO']) ? $data->fields['ADMISSIONNO'] : '';
        $houseno      = isset($data->fields['HOUSES']) ? $data->fields['HOUSES'] : '';
        
        if(!empty($houseno)){
        if(!array_key_exists($houseno,$distinct_houses)){
        $distinct_houses[$houseno] = $houseno; 	
        }
        
        $db->Execute("UPDATE SASTUDENTS SET HOUSECODE='{$houseno}' WHERE ADMNO='{$admissionno}'");
        }
        	
		$data->MoveNext();
		
		++$count;
		
		$progress= round(($count/$RecordCount)*100,0);
	
		sleep(.7);
		echo "
		<script>
          parent.$('#prg_copy_classes').progressbar({
            value: {$progress}
            });
</script>
		";
		
        }
  	  }
  	  
  	  if(count($distinct_houses)){
	  $RecordCount = sizeof($distinct_houses);
  	  $count=1;
  
  	  	foreach ($distinct_houses as $houseno=>$housename){
  	  	    $house = new ADODB_Active_Record('SAHOUSES',array('HSECODE'));
        	$house->Load("HSECODE='{$houseno}'");
			if(empty($house->_original)){
			 $house->id      = generateID($house->_tableat, 'ID', $db);
			 $house->hsecode = $houseno;
			}
			 $house->hsename = $housename;	

             if($house->Save()){
			  ++$count;
			 }
			 		
		$progress= round(($count/$RecordCount)*100,0);
	
		sleep(.7);
		echo "
		<script>
          parent.$('#prg_copy_student_houses').progressbar({
            value: {$progress}
            });
</script>
		";
		
  	  	}
  	  }
  	}else{
	echo "
<script>
 parent.$.messager.alert('Error','An Error Occured','error');
</script>
		";
	}
  	
  }
 
 function copy_student_subjects(){
 global $source,$db;
   	 // $source->debug=1;
 //student subjects
 $data = $source->Execute("SELECT * FROM STUDENTSUBJECTS order by ADMISSIONNO, YEAR ASC, FORM ASC");
  if($data){
  	  
  	 $RecordCount = $data->RecordCount();
  	 $count=1;
  
  	  if($RecordCount>0){
  	  	$distinct_houses = array();
        while (!$data->EOF){
      
        $year          = valueof($data->fields,'YEAR');
        $termcode      = valueof($data->fields,'TERM',1);
        $admissionno   = valueof($data->fields,'ADMISSIONNO');
        $subjectcode   = valueof($data->fields,'CODE');
        $streamcode    = valueof($data->fields,'CLASS');
        $form          = valueof($data->fields,'FORM');
        
        if(!empty($subjectcode)){
            $subject = new ADODB_Active_Record('SASTUDSUB',array('ADMNO','SUBJECTCODE','FORMCODE','STREAMCODE','YEARCODE','TERMCODE'));
        	$subject->Load("
        	ADMNO='{$admissionno}'
        	AND SUBJECTCODE='{$subjectcode}'
        	AND FORMCODE='{$form}'
        	AND STREAMCODE='{$streamcode}'
        	AND YEARCODE='{$year}'
        	AND TERMCODE='{$termcode}'
        	");
        	
			if(empty($subject->_original)){
			 $subject->id           = generateID($subject->_tableat, 'ID', $db);
			 $subject->admno        = $admissionno;
			 $subject->subjectcode  = $subjectcode;
			 $subject->formcode     = $form;
			 $subject->streamcode   = $streamcode;
			 $subject->yearcode     = $year;
			 $subject->termcode     = $termcode;
			
			}
			
			 if($subject->Save()){
			  ++$count;
			 }

        }
        	
		$data->MoveNext();
		
		$progress= round(($count/$RecordCount)*100,0);
	
		sleep(.7);
		echo "
		<script>
          parent.$('#prg_student_subjects').progressbar({
            value: {$progress}
            });
</script>
		";
		
        }
  	  }
  	}else{
	echo "
<script>
 parent.$.messager.alert('Error',$db->ErrorMsg(),'error');
</script>
		";
	}
	
}  	
	
 function copy_exam() {
 global $source,$db;
 

 //student exam
// $data = $source->Execute("select * from ANALYSIS WHERE ADMISSIONNO='7008' order by ADMISSIONNO, YEAR,TERM");
 $data = $source->Execute("select * from ANALYSIS  order by ADMISSIONNO, YEAR,TERM");
 //$subjects = a
 
 $subjects = $db->GetAssoc("select CODEOFFC,SUBJECTCODE from SASUBJECTS");
 
 $subjects = array(
'AGR'       => 'AGR',
'BIO'       => 'BIO',
'BUS'       => 'BUS',
'CHM'       => 'CHM',
'COM'       => 'COM',
'CRE'       => 'CRE',
'DRA'       => 'DRA',
'ENG'       => 'ENG',
'FRE'       => 'FRE',
'GEO'       => 'GEO',
'HIS'       => 'HIS',
'KIS'       => 'KIS',
'MAT'       => 'MAT',
'MUS'       => 'MUS',
'PHY'       => 'PHY',
'HOM'       => 'HOM' 
 );
 
 $db->debug=0;
 
 $points = $db->GetAssoc("select GRADECODE ,POINTS from SAKCPEGRADES");
 $maxid_now  = $db->GetOne("SELECT MAX(ID) AS RID FROM SASTUDSUB  ");
 $maxid  =  $maxid_now>1 ? $maxid_now+1 : 1;
 //var_dump($maxid);exit;
 //print_pre($subjects);
// print_pre($points);
 
  if(isset($data)){
  	  	  
  	 $RecordCount = $data->RecordCount();
  	 $count=1;
  
  	  if($RecordCount>0){
  	  	$row_data = array();
        while (!$data->EOF){
      
        //	print_pre($data->fields);
        	
        	$year          = valueof($data->fields,'YEAR');
            $termcode      = valueof($data->fields,'TERM',1);
            $admissionno   = valueof($data->fields,'ADMISSIONNO');
            $form          = valueof($data->fields,'FORM');
            $streamcode    = valueof($data->fields,'CLASS');
            $posclass      = valueof($data->fields,'POSCLASS');
            $position      = valueof($data->fields,'POSITION');
            $meangrade     = valueof($data->fields,'MEANGRADE');
            
         /*
            echo "\$admissionno={$admissionno} <br>";
            echo "\$form={$form} <br>";
            echo "\$streamcode={$streamcode} <br>";
            echo "\$year={$year} <br>";
            echo "\$termcode={$termcode} <br>";
            */
//            exit();
            
        	foreach ($data->fields as $col=>$val){
        		$col_left3_chars = substr($col,0,3);
				
				//echo "\$col={$col} <br>";
				//echo "\$col_left3_chars={$col_left3_chars} <br>";
				//print_pre($subjects);exit;
				
        		if(array_key_exists($col_left3_chars, $subjects)){
//        		echo "\$col={$col} <br>";
//        		echo "\$col_left3_chars={$col_left3_chars} <br>";
//        		echo "\$val={$val} <br>";
        		 $row_data[$col_left3_chars][$col] = $val;
        		}
        	}
        	
//        	print_pre($row_data['ENG']);
        	
        	foreach ($row_data as $subjectcode=>$subject_vars){
        	// echo "\$subjectcode={$subjectcode} <br>";
        	// print_pre($subject_vars);
        	 
        	 $cat1 = null;
        	 $cat2 = null;
        	 $exam = null;
        	 $total = null;
        	 $gradecode = null;
        	 $total_vars_arr = null;
        	 
        	 foreach ($subject_vars as $subjectcode_mixed => $subject_var){
        	 	//echo "\$subjectcode_mixed={$subjectcode_mixed} <br>";
        	 	$subjectcode_mixed_len = strlen($subjectcode_mixed);
        	 	//echo "\$subjectcode_mixed_len={$subjectcode_mixed_len} <br>";
        	 	$subjectcode_last_char = substr($subjectcode_mixed, -1,1 );
        	 	
        	 //	echo "\$subjectcode_last_char={$subjectcode_last_char} <br>";
                
                switch ($subjectcode_last_char){
                	case 'E':
                		$total_vars_arr =	split(' ',$subject_var);
                		$total          =	valueof($total_vars_arr, 0, '','trim');
//                		$gradecode      =	$total_vars_arr[1];
                		$gradecode      =	valueof($total_vars_arr, 1, '','trim');
                	break;
                	case 'C':
                		$cat1 =	 $subject_var;
                	break;
                	case 'X':
                		$exam =	 $subject_var;
                	break;
                	case '2':
                		$cat2 =	 $subject_var;
                	break;
                }
        	 }
        	 /*
        	    echo "\$cat1={$cat1} <br>";
                echo "\$cat2={$cat2} <br>";
                echo "\$exam={$exam} <br>";
                echo "\$total={$total} <br>";
                echo "\$gradecode={$gradecode} <hr>";
             */
			 
                $save = true;
                if(empty($cat1) && empty($cat2) && empty($exam) && empty($total) && empty($gradecode)){
                	$save = false;
                }
//                var_dump($save );
//                exit();
        	// exit();
        	 
        if($save){
        	
            $subject = new ADODB_Active_Record('SASTUDSUB',array('ADMNO','SUBJECTCODE','FORMCODE','STREAMCODE','YEARCODE','TERMCODE'));
        	
            $subject->Load("
        	ADMNO='{$admissionno}'
        	AND SUBJECTCODE='{$subjectcode}'
        	AND FORMCODE='{$form}'
        	AND STREAMCODE='{$streamcode}'
        	AND YEARCODE='{$year}'
        	AND TERMCODE='{$termcode}'
        	");
        	
			if(empty($subject->_original)){
			 $subject->id           = $maxid; //generateID($subject->_tableat, 'ID', $db);
			 $subject->admno        = $admissionno;
			 $subject->subjectcode  = $subjectcode;
			 $subject->formcode     = $form;
			 $subject->streamcode   = $streamcode;
			 $subject->yearcode     = $year;
			 $subject->termcode     = $termcode;
			
			}
			
			$subject->cat1     = $cat1;
			$subject->cat2     = $cat2;
			$subject->avgcat   = null;
			$subject->paper1   = $exam;
			$subject->avgexam  = $exam;
			$subject->total    = $total;
			$subject->gradecode    = $gradecode;
			$subject->points     = valueof($points, $gradecode);
//			 print_pre($subject);exit();
			 
			 if( $subject->Save() ){
			  ++$maxid;
			 }
			// exit();
			 
         }
        }
        
		//exit();
		++$count;
		$data->MoveNext();
		
		$progress= round(($count/$RecordCount)*100,0);
	
		sleep(.7);
		echo "
		<script>
          parent.$('#prg_copy_exam').progressbar({
            value: {$progress}
            });
        </script>
		";
		
        }
  	  }
  	}
	  	
		$db->Execute("update SASTUDSUB set CAT1P= round((CAT1/30)*100,2) where CAT1<=30");
		$db->Execute("update SASTUDSUB set CAT2P= round((CAT2/30)*100,2) where CAT2<=30");
		$db->Execute("update SASTUDSUB set CAT3P= round((CAT3/30)*100,2) where CAT3<=30"); 
		$db->Execute("update SASTUDSUB set CAT4P= round((CAT4/30)*100,2) where CAT4<=30"); 
		$db->Execute("update SASTUDSUB set CAT4P= round((CAT5/30)*100,2) where CAT5<=30"); 
		
		$db->Execute("update SASTUDSUB set PAPER1P= round((PAPER1/30)*100,2) where PAPER1<=30");
		$db->Execute("update SASTUDSUB set PAPER2P= round((PAPER2/30)*100,2) where PAPER2<=30");
		$db->Execute("update SASTUDSUB set PAPER3P= round((PAPER3/30)*100,2) where PAPER3<=30");
		$db->Execute("update SASTUDSUB set PAPER4P= round((PAPER4/30)*100,2) where PAPER4<=30"); 
		$db->Execute("update SASTUDSUB set PAPER4P= round((PAPER5/30)*100,2) where PAPER5<=30");
		
		$db->Execute("update SASTUDSUB set CAT1POINTS=12 where CAT1GRADECODE='A'"); 
		$db->Execute("update SASTUDSUB set CAT1POINTS=11 where CAT1GRADECODE='A-'"); 
		$db->Execute("update SASTUDSUB set CAT1POINTS=10 where CAT1GRADECODE='B+'"); 
		$db->Execute("update SASTUDSUB set CAT1POINTS=9 where CAT1GRADECODE='B'"); 
		$db->Execute("update SASTUDSUB set CAT1POINTS=8 where CAT1GRADECODE='B-'"); 
		$db->Execute("update SASTUDSUB set CAT1POINTS=7 where CAT1GRADECODE='C+'"); 
		$db->Execute("update SASTUDSUB set CAT1POINTS=6 where CAT1GRADECODE='C'"); 
		$db->Execute("update SASTUDSUB set CAT1POINTS=5 where CAT1GRADECODE='C-'"); 
		$db->Execute("update SASTUDSUB set CAT1POINTS=4 where CAT1GRADECODE='D+'"); 
		$db->Execute("update SASTUDSUB set CAT1POINTS=3 where CAT1GRADECODE='D'"); 
		$db->Execute("update SASTUDSUB set CAT1POINTS=2 where CAT1GRADECODE='D-'"); 
		$db->Execute("update SASTUDSUB set CAT1POINTS=1 where CAT1GRADECODE='E'"); 
		
		$db->Execute("update SASTUDSUB set CAT2POINTS=12 where CAT2GRADECODE='A'"); 
		$db->Execute("update SASTUDSUB set CAT2POINTS=11 where CAT2GRADECODE='A-'"); 
		$db->Execute("update SASTUDSUB set CAT2POINTS=10 where CAT2GRADECODE='B+'"); 
		$db->Execute("update SASTUDSUB set CAT2POINTS=9 where CAT2GRADECODE='B'"); 
		$db->Execute("update SASTUDSUB set CAT2POINTS=8 where CAT2GRADECODE='B-'"); 
		$db->Execute("update SASTUDSUB set CAT2POINTS=7 where CAT2GRADECODE='C+'"); 
		$db->Execute("update SASTUDSUB set CAT2POINTS=6 where CAT2GRADECODE='C'"); 
		$db->Execute("update SASTUDSUB set CAT2POINTS=5 where CAT2GRADECODE='C-'"); 
		$db->Execute("update SASTUDSUB set CAT2POINTS=4 where CAT2GRADECODE='D+'"); 
		$db->Execute("update SASTUDSUB set CAT2POINTS=3 where CAT2GRADECODE='D'"); 
		$db->Execute("update SASTUDSUB set CAT2POINTS=2 where CAT2GRADECODE='D-'"); 
		$db->Execute("update SASTUDSUB set CAT2POINTS=1 where CAT2GRADECODE='E'"); 
		
		$db->Execute("update SASTUDSUB set CAT3POINTS=12 where CAT3GRADECODE='A'"); 
		$db->Execute("update SASTUDSUB set CAT3POINTS=11 where CAT3GRADECODE='A-'"); 
		$db->Execute("update SASTUDSUB set CAT3POINTS=10 where CAT3GRADECODE='B+'"); 
		$db->Execute("update SASTUDSUB set CAT3POINTS=9 where CAT3GRADECODE='B'"); 
		$db->Execute("update SASTUDSUB set CAT3POINTS=8 where CAT3GRADECODE='B-'"); 
		$db->Execute("update SASTUDSUB set CAT3POINTS=7 where CAT3GRADECODE='C+'"); 
		$db->Execute("update SASTUDSUB set CAT3POINTS=6 where CAT3GRADECODE='C'"); 
		$db->Execute("update SASTUDSUB set CAT3POINTS=5 where CAT3GRADECODE='C-'"); 
		$db->Execute("update SASTUDSUB set CAT3POINTS=4 where CAT3GRADECODE='D+'"); 
		$db->Execute("update SASTUDSUB set CAT3POINTS=3 where CAT3GRADECODE='D'"); 
		$db->Execute("update SASTUDSUB set CAT3POINTS=2 where CAT3GRADECODE='D-'"); 
		$db->Execute("update SASTUDSUB set CAT3POINTS=1 where CAT3GRADECODE='E'"); 
		
		$db->Execute("update SASTUDSUB set PAPER1POINTS=12 where PAPER1GRADECODE='A'"); 
		$db->Execute("update SASTUDSUB set PAPER1POINTS=11 where PAPER1GRADECODE='A-'"); 
		$db->Execute("update SASTUDSUB set PAPER1POINTS=10 where PAPER1GRADECODE='B+'"); 
		$db->Execute("update SASTUDSUB set PAPER1POINTS=9 where PAPER1GRADECODE='B'"); 
		$db->Execute("update SASTUDSUB set PAPER1POINTS=8 where PAPER1GRADECODE='B-'"); 
		$db->Execute("update SASTUDSUB set PAPER1POINTS=7 where PAPER1GRADECODE='C+'"); 
		$db->Execute("update SASTUDSUB set PAPER1POINTS=6 where PAPER1GRADECODE='C'"); 
		$db->Execute("update SASTUDSUB set PAPER1POINTS=5 where PAPER1GRADECODE='C-'"); 
		$db->Execute("update SASTUDSUB set PAPER1POINTS=4 where PAPER1GRADECODE='D+'"); 
		$db->Execute("update SASTUDSUB set PAPER1POINTS=3 where PAPER1GRADECODE='D'"); 
		$db->Execute("update SASTUDSUB set PAPER1POINTS=2 where PAPER1GRADECODE='D-'"); 
		$db->Execute("update SASTUDSUB set PAPER1POINTS=1 where PAPER1GRADECODE='E'"); 
		
		$db->Execute("update SASTUDSUB set PAPER2POINTS=12 where PAPER2GRADECODE='A'"); 
		$db->Execute("update SASTUDSUB set PAPER2POINTS=11 where PAPER2GRADECODE='A-'"); 
		$db->Execute("update SASTUDSUB set PAPER2POINTS=10 where PAPER2GRADECODE='B+'"); 
		$db->Execute("update SASTUDSUB set PAPER2POINTS=9 where PAPER2GRADECODE='B'"); 
		$db->Execute("update SASTUDSUB set PAPER2POINTS=8 where PAPER2GRADECODE='B-'"); 
		$db->Execute("update SASTUDSUB set PAPER2POINTS=7 where PAPER2GRADECODE='C+'"); 
		$db->Execute("update SASTUDSUB set PAPER2POINTS=6 where PAPER2GRADECODE='C'"); 
		$db->Execute("update SASTUDSUB set PAPER2POINTS=5 where PAPER2GRADECODE='C-'"); 
		$db->Execute("update SASTUDSUB set PAPER2POINTS=4 where PAPER2GRADECODE='D+'"); 
		$db->Execute("update SASTUDSUB set PAPER2POINTS=3 where PAPER2GRADECODE='D'"); 
		$db->Execute("update SASTUDSUB set PAPER2POINTS=2 where PAPER2GRADECODE='D-'"); 
		$db->Execute("update SASTUDSUB set PAPER2POINTS=1 where PAPER2GRADECODE='E'"); 
		
		$db->Execute("update SASTUDSUB set PAPER3POINTS=12 where PAPER3GRADECODE='A'"); 
		$db->Execute("update SASTUDSUB set PAPER3POINTS=11 where PAPER3GRADECODE='A-'"); 
		$db->Execute("update SASTUDSUB set PAPER3POINTS=10 where PAPER3GRADECODE='B+'"); 
		$db->Execute("update SASTUDSUB set PAPER3POINTS=9 where PAPER3GRADECODE='B'"); 
		$db->Execute("update SASTUDSUB set PAPER3POINTS=8 where PAPER3GRADECODE='B-'"); 
		$db->Execute("update SASTUDSUB set PAPER3POINTS=7 where PAPER3GRADECODE='C+'"); 
		$db->Execute("update SASTUDSUB set PAPER3POINTS=6 where PAPER3GRADECODE='C'"); 
		$db->Execute("update SASTUDSUB set PAPER3POINTS=5 where PAPER3GRADECODE='C-'"); 
		$db->Execute("update SASTUDSUB set PAPER3POINTS=4 where PAPER3GRADECODE='D+'"); 
		$db->Execute("update SASTUDSUB set PAPER3POINTS=3 where PAPER3GRADECODE='D'"); 
		$db->Execute("update SASTUDSUB set PAPER3POINTS=2 where PAPER3GRADECODE='D-'"); 
		$db->Execute("update SASTUDSUB set PAPER3POINTS=1 where PAPER3GRADECODE='E'"); 
		
		$db->Execute("update SASTUDSUB set POINTS=12 where GRADECODE='A'"); 
		$db->Execute("update SASTUDSUB set POINTS=11 where GRADECODE='A-'"); 
		$db->Execute("update SASTUDSUB set POINTS=10 where GRADECODE='B+'"); 
		$db->Execute("update SASTUDSUB set POINTS=9 where GRADECODE='B'"); 
		$db->Execute("update SASTUDSUB set POINTS=8 where GRADECODE='B-'"); 
		$db->Execute("update SASTUDSUB set POINTS=7 where GRADECODE='C+'"); 
		$db->Execute("update SASTUDSUB set POINTS=6 where GRADECODE='C'"); 
		$db->Execute("update SASTUDSUB set POINTS=5 where GRADECODE='C-'"); 
		$db->Execute("update SASTUDSUB set POINTS=4 where GRADECODE='D+'"); 
		$db->Execute("update SASTUDSUB set POINTS=3 where GRADECODE='D'"); 
		$db->Execute("update SASTUDSUB set POINTS=2 where GRADECODE='D-'"); 
		$db->Execute("update SASTUDSUB set POINTS=1 where GRADECODE='E'"); 

		
  }
 	
 function copy_cats(){
 global $source,$db;
  
 $data = $source->Execute("
	select *
	from CATANALYSIS 
	order by ADMISSIONNO, YEAR,TERM
  ");
 
 $subjects = $db->GetAssoc("select CODEOFFC,SUBJECTCODE from SASUBJECTS");
 
 $subjects = array(
'AGR'       => 'AGR',
'BIO'       => 'BIO',
'BUS'       => 'BUS',
'CHM'       => 'CHM',
'COM'       => 'COM',
'CRE'       => 'CRE',
'DRA'       => 'DRA',
'ENG'       => 'ENG',
'FRE'       => 'FRE',
'GEO'       => 'GEO',
'HIS'       => 'HIS',
'KIS'       => 'KIS',
'MAT'       => 'MAT',
'MUS'       => 'MUS',
'PHY'       => 'PHY',
'HOM'       => 'HOM'  
 );
 
 $db->debug=0;
 
 $points = $db->GetAssoc("select GRADECODE ,POINTS from SAKCPEGRADES");
 $maxid_now  = $db->GetOne("SELECT MAX(ID) AS RID FROM SASTUDSUB  ");
 $maxid  =  $maxid_now>1 ? $maxid_now+1 : 1;
// print_pre($subjects);
 
  if(isset($data)){
  	  
  	 $RecordCount = $data->RecordCount();
  	 $count=1;
  
  	  if($RecordCount>0){
  	  	$row_data = array();
        while (!$data->EOF){
      
//        	print_pre($data->fields);
        	
        	$year          = valueof($data->fields,'YEAR');
            $termcode      = valueof($data->fields,'TERM',1);
            $admissionno   = valueof($data->fields,'ADMISSIONNO');
            $form          = valueof($data->fields,'FORM');
            $streamcode    = valueof($data->fields,'CLASS');
            $posclass      = valueof($data->fields,'POSCLASS');
            $position      = valueof($data->fields,'POSITION');
            $meangrade     = valueof($data->fields,'MEANGRADE');
            
         
//            echo "\$admissionno={$admissionno} <br>";
//            echo "\$form={$form} <br>";
//            echo "\$streamcode={$streamcode} <br>";
//            echo "\$year={$year} <br>";
//            echo "\$termcode={$termcode} <br>";
            
//            exit();
            
        	foreach ($data->fields as $col=>$val){
        		$col_left3_chars = substr($col,0,3);
				
//				echo "\$col={$col} <br>";
//				echo "\$val={$val} <br>";
//				echo "\$col_left3_chars={$col_left3_chars} <br>";
//				print_pre($subjects);exit;
				
        		if(array_key_exists($col_left3_chars, $subjects)){
//        		echo "\$col={$col} <br>";
//        		echo "\$col_left3_chars={$col_left3_chars} <br>";
//        		echo "\$val={$val} <br>";
        		 $row_data[$col_left3_chars][$col] = $val;
        		}
        	}
        	
//        	print_pre($row_data);
        	
        	
        	foreach ($row_data as $subjectcode=>$subject_vars){
//        	 echo "\$subjectcode={$subjectcode} <br>";
//        	 print_pre($subject_vars);
        	
        	 
        	 $cat1_score = null;
        	 $cat1_gradecode = null;
        	 $cat2_score = null;
        	 $cat2_gradecode = null;
        	 $cat3_score = null;
        	 $cat3_gradecode = null;
        	  
        	 
        	 $cats1= "{$subjectcode}C1";
        	 $cats2= "{$subjectcode}C2";
        	 $cats3= "{$subjectcode}C3";
        	 
//        	 echo "\$cats1={$cats1} <br>";
        	 
        	 if(array_key_exists($cats1,$subject_vars)){
        	 	$cats1_array     = explode(' ',$subject_vars[$cats1]);
        	 	$cat1_score      = valueof($cats1_array,0,null);
        	 	$cat1_gradecode  = valueof($cats1_array,1);
        	 }
        	 
        	 if(array_key_exists($cats2,$subject_vars)){
        	 	$cats2_array     = explode(' ',$subject_vars[$cats2]);
        	 	$cat2_score      = valueof($cats2_array,0);
        	 	$cat2_gradecode  = valueof($cats2_array,1);
        	 }
        	 
        	 if(array_key_exists($cats3,$subject_vars)){
        	 	$cats3_array     = explode(' ',$subject_vars[$cats3]);
        	 	$cat3_score      = valueof($cats3_array,0);
        	 	$cat3_gradecode  = valueof($cats3_array,1);
        	 }
        	 
//        	 echo "\$cat1_score={$cat1_score} <br>";
//        	 echo "\$cat1_gradecode={$cat1_gradecode} <br>";
//        	 
//        	 echo "\$cat2_score={$cat2_score} <br>";
//        	 echo "\$cat2_gradecode={$cat2_gradecode} <br>";
//        	 
//        	 echo "\$cat3_score={$cat3_score} <br>";
//        	 echo "\$cat3_gradecode={$cat3_gradecode} <br>";
//        	 
//        	 		exit();
//        	 		
//        	 foreach ($subject_vars as $subject_vars_key => $subject_vars_value){
//        	 	if( substr($subjectcode_mixed, -1,1 ) =='G'){
//        	 	  	
//        	 	}
//        	 }
//        	 exit();
        	 
        	 $cat1 = null;
        	 $cat2 = null;
        	 $exam = null;
        	 $total = null;
        	 $gradecode = null;
        	 $total_vars_arr = null;
        	 
        	 foreach ($subject_vars as $subjectcode_mixed => $subject_var){
//        	 	echo "\$subjectcode_mixed={$subjectcode_mixed} <br>";
        	 	$subjectcode_mixed_len = strlen($subjectcode_mixed);
//        	 	echo "\$subjectcode_mixed_len={$subjectcode_mixed_len} <br>";
        	 	$subjectcode_last_char = substr($subjectcode_mixed, -1,1 );
        	 	
//        	 	echo "\$subjectcode_last_char={$subjectcode_last_char} <br>";
                
                switch ($subjectcode_last_char){
                	case 'E':
                		$total_vars_arr =	split(' ',$subject_var);
                		$total          =	valueof($total_vars_arr, 0, '','trim');
//                		$gradecode      =	$total_vars_arr[1];
                		$gradecode      =	valueof($total_vars_arr, 1, '','trim');
                	break;
                	case 'C':
                		$cat1 =	 $subject_var;
                	break;
                	case 'X':
                		$exam =	 $subject_var;
                	break;
                	case '2':
                		$cat2 =	 $subject_var;
                	break;
                }
        	 }
        	 
//        	    echo "\$cat1={$cat1} <br>";
//                echo "\$cat2={$cat2} <br>";
//                echo "\$exam={$exam} <br>";
//                echo "\$total={$total} <br>";
//                echo "\$gradecode={$gradecode} <hr>";
			 
                $save = true;
                if(empty($cat1) && empty($cat2) && empty($exam) && empty($total) && empty($gradecode)){
                	$save = false;
                }
        	// exit();
        	 
        if($save){
        	
            $subject = new ADODB_Active_Record('SASTUDSUB',array('ADMNO','SUBJECTCODE','FORMCODE','STREAMCODE','YEARCODE','TERMCODE'));
        	            
            $subject->Load("
        	ADMNO='{$admissionno}'
        	AND SUBJECTCODE='{$subjectcode}'
        	AND FORMCODE='{$form}'
        	AND STREAMCODE='{$streamcode}'
        	AND YEARCODE='{$year}'
        	AND TERMCODE='{$termcode}'
        	");
			
        	if(empty($subject->_original)){
			 $subject->id           = $maxid; //generateID($subject->_tableat, 'ID', $db);
			 $subject->admno        = $admissionno;
			 $subject->subjectcode  = $subjectcode;
			 $subject->formcode     = $form;
			 $subject->streamcode   = $streamcode;
			 $subject->yearcode     = $year;
			 $subject->termcode     = $termcode;
			
        	}
			
        	if(empty($subject->cat1)){
			$subject->cat1     = $cat1_score;
        	}
        	
        	if(empty($subject->cat2)){
			$subject->cat2     = $cat2_score;
        	}
        	
        	if(empty($subject->cat3)){
			$subject->cat3     = $cat3_score;
        	}
        	
			$subject->cat1gradecode   = $cat1_gradecode;
			$subject->cat2gradecode   = $cat2_gradecode;
			$subject->cat3gradecode   = $cat3_gradecode;
			
			 //print_pre($subject);exit();
			 
			 if( $subject->Save() ){
			  ++$maxid;
			 }
			// exit();
			 
         }
        }
        
		++$count;
		
		$data->MoveNext();
		
		$progress = round(($count/$RecordCount)*100,0);
	
		sleep(.7);
		
		echo "
		<script>
          parent.$('#prg_copy_cats').progressbar({
            value: {$progress}
            });
         </script>
		";
		
        }
  	  }
  	}
  
  }
  
 
 function createNew($userid,  $username, $deptcode, $email){
            global $db;
            
 	        $groupcode   = $db->CacheGetOne(9000,"SELECT GROUPCODE FROM USERGROUPS WHERE IST=1");
 	        
            $record_user = new ADOdb_Active_Record('USERS',array('USERID'));
			$record_user->Load(" USERID='{$userid}' ");
			
			if(empty($record_user->_original))
			{
				$record_user->id                =  generateID('USERS','ID');
				$record_user->userid            =  $userid;
				$record_user->groupcode         =  $groupcode;
				$new =  true;
			}else{
				$new = false;
			}
			
			  $record_user->deptcode          =  $deptcode;
			  $record_user->username          =  Camelize($username);
              $record_user->email             =  $email;
              $record_user->audituser         =  'system';
              $record_user->auditdate         =  date('Y-m-d');
              $record_user->audittime         =  time();
                
			   if(empty($record_user->password)){
				$record_user->password = md5($record_user->userid);
			   }
                                      
			  if($record_user->Save()){
			  	return  true;
			  }
			  
			  return  false;
 }
 