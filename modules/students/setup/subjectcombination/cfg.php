<?php
/**
* auto created config file for modules/students/setup/subjectcombination
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-01-06 17:39:13
*/

$scriptname  = @end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'Subject Combination';//user-readable formart
 $cfg['appname']       = 'subjectcombination';//lower cased one word
 $cfg['datasrc']       = 'SASBC';//where to get data
 $cfg['datatbl']       = 'SASBC';//base data src [for updates & deletes]
 $cfg['form_width']    = 450;
 $cfg['form_height']   = 250;
 $cfg['window_width']    = 500;
 $cfg['window_height']   = 400;
 
 $cfg['pkcol']         = 'SBCCODE';//the primary key
 
 $cfg['tblbns']['chk_button_add']    = 1;
 $cfg['tblbns']['chk_button_edit']   = 1;
 $cfg['tblbns']['chk_button_delete'] = 1;
 $cfg['tblbns']['chk_button_import'] = 1;
 $cfg['tblbns']['chk_button_export'] = 1;
 

$cfg['columns']['w1lbu'] = array(
                      'dbcol'=>'SBCCODE',
                      'title'=>'Combination Code',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['nlov2'] = array(
                      'dbcol'=>'SBCNAME',
                      'title'=>'Combination  Name',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['uebcd'] = array(
                      'dbcol'=>'ISDEFAULT',
                      'title'=>'Is Default',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'checkbox',
                      'selectsrc'=> '',
                      );
                      



                      $combogrid_array   = array();
  