<?php
/**
* auto created config file for modules/students/setup/glaccounts
* @author coderX
* @todo by-me-beer
* @version 1.0
* @since 2015-05-21 21:52:02
*/

$scriptname  = end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'Gl Accounts';//user-readable formart
 $cfg['appname']       = 'gl accounts';//lower cased one word
 $cfg['datasrc']       = 'FINACCT';//where to get data
 $cfg['datatbl']       = 'FINACCT';//base data src [for updates & deletes]
 $cfg['form_width']    = 450;
 $cfg['form_height']   = 230;
 $cfg['grid_width']    = 550;
 $cfg['grid_height']   = 230;
 
 $cfg['pkcol']         = 'ACCOUNTNO';//the primary key
 
 $cfg['tblbns']['chk_button_add']    = 1;
 $cfg['tblbns']['chk_button_edit']   = 1;
 $cfg['tblbns']['chk_button_delete'] = 1;
 $cfg['tblbns']['chk_button_import'] = 1;
 $cfg['tblbns']['chk_button_export'] = 1;
 

$cfg['columns']['abbgk'] = array(
                      'dbcol'=>'ACCOUNTNO',
                      'title'=>'Account no',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      
$cfg['columns']['5dooz'] = array(
                      'dbcol'=>'ACCOUNTNAME',
                      'title'=>'Account  Name',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      
$cfg['columns']['awkpb'] = array(
                      'dbcol'=>'ACTIVECODE',
                      'title'=>'Is Active',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'SAYN|YNCODE|YNNAME|YNCODE',
                      );
                      