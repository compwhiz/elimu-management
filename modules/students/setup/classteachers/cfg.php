<?php
/**
* auto created config file for modules/students/setup/classteachers
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-01-07 07:17:08
*/

$scriptname  = @end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'Class Teachers';//user-readable formart
 $cfg['appname']       = 'classteachers';//lower cased one word
 $cfg['datasrc']       = 'VIEWCT';//where to get data
 $cfg['datatbl']       = 'SACT';//base data src [for updates & deletes]
 $cfg['form_width']    = 400;
 $cfg['form_height']   = 210;
 $cfg['window_width']    = 600;
 $cfg['window_height']   = 410;
 
 $cfg['pkcol']         = 'TCHNO';//the primary key
 
 $cfg['tblbns']['chk_button_add']    = 1;
 $cfg['tblbns']['chk_button_edit']   = 1;
 $cfg['tblbns']['chk_button_delete'] = 1;
 $cfg['tblbns']['chk_button_import'] = 1;
 $cfg['tblbns']['chk_button_export'] = 1;
 

$cfg['columns']['9irbd'] = array(
                      'dbcol'=>'SLTCODE',
                      'title'=>'Salutation',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>2,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['7ylvy'] = array(
                      'dbcol'=>'TCHNO',
                      'title'=>'Teacher',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'combogrid',
                      'selectsrc'=> 'SATEACHERS|TCHNO|FULLNAME|TCHNO',
                      );
                      



                      
$cfg['columns']['oyida'] = array(
                      'dbcol'=>'FULLNAME',
                      'title'=>'Teacher Name',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>2,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['2odly'] = array(
                      'dbcol'=>'YEARCODE',
                      'title'=>'Year',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'SAYEAR|YEARCODE|YEARNAME|YEARCODE',
                      );
                      



                      
$cfg['columns']['5fqmw'] = array(
                      'dbcol'=>'STREAMCODE',
                      'title'=>'Stream',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>3,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'SASTREAMS|STREAMCODE|STREAMNAME|STREAMCODE',
                      );
                      



                      
$cfg['columns']['uemls'] = array(
                      'dbcol'=>'STREAMNAME',
                      'title'=>'Stream',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>2,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['enpf7'] = array(
                      'dbcol'=>'FORMCODE',
                      'title'=>'Form',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>2,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      $combogrid_array   = array();
	 
$combogrid_array['7ylvy']['columns']['TCHNO']  = array( 'field'=>'TCHNO', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['7ylvy']['columns']['FULLNAME']  = array( 'field'=>'FULLNAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['7ylvy']['source'] ='SATEACHERS';
	 
$combogrid_array['2odly']['columns']['YEARCODE']  = array( 'field'=>'YEARCODE', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['2odly']['columns']['YEARNAME']  = array( 'field'=>'YEARNAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['2odly']['source'] ='SAYEAR';
	 
$combogrid_array['5fqmw']['columns']['STREAMCODE']  = array( 'field'=>'STREAMCODE', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['5fqmw']['columns']['STREAMNAME']  = array( 'field'=>'STREAMNAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['5fqmw']['source'] ='SASTREAMS';
  