<?php

  class teachers{
	private $id;
	private $datasrc;
	private $primary_key;
	private $tchno;
	
	public function __construct(){
		global $cfg;
		
		$this->id           = filter_input(INPUT_POST , 'id');
		$this->datasrc      = valueof($cfg,'datasrc');
		$this->primary_key  = valueof($cfg,'pkcol');
	}
	
	public function data(){
		global $db;
		
		$data = array();
		/**********/
		
        $tchno   = filter_input(INPUT_GET, 'tchno'); 
        $teacher = new ADODB_Active_Record('SATEACHERS', array('TCHNO'));
        $teacher->Load("TCHNO='{$tchno}' ");
        
		$data['tchno']         = valueof( $teacher, 'tchno');
		$data['salutation']    = valueof( $teacher, 'sltcode');
		$data['fullname']      = valueof( $teacher, 'fullname');
		$data['initials']      = valueof( $teacher, 'initials');
		$data['dob']           = valueof( $teacher, 'dob');
		
		$data['nationalid']    = valueof( $teacher, 'nationalid');
		$data['gender']        = valueof( $teacher, 'gndcode');
		$data['religion']      = valueof( $teacher, 'rlgcode');
		$data['maritalstate']  = valueof( $teacher, 'mstcode');
		
		$data['dept']           = valueof( $teacher, 'deptcode');
		$data['doe']            = valueof( $teacher, 'doe');
		$data['qualification']  = valueof( $teacher, 'tqfcode');
		
		$data['mobilephone']   = valueof( $teacher, 'mobilephone');
		$data['postalcode']    = valueof( $teacher, 'postalcode');
		$data['address']       = valueof( $teacher, 'address');
		$data['residence']     = valueof( $teacher, 'residence');
		$data['email']         = valueof( $teacher, 'email');
		
		$_SESSION['profile_teacher']      = $tchno;
		
	  if(defined('MAKE_FIELDS_UNIQUE') && defined('MNUID')) {
		foreach($data as $k=>$v){
		  $field_id = "{$k}_".MNUID; 
		  unset($data[$k]);
		  $data[$field_id] = $v;
		}
	  }

		return json_encode($data);
	}
		
	public function upload_signature(){
		
		if(!isset($_FILES['files'])){
		 return  json_response(0,'no signature uploaded');	
		}
		
		if(!isset($_SESSION['profile_teacher'])){
		 return  json_response(0,'Please Reload Teacher Profile');	
		}
		
		$tchno = $_SESSION['profile_teacher'];
		$name = $_FILES['files']['name'][0];
		$tmp_name = $_FILES['files']['tmp_name'][0];
		$name = $_FILES['files']['name'][0];
		$type = $_FILES['files']['type'][0];
		$info = pathinfo($tmp_name);
		
		$signature   = ROOT.'public/signatures/'.$tchno.'.jpg';
		
		if(file_exists($signature)){
			if(is_readable($signature)){
				@unlink($signature);
			}
		}
		
		if(!move_uploaded_file($tmp_name, $signature)){
		 return  json_response(0,'Upload Failed');		
		}
		
		@unlink($tmp_name);
		
        $signature_small = ROOT.'public/signatures/'.$tchno.'_resized.jpg';
        
        $image = new simpleimage();
        $image->load($signature);
        $image->resize(140,40);
        $image->save($signature_small);
        
        unlink($signature);
        
        rename($signature_small, $signature);
        
		return  json_response(1,'Photo Uploaded');	
		
	}
	
	public function signature(){
		$tchno      = filter_input(INPUT_POST, 'tchno');
		$signature_name = $tchno.'.jpg';
		$signature   = ROOT.'public/signatures/'.$signature_name;
        $rand = mt_rand();		
		if(file_exists($signature) && is_readable($signature)){
		 $web_img = '<img src="../../public/signatures/'.$signature_name.'?cache_reload='.$rand.'" border="0">';
		 $message = "OK";
		}else{
		 $web_img = '<img src="../../public/signatures/default.jpg?cache_reload='.$rand.'" border="0">';
		 $message = "NO PHOTO";
		}
		
		return json_encode( array(
	     'success' => 1,
	     'message' => $message,
	     'signature' => $web_img,
	    ));
	 
	}

	public function save(){
		global $db,$cfg;

    $user              = new user();		
    $tchno             = filter_input(INPUT_POST, ui::fi('tchno')); 
    $nationalid        = filter_input(INPUT_POST, ui::fi('nationalid')); 
    $password          = filter_input(INPUT_POST, ui::fi('password')); 
    
    $teacher = new ADODB_Active_Record('SATEACHERS', array('TCHNO'));
    $teacher->Load("TCHNO='{$tchno}' ");
    
    if(empty($teacher->_original)){
     $teacher->id       = generateID('SATEACHERS');
     $teacher->tchno    = $tchno; 
     $teacher->username = $tchno; 
    }
      
    if( !empty($password) ){
    $teacher->password = md5( strtolower($tchno).$password.strtolower($tchno));
    }
    
    $teacher->sltcode     = filter_input(INPUT_POST,  ui::fi('salutation')); 
    $teacher->fullname    = filter_input(INPUT_POST,  ui::fi('fullname') ,FILTER_SANITIZE_STRING); 
    $teacher->initials    = filter_input(INPUT_POST,  ui::fi('initials') ,FILTER_SANITIZE_STRING); 
    $teacher->dob         = filter_input(INPUT_POST,  ui::fi('dob')); 
    
    $teacher->deptcode    = filter_input(INPUT_POST,  ui::fi('dept')); 
    $teacher->doe         = filter_input(INPUT_POST,  ui::fi('doe')); 
    $teacher->tqfcode     = filter_input(INPUT_POST,  ui::fi('qualification')); 
    
    $teacher->nationalid  = trim($nationalid);
    $teacher->gndcode     = filter_input(INPUT_POST,  ui::fi('gender')); 
    $teacher->rlgcode     = filter_input(INPUT_POST,  ui::fi('religion')); 
    $teacher->mstcode     = filter_input(INPUT_POST,  ui::fi('maritalstate')); 
    
    $teacher->mobilephone  = filter_input(INPUT_POST,  ui::fi('mobilephone') ,FILTER_SANITIZE_STRING);
    $teacher->residence    = filter_input(INPUT_POST,  ui::fi('residence') ,FILTER_SANITIZE_STRING); 
    $teacher->postalcode   = filter_input(INPUT_POST,  ui::fi('postalcode') ,FILTER_SANITIZE_STRING);
    $teacher->address      = filter_input(INPUT_POST,  ui::fi('address') ,FILTER_SANITIZE_STRING); 
    $teacher->email        = filter_input(INPUT_POST,  ui::fi('email') , FILTER_VALIDATE_EMAIL);
    
    $teacher->audituser    = $user->userid;
    $teacher->auditdate    = date('Y-m-d'); 
    $teacher->audittime    = time();

    if($teacher->Save()){
     $groupcode = $db->CacheGetOne(9000,"SELECT GROUPCODE FROM USERGROUPS WHERE IST=1");
     $sysuser   = user::createNew($teacher->tchno, $teacher->fullname,  $teacher->deptcode,$teacher->email, $password ,$groupcode );
   
     return json_response(1,'record saved');
    }else{
     return json_response(0,'save failed');
    }

	}
	
	public function remove(){
		global $db, $cfg;
		json_response(0,'failed');
//        $grid = new grid();
//        return $grid->grid_remove_row();
	}
	
}
