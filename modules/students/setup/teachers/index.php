<?php

?>
<style>
.textbox{
	height:20px;
	margin:0;
	padding:0 2px;
	box-sizing:content-box;
}
td.label {   font-size: 12px; }
#signature {cursor:pointer;}
</style>

	<div style="margin:5px 0;">
	<table cellpadding="2" cellspacing="0" width="280px">
	 <tr>
	 <td nowrap class="label">Search</td>
	 <td><?php echo ui::form_input( 'text', 'find_tchno', 20, '', '', '', '', '', '' , ""); ?></td>
	 <td><a href="javascript:void(0)" class="easyui-linkbutton"  onclick="<?php echo $cfg['appname']; ?>.loadProfile();">View</a></td>
	 </tr>
	</table>
    </div>
	
	<form id="ff<?php echo MNUID; ?>" method="post" novalidate>
	<div class="easyui-tabs" data-options="tabWidth:112" style="height:303px;width:300px" fit="true">
		
		<div title="Official" style="padding:10px">
			
	    	<table cellpadding="2" cellspacing="0" width="100%">
	    	
	    		<tr>
	    			<td width="180px">Teacher No</td>
	    			<td colspan="2"><?php echo ui::form_input( 'text', 'tchno', 15, '', '', '', 'data-options="required:true, validType:\'minLength[3]\'"', '', 'easyui-textbox' , ""); ?></td>
	    		</tr>
	    		<tr>
	    			<td>Title:</td>
	    			<td colspan="2"><?php 
	    			echo dropdown::salutation('salutation','NA');  ?></td>
	    		</tr>
	    		
	    		<tr>
	    			<td>Full Name:</td>
	    			<td colspan="2"><?php echo ui::form_input( 'text', 'fullname', 15, '', '', '', 'data-options="required:true, validType:\'minLength[3]\'"', '', 'easyui-textbox' , ""); ?></td>
	    		</tr>
	    		
	    		<tr>
	    			<td>Initials:</td>
	    			<td colspan="2"><?php echo ui::form_input( 'text', 'initials', 10, '', '', '', 'data-options="required:true, validType:\'minLength[3]\'"', '', 'easyui-textbox' , ""); ?></td>
	    		</tr>
	    		
	    		<tr>
	    			<td  width="180px">Department:</td>
	    			<td>
	    				<?php echo dropdown::dept('dept', $selected_code='',$options='', 1);  ?>
	    			</td>
	    		</tr>
	    		
	    		<tr>
	    			<td>Employment Date:</td>
	    			<td><?php echo ui::form_input( 'text', 'doe', 15, '', '', '', 'data-options="required:false,formatter:dateYmdFormatter,parser:dateYmdParser" placeholder="YYYY-MM-DD"', '', 'easyui-datebox' , ""); ?></td>
	    		</tr>
	    		
	    		<tr>
	    			<td>Qualification:</td>
	    			<td colspan="2">
	    				<?php echo ui::form_select('qualification','SATQF','TQFCODE','TQFNAME','','');  ?>
	    			</td>
	    		</tr>
	    		
	    		<tr>
	    			<td valign="top">Signature:</td>
	    			<td>
	    			 <div id="signature" title="click to replace Signature" onclick="<?php echo $cfg['appname']; ?>.initiate_upload_signature();"><img src="./public/signatures/default.jpg" border="0"></div>
	    			</td>
	    		</tr>
	    		
	    	</table>
	    	
		</div>
		<div title="Personal" style="padding:10px">
	   
	    	<table cellpadding="2" cellspacing="0" width="100%">
	    		
	    		<tr>
	    			<td>Birth Date:</td>
	    			<td><?php echo ui::form_input( 'text', 'dob', 15, '', '', '', 'data-options="required:false,formatter:dateYmdFormatter,parser:dateYmdParser" placeholder="YYYY-MM-DD"', '', 'easyui-datebox' , ""); ?></td>
	    		</tr>
	    		
	    		<tr>
	    			<td width="180px">National ID</td>
	    			<td colspan="2"><?php echo ui::form_input( 'text', 'nationalid', 15, '', '', '', 'data-options="required:false, validType:\'minLength[5]\'"', '', 'easyui-textbox' , ""); ?></td>
	    		</tr>
	    		
	    		<tr>
	    			<td>Gender:</td>
	    			<td colspan="2">
	    				<?php echo dropdown::gender('gender','M');  ?>
	    			</td>
	    		</tr>
	    		
	    		<tr>
	    			<td>Religion:</td>
	    			<td colspan="2">
	    				<?php echo dropdown::religion('religion');  ?>
	    			</td>
	    		</tr>
	    		
	    		<tr>
	    			<td>Marital State:</td>
	    			<td colspan="2">
	    				<?php echo dropdown::maritalstate('maritalstate');  ?>
	    			</td>
	    		</tr>
	    		
	    		<tr>
	    			<td>Password:</td>
	    			<td colspan="2"><?php echo ui::form_input( 'password', 'password', 15, '', '', '', 'data-options="validType:\'minLength[5]\'"', '', 'easyui-textbox' , ""); ?></td>
	    		</tr>
	    		
	    	</table>
	   
	
		</div>
		
		<div title="Contacts" style="padding:10px">
			
	    	<table cellpadding="2" cellspacing="0" width="100%">
	    		
	    		<tr>
	    			<td>Mobile Phone:</td>
	    			<td><?php echo ui::form_input( 'text', 'mobilephone', 15, '', '', '', 'data-options="required:false, validType:\'minLength[10]\'"', '', 'easyui-textbox' , ""); ?></td>
	    		</tr>
	    		
	    		<tr>
	    			<td>Postal Code:</td>
	    			<td><?php echo ui::form_input( 'text', 'postalcode', 15, '', '', '', 'data-options=""', '', 'easyui-textbox' , ""); ?></td>
	    		</tr>
	    		
	    		<tr>
	    			<td>Address:</td>
	    			<td><?php echo ui::form_input( 'text', 'address', 15, '', '', '', 'data-options=""', '', 'easyui-textbox' , ""); ?></td>
	    		</tr>
	    		
	    		<tr>
	    			<td>Residence:</td>
	    			<td><?php echo ui::form_input( 'text', 'residence', 15, '', '', '', 'data-options=""', '', 'easyui-textbox' , ""); ?></td>
	    		</tr>
	    		
	    		<tr>
	    			<td>Personal Email:</td>
	    			<td><?php echo ui::form_input( 'text', 'email', 15, '', '', '', 'data-options=""', '', 'easyui-textbox' , ""); ?></td>
	    		</tr>
	    		
	    	</table>
	    	
		</div>
		
	</div>
   </form>
   
	<div style="margin:5px 0;"></div>
	<table cellpadding="2" cellspacing="0" width="300px">
	 <tr>
	 <td><a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-save'" onclick="<?php echo $cfg['appname']; ?>.saveProfile();">Save</a></td>
	 <td><a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-reload'" onclick="<?php echo $cfg['appname']; ?>.clearForm()">Reset</a></td>
	 </tr>
	</table>
	
<div id="dlg_upload<?php echo MNUID; ?>" class="easyui-dialog" title="Scanned Signature Upload" style="width:300px;height:120px;padding:5px" closed="true" >
 <form id="ff_signature<?php echo MNUID; ?>" class="form-horizontal" action="./" method="POST" enctype="multipart/form-data">
  <input  type="file" name="<?php echo ui::fi('teacher_signature'); ?>" id="<?php echo ui::fi('teacher_signature'); ?>" onchange="<?php echo $cfg['appname']; ?>.do_upload_signature()">
 </form>
 </div>
 
	<script>
	
       var <?php echo $cfg['appname']; ?> = {
		clearForm:function (){
			$('#ff<?php echo MNUID; ?>').form('clear');
			$('#signature').html('<img src="./public/signatures/default.jpg" border="0">');
		},
		loadProfile:function (){
		 var tchno = $('#<?php echo ui::fi('find_tchno'); ?>').combogrid('getValue');
		 $('#ff<?php echo MNUID; ?>').form('load', './endpoints/crud/?modvars=<?php echo $vars; ?>&function=data&tchno='+tchno);
		 <?php echo $cfg['appname']; ?>.load_signature();
		},
	    load_signature :function (){
		   var tchno = $('#<?php echo ui::fi('find_tchno'); ?>').combogrid('getValue');
		   var rdata = 'tchno='+tchno+'&modvars=<?php echo $vars; ?>&function=signature';	
		   $.post('./endpoints/crud/', rdata, function(data) {
		   if (data.success === 1 && (typeof data.signature!=undefined)) {
			$('#signature<?php echo MNUID; ?>').html(data.signature);
		   }else{
			$.messager.alert('Error',data.message,'error');
		   }
		  }, "json");
		},
		initiate_upload_signature:function (){
	   	var tchno = $('#<?php echo ui::fi('find_tchno'); ?>').combogrid('getValue');
	   	if(tchno===''){
		 $.messager.alert('Error','Select Teacher First','error');
		 return;	
		}else{
	   	$.messager.confirm('Confirm','Do you want to upload a Signature Now?',function(r){
         if (r){
	  	  $('#dlg_upload<?php echo MNUID; ?>').dialog('open');
         }
	   	});
		}
	   },
	   do_upload_signature:function(){
		var data = new FormData($('#ff_signature<?php echo MNUID; ?>')[0]);
		$.ajax({
        url : './endpoints/crud/?modvars=<?php echo $vars; ?>&function=upload_signature',
        type: 'POST',
        data: data,
        cache: false,
        async:'false',
        dataType: 'json',
        processData: false,
        contentType: false,
        success: function(data, textStatus, jqXHR){
         <?php echo $cfg['appname']; ?>.load_signature();
         $('#dlg_upload<?php echo MNUID; ?>').dialog('close');
        },
        error: function(jqXHR, textStatus, errorThrown){

        }
        });
       },
	   saveProfile:function (){
			var validate =  $('#ff<?php echo MNUID; ?>').form('validate');
			if(!validate){
			  $.messager.alert('Error','Fill In All Required Fields','error');
			 return;	
			}else{
			 $.messager.progress();
			 var fdata = $('#ff<?php echo MNUID; ?>').serialize() + '&modvars=<?php echo $vars; ?>&function=save';	
		     $.post('./endpoints/crud/', fdata, function(data) {
		     $.messager.progress('close');
            if (data.success === 1) {
               $.messager.show({title: 'Success',msg: data.message});
            } else {
               $.messager.alert('Error',data.message,'error');
            }
           }, "json");
           
		 }
		}
	 };
	  
	<?php
      echo ui::ComboGrid($combogrid_array,'find_tchno',"{$cfg['appname']}.loadProfile");
	?>
		
	</script>
