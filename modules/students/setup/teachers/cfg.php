<?php

if(!defined('MAKE_FIELDS_UNIQUE')){
 define('MAKE_FIELDS_UNIQUE' , true);
}

$scriptname  = @end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'Teachers';//user-readable formart
 $cfg['appname']       = 'teachers';//lower cased
 $cfg['datasrc']       = 'SATEACHERS';//where to get data
 $cfg['datatbl']       = 'SATEACHERS';//base data src [for updates & deletes]
 $cfg['form_width']    = 400;
 $cfg['form_height']   = 280;
 
 $cfg['pkcol']         = 'code';//the primary key

$combogrid_array   = array();
$combogrid_array['find_tchno']['columns']['TCHNO']       = array( 'field'=>'tchno', 'title'=>'Teacher No', 'width'=> 80 , 'isIdField' => true );
$combogrid_array['find_tchno']['columns']['FULLNAME']    = array( 'field'=>'fullname', 'title'=>'Full Name', 'width'=> 100, 'isTextField'=>true);
$combogrid_array['find_tchno']['columns']['INITIALS']    = array( 'field'=>'initials', 'title'=>'Initials', 'width'=>100);
$combogrid_array['find_tchno']['columns']['MOBILEPHONE'] = array( 'field'=>'mobile', 'title'=>'Mobile', 'width'=> 80);
$combogrid_array['find_tchno']['source'] ='SATEACHERS';
	
