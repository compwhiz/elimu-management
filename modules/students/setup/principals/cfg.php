<?php
/**
* auto created config file for modules/students/setup/principals
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-01-16 12:51:32
*/

$scriptname  = @end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'Principals';//user-readable formart
 $cfg['appname']       = 'principals';//lower cased one word
 $cfg['datasrc']       = 'VIEWPRC';//where to get data
 $cfg['datatbl']       = 'SAPRC';//base data src [for updates & deletes]
 $cfg['form_width']    = 400;
 $cfg['form_height']   = 250;
 $cfg['window_width']    = 500;
 $cfg['window_height']   = 400;
 
 $cfg['pkcol']         = 'TCHNO';//the primary key
 
 $cfg['tblbns']['chk_button_add']    = 1;
 $cfg['tblbns']['chk_button_edit']   = 1;
 $cfg['tblbns']['chk_button_delete'] = 1;
 $cfg['tblbns']['chk_button_import'] = 1;
 $cfg['tblbns']['chk_button_export'] = 1;
 

$cfg['columns']['3tvjq'] = array(
                      'dbcol'=>'SLTCODE',
                      'title'=>'Salutation',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>2,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['clmgz'] = array(
                      'dbcol'=>'TCHNO',
                      'title'=>'Staff No',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'combogrid',
                      'selectsrc'=> 'SATEACHERS|TCHNO|FULLNAME|TCHNO',
                      );
                      



                      
$cfg['columns']['slszo'] = array(
                      'dbcol'=>'FULLNAME',
                      'title'=>'Full Name',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>2,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['1vqvn'] = array(
                      'dbcol'=>'YEARSTART',
                      'title'=>'Year From',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'SAYEAR|YEARCODE|YEARNAME|YEARCODE',
                      );
                      



                      
$cfg['columns']['y8pvo'] = array(
                      'dbcol'=>'YEAREND',
                      'title'=>'Year To',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'SAYEAR|YEARCODE|YEARNAME|YEARCODE',
                      );
                      



                      $combogrid_array   = array();
	 
$combogrid_array['clmgz']['columns']['TCHNO']  = array( 'field'=>'TCHNO', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['clmgz']['columns']['FULLNAME']  = array( 'field'=>'FULLNAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['clmgz']['source'] ='SATEACHERS';
	 
$combogrid_array['1vqvn']['columns']['YEARCODE']  = array( 'field'=>'YEARCODE', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['1vqvn']['columns']['YEARNAME']  = array( 'field'=>'YEARNAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['1vqvn']['source'] ='SAYEAR';
	 
$combogrid_array['y8pvo']['columns']['YEARCODE']  = array( 'field'=>'YEARCODE', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['y8pvo']['columns']['YEARNAME']  = array( 'field'=>'YEARNAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['y8pvo']['source'] ='SAYEAR';
  