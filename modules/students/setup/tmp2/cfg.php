<?php
/**
* auto created config file for modules/students/setup/tmp2
* @author coderX
* @todo by-me-beer
* @version 1.0
* @since 2015-01-20 21:33:16
*/

$scriptname  = end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'Tmp2';//user-readable formart
 $cfg['appname']       = 'tmp2';//lower cased one word
 $cfg['datasrc']       = 'TMP2';//where to get data
 $cfg['datatbl']       = 'TMP2';//base data src [for updates & deletes]
 $cfg['form_width']    = 400;
 $cfg['form_height']   = 230;
 
 $cfg['pkcol']         = 'ID';//the primary key
 
 $cfg['tblbns']['chk_button_add']    = 1;
 $cfg['tblbns']['chk_button_edit']   = 1;
 $cfg['tblbns']['chk_button_delete'] = 1;
 $cfg['tblbns']['chk_button_import'] = 1;
 $cfg['tblbns']['chk_button_export'] = 1;
 

$cfg['columns']['bl3os'] = array(
                      'dbcol'=>'PROVINCE',
                      'title'=>'Province',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      
$cfg['columns']['impz1'] = array(
                      'dbcol'=>'DISTRICT',
                      'title'=>'District',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      
$cfg['columns']['smo4k'] = array(
                      'dbcol'=>'DIVISION',
                      'title'=>'Division',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      
$cfg['columns']['rphkz'] = array(
                      'dbcol'=>'LOCATION',
                      'title'=>'Location',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      
$cfg['columns']['pyzoe'] = array(
                      'dbcol'=>'SUBLOCATION',
                      'title'=>'Sublocation',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      