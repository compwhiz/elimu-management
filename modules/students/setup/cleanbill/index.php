<?php

/**
 * auto created index file for modules/students/setup/cleanbill
 * @author kenmsh@gmail.com
 *
 * @version 2.0
 * @since 2019-03-30 11:09:47
 */

require_once('cfg.php');
//require_once(BASEPATH.'init.php');

$class = CLASSFILE;
$classfile = CLASSFILE . '.php';

require_once("{$classfile}");

if (!class_exists($class)) {
    die('class--' . $classfile . 'not found');
}

$_class = new $class();
$grid = new grid();

/*
$module = array();
$module['url']    =  DIR;
$module['class']  =  $class;
$module_packed    =  packvars($module);
*/

?>

<div class="easyui-layout" data-options="fit:true">
    <div data-options="region:'north',split:true" style="height:100px">
   <div>
       <label for="">Teacher: </label>
       <select id="cc" class="easyui-combobox" name="dept" style="width:200px;">
           <option value="aa">aitem1</option>
           <option>bitem2</option>
           <option>bitem3</option>
           <option>ditem4</option>
           <option>eitem5</option>
       </select>
   </div>
        <label for="Form">Form: </label>
        <select id="cc" class="easyui-combobox" name="dept" style="width:200px;">
            <option value="aa">aitem1</option>
            <option>bitem2</option>
            <option>bitem3</option>
            <option>ditem4</option>
            <option>eitem5</option>
        </select>
    </div>
    <div data-options="region:'center'">
        <table class="easyui-datagrid">
            <thead>
            <tr>
                <th data-options="field:'code'">Subject</th>
                <th data-options="field:'name'">Class</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>English</td>
                <td></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>