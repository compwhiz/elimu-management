<?php
/**
* auto created config file for modules/students/setup/cleanbill
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2019-03-30 11:09:47
*/

$scriptname  = @end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = '';//user-readable formart
 $cfg['appname']       = '';//lower cased one word
 $cfg['datasrc']       = 'sateachers';//where to get data
 $cfg['datatbl']       = 'sateachers';//base data src [for updates & deletes]
 $cfg['form_width']    = 600;
 $cfg['form_height']   = 400;
 $cfg['window_width']    = 500;
 $cfg['window_height']   = 400;
 
 $cfg['pkcol']         = 'TCHNO';//the primary key
 
 $cfg['tblbns']['chk_button_add']    = 0;
 $cfg['tblbns']['chk_button_edit']   = 0;
 $cfg['tblbns']['chk_button_delete'] = 1;
 $cfg['tblbns']['chk_button_import'] = 0;
 $cfg['tblbns']['chk_button_export'] = 1;
 
