<?php

define('MAKE_FIELDS_UNIQUE' , true);

$scriptname  = @end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'Students';//user-readable formart
 $cfg['appname']       = 'students';//lower cased
 $cfg['datasrc']       = 'SASTUDENTS';//where to get data
 $cfg['datatbl']       = 'SASTUDENTS';//base data src [for updates & deletes]
 $cfg['form_width']    = 400;
 $cfg['form_height']   = 280;
 
 $cfg['pkcol']         = 'code';//the primary key

$combogrid_array   = array();
$combogrid_array['find_admno']['columns']['ADMNO']        = array( 'field'=>'admno', 'title'=>'Admission No', 'width'=> 80 , 'isIdField' => true );
$combogrid_array['find_admno']['columns']['FULLNAME']     = array( 'field'=>'fullname', 'title'=>'Full Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['find_admno']['columns']['STREAMCODE']   = array( 'field'=>'stream', 'title'=>'Stream', 'width'=>80);
$combogrid_array['find_admno']['source'] ='VIEWSP';
	
