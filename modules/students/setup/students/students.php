<?php

  class students{
	private $id;
	private $datasrc;
	private $primary_key;
	private $admno;
	
	public function __construct(){
		global $cfg;
		
		$this->id           = filter_input(INPUT_POST , 'id');
		$this->datasrc      = valueof($cfg,'datasrc');
		$this->primary_key  = valueof($cfg,'pkcol');
	}
	
	public function data(){
		global $db;
		
		$data = array();
		/**********/
//		$db->debug=1;
		
        $admno   = filter_input(INPUT_POST, 'admno');
        $student = new ADODB_Active_Record('SASTUDENTS', array('ADMNO'));
        $student->Load("ADMNO='{$admno}' ");
        
		$data['admno']         = valueof( $student, 'admno');
		$data['find_admno']    = valueof( $student, 'admno');
		$data['salutation']    = valueof( $student, 'sltcode');
		$data['fullname']      = valueof( $student, 'fullname');
		$data['initials']      = valueof( $student, 'initials');
		$data['dob']           = valueof( $student, 'dob');
		
		$data['gender']        = valueof( $student, 'gndcode');
		$data['religion']      = valueof( $student, 'rlgcode');
		$data['etccode']       = valueof( $student, 'etccode');
		
		$data['stream']        = valueof( $student, 'streamcode');
		$data['admdate']       = valueof( $student, 'admdate');
		$data['house']         = valueof( $student, 'housecode');
		$data['dorm']          = valueof( $student, 'dormcode');
		$data['indexno']       = valueof( $student, 'indexno');
		
		$data['kcpegrade']     = valueof( $student, 'kcpegrade');
		$data['kcpepoint']     = valueof( $student, 'kcpepoint',0);
		$data['kcpemean']      = valueof( $student, 'kcpemean',0);
		$data['kcpemarks']     = valueof( $student,'kcpemarks',0);
		
		$data['kcpepoint']     = $data['kcpepoint']*1>0 ? $data['kcpepoint'] : null;
		$data['kcpemean']      = $data['kcpemean']*1>0 ? $data['kcpemean'] : null;
		$data['kcpemarks']     = $data['kcpemarks']*1>0 ? $data['kcpemarks'] : null;
		
		$data['mobilephone']   = valueof( $student, 'mobilephone');
		$data['postalcode']    = valueof( $student, 'postalcode');
		$data['address']       = valueof( $student, 'address');
		$data['residence']     = valueof( $student, 'residence');
		$email                 = valueof( $student, 'email','','emailIsValid');
		$data['email']         = filter_var( $email, FILTER_VALIDATE_EMAIL) ? $email : null;
		
		$data['medstatus']     = valueof( $student, 'medstatus');
		$data['allergy1']      = valueof( $student, 'allergy1');
		$data['allergy2']      = valueof( $student, 'allergy2');
		$data['allergy3']      = valueof( $student, 'allergy3');
		
		$_SESSION['profile_admno']      = $admno;
		
		$kcpe = $db->GetAssoc("SELECT SUBJECTCODE, GRADECODE FROM SAKCPE WHERE ADMNO='{$admno}'");
		
		if($kcpe){
		 if(sizeof($kcpe)>0){
		   foreach ($kcpe as $subjectcode => $gradecode){
		   	$data["kcpe_subjects[{$subjectcode}]"] = $gradecode;
		   }
		 }
		}
		
	  if(defined('MAKE_FIELDS_UNIQUE') && defined('MNUID')) {
		foreach($data as $k=>$v){
		  $field_id = "{$k}_".MNUID; 
		  unset($data[$k]);
		  $data[$field_id] = $v;
		}
	  }

		
		return json_encode($data);
	}
	
	private function get_admno(){
		global $db;
		
		$y = date('Y');
		$y_prev = date('Y')-1;
		$admno = $db->GetOne('SELECT ADMNO FROM SASTAN WHERE YEARCODE='.$y);
		
		if(empty($admno)){
			$admno = $db->GetOne('SELECT ADMNO FROM SASTAN WHERE YEARCODE='.$y_prev);
			
			if(!empty($admno)){
				 $admno = $admno+1;
				 $id    = generateID('SASTAN');
				 $db->Execute("INSERT INTO  SASTAN(ID,YEARCODE,ADMNO) VALUES({$id},{$y},{$admno})");
				 return $admno;
			}
		}else{
			return $admno;
		}
		
		if(empty($admno)){
			return null;
		}
		
		
	}
	
	public function upload_photo(){
		
		$photo_field_id = ui::fi('student_photo');
		
		//print_pre($photo_field_id);
		//print_pre($_FILES);
		if(!isset($_FILES[$photo_field_id])){
		 return  json_response(0,'no photo uploaded');	
		}
		
		if(!isset($_SESSION['profile_admno'])){
		 return  json_response(0,'Please Reload Student Profile');	
		}
		
		$admno    = $_SESSION['profile_admno'];
		$name     = $_FILES[$photo_field_id]['name'];
		$tmp_name = $_FILES[$photo_field_id]['tmp_name'];
		$name     = $_FILES[$photo_field_id]['name'];
		$type     = $_FILES[$photo_field_id]['type'];
		$info     = pathinfo($tmp_name);
		
		if(empty($info)){
		 return  json_response(0,'Failed to Upload File ');		
		}
		
		$photo   = ROOT.'public/photos/'.$admno.'.jpg';
		
		if(file_exists($photo)){
		 if(is_readable($photo)){
		  @unlink($photo);
		 }
		}
		
		if(!move_uploaded_file($tmp_name, $photo)){
		 @unlink($tmp_name);
		 return  json_response(0,'Upload Failed');		
		}
		
        $photo_small = ROOT.'public/photos/'.$admno.'_resized.jpg';
        
        $image = new simpleimage();
        $image->load($photo);
        $image->resize(124,130);
        $image->save($photo_small);
        $image->save($photo_small);
        
        if(unlink($photo)){
         rename($photo_small, $photo);
	    }
        
		return  json_response(1,'Photo Uploaded');	
		
	}
	
	public function photo(){
		$admno      = filter_input(INPUT_POST, 'admno');
		$photo_name = $admno.'.jpg';
		$photo      = ROOT.'public/photos/'.$photo_name;
        $rand       = mt_rand();		
        
		if(file_exists($photo) && is_readable($photo)){
		 $web_img = '<img src="./public/photos/'.$photo_name.'?cache_reload='.$rand.'" border="0">';
		 $message = "OK";
		}else{
		 $web_img = '<img src="./public/photos/profile-default.gif?cache_reload='.$rand.'" border="0">';
		 $message = "NO PHOTO";
		}
		
		return json_encode( array(
	     'success' => 1,
	     'message' => $message,
	     'photo' => $web_img,
	    ));
	 
	}
	
	public function save(){
		global $db,$cfg;

    $user              = new user();		
    $admno             = filter_input(INPUT_POST, 'admno' );
    $update_next_no    = false;
    
    if(empty($admno)){
      return json_response(0,'First Search Student');	
    }
    
    $kcpegrade_points = $db->GetAssoc("select GRADECODE,POINTS from SAKCPEGRADES ");
    	
        if(isset($_POST['kcpe_subjects'])){
			$kcpepoint_total = 0;
			$kcpepoint = 0;
			$num_subjects = sizeof($_POST['kcpe_subjects']);
    		if($num_subjects>0){
    		 foreach ($_POST['kcpe_subjects'] as $subjectcode => $gradecode){
    		  $grade_points    = valueof($kcpegrade_points, $gradecode); 
    		  $kcpepoint_total += $grade_points; 
    		 }
    		 $kcpepoint = $kcpepoint_total/$num_subjects;
    		}
        }

    $student = new ADODB_Active_Record('SASTUDENTS', array('ADMNO'));
    $student->Load("ADMNO='{$admno}' ");
    
    if(empty($student->_original)){
     $student->id       = generateID('SASTUDENTS');
     $student->admno    = $admno;
     $student->username = $admno;
     $student->password = md5($student->admno);
    }
      
    $student->fullname    = filter_input(INPUT_POST, ui::fi('fullname')); 
    $student->dob         = filter_input(INPUT_POST, ui::fi('dob')); 
    
    $student->streamcode   = filter_input(INPUT_POST, ui::fi('stream')); 
    $student->dormcode     = filter_input(INPUT_POST, ui::fi('dorm'));
    $student->housecode    = filter_input(INPUT_POST, ui::fi('house'));
    $admdate               = filter_input(INPUT_POST, ui::fi('admdate'));
    $student->admdate      = !empty($admdate) ? $admdate : date('Y-m-d');
    
    $student->sbccode      = $db->GetOne("select SBCCODE from SASBC where ISDEFAULT=1");
    $student->statuscode   = $db->GetOne("select STATUSCODE from SASTS where ISDEFAULT=1");

    $student->indexno      = filter_input(INPUT_POST, ui::fi('indexno'));
    $student->kcpemarks    = filter_input(INPUT_POST , ui::fi('kcpemarks'), FILTER_SANITIZE_STRING);
    $student->kcpemean     = filter_input(INPUT_POST , ui::fi('kcpemean'), FILTER_SANITIZE_STRING);
    $student->kcpegrade    = filter_input(INPUT_POST , ui::fi('kcpegrade'), FILTER_SANITIZE_STRING);
    //$student->kcpepoint    = filter_input(INPUT_POST , ui::fi('kcpepoint'), FILTER_SANITIZE_STRING);
    $student->kcpepoint    = $kcpepoint;
    
    $student->gndcode      = filter_input(INPUT_POST, ui::fi('gender'));
    $student->rlgcode      = filter_input(INPUT_POST, ui::fi('religion'));
    $student->etccode      = filter_input(INPUT_POST, ui::fi('etccode')); 
    
    $student->mobilephone  = filter_input(INPUT_POST, ui::fi('mobilephone'));
    $student->residence    = filter_input(INPUT_POST, ui::fi('residence'));
    $student->postalcode   = filter_input(INPUT_POST, ui::fi('postalcode'));
    $student->address      = filter_input(INPUT_POST, ui::fi('address'));
    $student->email        = filter_input(INPUT_POST, ui::fi('email') , FILTER_VALIDATE_EMAIL);
    
    $student->medstatus    = filter_input(INPUT_POST, ui::fi('medstatus'));
    $student->allergy1     = filter_input(INPUT_POST, ui::fi('allergy1'));
    $student->allergy2     = filter_input(INPUT_POST, ui::fi('allergy2'));
    $student->allergy3     = filter_input(INPUT_POST, ui::fi('allergy3'));
    
    $student->audituser    = $user->userid;
    $student->auditdate    = date('Y-m-d');
    $student->audittime    = time();

//$db->debug=1;//remove 
		//print_pre($student);//remove 
		
    if($student->Save()) {
		
    	//save subjects
    	if(isset($_POST['kcpe_subjects'])){
    		if(sizeof($_POST['kcpe_subjects'])>0){
    			$db->Execute("DELETE FROM SAKCPE WHERE ADMNO='{$student->admno}'");
    			foreach ($_POST['kcpe_subjects'] as $subjectcode => $gradecode){
    			 $db->Execute("INSERT INTO SAKCPE(ADMNO,SUBJECTCODE,GRADECODE) VALUES('{$student->admno}','{$subjectcode}','{$gradecode}')");	
    			}
    		}
    	}

    if(empty($student->kcperank)){
      self::rank_kcpe( $student->streamcode );
    }
    
     return json_response(1,'record saved');
    }else{
     $error = $db->ErrorMsg();	
     return json_response(0,"save failed : {$error}");
    }
  }
  
	public function remove(){
		global $db, $cfg;
		$admno   = filter_input(INPUT_POST , 'admno', FILTER_SANITIZE_STRING);
        $student = new ADODB_Active_Record('SASTUDENTS', array('ADMNO'));
        $student->Load("ADMNO='{$admno}' ");
        
        if(!empty($student->_original)){
		 if($student->Delete()){
		  return json_response(1,'Student Deleted');	
		 }
        }else{
		  return json_response(0,'Student Not Found');
	    }
	}
	
    private function  rank_kcpe( $streamcode ){
    global $db;
	
      	$positioning = array();

     	$data = $db->GetAssoc("
     	select ADMNO, KCPEMARKS  from SASTUDENTS
     	where STREAMCODE='{$streamcode}'
     	order by KCPEMARKS desc
     	");

     	if($data){
     	 $total_array = array();
     	 foreach ($data as $id=>$score){
     	   $total_array[$score][] = $id;
     	 }

     	 if(sizeof($total_array)>0){
     	 	$count = 0;

     	 	foreach ($total_array as $total=>$students){

     	 		$count_skipped = sizeof($students);

     	 		if(sizeof($students)==1){
     	 		 ++$count;

     	 		 foreach ($students as $row_id){
     	 		  $positioning[$row_id] = $count;
     	 		 }

     	 		}else{
     	 		 $count_continue = $count + $count_skipped;
     	 		++$count;

     	 		foreach ($students as $row_id){
     	 		 $positioning[$row_id] = $count;
     	 		}

     	 		$count = $count_continue;

     	 	  }
     	 	}
     	   }

     	   $positioning_rev = array();

     	   foreach ($positioning as $k=>$v){
     	   	$positioning_rev[$v][]=$k;
     	   }

     	   if(sizeof($positioning_rev)>0){
     	 	foreach ($positioning_rev as $position=>$student_nos){
     	 	 $student_nos_sql = "('" . implode("','" , $student_nos) . "')";
       	     @$db->Execute("UPDATE SASTUDENTS SET KCPERANK={$position} WHERE ADMNO IN {$student_nos_sql}");
       	 	}
       	   }

     	}
   }

	
}
	
