<?php

/**
* auto created config file for modules/students/setup/feesitems
* @author coderX
* @todo by-me-beer
* @version 1.0
* @since 2015-05-21 21:49:34
*/

 final class feesitems {
 private $id;
 private $datasrc;
 private $primary_key;
	
 public function __construct(){
  global $cfg;
		
     $this->id           = filter_input(INPUT_POST , 'id');
     $this->datasrc      = valueof($cfg,'datasrc');
     $this->primary_key  = valueof($cfg,'pkcol');
 }
	
 public function save(){
  global $db,$cfg;
  
   $grid = new grid();
   return $grid->grid_save_row_simple();
 }
	
 public function remove(){
  global $db, $cfg;
  
   $grid = new grid();
  return $grid->grid_remove_row();
 }
	
 	
 public function export(){
  global $db, $cfg;
  
   $grid = new grid();
  return $grid->export();
 }
 	
 public function import(){
  global $db, $cfg;
  
   $grid = new grid();
  return $grid->import();
 }
 
}
