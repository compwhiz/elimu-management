<?php
/**
* auto created config file for modules/students/setup/feesitems
* @author coderX
* @todo by-me-beer
* @version 1.0
* @since 2015-05-21 21:49:34
*/

$scriptname  = end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'Fees Items';//user-readable formart
 $cfg['appname']       = 'fees items';//lower cased one word
 $cfg['datasrc']       = 'FINITEMS';//where to get data
 $cfg['datatbl']       = 'FINITEMS';//base data src [for updates & deletes]
 $cfg['form_width']    = 510;
 $cfg['form_height']   = 230;
 $cfg['grid_width']    = 600;
 $cfg['grid_height']   = 250;
 
 $cfg['pkcol']         = 'FITEMCODE';//the primary key
 
 $cfg['tblbns']['chk_button_add']    = 1;
 $cfg['tblbns']['chk_button_edit']   = 1;
 $cfg['tblbns']['chk_button_delete'] = 1;
 $cfg['tblbns']['chk_button_import'] = 0;
 $cfg['tblbns']['chk_button_export'] = 0;
 

$cfg['columns']['e3kxg'] = array(
                      'dbcol'=>'FITEMCODE',
                      'title'=>'Item Code',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      
$cfg['columns']['zvo7g'] = array(
                      'dbcol'=>'FITEMNAME',
                      'title'=>'Item  Name',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      
$cfg['columns']['gfcl6'] = array(
                      'dbcol'=>'MUCODE',
                      'title'=>'U.O.M',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'FINMU|MUCODE|MUNAME|MUCODE',
                      );
                      
$cfg['columns']['26csk'] = array(
                      'dbcol'=>'ACCTGL',
                      'title'=>'GL A/C',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      
$cfg['columns']['rl2hv'] = array(
                      'dbcol'=>'ISTUITION',
                      'title'=>'Is tuition',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'checkbox',
                      'selectsrc'=> '',
                      );
                      