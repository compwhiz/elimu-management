<?php
/**
* auto created config file for modules/students/setup/departments
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-01-12 13:08:12
*/

$scriptname  = @end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'Departments';//user-readable formart
 $cfg['appname']       = 'departments';//lower cased one word
 $cfg['datasrc']       = 'SADEPT';//where to get data
 $cfg['datatbl']       = 'SADEPT';//base data src [for updates & deletes]
 $cfg['form_width']    = 505;
 $cfg['form_height']   = 200;
 $cfg['window_width']    = 510;
 $cfg['window_height']   = 420;
 
 $cfg['pkcol']         = 'DEPTCODE';//the primary key
 
 $cfg['tblbns']['chk_button_add']    = 1;
 $cfg['tblbns']['chk_button_edit']   = 1;
 $cfg['tblbns']['chk_button_delete'] = 1;
 $cfg['tblbns']['chk_button_import'] = 1;
 $cfg['tblbns']['chk_button_export'] = 1;
 

$cfg['columns']['h3de7'] = array(
                      'dbcol'=>'DEPTCODE',
                      'title'=>'Dept. Code',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['e3wgy'] = array(
                      'dbcol'=>'DEPTNAME',
                      'title'=>'Dept. Name',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      $combogrid_array   = array();
  