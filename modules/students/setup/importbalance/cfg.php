<?php
/**
* auto created config file for modules/students/setup/importbalance
* @author coderX
* @todo by-me-beer
* @version 1.0
* @since 2014-09-21 16:39:15
*/

$scriptname  = end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'Import Balance';//user-readable formart
 $cfg['appname']       = 'import balance';//lower cased one word
 $cfg['datasrc']       = 'FASMSFEEBALANCE';//where to get data
 $cfg['datatbl']       = 'FASMSFEEBALANCE';//base data src [for updates & deletes]
 $cfg['form_width']    = 450;
 $cfg['form_height']   = 200;
 
 $cfg['pkcol']         = 'FEECODE';//the primary key
 
 $cfg['tblbns']['chk_button_add']    = 1;
 $cfg['tblbns']['chk_button_edit']   = 1;
 $cfg['tblbns']['chk_button_delete'] = 1;
 $cfg['tblbns']['chk_button_import'] = 1;
 $cfg['tblbns']['chk_button_export'] = 1;
 

$cfg['columns']['pr1ok'] = array(
                      'dbcol'=>'FEECODE',
                      'title'=>'Code',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      
$cfg['columns']['xybo7'] = array(
                      'dbcol'=>'YEARCODE',
                      'title'=>'Year',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'SAYEAR|YEARCODE|YEARNAME|YEARCODE',
                      );
                      
$cfg['columns']['hcgsp'] = array(
                      'dbcol'=>'TERMCODE',
                      'title'=>'Term',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'SATERMS|TERMCODE|TERMNAME|TERMCODE',
                      );
                      
$cfg['columns']['nkxgs'] = array(
                      'dbcol'=>'STREAMCODE',
                      'title'=>'Stream',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'SASTREAMS|STREAMCODE|STREAMNAME|STREAMCODE',
                      );
                      
$cfg['columns']['865ik'] = array(
                      'dbcol'=>'ADMNO',
                      'title'=>'Admno',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      
$cfg['columns']['xqyiv'] = array(
                      'dbcol'=>'FEEBAL',
                      'title'=>'Balance',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      
$cfg['columns']['tzvco'] = array(
                      'dbcol'=>'DADMOBILE',
                      'title'=>'Dad Mobile',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      
$cfg['columns']['fbkzm'] = array(
                      'dbcol'=>'MUMMOBILE',
                      'title'=>'Mum Mobile',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      