<?php
/**
* auto created config file for modules/students/setup/responsibilities
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-01-07 06:53:08
*/

$scriptname  = @end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'Responsibilities';//user-readable formart
 $cfg['appname']       = 'responsibilities';//lower cased one word
 $cfg['datasrc']       = 'VIEWRTD';//where to get data
 $cfg['datatbl']       = 'SARTD';//base data src [for updates & deletes]
 $cfg['form_width']    = 550;
 $cfg['form_height']   = 230;
 $cfg['window_width']    = 500;
 $cfg['window_height']   = 400;
 
 $cfg['pkcol']         = 'TCHNO';//the primary key
 
 $cfg['tblbns']['chk_button_add']    = 1;
 $cfg['tblbns']['chk_button_edit']   = 1;
 $cfg['tblbns']['chk_button_delete'] = 1;
 $cfg['tblbns']['chk_button_import'] = 1;
 $cfg['tblbns']['chk_button_export'] = 1;
 

$cfg['columns']['p0yil'] = array(
                      'dbcol'=>'SLTCODE',
                      'title'=>'Salutation',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>2,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['cfemk'] = array(
                      'dbcol'=>'TCHNO',
                      'title'=>'Teacher',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'SATEACHERS|TCHNO|FULLNAME|TCHNO',
                      );
                      



                      
$cfg['columns']['zj3xy'] = array(
                      'dbcol'=>'FULLNAME',
                      'title'=>'Name',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>2,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['9vyvq'] = array(
                      'dbcol'=>'RTSTART',
                      'title'=>'From',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'date',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['ym69c'] = array(
                      'dbcol'=>'RTEND',
                      'title'=>'To',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'date',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['0faea'] = array(
                      'dbcol'=>'RTCODE',
                      'title'=>'Resposibility',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>3,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'SART|RTCODE|RTNAME|RTCODE',
                      );
                      



                      
$cfg['columns']['v37en'] = array(
                      'dbcol'=>'RTNAME',
                      'title'=>'Resposibility',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>2,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      $combogrid_array   = array();
	 
$combogrid_array['cfemk']['columns']['TCHNO']  = array( 'field'=>'TCHNO', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['cfemk']['columns']['FULLNAME']  = array( 'field'=>'FULLNAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['cfemk']['source'] ='SATEACHERS';
	 
$combogrid_array['0faea']['columns']['RTCODE']  = array( 'field'=>'RTCODE', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['0faea']['columns']['RTNAME']  = array( 'field'=>'RTNAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['0faea']['source'] ='SART';
  