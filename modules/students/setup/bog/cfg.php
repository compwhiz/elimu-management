<?php
/**
* auto created config file for modules/students/setup/bog
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-01-07 07:25:39
*/

$scriptname  = @end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'B.o.g';//user-readable formart
 $cfg['appname']       = 'b.o.g';//lower cased one word
 $cfg['datasrc']       = 'VIEWBOG';//where to get data
 $cfg['datatbl']       = 'SABOG';//base data src [for updates & deletes]
 $cfg['form_width']    = 400;
 $cfg['form_height']   = 320;
 $cfg['window_width']    = 500;
 $cfg['window_height']   = 410;
 
 $cfg['pkcol']         = 'MEMBERID';//the primary key
 
 $cfg['tblbns']['chk_button_add']    = 1;
 $cfg['tblbns']['chk_button_edit']   = 1;
 $cfg['tblbns']['chk_button_delete'] = 1;
 $cfg['tblbns']['chk_button_import'] = 1;
 $cfg['tblbns']['chk_button_export'] = 1;
 

$cfg['columns']['gbmhe'] = array(
                      'dbcol'=>'BOGCATCODE',
                      'title'=>'Category',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>3,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'SABOGCAT|BOGCATCODE|BOGCATNAME|BOGCATCODE',
                      );
                      



                      
$cfg['columns']['r1yp8'] = array(
                      'dbcol'=>'BOGCATNAME',
                      'title'=>'Category',
                      'width'=>120,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>2,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['oog0q'] = array(
                      'dbcol'=>'MEMBERID',
                      'title'=>'No',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['wh7ay'] = array(
                      'dbcol'=>'FULLNAME',
                      'title'=>'Name',
                      'width'=>180,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['hbvbk'] = array(
                      'dbcol'=>'SLTCODE',
                      'title'=>'Saluation',
                      'width'=>90,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>3,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'SASLT|SLTCODE|SLTNAME|SLTCODE',
                      );
                      



                      
$cfg['columns']['abgen'] = array(
                      'dbcol'=>'GNDCODE',
                      'title'=>'Gender',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>3,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'SAGND|GNDCODE|GNDNAME|GNDCODE',
                      );
                      



                      
$cfg['columns']['ylfvi'] = array(
                      'dbcol'=>'MOBILEPHONE',
                      'title'=>'Mobile',
                      'width'=>120,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['tpbzs'] = array(
                      'dbcol'=>'RESIDENCE',
                      'title'=>'Residence',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      $combogrid_array   = array();
	 
$combogrid_array['gbmhe']['columns']['BOGCATCODE']  = array( 'field'=>'BOGCATCODE', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['gbmhe']['columns']['BOGCATNAME']  = array( 'field'=>'BOGCATNAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['gbmhe']['source'] ='SABOGCAT';
	 
$combogrid_array['hbvbk']['columns']['SLTCODE']  = array( 'field'=>'SLTCODE', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['hbvbk']['columns']['SLTNAME']  = array( 'field'=>'SLTNAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['hbvbk']['source'] ='SASLT';
	 
$combogrid_array['abgen']['columns']['GNDCODE']  = array( 'field'=>'GNDCODE', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['abgen']['columns']['GNDNAME']  = array( 'field'=>'GNDNAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['abgen']['source'] ='SAGND';
  