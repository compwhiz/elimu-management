<?php
/**
* auto created config file for modules/students/setup/pta
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-01-07 07:25:25
*/

$scriptname  = @end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'Pta';//user-readable formart
 $cfg['appname']       = 'pta';//lower cased one word
 $cfg['datasrc']       = 'VIEWPTA';//where to get data
 $cfg['datatbl']       = 'SAPTA';//base data src [for updates & deletes]
 $cfg['form_width']    = 450;
 $cfg['form_height']   = 320;
 $cfg['window_width']    = 500;
 $cfg['window_height']   = 400;
 
 $cfg['pkcol']         = 'STREAMCODE';//the primary key
 
 $cfg['tblbns']['chk_button_add']    = 1;
 $cfg['tblbns']['chk_button_edit']   = 1;
 $cfg['tblbns']['chk_button_delete'] = 1;
 $cfg['tblbns']['chk_button_import'] = 1;
 $cfg['tblbns']['chk_button_export'] = 1;
 

$cfg['columns']['ij01d'] = array(
                      'dbcol'=>'PTACATCODE',
                      'title'=>'Category',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>3,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'SAPTACAT|PTACATCODE|PTACATNAME|PTACATCODE',
                      );
                      



                      
$cfg['columns']['hj3cx'] = array(
                      'dbcol'=>'PTACATNAME',
                      'title'=>'Category',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>2,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['tk7jq'] = array(
                      'dbcol'=>'STREAMCODE',
                      'title'=>'Stream',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'SASTREAMS|STREAMCODE|STREAMNAME|STREAMCODE',
                      );
                      



                      
$cfg['columns']['q3onw'] = array(
                      'dbcol'=>'MEMBERID',
                      'title'=>'Member No.',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>3,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['tnfbn'] = array(
                      'dbcol'=>'FULLNAME',
                      'title'=>'Member Name',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['bveqc'] = array(
                      'dbcol'=>'SLTCODE',
                      'title'=>'Salutation',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>3,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'SASLT|SLTCODE|SLTNAME|SLTCODE',
                      );
                      



                      
$cfg['columns']['qlplh'] = array(
                      'dbcol'=>'GNDCODE',
                      'title'=>'Gender',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>3,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'SAGND|GNDCODE|GNDNAME|GNDCODE',
                      );
                      



                      
$cfg['columns']['vgase'] = array(
                      'dbcol'=>'MOBILEPHONE',
                      'title'=>'Mobile',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['8jv13'] = array(
                      'dbcol'=>'RESIDENCE',
                      'title'=>'Residence',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      $combogrid_array   = array();
	 
$combogrid_array['ij01d']['columns']['PTACATCODE']  = array( 'field'=>'PTACATCODE', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['ij01d']['columns']['PTACATNAME']  = array( 'field'=>'PTACATNAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['ij01d']['source'] ='SAPTACAT';
	 
$combogrid_array['tk7jq']['columns']['STREAMCODE']  = array( 'field'=>'STREAMCODE', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['tk7jq']['columns']['STREAMNAME']  = array( 'field'=>'STREAMNAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['tk7jq']['source'] ='SASTREAMS';
	 
$combogrid_array['bveqc']['columns']['SLTCODE']  = array( 'field'=>'SLTCODE', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['bveqc']['columns']['SLTNAME']  = array( 'field'=>'SLTNAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['bveqc']['source'] ='SASLT';
	 
$combogrid_array['qlplh']['columns']['GNDCODE']  = array( 'field'=>'GNDCODE', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['qlplh']['columns']['GNDNAME']  = array( 'field'=>'GNDNAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['qlplh']['source'] ='SAGND';
  