<?php
/**
* auto created config file for modules/students/setup/subjects
* @author kenmsh@gmail.com
* 
* @version 2.0
* @since 2016-01-07 16:20:35
*/

$scriptname  = @end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'Subjects';//user-readable formart
 $cfg['appname']       = 'subjects';//lower cased one word
 $cfg['datasrc']       = 'VIEWSUBJECTS';//where to get data
 $cfg['datatbl']       = 'SASUBJECTS';//base data src [for updates & deletes]
 $cfg['form_width']    = 420;
 $cfg['form_height']   = 290;
 $cfg['window_width']    = 550;
 $cfg['window_height']   = 400;
 
 $cfg['pkcol']         = 'SUBJECTCODE';//the primary key
 
 $cfg['tblbns']['chk_button_add']    = 1;
 $cfg['tblbns']['chk_button_edit']   = 1;
 $cfg['tblbns']['chk_button_delete'] = 1;
 $cfg['tblbns']['chk_button_import'] = 1;
 $cfg['tblbns']['chk_button_export'] = 1;
 

$cfg['columns']['mvd5x'] = array(
                      'dbcol'=>'CATCODE',
                      'title'=>'Category',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>3,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'SASUBJCATS|CATCODE|CATNAME|CATCODE',
                      );
                      



                      
$cfg['columns']['ozdeq'] = array(
                      'dbcol'=>'CATNAME',
                      'title'=>'Category',
                      'width'=>150,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>2,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['6gw0f'] = array(
                      'dbcol'=>'SUBJECTCODE',
                      'title'=>'Code',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['2jkum'] = array(
                      'dbcol'=>'SUBJECTNAME',
                      'title'=>'Subject Name',
                      'width'=>190,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['grkim'] = array(
                      'dbcol'=>'SHORTNAME',
                      'title'=>'Short Name',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>3,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['h5fut'] = array(
                      'dbcol'=>'CODEOFFC',
                      'title'=>'Official Code',
                      'width'=>150,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['gz5up'] = array(
                      'dbcol'=>'OFFERED',
                      'title'=>'Offered',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'SAYN|YNCODE|YNNAME|YNCODE',
                      );
                      



                      
$cfg['columns']['eryfh'] = array(
                      'dbcol'=>'SORTPOS',
                      'title'=>'Sort Position',
                      'width'=>140,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>4,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'numberspinner',
                      'selectsrc'=> '',
                      );
                      



                      $combogrid_array   = array();
	 
$combogrid_array['mvd5x']['columns']['CATCODE']  = array( 'field'=>'CATCODE', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['mvd5x']['columns']['CATNAME']  = array( 'field'=>'CATNAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['mvd5x']['source'] ='SASUBJCATS';
	 
$combogrid_array['gz5up']['columns']['YNCODE']  = array( 'field'=>'YNCODE', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['gz5up']['columns']['YNNAME']  = array( 'field'=>'YNNAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['gz5up']['source'] ='SAYN';
  