<?php

/**
* auto created index file for modules/students/setup/recordattendance
* @author kenmsh@gmail.com
* 
* @version 2.0
* @since 2016-02-29 17:27:26
*/

 $school          =  new school();
 $user            =  new user();
 $years           = $db->GetAssoc('SELECT YEARCODE,YEARNAME FROM SAYEAR ORDER BY YEARCODE');
 $terms           =  $db->GetAssoc('SELECT TERMCODE,TERMNAME FROM SATERMS ORDER BY TERMCODE');
 $subjects_array  =  $db->GetAssoc("SELECT SUBJECTCODE,SUBJECTNAME FROM SASUBJECTS order by SUBJECTCODE");
 $streams_data    = $db->GetAssoc('SELECT STREAMCODE,STREAMNAME,FORMCODE FROM SASTREAMS');

 $my_subjects     =  $db->GetArray("
	SELECT SUBJECTCODE,STREAMCODE,YEARCODE FROM SATSA
	ORDER BY YEARCODE,STREAMCODE ASC
   ");

 $my_list    = array();
 $my_streams = array();
 $streams    = array();
 $subjects   = array();
 
 $forms = array();
 if(isset($streams_data)){
  if(sizeof($streams_data)>0){	
   foreach ($streams_data as $streamcode=>$stream_data){
   	$streamname = valueof($stream_data, 'STREAMNAME');
   	$formcode   = valueof($stream_data, 'FORMCODE');
   	$forms[$formcode] = "Form {$formcode}";
   	$forms[$streamcode] = "Form {$formcode}-{$streamname}";
   }
  }
 }
 

$table_id =  ui::fi('tblMarks');
?>
<style>
table#<?php echo $table_id;?> {font-size:12px;font-family:Verdana}
table.tblSubSetup {font-size:12px;font-family:Verdana}
table.tblSubSetup td.label{ background-color:#95B8E7;}
table.tblSubSetup td.cat{ background-color:#E2EDFF;}
table.tblSubSetup td.exam{ background-color:#FFE48D;}

table#<?php echo $table_id;?> a.delete { color: #555;}
table#<?php echo $table_id;?> a.delete:hover { color: #D00;}
table#<?php echo $table_id;?> a.comments { color: #555;}
table#<?php echo $table_id;?> a.comments:hover { color: #0D0;}

table#<?php echo $table_id;?> a.active-sort { color: #00F;}
table#<?php echo $table_id;?> a.active-sort:hover { color: #D00;}
table#<?php echo $table_id;?> a.inactive-sort { color: #555;}
table#<?php echo $table_id;?> a.inactive-sort:hover { color: #00F;}

td.input input {
    width: 50px;
    box-sizing: border-box;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
}

td.header {
 border-top:1px solid #999;
 border-bottom:1px dotted #999;
 padding-left: 5px;
}

td.input {
 border-bottom:1px dotted #999;
 border-right:1px dotted #999;
 padding-left: 5px;
}

td.left {
 border-left:1px dotted #999;
}

td.right {
 border-right:1px dotted #999;
}

#loading {
width: 100%;
position: absolute;
}

#pagination
{
text-align:center;
margin-left:100px;

}
li.nav{
list-style: none;
float: left;
margin-left: 5px;
padding:4px;
border:solid 1px #dddddd;
color:#0063DC;
font-size:12px;
}
li.nav:hover
{
color:#FF0084;
cursor: pointer;
}

li.pagination li.page-active{
	border: 4px solid #c2e1f5;
}

a.marks-menu{
text-decoration:none;
padding:4px;
}
</style>


    <div id="content<?php echo MNUID; ?>"  class="easyui-panel" title="" fit="true" >
	  <form id="<?php echo ui::fi('ff'); ?>" method="post" onsubmit="return false;">
	  <table cellpadding="2" cellspacing="0" width="100%">

	  <tr>
			<td width="180px">Year:</td>
			<td>
			 <?php echo ui::form_select_fromArray('yearcode', $years, $school->active_yearcode," onchange=\"\" ");  ?>
			</td>
		</tr>

		<tr>
			<td>Term:</td>
			<td>
			<?php echo ui::form_select_fromArray('termcode', $terms, $school->active_termcode,"onchange=\"\"");  ?>
			</td>
		</tr>

		<tr>
			<td >Stream</td>
			<td>
			 <div id="div_streams<?php echo MNUID; ?>">
			 <?php echo ui::form_select_fromArray('streamcode', $forms,'',"onchange=\"\"",'',false,200);  ?>
			 </div>
			</td>
		</tr>

        <tr>
	    <td>Date:</td>
	    <td><?php echo ui::form_input( 'text', 'attdate', 15, '', '', '', 'data-options="required:true,formatter:dateYmdFormatter,parser:dateYmdParser" placeholder="YYYY-MM-DD"', '', 'easyui-datebox' , ""); ?></td>
	    </tr>
	    
		<tr>
			<td>&nbsp;</td>
			<td>
			 <a href="javascript:void(0)" class="easyui-linkbutton" onclick="<?php echo $cfg['appname']; ?>.print_report();"><i class="fa fa-print"></i> Open Report</a>
			</td>
		</tr>

	 </table>
	</form>
  </div>

    <script>

   var <?php echo $cfg['appname']; ?> = {
     load_stage:function (){
	   ui.cc('load_stage', 'modvars=<?php echo $vars; ?>', 'content<?php echo MNUID; ?>');
     },
     clearForm:function (){
		$('#<?php echo ui::fi('ff'); ?>').form('clear');
	 },
     print_report:function (r){
  	  var w=window.open('./endpoints/print/?modvars=<?php echo $vars; ?>&'+$('#<?php echo ui::fi('ff'); ?>').serialize(),'<?php echo ui::fi('pw'); ?>','height=800,width=830,toolbar=no,menubar=no,directories=no,location=no,scrollbars=yes,status=no,resizable=no,fullscreen=no,top=0,left=0');
          w.focus();
     },
	}

</script>
