<?php
/**
* auto created config file for modules/students/setup/recordattendance
* @author kenmsh@gmail.com
* 
* @version 2.0
* @since 2016-02-29 17:27:26
*/


if(!defined('MAKE_FIELDS_UNIQUE')){
 define('MAKE_FIELDS_UNIQUE' , true);
}

$scriptname  = @end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'Record Attendance';//user-readable formart
 $cfg['appname']       = 'recordattendance';//lower cased one word
 $cfg['datasrc']       = '';//where to get data
 $cfg['datatbl']       = '';//base data src [for updates & deletes]
 $cfg['form_width']    = 400;
 $cfg['form_height']   = 200;
 $cfg['window_width']    = 600;
 $cfg['window_height']   = 500;
 
 $cfg['pkcol']         = '0';//the primary key
 
