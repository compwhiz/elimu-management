<?php

/**
* auto created index file for modules/students/attendance/reports/attendancereport
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-03-01 09:07:56
*/

 $school          =  new school();
 $user            =  new user();
 $years           = $db->GetAssoc('SELECT YEARCODE,YEARNAME FROM SAYEAR ORDER BY YEARCODE');
 $terms           =  $db->GetAssoc('SELECT TERMCODE,TERMNAME FROM SATERMS ORDER BY TERMCODE');
 $subjects_array  =  $db->GetAssoc("SELECT SUBJECTCODE,SUBJECTNAME FROM SASUBJECTS order by SUBJECTCODE");
 $streams_data    = $db->GetAssoc('SELECT STREAMCODE,STREAMNAME,FORMCODE FROM SASTREAMS');

 $my_subjects     =  $db->GetArray("
	SELECT SUBJECTCODE,STREAMCODE,YEARCODE FROM SATSA
	ORDER BY YEARCODE,STREAMCODE ASC
   ");

 $my_list    = array();
 $my_streams = array();
 $streams    = array();
 $subjects   = array();
 
 $forms = array();
 if(isset($streams_data)){
  if(sizeof($streams_data)>0){	
   foreach ($streams_data as $streamcode=>$stream_data){
   	$streamname = valueof($stream_data, 'STREAMNAME');
   	$formcode   = valueof($stream_data, 'FORMCODE');
   	$forms[$formcode] = "Form {$formcode}";
   	$forms[$streamcode] = "Form {$formcode}-{$streamname}";
   }
  }
 }
 
$attdate      = isset($_SESSION['att']['attdate']) ? $_SESSION['att']['attdate'] : date('Y-m-d');
$table_id =  ui::fi('tblMarks');
?>
<style>
table#<?php echo $table_id;?> {font-size:12px;font-family:Verdana}
table.tblSubSetup {font-size:12px;font-family:Verdana}
table.tblSubSetup td.label{ background-color:#95B8E7;}
table.tblSubSetup td.cat{ background-color:#E2EDFF;}
table.tblSubSetup td.exam{ background-color:#FFE48D;}

table#<?php echo $table_id;?> a.delete { color: #555;}
table#<?php echo $table_id;?> a.delete:hover { color: #D00;}
table#<?php echo $table_id;?> a.comments { color: #555;}
table#<?php echo $table_id;?> a.comments:hover { color: #0D0;}

table#<?php echo $table_id;?> a.active-sort { color: #00F;}
table#<?php echo $table_id;?> a.active-sort:hover { color: #D00;}
table#<?php echo $table_id;?> a.inactive-sort { color: #555;}
table#<?php echo $table_id;?> a.inactive-sort:hover { color: #00F;}

td.input input {
    width: 50px;
    box-sizing: border-box;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
}

td.header {
 border-top:1px solid #999;
 border-bottom:1px dotted #999;
 padding-left: 5px;
}

td.input {
 border-bottom:1px dotted #999;
 border-right:1px dotted #999;
 padding-left: 5px;
}

td.left {
 border-left:1px dotted #999;
}

td.right {
 border-right:1px dotted #999;
}

#loading {
width: 100%;
position: absolute;
}

#pagination
{
text-align:center;
margin-left:100px;

}
li.nav{
list-style: none;
float: left;
margin-left: 5px;
padding:4px;
border:solid 1px #dddddd;
color:#0063DC;
font-size:12px;
}
li.nav:hover
{
color:#FF0084;
cursor: pointer;
}

li.pagination li.page-active{
	border: 4px solid #c2e1f5;
}

a.marks-menu{
text-decoration:none;
padding:4px;
}
</style>


    <div id="content<?php echo MNUID; ?>"  class="easyui-panel" title="" fit="true" >
	  <form id="<?php echo ui::fi('ff'); ?>" method="post" onsubmit="return false;">
	  <table cellpadding="2" cellspacing="0" width="100%">

	  <tr>
			<td width="180px">Year:</td>
			<td>
			 <?php echo ui::form_select_fromArray('yearcode', $years, $school->active_yearcode," onchange=\"\" ");  ?>
			</td>
		</tr>

		<tr>
			<td>Term:</td>
			<td>
			<?php echo ui::form_select_fromArray('termcode', $terms, $school->active_termcode,"onchange=\"\"");  ?>
			</td>
		</tr>

		<tr>
			<td >Stream</td>
			<td>
			 <div id="div_streams<?php echo MNUID; ?>">
			 <?php echo ui::form_select_fromArray('streamcode', $forms,'',"onchange=\"\"",'',false,200);  ?>
			 </div>
			</td>
		</tr>

        <tr>
	    <td>Date:</td>
	    <td><?php echo ui::form_input( 'text', 'attdate', 15, '', '', '', 'data-options="required:true,formatter:dateYmdFormatter,parser:dateYmdParser" placeholder="YYYY-MM-DD"', '', 'easyui-datebox' , ""); ?></td>
	    </tr>
	    
		<tr>
			<td>&nbsp;</td>
			<td>
			 <button onclick="<?php echo $cfg['appname']; ?>.preload();return false;"><i class="fa check-square"></i> Enter Attendance</button>
			</td>
		</tr>

	 </table>
	</form>
  </div>

    <div id="pager" ></div>
    <div id="loading" ></div>

    <div id="dlg<?php echo MNUID; ?>" class="easyui-dialog" title="Subject Setup" data-options="iconCls:'icon-save'" closed="true" style="width:550px;height:320px;padding:10px">
		<div id="div_setup<?php echo MNUID; ?>"></div>
	</div>


	<div id="<?php echo ui::fi('dlg_comments'); ?>" class="easyui-dialog" style="width:350px;height:160px;padding:2px" closed="true" buttons="#<?php echo ui::fi('dlg-buttons'); ?>">
	 <form id="<?php echo ui::fi('ff_comments') ?>" method="post" novalidate onsubmit="return false;">
	 <input type="hidden" id="id" name="id" >
	 <input type="hidden" id="status" name="status" >
	 <input type="hidden" id="admno" name="admno" >
	 <textarea name="comments" id="comments" cols="35" rows="3" ></textarea>
	 </form>
	</div>

	<div id="<?php echo ui::fi('dlg-buttons'); ?>">
		<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="<?php echo $cfg['appname']; ?>.save_attendance_with_comments();" >Save</a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#<?php echo ui::fi('dlg_comments'); ?>').dialog('close')">Cancel</a>
	</div>

    <script>
   var <?php echo $cfg['appname']; ?> = {
   load_stage:function (){
	   ui.cc('load_stage', 'modvars=<?php echo $vars; ?>', 'content<?php echo MNUID; ?>');
   },
   list_streams:function (){
    $.post('./endpoints/crud/',   'modvars=<?php echo $vars; ?>&function=list_streams&'+$('#<?php echo ui::fi('ff'); ?>').serialize()   , function( html ){
	$('#div_subjects<?php echo MNUID; ?>').html('&nbsp;');
  	$('#div_streams<?php echo MNUID; ?>').html(html);
  	});
   },
   list_subjects:function (){
    $.post('./endpoints/crud/',   'modvars=<?php echo $vars; ?>&function=list_subjects&'+$('#<?php echo ui::fi('ff'); ?>').serialize()   , function( html ){
  	$('#div_subjects<?php echo MNUID; ?>').html(html);
  	});
   },
   preload:function (){
  	    var yearcode   = $('#<?php echo ui::fi('yearcode'); ?>').val();
  	    var termcode   = $('#<?php echo ui::fi('termcode'); ?>').val();
  	    var streamcode = $('#<?php echo ui::fi('streamcode'); ?>').val();
  	    var attdate    = $('#<?php echo ui::fi('attdate'); ?>').datebox('getValue');
  	    $.messager.progress();
  	    $('#content<?php echo MNUID; ?>').panel({title:"Form "+streamcode+" Date "+attdate+" .Year "+yearcode+" Term "+termcode+""})
  		 $.post('./endpoints/crud/',   'modvars=<?php echo $vars; ?>&function=preload&'+ $('#<?php echo ui::fi('ff'); ?>').serialize()  , function( pager_content ){
  		 	$.messager.progress('close');
  		 	<?php echo $cfg['appname']; ?>.open_subject();
  		 });
    },
    open_subject:function (){
 	 $("#content<?php echo MNUID; ?>").load("./endpoints/crud/?modvars=<?php echo $vars; ?>&function=data&rows=10&page=1");
 	 $('#button_print').linkbutton('enable');
     },
     paginate:function (lid,pageNum,sortcol,sortorder){
		$("#content<?php echo MNUID; ?>").load("./endpoints/crud/?modvars=<?php echo $vars; ?>&function=data&sortcol="+sortcol+"&sortorder="+sortorder+"&rows=10&page=" + pageNum);
     },
     pagesort:function (sortcol,sortorder,pageNum){
		$("#content<?php echo MNUID; ?>").load("./endpoints/crud/?modvars=<?php echo $vars; ?>&function=data&sortcol="+sortcol+"&sortorder="+sortorder+"&rows=10&page=" + pageNum);
     },
     print_subject:function (r){
  	  var w=window.open('./endpoints/print/?modvars=<?php echo $vars; ?>&r='+r,'<?php echo ui::fi('pw'); ?>','height=800,width=830,toolbar=no,menubar=no,directories=no,location=no,scrollbars=yes,status=no,resizable=no,fullscreen=no,top=0,left=0');
          w.focus();
     },
	  makeTableNav:function (){
         $('#tbl').tableNav();
         return $('#tbl input').eq(0).click();
	  },
      clearForm:function (){
		$('#<?php echo ui::fi('ff'); ?>').form('clear');
	   },
	  save_attendance:function (admno,name,select_id){
		     var status = $('#'+select_id+'_<?php echo MNUID; ?>').val();
		      if(status==3){
               $('#<?php echo ui::fi('dlg_comments'); ?>').dialog({
                title: 'Why was '+name+' Absent?',
                closed: false,
                resizable: false,
                modal: true,
               });
               $('input#status').val(status);
               $('input#admno').val(admno);
			  }else{
			  var fdata = 'admno='+ admno+'&status='+ status + '&modvars=<?php echo $vars; ?>&function=save';
		      $.post('./endpoints/crud/', fdata, function(data) {

               if (data.success === 1) {
                 //ok
                } else {
				 $.messager.alert('Error',data.message,'error');
             }
            }, "json");
		   }
		},
	  save_attendance_with_comments:function ( ){
		      var comment = $('#<?php echo ui::fi('comments'); ?>').val();
		      if(comments==''){
				$.messager.alert('Error','Enter Reason for Absence','error');
               return;
		      }
			  var fdata  =  $('#<?php echo ui::fi('ff_comments'); ?>').serialize() + '&modvars=<?php echo $vars; ?>&function=save';
		      $.post('./endpoints/crud/', fdata, function(data) {
                $('#<?php echo ui::fi('dlg_comments'); ?>').dialog('close');
               if (data.success === 1) {
                 $('#<?php echo ui::fi('comments'); ?>').val('');
                } else {
				 $.messager.alert('Error',data.message,'error');
             }
            }, "json");
		},
	}

</script>
