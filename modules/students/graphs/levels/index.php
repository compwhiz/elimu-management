<?php

require_once('cfg.php');

$class     = CLASSFILE;
$classfile = CLASSFILE.'.php';

require_once("{$classfile}");	

 if(!class_exists($class)){
 	die('class--' .$classfile. 'not found');
 }
 
 $_class = new $class();
 $grid   = new grid();
 
 $module = array();
 
 $module['url']    =  DIR;
 $module['class']  =  $class;
 $module_packed    =  packvars($module);
 
 $data_key = $db->GetAssoc("SELECT GNDCODE, GNDNAME FROM ACGND");
 $stats = $db->GetArray("
SELECT  LVLNAME,GNDCODE, COUNT(DISTINCT(REGNO)) NUM 
FROM VIEWSP
GROUP BY LVLNAME,GNDCODE
ORDER BY LVLNAME,COUNT(DISTINCT(REGNO)) DESC
  ");
 
 $data        = array();
 $keys        = array();
// print_pre($stats);
 if(count($stats)>0){
 	foreach ($stats as $stat){
 		
 		$lvlname  = valueof($stat,'LVLNAME',"NA*");
 		$gndcode  = valueof($stat,'GNDCODE');
 		$num      = valueof($stat,'NUM');
 		
// 		echo "\$gndcode={$gndcode} <br>";
// 		echo "\$lvlname={$lvlname} <br>";
// 		echo "\$num={$num} <br>";
 		
 		$data[$lvlname][$gndcode] = $num;
 		$keys[$gndcode] =$gndcode;
// 		 print_pre($data);exit();
 	}
 }
 
// print_pre($keys);
// print_pre($data);

 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <link rel="stylesheet" type="text/css" href="../../public/css/themes/default/easyui.css">
	<script type="text/javascript" src="../../public/js/jquery.min.js"></script>
<script>
$(function () {
    $('#container').highcharts({
        data: {
            table: document.getElementById('datatable')
        },
        chart: {
            type: 'column'
        },
        title: {
            text: 'Population by Level of Study'
        },
        yAxis: {
            allowDecimals: false,
            title: {
                text: 'Units'
            }
        },
        tooltip: {
            formatter: function() {
                return '<b>'+ this.series.name +'</b><br/>'+
                    this.point.y +' '+ this.point.name.toLowerCase();
            }
        }
    });
});
</script>
<style>
#datatable th {
 border-bottom:1px solid #ccc;
}
#datatable th {
font-size:12px;
}
#datatable td {
font-size:12px;
border-bottom:1px solid #ccc;
padding:4px;
}

</style>
</head>
<body>

<script src="../../public/js/highcharts/highcharts.js"></script>
<script src="../../public/js/highcharts/modules/data.js"></script>
<script src="../../public/js/highcharts/modules/exporting.js"></script>
<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

<table id="datatable">
	<thead>
		<tr>
			<th></th>
			<?php
			 foreach ($keys as $key){
			 	echo "<th>{$key}</th>";
			 }
			?>
		</tr>
	</thead>
	<tbody>
		<?php
      foreach ($data as $k=>$v){
	
	   echo "<tr>";
	   echo "<th>{$k}</th>";
	   
	    foreach ($keys as $key){
		echo "<td>".valueof($v,$key,0)."</td>";
		}
		
	  echo "</tr>";
	 
    }
	?>	
	</tbody>
</table>
<?php
if(sizeof($data_key)>0){
	foreach ($data_key as $k=>$v){
		echo "{$k}:{$v}<br>";
	}
}
?>
</body>
</html>