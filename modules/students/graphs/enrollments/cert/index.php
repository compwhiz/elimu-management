<?php

require_once('cfg.php');

$class     = CLASSFILE;
$classfile = CLASSFILE.'.php';

require_once("{$classfile}");	

 if(!class_exists($class)){
 	die('class--' .$classfile. 'not found');
 }
 
 $_class = new $class();
 $grid   = new grid();
 
 $module = array();
 
 $module['url']    =  DIR;
 $module['class']  =  $class;
 $module_packed    =  packvars($module);
 
 
 $data = array();
 $cohorts = array();
 $cohorts_plain = array();
 $x = array();
// $db->debug=1;
 
 $data_raw = $db->GetArray("
	SELECT DISTINCT PRGCODE, PRGNAME,COHORTCODE, COUNT(DISTINCT(REGNO)) NUM 
	FROM VIEWSP
	WHERE LVCODE='0'
	GROUP BY PRGCODE, PRGNAME,COHORTCODE
	ORDER BY COUNT(DISTINCT(REGNO)) DESC
  ");
 
// print_pre($data_raw);
 
 if(sizeof($data_raw)>0){
 	
 	$count = 1;
 	$max = 20;
 	
 	foreach ($data_raw as $index => $data_part){
 		
 		if($count<=$max){
 			$data[$index] = $data_part;
 		}
 		
 	 ++$count;	
 	}
 	
// 	print_pre($data);exit();
 	
 	foreach ($data as $data_part){
 		$prgcode     = valueof($data_part,'PRGCODE');
 		$prgdesc     = valueof($data_part,'PRGNAME');
 		$cohortcode  = valueof($data_part,'COHORTCODE');
 		$num         = valueof($data_part,'NUM');
 		
 		$cohorts[$cohortcode][] = array($prgcode,$num);
 		$x[$prgcode][$cohortcode] =$num;
 		$cohorts_plain[$cohortcode] = $cohortcode;
 	}
 }
// print_pre($cohorts);
ksort($cohorts_plain);
// print_pre($cohorts_plain);
// print_pre($x);
 
 if(sizeof($cohorts_plain)>0 && sizeof($x)>0){
 	foreach ($cohorts_plain as $cohortcode){
 		foreach ($x as $progcode=>$prog_cohorts){
// 		  	echo "\$progcode={$progcode} <br>";
// 		  	print_pre($prog_cohorts);
 		  	//exit();
 		  	
 		  	if(!array_key_exists($cohortcode, $prog_cohorts)){
 		  		$x[$progcode][$cohortcode] = 0;
 		  	}
 		  	krsort($x[$progcode]);
 		  	
 		  	
 		}
 	}
 }
//  print_pre($x);
// exit();
 
  $series = array();
  
  foreach ($x as $prcode =>$vars){
//  	echo "\$prcode={$prcode} <br>";
//  	print_pre($vars);
  	
  	$series[] = "{
                name: '{$prcode}',
                data: [". implode(',', $vars)."]
            }";
  }
  
//  print_pre($series);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style>
th {
 border-bottom:1px solid #ccc;
}
th {
font-size:12px;
}
td {
font-size:12px;
border-bottom:1px solid #ccc;
padding:4px;
}
</style>
    <link rel="stylesheet" type="text/css" href="../../public/css/themes/default/easyui.css">
	<script type="text/javascript" src="../../public/js/jquery.min.js"></script>
<script>
$(function () {
        $('#container').highcharts({
            chart: {
                type: 'line'
            },
            title: {
                text: 'Enrolments'
            },
            subtitle: {
                text: 'upto <?php echo date('Y'); ?>'
            },
            xAxis: {
                categories: [<?php  echo "'" . implode("','", $cohorts_plain ). "'" ?>]
            },
            yAxis: {
                title: {
                    text: 'Students'
                }
            },
            tooltip: {
                valueSuffix: 'Students'
            },
            plotOptions: {
                line: {
                    dataLabels: {
                        enabled: true
                    },
                    enableMouseTracking: true
                }
            },
            credits: {
                enabled: false
            },
            series: [<?php echo implode(',', $series); ?>]
        });
    });
</script>
</head>
<body>

<script src="../../public/js/highcharts/highcharts.js"></script>
<script src="../../public/js/highcharts/modules/exporting.js"></script>
<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
<?php
$data_key = $db->GetAssoc("SELECT PRGCODE, PRGNAME FROM VIEWSP WHERE LVCODE='0'");

if(sizeof($data_key)>0){
	echo '<table style="font-size:12px;cell-padding:4px">';
	 echo "<tr>";
	    echo "<th>Key</th>";
	    echo "<th>Value</th>";
	   echo "</tr>"; 
	foreach ($data_key as $k=>$v){
	   echo "<tr>";
	    echo "<td >{$k}</td>";
	    echo "<td>{$v}</td>";
	   echo "</tr>"; 
	}
	echo '</table>';
}
?>
</body>
</html>