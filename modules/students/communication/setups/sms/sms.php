<?php


/**
* auto created config file for modules/students/communication/setups/sms
* @author kenmsh@gmail.com
* 
* @version 2.0
* @since 2016-03-08 09:07:46
*/

 final class sms {
 private $id;
 private $datasrc;
 private $primary_key;
	
 public function __construct(){
  global $cfg;
		
     $this->id           = filter_input(INPUT_POST , 'id');
     $this->datasrc      = valueof($cfg,'datasrc');
     $this->primary_key  = valueof($cfg,'pkcol');
 }
	
 public function list_params(){
  global $db,$cfg;
  $gateway         =  filter_input(INPUT_POST , ui::fi('gateway'));
  $gateway_file    =  (ROOT .'classes/sms/'.$gateway);
  $replace         = array('.php','class.');
  $className       = str_replace($replace,'',$gateway);
  $methodName      = '__construct';

  if(empty($gateway)) die ("Select a Gateway");
  
  if(!is_readable($gateway_file)) die ("SMS Gateway File <b>'{$gateway_file}'</b> does not exist or is not readable!");
  
  require_once($gateway_file);

  if(!class_exists($className)) echo ("SMS Gateway Class <b>'{$className}'</b> does not exist in File <b>'{$gateway_file}'</b>");

 $r                          = new ReflectionMethod($className, $methodName);
 $NumberOfRequiredParameters = $r->getNumberOfRequiredParameters();
 $params                     = $r->getParameters();

$setup    =  $db->GetRow("select * from SASMSCFG WHERE GATEWAY='{$gateway}' ");

$setup_params   = valueof($setup, 'PARAMS'); 
$setup_isactive = valueof($setup, 'ISACTIVE');
$isactive_checked = $setup_isactive ==1 ? "checked" : '';
$isactive_id    = ui::fi('isactive');
$saved_params   = array(); 

if(!empty($setup_params))
{
 $saved_params =   unserialize( base64_decode($setup_params) );
}

 echo '<table cellpadding="0" cellspacing="0" width="100%" border="0" >';

 echo '<tr>';
 echo '<td width="120px"><b>Param</b></td>';
 echo '<td><b>Value</b></td>';
 echo '</tr>';

 foreach ($params as $param) {

    $paramName       = $param->getName();
    $paramisOptional = $param->isOptional();
    $value           = valueof($saved_params,  $paramName); 
    $textbox_id      = "param[{$paramName}]";
    $textbox_type    = strstr($paramName,'password') ? 'password' : 'text';
    $textbox         = "<input type=\"{$textbox_type}\" value=\"{$value}\" id=\"{$textbox_id}\" name=\"{$textbox_id}\" size=\"25\" autocomplete=\"off\" />";
    
	echo '<tr>';
	 echo '<td>'.$paramName.'</td>';
	 echo '<td>'.$textbox.'</td>';
	echo '</tr>';

 }
 
    echo '<tr>';
	 echo '<td>Default Gateway</td>';
	 echo '<td><input type="checkbox" id="'.$isactive_id.'" name="'.$isactive_id.'" value="1" '.$isactive_checked.'> <label for="copy_cats">is Default?</label></td>';
	echo '</tr>';

 echo '<tr>';
 echo '<td>&nbsp;</td>';
 echo '<td>&nbsp;</td>';
 echo '</tr>';

 echo '<tr>';
 echo '<td>&nbsp;</td>';
 echo '<td><button onclick="'.$cfg['appname'].'.save();return false;"> Save</button></td>';
 echo '</tr>';

 echo '</table>';
  
 }
	
	
 public function save(){
  global $db,$cfg;

  if(isset($_POST['param']) && sizeof($_POST['param'])>0)
  {

  $gateway       =  filter_input(INPUT_POST , ui::fi('gateway'));
  $params        =  base64_encode( serialize($_POST['param']) );
  $isactive      = filter_input(INPUT_POST , ui::fi('isactive'));
  $user          =  new user();

  if($isactive==1){
   $db->Execute("update SASMSCFG set ISACTIVE=null ");
  }
  
  $record        =  new ADODB_Active_Record('SASMSCFG', array('GATEWAY'));
    
    $record->Load("
    GATEWAY='{$gateway}'
    ");
    
    if(empty($record->_original)){
	 $record->id         = generateID($record->_table);
     $record->gateway    = $gateway;
    }

    $record->params      = $params;

    $record->isactive    = $isactive;
    
    $record->audituser   = $user->userid;
    $record->auditdate   = date('Y-m-d');
    $record->audittime   = time();
    	
    $save   = $record->Save();

  if($save){
   return json_response(1,'SMS Settings Saved');
  }else{
   return json_response(0,'Save Failed.Try later');
  }
  
  }  

  //return self::list_params();
  
 }

}
