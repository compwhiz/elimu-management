<?php

/**
* auto created config file for modules/students/communication/communicate/singlesms
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-03-23 08:34:00
*/

 final class singlesms {
 private $id;
 private $datasrc;
 private $primary_key;
	
 public function __construct(){
  global $cfg;
		
     $this->id           = filter_input(INPUT_POST , 'id');
     $this->datasrc      = valueof($cfg,'datasrc');
     $this->primary_key  = valueof($cfg,'pkcol');
 }
	
 
	public function data(){
		global $db;
		
		$data = array();
		/**********/
		
        $admno   = filter_input(INPUT_POST, 'admno');
        $student = new ADODB_Active_Record('SASTUDENTS', array('ADMNO'));
        $student->Load("ADMNO='{$admno}' ");
        
		$data['fullname']      = valueof( $student, 'fullname');
		$data['dob']           = valueof( $student, 'dob');
		
		$data['gender']        = valueof( $student, 'gndcode');
		
		$data['stream']        = valueof( $student, 'streamcode');
		$data['house']         = valueof( $student, 'housecode');
		$data['dorm']          = valueof( $student, 'dormcode');
		
		$data['mobilephone']   = valueof( $student, 'mobilephone');
		$email                 = valueof( $student, 'email','','emailIsValid');
		$data['email']         = filter_var( $email, FILTER_VALIDATE_EMAIL) ? $email : null;

		  if(defined('MAKE_FIELDS_UNIQUE') && defined('MNUID')) {
			foreach($data as $k=>$v){
			  $field_id = "{$k}_".MNUID; 
			  unset($data[$k]);
			  $data[$field_id] = $v;
			}
		  }
		
		return json_encode($data);
	}
	
 

  public function send(){
   global $db;
   
	$message      =  filter_input(INPUT_POST , 'sms_single', FILTER_SANITIZE_STRING);
	$message      =  centerTrim($message);
	$admno        =  filter_input(INPUT_POST , ui::fi('find_admno'), FILTER_SANITIZE_STRING);
     
    if(empty($message)){
      return json_response(0, 'Enter SMS Message');
	}
	
//	if(strlen($message)>160){
//      return json_response(0, 'SMS is too long');
//	}
     
    if(empty($admno)){
      return json_response(0, 'Select Student');
	}

    $SMSGateway   =  new sms();
    
    if(!empty($SMSGateway->error)){
      return json_response(0, $SMSGateway->error);
	}

    $student      =  new student( $admno );
    
    if(empty($student->admno)){
      return json_response(0, 'Student Not Found');
	}

    if(empty($student->mobilephone)){
      return json_response(0, 'Student Mobile Phone Not Found');
	}

    if(strlen($student->mobilephone)<10){
      return json_response(0, 'Student Mobile Phone Not Valid');
	}

	$results    = $SMSGateway->sendMessage( $student->mobilephone, $message);

	return json_response(1, "Sent SMS to {$student->mobilephone}");

  }
    
 }
