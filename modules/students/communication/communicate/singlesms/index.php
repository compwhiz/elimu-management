<?php

/**
* auto created index file for modules/students/communication/communicate/singlesms
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-03-22 08:38:00
*/

 
?>

	<form id="<?php echo ui::fi('ff'); ?>" method="post" novalidate>
		<table cellpadding="2" cellspacing="0" width="100%">
		    <tr>
			 <td width="30%">&nbsp;</td>	
			 <td>&nbsp;</td>	
	    	</tr>
	    	
			<tr>
	    	 <td>Student</td>
	    	 <td><?php echo ui::form_input( 'text', 'find_admno', 20, '', '', '', '', '', '' , ""); ?></td>
	    	</tr>

	    	<tr>
			 <td>Class</td>	
			 <td><?php echo ui::form_input( 'text', 'stream', 20, '', '', '', 'readonly disabled', '', '' , ""); ?></td>	
	    	</tr>
	    	
	    	<tr>
			<td>Phone</td>	
			 <td><?php echo ui::form_input( 'text', 'mobilephone', 20, '', '', '', 'readonly disabled', '', '' , ""); ?></td>	
	    	</tr>
	    	
	    	<tr>
			<td>Email</td>	
			 <td><?php echo ui::form_input( 'text', 'email', 20, '', '', '', 'readonly disabled', '', '' , ""); ?></td>	
	    	</tr>
	    	
		    <tr>
			 <td>SMS:</td>
			 <td><textarea name="sms_single" id="sms_single" cols="40"  rows="4"   class=""></textarea></td>
			</tr>
			
		    <tr>
			 <td>&nbsp;</td>
			 <td>
			 <a href="javascript:void(0)"  id="btnSendSMSSingle" class="easyui-linkbutton" onclick="<?php echo $cfg['appname']; ?>.send();" style="min-height:30px;vertical-align:center" disabled ><i class="fa fa-paper-plane"></i> Send</a>
			 </td>
			</tr>
			
		</table>
     </form>
   
	<script>
		
		var <?php echo $cfg['appname']; ?> = {
		clearForm:function (){
			$('#<?php echo ui::fi('ff'); ?>').form('clear');
		},
		loadProfile:function (){
		  var admno = $('#<?php echo ui::fi('find_admno'); ?>').combogrid('getValue');
		  var rdata = 'admno='+admno+'&modvars=<?php echo $vars; ?>&function=data';	
		   $.post('./endpoints/crud/', rdata, function(data) {
             $('#<?php echo ui::fi('ff'); ?>').form('load',data);
           }, "json");
	      $('#btnSendSMSSingle').linkbutton('enable');
		},
		send:function (){
		 var fdata = $('#<?php echo ui::fi('ff'); ?>').serialize()  + '&modvars=<?php echo $vars; ?>&function=send';
		 $.messager.progress();
		 $.post('./endpoints/crud/', fdata, function(data) {
		       $.messager.progress('close');
		       $('#btnSendSMSSingle').linkbutton('enable');
		       if (data.success === 1) {
                 $.messager.alert('Status',data.message,'info');
                 <?php echo $cfg['appname']; ?>.clearForm();
               } else {
                $.messager.alert('Error',data.message,'error');
               }
         }, "json");
		}
	}

	<?php
      echo ui::ComboGrid($combogrid_array,'find_admno',"{$cfg['appname']}.loadProfile");
	?>
			
	</script>
