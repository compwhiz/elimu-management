<?php
/**
* auto created config file for modules/students/communication/communicate/outbox
* @author kenmsh@gmail.com
* 
* @version 2.0
* @since 2017-02-19 07:08:14
*/

$scriptname  = @end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter   = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/';

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter);

$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] );

 $cfg                  = array();
 $cfg['apptitle']      = 'Out Box';//user-readable formart
 $cfg['appname']       = 'outbox';//lower cased one word
 $cfg['datasrc']       = 'SYSQSMS';//where to get data
 $cfg['datatbl']       = 'SYSQSMS';//base data src [for updates & deletes]
 $cfg['form_width']    = 400;
 $cfg['form_height']   = 200;
 $cfg['window_width']    = 700;
 $cfg['window_height']   = 400;

 $cfg['pkcol']         = 'ID';//the primary key

 $cfg['tblbns']['chk_button_add']    = 0;
 $cfg['tblbns']['chk_button_edit']   = 1;
 $cfg['tblbns']['chk_button_delete'] = 0;
 $cfg['tblbns']['chk_button_import'] = 0;
 $cfg['tblbns']['chk_button_export'] = 0;


$cfg['columns']['ehozeg'] = array(
                      'dbcol'=>'MOBILE',
                      'title'=>'Mobile',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'alwdie'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );





$cfg['columns']['rtykjs'] = array(
                      'dbcol'=>'SMS',
                      'title'=>'Sms',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'alwdie'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );





$cfg['columns']['uazcnv'] = array(
                      'dbcol'=>'SENT',
                      'title'=>'Sent',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>4,
                      'alwdie'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );





$cfg['columns']['sqjzou'] = array(
                      'dbcol'=>'SENTDATE',
                      'title'=>'Sentdate',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>4,
                      'alwdie'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'date',
                      'selectsrc'=> '',
                      );





$cfg['columns']['doftsx'] = array(
                      'dbcol'=>'QUEUEDATE',
                      'title'=>'Date',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'alwdie'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'date',
                      'selectsrc'=> '',
                      );




$combogrid_array   = array();
