<?php

/**
* auto created config file for modules/students/exams/reports/smsresults
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-01-12 09:11:55
*/

 final class smsresults {
 private $id;
 private $datasrc;
 private $primary_key;

 public function __construct(){
  global $cfg;

     $this->id           = filter_input(INPUT_POST , 'id');
     $this->datasrc      = valueof($cfg,'datasrc');
     $this->primary_key  = valueof($cfg,'pkcol');
 }

 public function list_exams(){
		global $db;

	 $formcode     = filter_input(INPUT_POST , ui::fi('form'));
	 $examcode     = filter_input(INPUT_POST , ui::fi('exam'));
	 $yearcode     = filter_input(INPUT_POST , ui::fi('year'));
	 $termcode     = filter_input(INPUT_POST , ui::fi('term'));

	 if(strlen($formcode)>1){
	 	$formcode = substr($formcode,0,1);
	 }

	  $exams = $db->GetRow("
       SELECT S.CATNAME1 , S.CATNAME2 , S.CATNAME3 , S.CATNAME4 , S.CATNAME5,
        S.EXAMNAME1 , S.EXAMNAME2 , S.EXAMNAME3 , S.EXAMNAME4 , S.EXAMNAME5
        FROM SATSSG S
        WHERE S.FORMCODE='{$formcode}'
        AND S.YEARCODE='{$yearcode}'
        AND S.TERMCODE='{$termcode}'
       ");

     $exams_array = array();

       if($exams){
       	if(sizeof($exams)>=1){

       		for ($c=1;$c<=5;++$c){
       		 if(isset($exams["CATNAME{$c}"])){
       		 	 $exams_array["CAT{$c}"] = $exams["CATNAME{$c}"];
       		 }
       		}

       		for ($e=1;$e<=5;++$e){
       		 if(isset($exams["EXAMNAME{$e}"])){
       		 	 $exams_array["PAPER{$e}"] = $exams["EXAMNAME{$e}"];
       		 }
       		}

       		$exams_array['TOTAL'] = 'Combined';

       	}
       }

	  return ui::form_select_fromArray('exam',$exams_array, $examcode," onchange=\"\" ");

	}

 public function pre_run() {
		global $db;

	$formcode     = filter_input(INPUT_POST , ui::fi('form'));
	$examcode     = filter_input(INPUT_POST , ui::fi('exam'));
	$yearcode     = filter_input(INPUT_POST , ui::fi('year'));
	$termcode     = filter_input(INPUT_POST , ui::fi('term'));

	$col     = 'FORMCODE';

	if(strlen($formcode)>1){
	 $formcode = substr($formcode,0,1);
	 $col     = 'STREAMCODE';
	}


    $missing_phones  = $db->Execute("select ADMNO,FULLNAME,STREAMCODE from VIEWSP where {$col}='{$formcode}' AND (MOBILEPHONE IS NULL OR MOBILEPHONE='' OR {$db->length}(MOBILEPHONE)<10)");
    $num             = @$missing_phones->RecordCount();

    if($num<0){
	 return json_response(0,"{$num} Student Are Missing Phone");
    }else{
	 return json_response(1,"ok");
	}


  }

 public function send(){
   global $db;

	$formstream   = filter_input(INPUT_POST , ui::fi('form'));
	$exam         = filter_input(INPUT_POST , ui::fi('exam'));
	$exam_name    = filter_input(INPUT_POST , ui::fi('exam'));
	$yearcode     = filter_input(INPUT_POST , ui::fi('year'));
	$termcode     = filter_input(INPUT_POST , ui::fi('term'));
	$admno        = filter_input(INPUT_POST , ui::fi('find_admno'));
	$school       = new school();
	$school_ar    = explode(' ',$school->name);
	$school_nm    = valueof($school_ar, 0); 
	$fsCol        = 'FORMCODE';

	if(strlen($formstream)>1){
	 $formcode = substr($formstream,0,1);
	 $fsCol     = 'STREAMCODE';
	}

    $sql_extra    =  !empty($admno) ? "  and ADMNO='{$admno}' " : '';
    $positioning  = 'p';
    $disp_score   = 'p';

    if(strlen($formstream)>1) {
     $formstream_code_col = "STREAMCODE";
     $positioning_col     = "POSTREAM";
     $formcode            = substr($formstream,0,1);

     switch($positioning){
	 case 'p':
      $positioning_col        = 'POSTREAM_POINTS';
      $display_col_name       = 'TOTALP';
      $display_col_title      = 'PNTS';
      $display_col_gradecode  = 'GRADECODEP';
      $positioning_by_descr   = ' POINTS ';
     break;
     case 'm':
     default:
      $positioning_col        = 'POSTREAM_MARKS';
      $display_col_name       = 'TOTALM';
      $display_col_title      = 'AVE';
      $display_col_gradecode  = 'GRADECODEM';
      $positioning_by_descr   = ' MARKS ';
     break;
   }

  }else{
   $formstream_code_col = "FORMCODE";
   $positioning_col     = "POSFORM";
   $formcode            = $formstream;

   switch($positioning){
	case 'p':
     $positioning_col        = 'POSFORM_POINTS';
     $display_col_name       = 'TOTALP';
     $display_col_title      = 'PNTS';
     $display_col_gradecode  = 'GRADECODEP';
     $positioning_by_descr   = ' POINTS ';
    break;
    case 'm':
    default:
     $positioning_col        = 'POSFORM_MARKS';
     $display_col_name       = 'TOTALM';
     $display_col_title      = 'AVE';
     $display_col_gradecode  = 'GRADECODEM';
     $positioning_by_descr   = ' MARKS ';
    break;
  }
 }

  //overide
  switch($positioning){
	case 'p':
     $positioning_col        = 'POSFORM_POINTS';
     $display_col_name       = 'TOTALP';
     $display_col_title      = 'PNTS';
     $display_col_gradecode  = 'GRADECODEP';
     $positioning_by_descr   = ' POINTS ';
    break;
    case 'm':
    default:
     $positioning_col        = 'POSFORM_MARKS';
     $display_col_name       = 'TOTALM';
     $display_col_title      = 'AVE';
     $display_col_gradecode  = 'GRADECODEM';
     $positioning_by_descr   = ' MARKS ';
    break;
  }
  
  switch ($disp_score){
	case 'r':
	  $score_column_postfix = 'R';
	 break;
	 case 'p':
	 default:
	  $score_column_postfix = 'P';
	 break;
 }

 $exam_column      = $exam.$score_column_postfix;
 $examdesc_joined  = "Form {$formstream} {$exam_name} T{$termcode} {$yearcode}";
 $examdesc         = ($examdesc_joined);

 if(strstr($exam,'CAT') || strstr($exam,'PAPER')){
  $gradecode_col = "{$exam}GRADECODE";
 }else{
  $gradecode_col = "GRADECODE";
 }

 $students  = $db->Execute("
        SELECT ADMNO,FULLNAME,MOBILEPHONE,STREAMCODE,SUBJECTCODE,PICKED,{$exam_column} SCORE,{$gradecode_col} GRADECODE FROM VIEWSTUDENTSUBJECTS
        WHERE YEARCODE='{$yearcode}'
        AND TERMCODE='{$termcode}'
        AND {$formstream_code_col}='{$formstream}'
        {$sql_extra}
        and {$exam}>0
        ORDER BY STREAMCODE, SORTPOS,CODEOFFC
        ");

   if(!$students){
    return json_response(0, "An Error Occured. Try again later");
   }

   if($students->RecordCount()==0){
    return json_response(0, "No Student To Display");
   }


	$student_means_sql = "
	SELECT ADMNO,  FORMCODE, STREAMCODE,
	TOTAL,TOTALMAX,NUMSUBJ,TOTALP,TOTALM,GRADECODEP,GRADECODEM,
	KCPEGRADE, KCPEMEAN, KCPEPOINT, KCPEMARKS,KCPERANK,
	POSTREAM_MARKS,POSFORM_MARKS,POSTREAM_POINTS,POSFORM_POINTS,
	COMMENTS,COMMENTS2
	FROM VIEWSTUDENTSMEAN
	WHERE YEARCODE='{$yearcode}'
	AND FORMCODE='{$formcode}'
	AND TERMCODE='{$termcode}'
	AND EXAM='{$exam}'
	";

	$student_means_sql   .= !empty($admno) ?" AND ADMNO='{$admno}' " : '';
	$student_means_sql   .= ' order by POSFORM_MARKS asc ';

	$student_means      = $db->GetAssoc($student_means_sql);

	if(!$student_means){
     return json_response(0, "An Error Occured. Try again later");
    }

    $form_student_count = $db->GetOne("
	SELECT count(ADMNO) num
	FROM VIEWSTUDENTSMEAN
	WHERE YEARCODE='{$yearcode}'
	AND FORMCODE='{$formcode}'
	AND TERMCODE='{$termcode}'
	AND EXAM='{$exam}'
	");

	$SMSGateway    =  new sms();

    if(!empty($SMSGateway->error)){
      return json_response(0, $SMSGateway->error);
	}

 	$subjects_data = array();
 	$distinct_subjects = array();
 	$distinct_subjects_official_codes = array();
 	$summary_grades = array();
 	$summary_streams = array();
 	$summary_points = array();
 	$summary = array();


 foreach($students as $student){
	
	$admno            = valueof($student, 'ADMNO');
	$fullname         = valueof($student, 'FULLNAME');
	$streamcode       = valueof($student, 'STREAMCODE');
	$mobilephone      = valueof($student, 'MOBILEPHONE');
	$score            = valueof($student, 'SCORE',0,'number_format');
	$marks            = isset($student_means[$admno]['TOTAL']) ? $student_means[$admno]['TOTAL'] : null;
	$numsubj          = isset($student_means[$admno]['NUMSUBJ']) ? $student_means[$admno]['NUMSUBJ'] : null;
	$totalp           = isset($student_means[$admno]['TOTALP']) ? $student_means[$admno]['TOTALP'] : null;
	$totalm           = isset($student_means[$admno]['TOTALM']) ? $student_means[$admno]['TOTALM'] : null;
	$postream_marks   = isset($student_means[$admno]['POSTREAM_MARKS']) ? $student_means[$admno]['POSTREAM_MARKS'] : null;
	$posform_marks    = isset($student_means[$admno]['POSFORM_MARKS']) ? $student_means[$admno]['POSFORM_MARKS'] : null;
	$postream_points  = isset($student_means[$admno]['POSTREAM_POINTS']) ? $student_means[$admno]['POSTREAM_POINTS'] : null;
	$posform_points   = isset($student_means[$admno]['POSFORM_POINTS']) ? $student_means[$admno]['POSFORM_POINTS'] : null;
	$position         = isset($student_means[$admno][$positioning_col]) ? $student_means[$admno][$positioning_col] : null;
	$gradecode_mean   = isset($student_means[$admno][$display_col_gradecode]) ? $student_means[$admno][$display_col_gradecode] : null;

	if(!strstr($mobilephone,'+254') || substr($mobilephone,0,4)!='+254'  ){
    $mobilephone = '+254'.$mobilephone;
	}

	if( strlen($mobilephone)==13) {
	$subjectcode  = valueof($student, 'SUBJECTCODE');
	$gradecode_subject    = valueof($student, 'GRADECODE');
	$picked       = valueof($student, 'PICKED');

	if($picked!=1){
     $subjectcode = "*{$subjectcode}";
	}

	$subjects_data[$admno]['mobilephone']  = $mobilephone;
	$subjects_data[$admno]['streamcode']   = $streamcode;
	$subjects_data[$admno]['fullname']     = $fullname;
	$subjects_data[$admno]['marks']        = $marks;
	$subjects_data[$admno]['position']     = $position;
	$subjects_data[$admno]['gradecodemn']  = $gradecode_mean;
	$subjects_data[$admno]['subjects'][]   = "{$subjectcode}{$score} {$gradecode_subject}";

    }
 }

  $num_sent_to  = 0;
  $next_id           = generateID('SYSQSMS');
  $queuedate         =  date('Y-m-d H:i:s');
  foreach($subjects_data as $admno=> $student ){
   $mobilephone  = valueof($student, 'mobilephone');
   $streamcode   = valueof($student, 'streamcode');
   $fullname     = valueof($student, 'fullname');
   $namearray    = explode(' ', $fullname);
   $namefirst    = valueof($namearray, 0); 
   $marks        = valueof($student, 'marks','','number_format');
   $gradecodemn  = valueof($student, 'gradecodemn');
   $position     = valueof($student, 'position');
   $results      = valueof($student, 'subjects');
   $results_str  = implode( ',', $results);
   $sms_msg      = centerTrim("{$namefirst} {$admno} {$streamcode} {$yearcode} Term {$termcode} OPENER RSLTS. {$results_str}. TOTAL {$marks} MG {$gradecodemn} POS {$position}/{$form_student_count}");
//echo "\$sms_msg={$sms_msg} <br>";//remove 

   $results      = $SMSGateway->sendMessage($mobilephone, $sms_msg);
   
   $sql_sms   = "insert into SYSQSMS(ID,MOBILE,SMS,QUEUEDATE)values( {$next_id},'{$mobilephone}','{$sms_msg}','{$queuedate}')";

   if($db->Execute($sql_sms))
   {
    ++$next_id;
    ++$num_sent_to;
   }
   
  }
    $s = $num_sent_to>1 ? '' : 'es';

	//return json_response(1, "Sent {$num_sent_to}  SMS{$s} to queue");
	return json_response(1, "Sent SMS to {$num_sent_to} Recipient{$s}");

  }



}
