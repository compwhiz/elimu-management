<?php

/**
* auto created config file for modules/students/exams/otherranking
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-02-29 15:50:06
*/

 final class otherranking {
 private $id;
 private $datasrc;
 private $primary_key;
 
	public function __construct(){
		global $cfg;
		
		$this->id           = filter_input(INPUT_POST , 'id');
		$this->datasrc      = valueof($cfg,'datasrc');
		$this->primary_key  = valueof($cfg,'pkcol');
	}
	
	
	public function list_streams(){
		global $db,$cfg;

	 $subjectcode  =  filter_input(INPUT_POST , ui::fi('subjectcode'));
     $streamcode   =  filter_input(INPUT_POST , ui::fi('streamcode'));
     $yearcode     =  filter_input(INPUT_POST , ui::fi('yearcode'));
     $termcode     =  filter_input(INPUT_POST , ui::fi('termcode'));

	 $streams_array = array();
     $user          = new user();

     $records   =  $db->GetAssoc("
		SELECT distinct STREAMCODE,FORMCODE FROM SASTUDSUB
		WHERE YEARCODE='{$yearcode}'
		ORDER BY STREAMCODE ASC
     ");

	  if($records){
	  	if(count($records)>0){
	  	  foreach ($records as  $streamcode => $formcode ){
	  	  	$streams_array[$formcode]   = "Form {$formcode}";
	  	  	//$streams_array[$streamcode] = "  {$streamcode}";
	  	  }
	  	}
	  }

	  return ui::form_select_fromArray('formcode',$streams_array, ''," onchange=\"{$cfg['appname']}.list_exams();\" ");

	}

    public function list_exams(){
		global $db,$cfg;

	 //$formcode     =  filter_input(INPUT_POST , ui::fi('formcode'));
     $yearcode     =  filter_input(INPUT_POST , ui::fi('yearcode'));
     $termcode     =  filter_input(INPUT_POST , ui::fi('termcode'));
     	
     //$formstream_col = 'FORMCODE';
     //
	 //if(strlen($formcode)>1){
	 //$formcode       = substr($formcode,0,1);
	 //$formstream_col = 'FORMCODE';
	 //}
	 
	  $exams = $db->GetRow("
       SELECT S.CATNAME1 , S.CATNAME2 , S.CATNAME3 , S.CATNAME4 , S.CATNAME5,
        S.EXAMNAME1 , S.EXAMNAME2 , S.EXAMNAME3 , S.EXAMNAME4 , S.EXAMNAME5
        FROM SATSSG S
        WHERE S.YEARCODE='{$yearcode}'
        AND S.TERMCODE='{$termcode}'
        AND S.CATNAME1 IS NOT NULL
       ");
        
        //AND  S.FORMCODE='{$formcode}'

     $exams_array = array();

       if($exams){
       	if(sizeof($exams)>=1){

       		for ($c=1;$c<=5;++$c){
       		 if(isset($exams["CATNAME{$c}"])){
       		 	 $exams_array["CAT{$c}"] = $exams["CATNAME{$c}"];
       		 }
       		}

       		for ($e=1;$e<=5;++$e){
       		 if(isset($exams["EXAMNAME{$e}"])){
       		 	 $exams_array["PAPER{$e}"] = $exams["EXAMNAME{$e}"];
       		 }
       		}

       		$exams_array['TOTAL'] = 'Combined';

       	}
       }


	  //return ui::form_select_fromArray('exam',$exams_array, ''," onchange=\"{$cfg['appname']}.list_subjects()\" ");
	  return ui::form_select_fromArray('exam',$exams_array, ''," onchange=\"\" ");

	}
	
	public function list_subjects(){
		global $db;
		
     //$streamcode   =  filter_input(INPUT_POST , ui::fi('streamcode'));
     $formcode     =  filter_input(INPUT_POST , ui::fi('formcode'));
     $yearcode     =  filter_input(INPUT_POST , ui::fi('yearcode'));
     $termcode     =  filter_input(INPUT_POST , ui::fi('termcode'));
     	
     $formstream_col = 'FORMCODE';
     
	 if(strlen($formcode)>1){
	 $formcode       = substr($formcode,0,1);
	 $formstream_col = 'FORMCODE';
	 }
	 
     $user         =  new user();

     $subjects      =  $db->GetArray("
		SELECT DISTINCT SUBJECTCODE,count(DISTINCT ADMNO) NUM FROM SASTUDSUB
		WHERE YEARCODE='{$yearcode}'
		WHERE FORMCODE='{$formcode}'
		ORDER BY SUBJECTCODE ASC
     ");

	  $subjects_array = array();

	  if($subjects){
	  	if(count($subjects)>0){
	  	  foreach ($subjects as $subject=>$students){
	  	  	$subjectcode  = valueof($subject,'SUBJECTCODE');
	  	  	$subjects_array[$subjectcode] = "{$subjectcode}-({$students})";
	  	  }
	  	}
	  }

	  return ui::form_select_fromArray('subjectcode',$subjects_array, ''," onchange=\"\" ");

	}
 
 }
