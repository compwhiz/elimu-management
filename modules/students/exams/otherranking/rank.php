<?php

@ignore_user_abort(true); 
@set_time_limit(0); 
@ini_set("max_execution_time", 600000000);

$ranking = new ranking();
$ranking->rank();

  class ranking{
	private $id;
	private $_formcode;
	private $_streamcode;
	private $_yearcode;
	private $_termcode;
	private $_exam;
	private $_subjectcode;
	
	public function __construct(){
		global $cfg;
		
	}
	
	
	public function rank() {
		global $db,$cfg;
    
       $yearcode          = filter_input(INPUT_GET , ui::fi('yearcode'));
       $termcode          = filter_input(INPUT_GET , ui::fi('termcode'));
       $formcode          = filter_input(INPUT_GET , ui::fi('formcode'));
       $exam              = filter_input(INPUT_GET , ui::fi('exam'));
       $rankby            = filter_input(INPUT_GET , ui::fi('rankby'));
       
       if(empty($yearcode)){
        echo "<script>parent.$.messager.alert('Error','Please Select Year','error');parent.\$('#btnRank').linkbutton('enable');</script>";	
        die;
       }
       
       if(empty($termcode)){
        echo "<script>parent.$.messager.alert('Error','Please Select Term','error');parent.\$('#btnRank').linkbutton('enable');</script>";	
        die;
       }
       
       if(empty($exam)){
        echo "<script>parent.$.messager.alert('Error','Please Select Exam','error');parent.\$('#btnRank').linkbutton('enable');</script>";	
        die;
       }
       
       if(empty($rankby)){
        echo "<script>parent.$.messager.alert('Error','Please Select Rank By','error');parent.\$('#btnRank').linkbutton('enable');</script>";	
        die;
       }
       
       //if(empty($formcode)){
        //echo "<script>parent.$.messager.alert('Error','Please Select Form','error');parent.\$('#btnRank').linkbutton('enable');</script>";	
        //die;
       //}

        switch($rankby){
        default:
        case 1:
         $group_col  = 'DORMCODE';
         $update_col = 'POSDORM_POINTS';
        break;
        case 2:
         $group_col  = 'HOUSECODE';
         $update_col = 'POSHSE_POINTS';
        break;
	    }
		
     	$data = $db->GetAssoc("
     	select ADMNO, {$group_col},TOTALP,TOTALM,round(TOTALP*TOTALM,2) TOTMULTI  from VIEWSTUDENTSMEAN
     	where YEARCODE='{$yearcode}'
     	and TERMCODE='{$termcode}'
     	and EXAM='{$exam}'
     	order by TOTALP DESC,TOTALM DESC,(TOTALP*TOTALM) DESC
     	");

     	if($data){
     	 $positions = array();

     	 foreach ($data as $admno=>$scores){
		   $scorep = valueof($scores,'TOTALP');
		   $scorem = valueof($scores,'TOTALM');
		   $totmulti = valueof($scores,'TOTMULTI');
     	   $positions[$totmulti][] = "'{$admno}'";
     	 }
        
	     $num_records = sizeof($positions);

		 $pos = 1;
	     foreach($positions as $position => $admnos){
		  $admnos_str = implode(',', $admnos);
		  
		  $update     = $db->Execute("
		  UPDATE SASTMN SET {$update_col}={$pos}
		  where YEARCODE='{$yearcode}'
     	  and TERMCODE='{$termcode}'
     	  and EXAM='{$exam}'
     	  and ADMNO IN ({$admnos_str})
     	 ");
		  
		if($update){
		++$pos;
		$progress = ($count/$num_records)*100;
		$progress = round($progress,0);

		echo "
		<script>
          parent.\$('#".ui::fi('prg_rank')."').progressbar({ value: {$progress}});
        </script>
		";
		
		sleep(.7);
		flush();
		
		++$count;
   
		 }
		 
		 }
		 
	    }
	    
      
        echo "
		<script>
		  parent.alert('Done Ranking');
          parent.\$('#btnRankOther').linkbutton('enable');
          parent.\$('#".ui::fi('prg_rank')."').progressbar({ value: 100});
          parent.\$('#".ui::fi('div_spinner')."').html('&nbsp;');
          parent.{$cfg['appname']}.print_report();
        </script>
		";

	}
    
  }
