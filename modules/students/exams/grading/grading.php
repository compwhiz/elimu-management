<?php

class grading
{
    private $id;
    private $datasrc;
    private $primary_key;
    private $regno;
    private $positioning = array();

    private $_formcode;
    private $_streamcode;
    private $_yearcode;
    private $_termcode;
    private $_exam;
    private $_position_by_marks_col;
    private $_position_by_points_col;
    private $_numsubjects;
    private $_minsubjects;

    private $_cat_required          = array();
    private $_cat_done              = array();
    private $_cat_picked            = array();
    private $_cat_dropped           = array();
    private $_cat_mandatory         = array();
    private $_cat_mandatory_missing = array();
    private $_comnination_codes     = array();

    public function __construct()
    {
        global $cfg;
        $this->id = filter_input(INPUT_POST, 'id');
        $this->datasrc = valueof($cfg, 'datasrc');
        $this->primary_key = valueof($cfg, 'pkcol');
    }

    public function list_subjects()
    {
        global $db;


        $formcode = filter_input(INPUT_POST, ui::fi('form'));
        $yearcode = filter_input(INPUT_POST, ui::fi('year'));
        $termcode = filter_input(INPUT_POST, ui::fi('term'));

        $subjects = $db->GetArray("
       SELECT DISTINCT S.SUBJECTCODE
        FROM SATSS S
        INNER JOIN SASTREAMS T ON T.STREAMCODE = S.STREAMCODE
        WHERE T.FORMCODE='{$formcode}'
        AND S.YEARCODE='{$yearcode}'
        AND S.TERMCODE='{$termcode}'
       ");

        $subjects_array = array();

        if ($subjects) {
            if (count($subjects) > 0) {
                foreach ($subjects as $subject) {
                    $subjectcode = valueof($subject, 'SUBJECTCODE');
                    $subjects_array[$subjectcode] = $subjectcode;
                }
            }
        }

        return ui::form_select_fromArray('subject', $subjects_array, '', " onchange=\"list_exams();\" ");

    }

    public function list_exams()
    {
        global $db;

        $formcode = filter_input(INPUT_POST, ui::fi('form'));
        $examcode = filter_input(INPUT_POST, ui::fi('exam'));
        $yearcode = filter_input(INPUT_POST, ui::fi('year'));
        $termcode = filter_input(INPUT_POST, ui::fi('term'));

        if (strlen($formcode) > 1) {
            $formcode = substr($formcode, 0, 1);
        }

        $exams = $db->GetRow("
       SELECT S.CATNAME1 , S.CATNAME2 , S.CATNAME3 , S.CATNAME4 , S.CATNAME5,
        S.EXAMNAME1 , S.EXAMNAME2 , S.EXAMNAME3 , S.EXAMNAME4 , S.EXAMNAME5
        FROM SATSSG S
        WHERE S.FORMCODE='{$formcode}'
        AND S.YEARCODE='{$yearcode}'
        AND S.TERMCODE='{$termcode}'
       ");

        $exams_array = array();

        if ($exams) {
            if (sizeof($exams) >= 1) {

                for ($c = 1; $c <= 5; ++$c) {
                    if (isset($exams["CATNAME{$c}"])) {
                        $exams_array["CAT{$c}"] = $exams["CATNAME{$c}"];
                    }
                }

                for ($e = 1; $e <= 5; ++$e) {
                    if (isset($exams["EXAMNAME{$e}"])) {
                        $exams_array["PAPER{$e}"] = $exams["EXAMNAME{$e}"];
                    }
                }

                $exams_array['TOTAL'] = 'Combined';

            }
        }


//	  print_pre($exams_array);
        return ui::form_select_fromArray('exam', $exams_array, $examcode, " onchange=\"\" ");

    }

    private function get_subjects_config()
    {
        global $db;
        return $db->GetRow("SELECT NUMSUBJECTS,MINSUBJECTS FROM SATSSG WHERE FORMCODE='{$this->_formcode}' AND YEARCODE='{$this->_yearcode}' AND TERMCODE='{$this->_termcode}'");
    }

    public function grade()
    {
        global $db;
        //$db->debug=1;//remove
        @ignore_user_abort(true);
        @set_time_limit(0);
        @ini_set("max_execution_time", 600000000);

        $yearcode = filter_input(INPUT_POST, ui::fi('year'));
        $termcode = filter_input(INPUT_POST, ui::fi('term'));
        $form_stream_code = filter_input(INPUT_POST, ui::fi('form'));
        $formcode = filter_input(INPUT_POST, ui::fi('form'));
        $exam = filter_input(INPUT_POST, ui::fi('exam'));
        $positioning_col = filter_input(INPUT_POST, 'positioning');
        $disp_score = filter_input(INPUT_POST, 'subjscore');
        //$position_subjects  = filter_input(INPUT_POST , 'position_subjects');
        $position_subjects = 0;

        if (strlen($form_stream_code) == 1) {
            $form_stream = 'FORMCODE';
        } else {
            $form_stream = 'STREAMCODE';
            $formcode = substr($formcode, 0, 1);
        }

        //exit();
        if (empty($yearcode)) {
            return json_response(0, "Please Select Year");
        }

        if (empty($termcode)) {
            return json_response(0, "Please Select Term");
        }

        if (empty($formcode)) {
            return json_response(0, "Please Form or a Stream");
        }

        if (empty($exam)) {
            return json_response(0, "Please Select Exam");
        }

        if ($position_subjects == 1) {
            self::position_subjects($yearcode, $termcode, $formcode, $exam);
        }

        switch ($exam) {
            case 'CAT1':
                $exam_configmax_col = 'CATMAX1';
                break;
            case 'CAT2':
                $exam_configmax_col = 'CATMAX2';
                break;
            case 'CAT3':
                $exam_configmax_col = 'CATMAX3';
                break;
            case 'CAT4':
                $exam_configmax_col = 'CATMAX4';
                break;
            case 'CAT5':
                $exam_configmax_col = 'CATMAX5';
                break;
            case 'PAPER1':
                $exam_configmax_col = 'EXAMMAX1';
                break;
            case 'PAPER2':
                $exam_configmax_col = 'EXAMMAX2';
                break;
            case 'PAPER3':
                $exam_configmax_col = 'EXAMMAX3';
                break;
            case 'PAPER4':
                $exam_configmax_col = 'EXAMMAX4';
                break;
            case 'PAPER5':
                $exam_configmax_col = 'EXAMMAX5';
                break;
            case 'TOTAL':
                $exam_configmax_col = 'EXAMMAX5';
                break;

        }

        $this->_exam = $exam;

        $rawscorecol = $exam;

        if (strstr($exam, 'CAT') || strstr($exam, 'PAPER')) {
            $pointscol = "{$exam}POINTS";
            $this->_position_by_marks_col = $exam;
            $this->_position_by_points_col = $pointscol;
        } else {
            $pointscol = "POINTS";
            $this->_position_by_marks_col = "TOTALM";
            $this->_position_by_points_col = "TOTALP";

        }

        if ($exam != 'TOTAL') {

            switch ($disp_score) {
                case 'r':
                    $score_column_postfix = 'R';
                    $exam_totals_col = $pointscol;
                    break;
                case 'p':
                default:
                    $score_column_postfix = 'P';
                    $exam_totals_col = $rawscorecol;
                    break;
            }

        } else {
            $score_column_postfix = '';
            $exam_totals_col = $pointscol;
        }

//echo "\$exam={$exam} <br>";//remove 
//exit();//remove 

        $exam_column = $exam . $score_column_postfix;

        $pointscol = strtoupper($pointscol);

        $settings = $db->GetRow("select SBCCODE,REQUIRED,MANDATORY from SAFCS WHERE YEARCODE='{$yearcode}'
       AND TERMCODE='{$termcode}'
       AND FORMCODE='{$formcode}'
       ");

        if (empty($settings)) {
            return json_response(0, "Please Setup Required Subjects for Form {$formcode} , {$yearcode}- Term {$termcode} ");
        }

        $combinationcode = valueof($settings, 'SBCCODE');
        $required[$combinationcode] = isset($settings['REQUIRED']) && !empty($settings['REQUIRED']) ? unserialize(base64_decode($settings['REQUIRED'])) : array();
        $mandatory[$combinationcode] = isset($settings['MANDATORY']) && !empty($settings['MANDATORY']) ? unserialize(base64_decode($settings['MANDATORY'])) : array();

        $this->_cat_required = $required;

        $exam_config = $db->GetRow("
       SELECT NUMSUBJECTS,MINSUBJECTS,CATMAX1,CATMAX2,CATMAX3,CATMAX4,CATMAX5,
       EXAMMAX1,EXAMMAX2,EXAMMAX3,EXAMMAX4,EXAMMAX5,GSYSCODE
       FROM SATSSG
       WHERE FORMCODE='{$formcode}'
       AND YEARCODE='{$yearcode}'
       AND TERMCODE='{$termcode}'
       ");

        if ($exam != 'TOTAL') {
            $exam_configmax = valueof($exam_config, $exam_configmax_col, 100);
        } else {
            $exam_configmax = 100;
        }

        $exams_config_gsyscode = valueof($exam_config, 'GSYSCODE');


        $subjects_data = $db->GetAssoc("
      select S.SUBJECTCODE,C.CATCODE,C.CATNAME,S.SUBJECTNAME FROM SASUBJECTS S
      left join SASUBJCATS C ON C.CATCODE=S.CATCODE
      order by C.SORTPOS,S.SUBJECTCODE asc
     ");

        $subject_cats = array();
        $subjects_all = array();

        if (count($subjects_data) >= 1) {
            foreach ($subjects_data as $subjectcode => $subject) {
                $catcode = valueof($subject, 'CATCODE');
                $catname = valueof($subject, 'CATNAME');
                $subjectname = valueof($subject, 'SUBJECTNAME');

                if (!array_key_exists($catcode, $subject_cats)) {
                    $subject_cats[$catcode] = $catname;
                }

                $subjects_all[$catcode][] = array(
                    'subjectcode' => $subjectcode,
                    'subjectname' => $subjectname,
                );

                foreach ($mandatory as $combinationcode => $mandatory_subjects) {
                    if (array_key_exists($subjectcode, $mandatory_subjects)) {
                        $this->_cat_mandatory[$combinationcode][$catcode][] = $subjectcode;
                    }
                }
            }
        }

        $this->_formcode = $formcode;
        $this->_yearcode = $yearcode;
        $this->_termcode = $termcode;
        $this->_numsubjects = valueof($exam_config, 'NUMSUBJECTS');
        $this->_minsubjects = valueof($exam_config, 'MINSUBJECTS');

        $data = $db->Execute("SELECT * FROM VIEWSTUDENTSUBJECTS
        WHERE YEARCODE='{$yearcode}'
        AND TERMCODE='{$termcode}'
        AND {$form_stream}='{$form_stream_code}'
        AND {$exam}>0
        ORDER BY STREAMCODE, ADMNO, SORTPOS,SUBJECTCODE
        ");

        //AND {$exam_column}>=0
        if ($data) {

            $db->Execute("
         update SASTUDSUB set PICKED=NULL
        WHERE YEARCODE='{$yearcode}'
        AND TERMCODE='{$termcode}'
        AND {$form_stream}='{$form_stream_code}'
        ");

            $streams_data = array();
            while (!$data->EOF) {
                $admno = valueof($data->fields, 'ADMNO');
                $subjectcode = valueof($data->fields, 'SUBJECTCODE');
                $catcode = valueof($data->fields, 'CATCODE');
                $formcode = valueof($data->fields, 'FORMCODE');
                $streamcode = valueof($data->fields, 'STREAMCODE');
                $sbccode = $combinationcode;

                $this->_comnination_codes[$admno] = $sbccode;

                $exam_column_value = valueof($data->fields, $exam_column);

                $exam_column_value = round($exam_column_value, 0);

                $streams_data[$formcode][$streamcode][$admno][$subjectcode]['score'] = $exam_column_value;
                $streams_data[$formcode][$streamcode][$admno][$subjectcode]['points'] = valueof($data->fields, $pointscol);
                $streams_data[$formcode][$streamcode][$admno][$subjectcode]['catcode'] = valueof($data->fields, 'CATCODE');

                $data->MoveNext();
            }

            if (sizeof($streams_data) > 0) {
                foreach ($streams_data as $formcode => $formdata) {
                    foreach ($formdata as $streamcode => $streamdata) {
                        foreach ($streamdata as $admno => $subjects) {
                            $this->_cat_done = array();
                            $this->_cat_picked = array();
                            $this->_cat_mandatory_missing = array();
                            $sql_updates_subjects = array();
                            $unpicked_scores = array();
                            $count_to_pick = 0;
                            $num_unpicked = 0;
                            $less_picked = 0;
                            $subject_total = 0;
                            $subject_points = 0;
                            $subject_count_all = 0;
                            $subject_count = 0;
                            $points = 0;
                            $points_marks = 0;
                            $grade_code_points = null;
                            $grade_code_marks = null;

                            $my_combinationcode = $this->_comnination_codes[$admno];

                            //check no per category
                            foreach ($subjects as $subjectcode => $subject) {

                                $subject_catcode = valueof($subject, 'catcode');
                                $subject_score = valueof($subject, 'score');

                                $this->_cat_done[$subject_catcode][$subjectcode] = $subject_score;

                            }

                            if (isset($this->_cat_mandatory[$my_combinationcode]) && sizeof($this->_cat_mandatory[$my_combinationcode]) > 0) {
                                foreach ($this->_cat_mandatory[$my_combinationcode] as $catcode => $mandatory_subjects) {
                                    foreach ($mandatory_subjects as $mandatory_subjectcode) {
                                        if (isset($this->_cat_done[$catcode])) {
                                            if (array_key_exists($mandatory_subjectcode, $this->_cat_done[$catcode])) {
                                                $this->_cat_picked[$catcode][$mandatory_subjectcode] = $this->_cat_done[$catcode][$mandatory_subjectcode];
                                            } else {
                                                $this->_cat_mandatory_missing[$catcode][$admno][] = $mandatory_subjectcode;
                                            }
                                        }
                                    }
                                }
                            }

                            if (isset($this->_cat_required[$sbccode]) && sizeof($this->_cat_required[$sbccode]) > 0) {
                                foreach ($this->_cat_required[$sbccode] as $catcode => $required_count) {

                                    $num_cat_done = isset($this->_cat_done[$catcode]) ? sizeof($this->_cat_done[$catcode]) : 0;
                                    $num_cat_picked = isset($this->_cat_picked[$catcode]) ? sizeof($this->_cat_picked[$catcode]) : 0;

                                    if ($num_cat_picked < $required_count) {

                                        if (isset($this->_cat_done[$catcode]) && sizeof($this->_cat_done[$catcode]) > 0) {
                                            arsort($this->_cat_done[$catcode]);
                                        }

                                        $count_pos = $num_cat_picked > 0 ? $num_cat_picked + 1 : 1;

                                        if (isset($this->_cat_done[$catcode]) && sizeof($this->_cat_done[$catcode]) > 0) {
                                            foreach ($this->_cat_done[$catcode] as $cat_subject_code => $cat_subject_score) {
                                                if ($count_pos <= $required_count) {
                                                    if (isset($this->_cat_picked[$catcode])) {
                                                        if (!array_key_exists($cat_subject_code, $this->_cat_picked[$catcode])) {
                                                            $this->_cat_picked[$catcode][$cat_subject_code] = $cat_subject_score;
                                                            ++$count_pos;
                                                        }
                                                    } else {
                                                        $this->_cat_picked[$catcode][$cat_subject_code] = $cat_subject_score;
                                                        ++$count_pos;
                                                    }

                                                }

                                            }
                                        }
                                    }
                                }//each cat required
                            }

                            $subject_count = count($subjects);
                            $subject_total = 0;
                            $subject_points = 0;

                            $picked_subjects = array();
                            $unpicked_subjects = array();

                            if (isset($this->_cat_picked)) {
                                if (sizeof($this->_cat_picked) > 0) {
                                    foreach ($this->_cat_picked as $catcode => $cat_subjects) {
                                        foreach ($cat_subjects as $subjectcode => $subject_scores) {
                                            if (array_key_exists($subjectcode, $subjects)) {
                                                $picked_subjects[$subjectcode] = $subjects[$subjectcode];
                                            }
                                        }
                                    }
                                }
                            }

                            foreach ($subjects as $subjectcode => $subject_scores) {
                                if (!array_key_exists($subjectcode, $picked_subjects)) {
                                    $unpicked_subjects[$subjectcode] = $subjects[$subjectcode];
                                }
                            }

                            $num_picked = count($picked_subjects);
                            $less_picked = $this->_numsubjects - $num_picked;

                            if ($less_picked > 0) {
                                /**
                                 * get unpicked into 2 dimension array
                                 * 24SEPT14-0650HRS
                                 */
                                $unpicked_scores = array();

                                foreach ($unpicked_subjects as $subjectcode => $unpicked_subject) {
                                    $unpicked_scores[$subjectcode] = valueof($unpicked_subject, 'score');
                                }

                                /**
                                 * sort unpicked by score desc
                                 */
                                if (sizeof($unpicked_scores) > 0) {
                                    arsort($unpicked_scores);
                                }

                                if (sizeof($unpicked_scores) > 0) {
                                    $count_to_pick = 1;
                                    /**
                                     * loop sorted unpicked until ==$less_picked
                                     */
                                    foreach ($unpicked_scores as $subjectcode => $score) {
                                        if ($count_to_pick <= $less_picked) {
                                            $picked_subjects[$subjectcode] = $unpicked_subjects[$subjectcode];
                                            unset($unpicked_subjects[$subjectcode]);
                                        }
                                        ++$count_to_pick;
                                    }
                                }
                            }

                            $num_picked = count($picked_subjects);

                            if (isset($picked_subjects)) {
                                if ($num_picked > 0) {
                                    $sql_updates_subjects = array();
                                    foreach ($picked_subjects as $subjectcode => $subject_scores) {
                                        $sql_updates_subjects[] = "'{$subjectcode}'";
                                    }

                                    $sql_updates_subjects_string = '(' . implode(',', $sql_updates_subjects) . ')';

                                    $db->Execute("
       	 		        update SASTUDSUB set PICKED=1
       	 		        where ADMNO='{$admno}'
       	 		        and YEARCODE='{$yearcode}'
                        and TERMCODE='{$termcode}'
       	 		        and SUBJECTCODE in {$sql_updates_subjects_string}
                        ");

                                }
                            }

                            if (isset($picked_subjects)) {
                                if (sizeof($picked_subjects) > 0) {
                                    foreach ($picked_subjects as $subjectcode => $subject_scores) {
                                        $subject_total += valueof($subject_scores, 'score');
                                        $subject_points += valueof($subject_scores, 'points');
                                    }
                                }
                            }

                            $subject_count_all = count($subjects);
                            $subject_count = count($picked_subjects);

                            $points = round(($subject_points) / $this->_numsubjects, 2);
                            $points_marks = round(($subject_total) / $this->_numsubjects, 2);

                            if ($disp_score == 'p') {
                                $points_markspercent = $points_marks;
                                $subject_total_max = $subject_count * 100;
                            } else {
                                $subject_total_max = $subject_count * $exam_configmax;
                                if ($exam_configmax > 0) {
                                    $points_markspercent = (($points_marks / $exam_configmax) * 100);
                                } else {
                                    $points_markspercent = $exam_configmax;
                                }
                            }
// PROBLEMS START HERE
                            if ($subject_count >= $this->_minsubjects) {

                                $grade_code_points = self::get_grade_code($points);
                                $Get_Grade = self::Get_Grade($exams_config_gsyscode, $points_markspercent);

                                $grade_code_marks = valueof($Get_Grade, 'GRADECODE');
                                $comments = valueof($Get_Grade, 'COMMENTS');
                                $comments2 = valueof($Get_Grade, 'COMMENTS2');

                            } elseif ($subject_count == 0) {
                                $grade_code_points = 'X';
                                $grade_code_marks = 'X';
                                $subject_total = 0;
                                $points_marks = 0;
                                $subject_points = 0;
                                $comments = '';
                                $comments2 = '';
                            } else {
                                $grade_code_points = 'Z';
                                $grade_code_marks = 'Z';
                                $subject_total = 0;
                                $points_marks = 0;
                                $subject_points = 0;
                                $comments = '';
                                $comments2 = '';
                            }


                            //remove
                            $t = 0;
                            //foreach ($picked_subjects as $picked_subject){
                            //echo $t += $picked_subject['score'];
                            //echo("<br>");
                            //}
                            //
                            //print_pre($picked_subjects);
                            //print_pre($unpicked_subjects);
                            //echo "\$admno={$admno} ={$t}<br>";
                            //exit();
                            //echo "\$subject_points={$subject_points} <br>";
                            //echo "\$subject_count={$subject_count} <br>";
                            //echo "\$points={$points} <hr>";
                            //echo "\$this->_minsubjects={$this->_minsubjects} <hr>";
                            //
                            //echo "\$subject_total={$subject_total} <br>";
                            //echo "\$subject_count={$subject_count} <br>";
                            //echo "\$points_marks={$points_marks} <hr>";
                            //echo "\$points_markspercent={$points_markspercent} <hr>";
                            //
                            //echo "\$grade_code_points={$grade_code_points} <br>";
                            //echo "\$grade_code_marks={$grade_code_marks} <br>";
                            //exit();
//
//       	 			$db->debug=1;

                            self::save_student_mean($admno, $yearcode, $termcode, $formcode, $streamcode, $exam, $subject_total, $subject_total_max, $subject_count_all, $subject_points, $points_marks, $grade_code_points, $grade_code_marks, $comments, $comments2);
                            //exit();

                        }//student

                        self::position_stream_by_marks($yearcode, $termcode, $streamcode, $exam);
                        self::position_stream_by_points($yearcode, $termcode, $streamcode, $exam);
                    }//stream
                    self::position_form_by_marks($yearcode, $termcode, $formcode, $exam);
                    self::position_form_by_points($yearcode, $termcode, $formcode, $exam);
                }//form
            }//data

            if (sizeof($this->positioning) > 0) {
                foreach ($this->positioning as $row_id => $positions) {

                    $sql_update_cols = array();

                    foreach ($positions as $column => $value) {
                        $sql_update_cols[] = "{$column}={$value}";
                    }

                    $sql_update_cols_string = implode(',', $sql_update_cols);
                    $db->Execute("UPDATE SASTMN SET {$sql_update_cols_string} WHERE ID={$row_id}");
                }
            }

            return json_response(1, "Grading for {$yearcode}/Term {$termcode}, {$form_stream_code} Complete");

        } else {
            return json_response(0, "No Data Found");
        }


    }

    private function get_grade_code($points)
    {
        global $db;
        return $db->CacheGetOne(1200, "select GRADECODE from SAMGRD WHERE MIN<={$points} AND MAX>={$points}");
    }

    private function Get_Grade($gradesyscode = 'KN', $total)
    {
        global $db;
        $total = $total > 0 ? $total : 0;
        return $db->CacheGetRow(1200, "SELECT GRADECODE,COMMENTS,COMMENTS2 FROM SAXGRD WHERE GSYSCODE='{$gradesyscode}' AND MIN<={$total} AND MAX>={$total}");
    }

    private function save_student_mean($admno, $yearcode, $termcode, $formcode, $streamcode, $exam, $subject_total, $subject_total_max, $numsubj, $totalp, $totalm, $gradecodep, $gradecodem, $comments, $comments2)
    {
        //print_pre(func_get_args());//remove
        //echo "\$totalp={$totalp} <br>";//remove
        //echo "\$gradecodep={$gradecodep} <br>";//remove
        //echo "\$totalm={$totalm} <br>";//remove
        //echo "\$gradecodem={$gradecodem} <br>";//remove
        //exit();
        $record = new ADODB_Active_Record('SASTMN', array('ADMNO', 'YEARCODE', 'TERMCODE', 'EXAM'));
        $record->Load("ADMNO='{$admno}' AND YEARCODE='{$yearcode}' AND TERMCODE='{$termcode}' AND EXAM='{$exam}' ");

        if (empty($record->_original)) {
            $record->id = generateID($record->_tableat);
            $record->admno = $admno;
            $record->yearcode = $yearcode;
            $record->termcode = $termcode;
            $record->exam = $exam;
        }

        $record->formcode = $formcode;
        $record->streamcode = $streamcode;
        $record->total = $subject_total;
        $record->totalmax = $subject_total_max;
        $record->numsubj = $numsubj;
        $record->totalp = $totalp;
        $record->totalm = $totalm;

        $record->gradecodep = $gradecodep;
        $record->gradecodem = $gradecodem;

        $record->comments = $comments;
        $record->comments2 = $comments2;

        $save = $record->Save();

        //print_pre($record);//remove
//exit();
        if ($save) {
            return true;
        }

        return false;

    }

    private function position_form_by_marks($yearcode, $termcode, $formcode, $exam)
    {
        global $db;

        $data = $db->GetAssoc("
     	select ID, TOTALM  from SASTMN
     	where YEARCODE='{$yearcode}'
     	and TERMCODE='{$termcode}'
     	and FORMCODE='{$formcode}'
     	and EXAM='{$exam}'
     	order by TOTALM desc
     	");

        if ($data) {
            $total_array = array();

            foreach ($data as $id => $score) {
                $total_array[$score][] = $id;
            }

            if (sizeof($total_array) > 0) {
                $count = 0;

                foreach ($total_array as $total => $students) {

                    $count_skipped = sizeof($students);

                    if (sizeof($students) == 1) {
                        ++$count;

                        foreach ($students as $row_id) {
                            $this->positioning[$row_id]['POSFORM_MARKS'] = $count;
                        }

                    } else {
                        $count_continue = $count + $count_skipped;
                        ++$count;

                        foreach ($students as $row_id) {
                            $this->positioning[$row_id]['POSFORM_MARKS'] = $count;
                        }

                        $count = $count_continue;

                    }

                }
            }
        }

    }

    private function position_stream_by_marks($yearcode, $termcode, $streamcode, $exam)
    {
        global $db;

        $data = $db->GetAssoc("
     	select ID, TOTALM  from SASTMN
     	where YEARCODE='{$yearcode}'
     	and TERMCODE='{$termcode}'
     	and STREAMCODE='{$streamcode}'
     	and EXAM='{$exam}'
     	order by TOTALM desc
     	");

        if ($data) {
            $total_array = array();

            foreach ($data as $id => $score) {
                $total_array[$score][] = $id;
            }

            if (sizeof($total_array) > 0) {
                $count = 0;

                foreach ($total_array as $total => $students) {

                    $count_skipped = sizeof($students);

                    if (sizeof($students) == 1) {
                        ++$count;

                        foreach ($students as $row_id) {
                            $this->positioning[$row_id]['POSTREAM_MARKS'] = $count;
                        }

                    } else {
                        $count_continue = $count + $count_skipped;
                        ++$count;

                        foreach ($students as $row_id) {
                            $this->positioning[$row_id]['POSTREAM_MARKS'] = $count;
                        }

                        $count = $count_continue;

                    }

                }
            }
        }
    }

    private function position_form_by_points($yearcode, $termcode, $formcode, $exam)
    {
        global $db;


        //$data = $db->GetAssoc("
        //select ID, TOTALP  from SASTMN
        //where YEARCODE='{$yearcode}'
        //and TERMCODE='{$termcode}'
        //and FORMCODE='{$formcode}'
        //and EXAM='{$exam}'
        //order by TOTALP desc
        //");

        $data = $db->GetAssoc("
     	select ID, TOTALP,TOTALM,(TOTALP*TOTALM) TOTMULTI  from SASTMN
     	where YEARCODE='{$yearcode}'
     	and TERMCODE='{$termcode}'
     	and FORMCODE='{$formcode}'
     	and EXAM='{$exam}'
     	order by TOTALP DESC,TOTALM DESC,(TOTALP*TOTALM) DESC
     	");
        //order by TOTALP desc,TOTALM desc
        //print_pre($data);//remove

        if ($data) {
            $total_array = array();

            foreach ($data as $id => $scores) {
                $scorep = valueof($scores, 'TOTALP');
                $scorem = valueof($scores, 'TOTALM');
                $totmulti = valueof($scores, 'TOTMULTI');
                $total_array[$totmulti][] = $id;
            }

            //print_pre($total_array);//remove

            if (sizeof($total_array) > 0) {
                $count = 0;

                foreach ($total_array as $total => $students) {

                    $count_skipped = sizeof($students);

                    if (sizeof($students) == 1) {
                        ++$count;

                        foreach ($students as $row_id) {
                            $this->positioning[$row_id]['POSFORM_POINTS'] = $count;
                        }

                    } else {
                        $count_continue = $count + $count_skipped;
                        ++$count;

                        foreach ($students as $row_id) {
                            $this->positioning[$row_id]['POSFORM_POINTS'] = $count;
                        }

                        $count = $count_continue;

                    }

                }
            }
        }
    }

    private function position_stream_by_points($yearcode, $termcode, $streamcode, $exam)
    {
        global $db;

        //$data = $db->GetAssoc("
        //select ID, TOTALP  from SASTMN
        //where YEARCODE='{$yearcode}'
        //and TERMCODE='{$termcode}'
        //and STREAMCODE='{$streamcode}'
        //and EXAM='{$exam}'
        //order by TOTALP desc
        //");

        $data = $db->GetAssoc("
     	select ID, TOTALP,TOTALM,(TOTALP*TOTALM) TOTMULTI  from SASTMN
     	where YEARCODE='{$yearcode}'
     	and TERMCODE='{$termcode}'
     	and STREAMCODE='{$streamcode}'
     	and EXAM='{$exam}'
     	order by TOTALP DESC,TOTALM DESC,(TOTALP*TOTALM) DESC
     	");
        //order by TOTALP desc,TOTALM desc

        if ($data) {
            $total_array = array();

            //foreach ($data as $id=>$score){
            //$total_array[$score][] = $id;
            //}

            foreach ($data as $id => $scores) {
                $scorep = valueof($scores, 'TOTALP');
                $scorem = valueof($scores, 'TOTALM');
                $totmulti = valueof($scores, 'TOTMULTI');
                //$total_array[$scorem][] = $id;
                $total_array[$totmulti][] = $id;
            }

            if (sizeof($total_array) > 0) {
                $count = 0;

                foreach ($total_array as $total => $students) {

                    $count_skipped = sizeof($students);

                    if (sizeof($students) == 1) {
                        ++$count;

                        foreach ($students as $row_id) {
                            $this->positioning[$row_id]['POSTREAM_POINTS'] = $count;
                        }

                    } else {
                        $count_continue = $count + $count_skipped;
                        ++$count;

                        foreach ($students as $row_id) {
                            $this->positioning[$row_id]['POSTREAM_POINTS'] = $count;
                        }

                        $count = $count_continue;

                    }

                }//array total p
            }
        }

    }

    private function position_subjects($yearcode, $termcode, $formcode, $exam)
    {
        global $db;

        $position_col = strtoupper("{$exam}POS");

        $subjects = $db->GetAssoc("
     	select DISTINCT SUBJECTCODE,CODEOFFC from
     	VIEWSTUDENTSUBJECTS
     	where YEARCODE='{$yearcode}' and TERMCODE='{$termcode}' and FORMCODE='{$formcode}'
     	");

        if (!($subjects)) return;
        if (empty($subjects)) return;

        foreach ($subjects as $subjectcode => $subjectname) {

            $subject_positioning = array();

            $data = $db->GetAssoc("
     	select ID, {$exam}  from VIEWSTUDENTSUBJECTS
     	where YEARCODE='{$yearcode}'
     	and TERMCODE='{$termcode}'
     	and FORMCODE='{$formcode}'
     	AND SUBJECTCODE='{$subjectcode}'
     	order by {$exam} desc
     	");

            if ($data) {
                $total_array = array();

                foreach ($data as $id => $score) {
                    $total_array[$score][] = $id;
                }


                if (sizeof($total_array) > 0) {
                    $count = 0;

                    foreach ($total_array as $total => $students) {

                        $count_skipped = sizeof($students);

                        if (sizeof($students) == 1) {
                            ++$count;

                            foreach ($students as $row_id) {
                                $subject_positioning[$row_id] = $count;
                            }

                        } else {
                            $count_continue = $count + $count_skipped;
                            ++$count;

                            foreach ($students as $row_id) {
                                $subject_positioning[$row_id] = $count;
                            }

                            $count = $count_continue;

                        }
                    }
                }

                $subject_positioning_rev = array();

                foreach ($subject_positioning as $k => $v) {
                    $subject_positioning_rev[$v][] = $k;
                }

                if (sizeof($subject_positioning_rev) > 0) {
                    foreach ($subject_positioning_rev as $position => $student_ids) {
                        $student_ids_sql = '(' . implode(',', $student_ids) . ')';
                        $db->Execute("UPDATE SASTUDSUB SET {$position_col}={$position} WHERE ID IN {$student_ids_sql}");
                    }
                }

            }
        }
    }

}
