<?php


 $school = new school();
  
 $streams_data = $db->GetAssoc('SELECT STREAMCODE,STREAMNAME,FORMCODE FROM SASTREAMS');
 $years = $db->GetAssoc('SELECT YEARCODE,YEARNAME FROM SAYEAR ORDER BY YEARCODE');
 $terms = $db->GetAssoc('SELECT TERMCODE,TERMNAME FROM SATERMS ORDER BY TERMCODE');
 $exams = $db->GetAssoc('SELECT TERMCODE,TERMNAME FROM SATERMS ORDER BY TERMCODE');
 
 $forms = array();
 if(isset($streams_data)){
  if(sizeof($streams_data)>0){	
   foreach ($streams_data as $streamcode=>$stream_data){
   	$streamname = valueof($stream_data, 'STREAMNAME');
   	$formcode   = valueof($stream_data, 'FORMCODE');
   	$forms[$formcode] = "Form {$formcode}";
   	$forms[$streamcode] = "Form {$formcode}-{$streamname}";
   }
  }
 }
 
 ?>
<style>
div#<?php echo ui::fi('div_grade'); ?> { color:#500;}
</style>

   <div class="easyui-panel" title=""  style="font-size:12px;font-family:Verdana;"  fit="true">
    <div style="padding:10px">
	 <form id="ff<?php echo MNUID; ?>" method="post" novalidate>
		
	    	<table cellpadding="2" cellspacing="0" width="100%" >
	    	
	    		<tr>
	    			<td>Year:</td>
	    			<td colspan="2" >
	    				<?php echo ui::form_select_fromArray('year', $years, $school->active_yearcode," onchange=\"{$cfg['appname']}.list_exams();\" ");  ?>
	    			</td>
	    		</tr>
	    		
	    		<tr>
	    			<td>Term:</td>
	    			<td colspan="2" >
	    				<?php echo ui::form_select_fromArray('term', $terms,$school->active_termcode,"onchange=\"{$cfg['appname']}.list_exams();\"");  ?>
	    			</td>
	    		</tr>
	    		
	    		<tr>
	    			<td width="180px">Form</td>
	    			<td colspan="2" >
	    			<?php echo ui::form_select_fromArray('form', $forms,'',"onchange=\"{$cfg['appname']}.list_exams();\"");  ?>
	    			</td>
	    		</tr>
	    		
	    		<tr>
	    			<td>Exam:</td>
	    			<td colspan="2" >
	    			   <div id="<?php echo ui::fi('div_exams'); ?>">
	    				<?php echo ui::form_select_fromArray('exam', $exams);  ?>
	    				</div>
	    			</td>
	    		</tr>
	    		
	    		<tr>
	    			<td nowrap>After Regrade Print Option</td>
	    			<td colspan="2" >
	    			 <input type="radio" name="<?php echo ui::fi('subjscore');?>" id="<?php echo ui::fi('subjscorer');?>"  value="r" checked ><label for="<?php echo ui::fi('subjscorer');?>">as Raw Marks</label>
	    			 <input type="radio" name="<?php echo ui::fi('subjscore');?>" id="<?php echo ui::fi('subjscorep');?>"  value="p"><label for="<?php echo ui::fi('subjscorep');?>">Out of 100%</label>
	    			</td>
	    		</tr>
	    		
	    		<tr>
	    			<td colspan="3" >&nbsp;</td>
	    		</tr>
	    		
	    		<tr>
	    			<td><div id="<?php echo ui::fi('div_grade'); ?>">&nbsp;</div></td>
	    			<td colspan="2" ><div id="<?php echo ui::fi('prg_regrade'); ?>" class="easyui-progressbar" data-options="value:0" style="width:200px;"></div></td>
	    		</tr>
	    		
	    		<tr>
	    			<td>&nbsp;</td>
	    			<td>
	    			 <a href="javascript:void(0)" class="easyui-linkbutton"  onclick="<?php echo $cfg['appname']; ?>.regrade();" id="btnRegrade"><i class="fa fa-spinner"></i>  Re Grade All Subjects</a>
	    			</td>
	    			<td><div id="<?php echo ui::fi('div_spinner'); ?>">&nbsp;</div></td>
	    		</tr>
	    		
	    	</table>
 
	   <iframe name="fr<?php echo MNUID; ?>" id="fr<?php echo MNUID; ?>" frameborder="0" height="0px" width="100%" src="" scrolling="no"></iframe>
     </form>
    </div>
   </div>
	
	<script>
		
		var <?php echo $cfg['appname']; ?> = {
		clearForm:function (){
			$('#ff<?php echo MNUID; ?>').form('clear');
		},
		list_exams:function (){
            $.post('./endpoints/crud/',   'modvars=<?php echo $vars; ?>&function=list_exams&'+$('#ff<?php echo MNUID; ?>').serialize()   , function( html ){
  		 	$('#<?php echo ui::fi('div_exams'); ?>').html(html);
  		 });
		},
		regrade:function (){
			 var exam = $('#<?php echo ui::fi('exam');?>').val();
			 var exam_name  = $("#<?php echo ui::fi('exam');?> option:selected").text();
			 $.messager.progress();
			 var fdata = $('#ff<?php echo MNUID; ?>').serialize()  + '&modvars=<?php echo $vars; ?>&function=pre_run';
		     $.post('./endpoints/crud/', fdata, function(data) {
		     $.messager.progress('close');
             if (data.success === 1) {
                $('#<?php echo ui::fi('prg_regrade'); ?>').progressbar({ value: 0 });
                $('#btnRegrade').linkbutton('disable');
                $('#<?php echo ui::fi('div_spinner'); ?>').html('<img src="./public/images/spinner.gif" >');
                $('#fr<?php echo MNUID; ?>').attr('src', './endpoints/go/?to=regrade.php&modvars=<?php echo $vars; ?>&'+$('#ff<?php echo MNUID; ?>').serialize());
              } else {
                $.messager.alert('Error',data.message,'error');
             }
            }, "json");
		},
        print_marks:function (){
         var fdata      = $('#ff<?php echo MNUID; ?>').serialize();
         var exam_name  = $("#<?php echo ui::fi('exam');?> option:selected").text();
		 var <?php echo ui::fi('w'); ?>=window.open('../../endpoints/print/?modvars=<?php echo $vars; ?>&exam_name='+exam_name+'&'+fdata,'<?php echo ui::fi('pw'); ?>','height=800,width=900,toolbar=no,menubar=no,directories=no,location=no,scrollbars=yes,status=no,resizable=no,fullscreen=no,top=0,left=0');
          if(<?php echo ui::fi('w'); ?>){
			 <?php echo ui::fi('w'); ?>.focus();
		  }else{
			  alert('Allow Popups to Open from this Web Address');
		  }
	   },
     }
	</script>
