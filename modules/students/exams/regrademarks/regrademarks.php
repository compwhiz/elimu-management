<?php

  class regrademarks{
	private $id;
	private $datasrc;
	private $primary_key;
	private $regno;
	private $positioning = array();
	
	private $_formcode;
	private $_streamcode;
	private $_yearcode;
	private $_termcode;
	private $_exam;
	private $_position_by_marks_col;
	private $_position_by_points_col;
	private $_numsubjects;
	private $_minsubjects;
	
	private $_cat_required   = array();
	private $_cat_done       = array();
	private $_cat_picked     = array();
	private $_cat_dropped    = array();
	private $_cat_mandatory  = array();
	private $_cat_mandatory_missing  = array();
	private $_comnination_codes  = array();
	
	private $_setupmax_col;
	private $_setupmax;
	private $_setup  = array();
	
	public function __construct(){
		global $cfg;
		
		$this->id           = filter_input(INPUT_POST , 'id');
		$this->datasrc      = valueof($cfg,'datasrc');
		$this->primary_key  = valueof($cfg,'pkcol');
	}
	
	public function list_subjects(){
		global $db;
		
	 $formcode     = filter_input(INPUT_POST , ui::fi('form'));	
	 $yearcode     = filter_input(INPUT_POST , ui::fi('year'));	
	 $termcode     = filter_input(INPUT_POST , ui::fi('term'));
		
	  $subjects = $db->GetArray("
       SELECT DISTINCT S.SUBJECTCODE 
        FROM SATSS S
        INNER JOIN SASTREAMS T ON T.STREAMCODE = S.STREAMCODE
        WHERE T.FORMCODE='{$formcode}'
        AND S.YEARCODE='{$yearcode}'
        AND S.TERMCODE='{$termcode}'
       ");
     
	  $subjects_array = array();
	  
	  if($subjects){
	  	if(count($subjects)>0){
	  	  foreach ($subjects as $subject){
	  	  	$subjectcode  = valueof($subject,'SUBJECTCODE');
	  	  	$subjects_array[$subjectcode] = $subjectcode;
	  	  }
	  	}
	  }
	  
	  return ui::form_select_fromArray('subject',$subjects_array, ''," onchange=\"{$cfg['appname']}.list_exams();\" ");
	  
	}
	
	public function list_exams(){
		global $db;
		
	 $formcode     = filter_input(INPUT_POST , ui::fi('form'));	
	 $examcode     = filter_input(INPUT_POST , ui::fi('exam'));	
	 $yearcode     = filter_input(INPUT_POST , ui::fi('year'));	
	 $termcode     = filter_input(INPUT_POST , ui::fi('term'));
		
	 if(strlen($formcode)>1){
	 	$formcode = substr($formcode,0,1);
	 }
	 
	  $exams = $db->GetRow("
       SELECT S.CATNAME1 , S.CATNAME2 , S.CATNAME3 , S.CATNAME4 , S.CATNAME5,
        S.EXAMNAME1 , S.EXAMNAME2 , S.EXAMNAME3 , S.EXAMNAME4 , S.EXAMNAME5
        FROM SATSSG S
        WHERE S.FORMCODE='{$formcode}'
        AND S.YEARCODE='{$yearcode}'
        AND S.TERMCODE='{$termcode}'
       ");
	  
     $exams_array = array();
      
       if($exams){
       	if(sizeof($exams)>=1){
       		
       		for ($c=1;$c<=5;++$c){
       		 if(isset($exams["CATNAME{$c}"])){
       		 	 $exams_array["CAT{$c}"] = $exams["CATNAME{$c}"];
       		 }
       		}
       		
       		for ($e=1;$e<=5;++$e){
       		 if(isset($exams["EXAMNAME{$e}"])){
       		 	 $exams_array["PAPER{$e}"] = $exams["EXAMNAME{$e}"];
       		 }
       		}
       		
       		$exams_array['TOTAL'] = 'Combined';
       		
       	}
       }
	  
       
	  return ui::form_select_fromArray('exam',$exams_array, $examcode," onchange=\"\" ");
	  
	}
	
	public function pre_run() {
		global $db;
		
       $yearcode          = filter_input(INPUT_POST , ui::fi('year'));
       $termcode          = filter_input(INPUT_POST , ui::fi('term'));
       $form_stream_code  = filter_input(INPUT_POST , ui::fi('form'));
       $formcode          = filter_input(INPUT_POST , ui::fi('form'));
       $exam              = filter_input(INPUT_POST , ui::fi('exam'));
       
       if(empty($exam)){
        return json_response(0,"Please Exam Global Settings Form {$formcode} , {$yearcode} - Term {$termcode} ");
       }
       
       switch ($exam){
       	case 'CAT1':
       	 $this->_setupmax_col = 'CATMAX1';
       	 $exam_name_col = 'CATNAME1';
       	break;	
       	case 'CAT2':
       	 $this->_setupmax_col = 'CATMAX2';
       	 $exam_name_col = 'CATNAME2';
       	break;	
       	case 'CAT3':
       	 $this->_setupmax_col = 'CATMAX3';
       	 $exam_name_col = 'CATNAME3';
       	break;	
       	case 'CAT4':
       	 $this->_setupmax_col = 'CATMAX4';
       	 $exam_name_col = 'CATNAME4';
       	break;	
       	case 'CAT5':
       	 $this->_setupmax_col = 'CATMAX5';
       	 $exam_name_col = 'CATNAME5';
       	break;	
       	case 'PAPER1':
       	 $this->_setupmax_col = 'EXAMMAX1';
       	 $exam_name_col = 'EXAMNAME1';
       	break;	
       	case 'PAPER2':
       	 $this->_setupmax_col = 'EXAMMAX2';
       	 $exam_name_col = 'EXAMNAME2';
       	break;	
       	case 'PAPER3':
       	 $this->_setupmax_col = 'EXAMMAX3';
       	 $exam_name_col = 'EXAMNAME3';
       	break;	
       	case 'PAPER4':
       	 $this->_setupmax_col = 'EXAMMAX4';
       	 $exam_name_col = 'EXAMNAME4';
       	break;	
       	case 'PAPER5':
       	 $this->_setupmax_col = 'EXAMMAX5';
       	 $exam_name_col = 'EXAMNAME5';
       	break;	
       	case 'TOTAL':
       	 $this->_setupmax_col = 'EXAMMAX1';
       	 $exam_name_col = 'TOTAL';
       	break;	
       }
       
       
	   if(strlen($form_stream_code)==1){
	 	$form_stream = 'FORMCODE';
	   }else{
	   	$form_stream = 'STREAMCODE';
	   	$formcode = substr($formcode,0,1);
	   }
	   
       $this->_exam  = $exam;

       $settings = $db->GetAssoc("select SBCCODE,REQUIRED,MANDATORY from SAFCS 
       WHERE YEARCODE='{$yearcode}' 
       AND TERMCODE='{$termcode}' 
       AND FORMCODE='{$formcode}'
       ");
      
       if(empty($settings)){
        return json_response(0,"Please Setup Required Subjects for Form {$formcode} , {$yearcode} - Term {$termcode} ");
       }

       $this->_setup =  $db->GetRow("SELECT * FROM SATSSG WHERE FORMCODE='{$formcode}' AND YEARCODE='{$yearcode}' AND TERMCODE='{$termcode}'");
       
       if(empty($this->_setup)){
        return json_response(0,"Please Do Exam Global Setup for Form {$formcode} , {$yearcode} - Term {$termcode} ");
       }
       
       if($exam=='TOTAL'){
       $this->_setupmax         = (100);
       }else{
       $this->_setupmax         = valueof($this->_setup, $this->_setupmax_col,100);
       }
       
       $exams_config_gsyscode   = valueof($this->_setup, 'GSYSCODE');
       
       $data = $db->Execute("SELECT ID,ADMNO,SUBJECTCODE,{$exam} FROM VIEWSTUDENTSUBJECTS 
        WHERE YEARCODE='{$yearcode}'
        AND TERMCODE='{$termcode}' 
        AND {$form_stream}='{$form_stream_code}'
        AND {$exam}>0
        ORDER BY STREAMCODE, ADMNO, SORTPOS,SUBJECTCODE
        ");
	  	  
       if($data) {
       	    $num_records = $data->RecordCount();
       	 if($num_records>0){
       	  return json_response(1,'OK');
       	 }else{ 	
          return json_response(0,"No Records Found for Form {$formcode} , {$yearcode} - Term {$termcode} ");
         }
       }else{
        return json_response(0,"Error :".$db->ErrorMsg());
       }

	}
    
 }
