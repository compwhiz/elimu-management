<?php


 $school          = new school();
 $user            = new user();
 $terms           = $db->GetAssoc('SELECT TERMCODE,TERMNAME FROM SATERMS ORDER BY TERMCODE');
 $subjects_array  =  $db->GetAssoc("SELECT SUBJECTCODE,SUBJECTNAME FROM SASUBJECTS order by SUBJECTCODE");

 $my_subjects     =  $db->GetArray("
	SELECT SUBJECTCODE,STREAMCODE,YEARCODE FROM SATSA
	ORDER BY YEARCODE,STREAMCODE ASC
   ");


 $my_list    = array();
 $my_streams = array();
 $streams = array();
 $years = array();
 $subjects = array();
 $exams = array();

 if( sizeof($my_subjects)>0){
 	foreach ($my_subjects as $my_subject){
 		$subjectcode  = valueof($my_subject, 'SUBJECTCODE');
 		$streamcode   = valueof($my_subject, 'STREAMCODE');
 		$yearcode     = valueof($my_subject, 'YEARCODE');

 		$years[$yearcode] = $yearcode;
 		$subjects[$subjectcode] = valueof($subjects_array,$subjectcode);
 	}
 }

 ?>
	<form id="<?php echo ui::fi('ff'); ?>" method="post" onsubmit="return false;">
	  <table cellpadding="2" cellspacing="0" width="100%">

	  <tr>
			<td width="180px">Year:</td>
			<td>
			 <?php echo ui::form_select_fromArray('yearcode', $years, $school->active_yearcode," onchange=\"{$cfg['appname']}.list_streams();\" ");  ?>
			</td>
		</tr>

		<tr>
			<td>Term:</td>
			<td>
			<?php echo ui::form_select_fromArray('termcode', $terms,'',"onchange=\"{$cfg['appname']}.list_streams();\"");  ?>
			</td>
		</tr>

		<tr>
			<td >Form</td>
			<td>
			 <div id="<?php echo ui::fi('div_streams'); ?>">
			 <?php echo ui::form_select_fromArray('streamcode', $streams,'',"onchange=\"{$cfg['appname']}.list_exams();\"");  ?>
			 </div>
			</td>
		</tr>

	    <tr>
	     <td>Exam:</td>
	     <td>
	      <div id="<?php echo ui::fi('div_exams'); ?>">
	       <?php echo ui::form_select_fromArray('exam', $exams,'',"onchange=\"{$cfg['appname']}.list_subjects();\"");  ?>
	      </div>
	     </td>
	    </tr>
	    		
<!--
		<tr>
			<td>Subject:</td>
			<td>
			   <div id="<?php echo ui::fi('div_subjects'); ?>">
				<?php echo ui::form_select_fromArray('subjectcode', $subjects);  ?>
				</div>
			</td>
		</tr>
-->

		<tr>
			<td>&nbsp;</div></td>
			<td colspan="2" >&nbsp;</td>
		</tr>
		

		<tr>
			<td><div id="<?php echo ui::fi('div_grade'); ?>">&nbsp;</div></td>
			<td colspan="2" ><div id="<?php echo ui::fi('prg_rank'); ?>" class="easyui-progressbar" data-options="value:0" style="width:200px;"></div></td>
		</tr>
		
		<tr>
			<td>&nbsp;</td>
			<td>
			 <a href="javascript:void(0)" class="easyui-linkbutton"  onclick="<?php echo $cfg['appname']; ?>.rank();" id="btnRank"><i class="fa fa-spinner"></i>  Rank Subjects</a>
			</td>
			<td><div id="<?php echo ui::fi('div_spinner'); ?>">&nbsp;</div></td>
		</tr>
	    		
	 </table>
	</form>
	<iframe name="<?php echo ui::fi('fr'); ?>" id="<?php echo ui::fi('fr'); ?>" frameborder="0" height="0px" width="100%" src="" scrolling="no"></iframe>
	<script>
		
	 var <?php echo $cfg['appname']; ?> = {
		 clearForm:function (){
			$('#<?php echo ui::fi('ff'); ?>').form('clear');
		},
		list_streams:function (){
         $.post('./endpoints/crud/',   'modvars=<?php echo $vars; ?>&function=list_streams&'+$('#<?php echo ui::fi('ff'); ?>').serialize()   , function( html ){
	     $('#<?php echo ui::fi('div_subjects'); ?>').html('&nbsp;');
	     $('#<?php echo ui::fi('div_exams'); ?>').html('&nbsp;');
  	     $('#<?php echo ui::fi('div_streams'); ?>').html(html);
  	     });
        },
        list_exams:function (){
            $.post('./endpoints/crud/',   'module=<?php echo $vars; ?>&function=list_exams&'+$('#<?php echo ui::fi('ff'); ?>').serialize()   , function( html ){
  		 	$('#<?php echo ui::fi('div_exams'); ?>').html(html);
  		 });
		},
        list_subjects:function (){
          $.post('./endpoints/crud/',   'modvars=<?php echo $vars; ?>&function=list_subjects&'+$('#<?php echo ui::fi('ff'); ?>').serialize()   , function( html ){
  	      $('#<?php echo ui::fi('div_subjects'); ?>').html(html);
  	      });
        },
        rank:function (){
			var fdata      = $('#<?php echo ui::fi('ff'); ?>').serialize();
            $('#<?php echo ui::fi('prg_rank'); ?>').progressbar({ value: 0 });
            $('#btnRank').linkbutton('disable');
            $('#<?php echo ui::fi('div_spinner'); ?>').html('<img src="./public/images/spinner.gif" >');
            $('#<?php echo ui::fi('fr'); ?>').attr('src', './endpoints/go/?to=rank.php&modvars=<?php echo $vars; ?>&'+fdata);
		},
         print_report:function (){
         var fdata      = $('#<?php echo ui::fi('ff'); ?>').serialize();
         var exam_name  = $("#<?php echo ui::fi('exam'); ?> option:selected").text();
  	     var <?php echo ui::fi('w'); ?>=window.open('./endpoints/print/?modvars=<?php echo $vars; ?>&exam_name='+exam_name+'&'+fdata,'<?php echo ui::fi('pw'); ?>','height=800,width=830,toolbar=no,menubar=no,directories=no,location=no,scrollbars=yes,status=no,resizable=no,fullscreen=no,top=0,left=0');
          <?php echo ui::fi('w'); ?>.focus();
        },
	  }
			
	</script>
	
