<?php

@ignore_user_abort(true);
@set_time_limit(0);
@ini_set("max_execution_time", 600000000);

$ranking = new ranking();
$ranking->rank();

class ranking
{
    private $id;
    private $_formcode;
    private $_streamcode;
    private $_yearcode;
    private $_termcode;
    private $_exam;
    private $_subjectcode;

    public function __construct()
    {
        global $cfg;

    }


    public function rank()
    {
        global $db, $cfg;

        $yearcode = filter_input(INPUT_GET, ui::fi('yearcode'));
        $termcode = filter_input(INPUT_GET, ui::fi('termcode'));
        $formcode = filter_input(INPUT_GET, ui::fi('formcode'));
        $exam = filter_input(INPUT_GET, ui::fi('exam'));
        $subjectcode = filter_input(INPUT_GET, ui::fi('subjectcode'));

        if (empty($yearcode)) {
            echo "<script>parent.$.messager.alert('Error','Please Select Year','error');parent.\$('#btnRank').linkbutton('enable');</script>";
            die;
        }

        if (empty($termcode)) {
            echo "<script>parent.$.messager.alert('Error','Please Select Term','error');parent.\$('#btnRank').linkbutton('enable');</script>";
            die;
        }

        if (empty($exam)) {
            echo "<script>parent.$.messager.alert('Error','Please Select Exam','error');parent.\$('#btnRank').linkbutton('enable');</script>";
            die;
        }

        if (empty($formcode)) {
            echo "<script>parent.$.messager.alert('Error','Please Select Form','error');parent.\$('#btnRank').linkbutton('enable');</script>";
            die;
        }

        $streams = $db->CacheGetAssoc(1200, "
     	select DISTINCT STREAMCODE,FORMCODE from
     	VIEWSTUDENTSUBJECTS
     	where YEARCODE='{$yearcode}' and TERMCODE='{$termcode}' and FORMCODE='{$formcode}'
     	");

        if (sizeof($streams) == 0) return 'No Streams Found';

        //$subjectcode = 'GEO';
        $subjects_sql = "
     	select DISTINCT SUBJECTCODE,CODEOFFC from
     	VIEWSTUDENTSUBJECTS where YEARCODE='{$yearcode}' 
     	and TERMCODE='{$termcode}' 
     	and FORMCODE='{$formcode}'
     	";

        $subjects_sql .= !empty($subjectcode) ? " and SUBJECTCODE='{$subjectcode}'  " : '';
        //$subjects_sql     .=  " and STREAMCODE='4N'  " ;

        $subjects = $db->CacheGetAssoc(1200, $subjects_sql);

        //print_pre($subjects);//remove

        if (!($subjects)) return;
        if (empty($subjects)) return;

        foreach ($subjects as $subjectcode => $subjectname) {

            $subject_positioning = array();

            // Code that is used to rank subjects and should be ported to report card.

            $data_form = $db->Execute("
     	SELECT ID,
          CASE 
            WHEN @{$exam} = COALESCE({$exam}, 0) THEN @rownum 
            ELSE @rownum := @rownum + 1 
          END AS POSITION,
          @{$exam} := COALESCE({$exam}, 0) {$exam}
		  FROM SASTUDSUB
		  JOIN (SELECT @rownum := 0, @{$exam} := NULL) r
		  WHERE YEARCODE={$yearcode} AND TERMCODE={$termcode} AND FORMCODE={$formcode} AND SUBJECTCODE='{$subjectcode}'
		  ORDER BY {$exam} DESC
     	");

            if ($data_form) {
                $count = 1;
                $num_records = $data_form->RecordCount();
                $positions = array();
                $position_colf = strtoupper("{$exam}POSF");

                $pos_fix_add = 0;
                foreach ($data_form as $record) {
                    $position = valueof($record, 'POSITION');

                    if ($position == 0) {
                        $pos_fix_add = 1;
                    }

                    $position = $position + $pos_fix_add;
                    $id = valueof($record, 'ID');
                    $positions[$position][] = $id;
                }

                //print_pre($positions);//remove
                //exit();
                foreach ($positions as $position => $ids) {
                    $ids_str = implode(',', $ids);
                    $update = $db->Execute("UPDATE SASTUDSUB SET {$position_colf}={$position} WHERE ID IN ({$ids_str}) ");

                    if ($update) {
                        $progress = ($count / $num_records) * 100;
                        $progress = round($progress, 0);

                        echo "
		<script>
          parent.\$('#" . ui::fi('prg_rank') . "').progressbar({ value: {$progress}});
          parent.\$('#" . ui::fi('div_grade') . "').html('Form: {$formcode} {$subjectcode}: <b>{$count}</b>/<b>{$num_records}</b>');
        </script>
		";

                        sleep(.7);
                        flush();
                        ++$count;

                    }

                }

            }

            foreach ($streams as $streamcode => $formcode) {


                $data_stream = $db->Execute("
     	SELECT ID,
          CASE 
            WHEN @{$exam} = COALESCE({$exam}, 0) THEN @rownum 
            ELSE @rownum := @rownum + 1 
          END AS POSITION,
          @{$exam} := COALESCE({$exam}, 0) {$exam}
		  FROM SASTUDSUB
		  JOIN (SELECT @rownum := 0, @{$exam} := NULL) r
		  WHERE YEARCODE={$yearcode} AND TERMCODE={$termcode} AND FORMCODE={$formcode} AND STREAMCODE='{$streamcode}' AND SUBJECTCODE='{$subjectcode}'
		  ORDER BY {$exam} DESC
     	");


                if ($data_stream) {
                    $count = 1;
                    $num_records = $data_stream->RecordCount();
                    $positions = array();
                    $position_cols = strtoupper("{$exam}POS");

                    $pos_fix_add = 0;
                    foreach ($data_stream as $record) {
                        $position = valueof($record, 'POSITION');

                        if ($position == 0) {
                            $pos_fix_add = 1;
                        }

                        $position = $position + $pos_fix_add;
                        $id = valueof($record, 'ID');
                        $positions[$position][] = $id;
                    }

                    foreach ($positions as $position => $ids) {
                        $ids_str = implode(',', $ids);
                        $update = $db->Execute("UPDATE SASTUDSUB SET {$position_cols}={$position} WHERE ID IN ({$ids_str}) ");

                        if ($update) {
                            $progress = ($count / $num_records) * 100;
                            $progress = round($progress, 0);

                            echo "
		<script>
          parent.\$('#" . ui::fi('prg_rank') . "').progressbar({ value: {$progress}});
          parent.\$('#" . ui::fi('div_grade') . "').html('Streams: {$streamcode},{$subjectcode} : <b>{$count}</b>/<b>{$num_records}</b>');
        </script>
		";

                            sleep(.7);
                            flush();
                            ++$count;

                        }
                    }
                }//data_stream
            }//stream
        }//each subject

        echo "
		<script>
		  parent.alert('Done Ranking');
          parent.\$('#btnRank').linkbutton('enable');
          parent.\$('#" . ui::fi('prg_rank') . "').progressbar({ value: 100});
          parent.\$('#" . ui::fi('div_spinner') . "').html('&nbsp;');
        </script>
		";

    }

}
