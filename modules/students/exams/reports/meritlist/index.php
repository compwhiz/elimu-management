<?php

 $school = new school();
 $streams_data = $db->GetAssoc('SELECT STREAMCODE,STREAMNAME,FORMCODE FROM SASTREAMS');
 $years = $db->GetAssoc('SELECT YEARCODE,YEARNAME FROM SAYEAR ORDER BY YEARCODE');
 $terms = $db->GetAssoc('SELECT TERMCODE,TERMNAME FROM SATERMS ORDER BY TERMCODE');

 $forms = array();
 $display = array();
 $exams = array();

 if(isset($streams_data)){
  if(sizeof($streams_data)>0){
   foreach ($streams_data as $streamcode=>$stream_data){
   	$streamname = valueof($stream_data, 'STREAMNAME');
   	$formcode   = valueof($stream_data, 'FORMCODE');
   	$forms[$formcode] = "Form {$formcode}";
   	$forms[$streamcode] = "Form {$formcode}-{$streamname}";
   }
  }
 }


$display[1000]   = 'All';
$display[10]  = 'Top 10';
$display[20]  = 'Top 20';
$display[-10] = 'Last 10';
$display[-20] = 'Last 20';
?>
  <div class="easyui-panel" title="" fit="true">
    <div style="padding:5px">
	 <form id="<?php echo ui::fi('ff'); ?>" method="post" novalidate>

	    	<table cellpadding="2" cellspacing="0" width="100%">

	    		<tr>
	    			<td width="150px">Year:</td>
	    			<td>
	    				<?php echo ui::form_select_fromArray('year', $years, $school->active_yearcode ," onchange=\"{$cfg['appname']}.list_exams();\" ");  ?>
	    			</td>
	    		</tr>

	    		<tr>
	    			<td>Term:</td>
	    			<td>
	    				<?php echo ui::form_select_fromArray('term', $terms,$school->active_termcode,"onchange=\"{$cfg['appname']}.list_exams();\"");  ?>
	    			</td>
	    		</tr>

	    		<tr>
	    			<td >Form</td>
	    			<td>
	    			<?php echo ui::form_select_fromArray('form', $forms,'',"onchange=\"{$cfg['appname']}.list_exams();\"");  ?>
	    			</td>
	    		</tr>

	    		<tr>
	    			<td>Exam:</td>
	    			<td>
	    			   <div id="<?php echo ui::fi('div_exams'); ?>">
	    				<?php echo ui::form_select_fromArray('exam', $exams);  ?>
	    				</div>
	    			</td>
	    		</tr>

	    		<tr>
	    			<td>Positioning</td>
	    			<td>
	    			 <input type="radio" name="positioning_ml" id="posm"  value="m" checked ><label for="posm">By Marks</label>
	    			 <input type="radio" name="positioning_ml" id="posp"  value="p"><label for="posp">By Points</label>
	    			</td>
	    		</tr>

	    		<tr>
	    			<td>Score to Display</td>
	    			<td>
	    			 <input type="radio" name="subjscore_ml" id="subjscorer"  value="r" checked ><label for="subjscorer">as Raw Marks</label>
	    			 <input type="radio" name="subjscore_ml" id="subjscorep"  value="p"><label for="subjscorep">Out of 100%</label>
	    			</td>
	    		</tr>

	    		<tr>
	             <td>Students to Display:</td>
	             <td>
	              <?php echo ui::form_select_fromArray('display', $display,1000,"");  ?>
	             </td>
	             </tr>

	    		<tr>
	    		 <td colspan="2"><hr></td>
	    		</tr>

	    		<tr>
	    			<td>&nbsp;</td>
	    			<td>
	    			 <a href="javascript:void(0)" class="easyui-linkbutton" onclick="<?php echo $cfg['appname']; ?>.print_report();"><i class="fa fa-print"></i> Open Report</a>
	    			</td>
	    		</tr>

	    	</table>

     </form>
    </div>
   </div>

	<script>

		var <?php echo $cfg['appname']; ?> = {
		 clearForm:function (){
			$('#<?php echo ui::fi('ff'); ?>').form('clear');
		},
		list_exams:function (){
            $.post('./endpoints/crud/',   'modvars=<?php echo $vars; ?>&function=list_exams&'+$('#<?php echo ui::fi('ff'); ?>').serialize()   , function( html ){
  		 	$('#<?php echo ui::fi('div_exams'); ?>').html(html);
  		 });
		},
        print_report:function (){
         var fdata = $('#<?php echo ui::fi('ff'); ?>').serialize();
         var exam_name  = $("#<?php echo ui::fi('exam'); ?> option:selected").text();
  	     var <?php echo ui::fi('w'); ?>=window.open('./endpoints/print/?modvars=<?php echo $vars; ?>&exam_name='+exam_name+'&'+fdata,'<?php echo ui::fi('pw'); ?>','height=800,width=1000,toolbar=no,menubar=no,directories=no,location=no,scrollbars=yes,status=no,resizable=no,fullscreen=no,top=0,left=0');
          <?php echo ui::fi('w'); ?>.focus();
        },
	  }

	</script>

</html>
