<?php

/**
* auto created config file for modules/students/exams/reports/rawmarks
* @author kenmsh@gmail.com
* 
* @version 2.0
* @since 2016-03-23 14:12:09
*/

 final class rawmarks {
 private $id;
 private $datasrc;
 private $primary_key;
	
 public function __construct(){
  global $cfg;
		
     $this->id           = filter_input(INPUT_POST , 'id');
     $this->datasrc      = valueof($cfg,'datasrc');
     $this->primary_key  = valueof($cfg,'pkcol');
 }
	
 
	public function list_subjects(){
		global $db,$cfg;
		
	 $formcode     = filter_input(INPUT_POST , ui::fi('form'));	
	 $yearcode     = filter_input(INPUT_POST , ui::fi('year'));	
	 $termcode     = filter_input(INPUT_POST , ui::fi('term'));
		

	  $subjects = $db->GetArray("
       SELECT DISTINCT S.SUBJECTCODE 
        FROM SATSS S
        INNER JOIN SASTREAMS T ON T.STREAMCODE = S.STREAMCODE
        WHERE T.FORMCODE='{$formcode}'
        AND S.YEARCODE='{$yearcode}'
        AND S.TERMCODE='{$termcode}'
       ");
     
	  $subjects_array = array();
	  
	  if($subjects){
	  	if(count($subjects)>0){
	  	  foreach ($subjects as $subject){
	  	  	$subjectcode  = valueof($subject,'SUBJECTCODE');
	  	  	$subjects_array[$subjectcode] = $subjectcode;
	  	  }
	  	}
	  }
	  
	  return ui::form_select_fromArray('subject',$subjects_array, ''," onchange=\"{$cfg['appname']}.list_exams();\" ");
	  
	}
	
	public function list_exams(){
		global $db,$cfg;
		
	 $formcode     = filter_input(INPUT_POST , ui::fi('form'));	
	 $yearcode     = filter_input(INPUT_POST , ui::fi('year'));	
	 $termcode     = filter_input(INPUT_POST , ui::fi('term'));
	 $examcode     = filter_input(INPUT_POST , ui::fi('exam'));	
	 
	 if(strlen($formcode)>1){
	 	$formcode = substr($formcode,0,1);
	 }
	 
	  $exams = $db->GetRow("
       SELECT S.CATNAME1 , S.CATNAME2 , S.CATNAME3 , S.CATNAME4 , S.CATNAME5,
        S.EXAMNAME1 , S.EXAMNAME2 , S.EXAMNAME3 , S.EXAMNAME4 , S.EXAMNAME5
        FROM SATSSG S
        WHERE S.FORMCODE='{$formcode}'
        AND S.YEARCODE='{$yearcode}'
        AND S.TERMCODE='{$termcode}'
       ");
	  
     $exams_array = array();
      
       if($exams){
       	if(sizeof($exams)>=1){
       		
       		for ($c=1;$c<=5;++$c){
       		 if(isset($exams["CATNAME{$c}"])){
       		 	 $exams_array["CAT{$c}"] = $exams["CATNAME{$c}"];
       		 }
       		}
       		
       		for ($e=1;$e<=5;++$e){
       		 if(isset($exams["EXAMNAME{$e}"])){
       		 	 $exams_array["PAPER{$e}"] = $exams["EXAMNAME{$e}"];
       		 }
       		}
       		
       		$exams_array['TOTAL'] = 'Combined';
       		
       	}
       }
       
	  return ui::form_select_fromArray('exam',$exams_array, $examcode," onchange=\"\" ");
	  
	}
	
 
}
