<?php

$yearcode    = filter_input(INPUT_GET , ui::fi('year'));
$termcode    = filter_input(INPUT_GET , ui::fi('term'));
$admno       = filter_input(INPUT_GET , ui::fi('find_admno'));
$formstream  = filter_input(INPUT_GET , ui::fi('form'));
$exam        = filter_input(INPUT_GET , ui::fi('exam'));
$positioning = filter_input(INPUT_GET , ui::fi('positioning'));

if(strlen($formstream)>1){
  $formstream_code_col = "STREAMCODE";
  $positioning_col     = "POSTREAM";
  $formcode            = substr($formstream,0,1);

   switch($positioning){
	case 'p':
     $positioning_col        = 'POSTREAM_POINTS';
     $display_col_name       = 'TOTALP';
     $display_col_title      = 'PNTS';
     $display_col_gradecode  = 'GRADECODEP';
     $positioning_by_descr   = ' POINTS ';
    break;
    case 'm':
    default:
     $positioning_col        = 'POSTREAM_MARKS';
     $display_col_name       = 'TOTALM';
     $display_col_title      = 'AVE';
     $display_col_gradecode  = 'GRADECODEM';
     $positioning_by_descr   = ' MARKS ';
    break;
  }

}else{
   $formstream_code_col = "FORMCODE";
   $positioning_col     = "POSFORM";
   $formcode            = $formstream;

   switch($positioning){
	case 'p':
     $positioning_col        = 'POSFORM_POINTS';
     $display_col_name       = 'TOTALP';
     $display_col_title      = 'PNTS';
     $display_col_gradecode  = 'GRADECODEP';
     $positioning_by_descr   = ' POINTS ';
    break;
    case 'm':
    default:
     $positioning_col        = 'POSFORM_MARKS';
     $display_col_name       = 'TOTALM';
     $display_col_title      = 'AVE';
     $display_col_gradecode  = 'GRADECODEM';
     $positioning_by_descr   = ' MARKS ';
    break;
  }

}

 switch($positioning){
	case 'p':
     $subject_scorecol_postfix  = 'P';
    break;
    case 'm':
    default:
     $subject_scorecol_postfix  = '';
    break;
  }

$subject_scorecol_postfix  = 'P';
$exam                      = empty($exam) ? 'TOTAL' : $exam;

$prefix_meangrade_gradecode = 'P';//M
$prefix_meangrade_pos_col   = 'POINTS';//MARKS
$prefix_meangrade_pos_col   = $positioning_by_descr;//'POINTS';//MARKS


$school      = new school(120);
$dateNow     = textDate(null, true);

$where_am_i   = str_replace($_SERVER['DOCUMENT_ROOT'],'', ROOT);
$server_name  = $_SERVER['SERVER_NAME'];
$server_port  = $_SERVER['SERVER_PORT'];

$x = pathinfo(realpath($_SERVER['DOCUMENT_ROOT']));
$y = pathinfo(realpath(ROOT));
$z = '';

if(isset($x['dirname']) && isset($y['dirname'])){
 if($x != $y){
  $z = $y['filename'];
 }
}

$server_root =  "http://{$server_name}:{$server_port}/{$z}/";

require_once ( ROOT.'lib/jpgraph/jpgraph.php');
require_once ( ROOT.'lib/jpgraph/jpgraph_bar.php');
require_once ( ROOT.'lib/jpgraph/jpgraph_line.php');


//cleanup
 $files = glob(ROOT.'tmp_files/*.png');
 foreach($files as $file){
  if(is_file($file)){
    unlink($file);
  }
 }


  function print_report_form( $admno, $array_student_subjects_data ){
 	global $db, $subject_config, $global_exams_setup, $num_subjects_students,$subject_teachers,$student_means,$yearcode, $termcode,
 	$school,$dateNow,$num_streams_students,$num_form_students,$numsubjects_max_score,$numsubjects_max_points,$exam,
 	$classTeachers, $principal_name,$signature,$next_term_starts,$server_root,$header_color,$prefix_meangrade_gradecode,
 	$prefix_meangrade_pos_col,$positioning_col,$display_col_name,$subject_scorecol_postfix;

 	 $subjects = array();
 	 $terms_progress = array();
 	 $terms_progress_per_term = array();

 	 $student_mean = isset($student_means[$admno]) ? $student_means[$admno] : array();

     $progress = $db->GetArray("
     select FORMCODE,TERMCODE,TOTAL,TOTALP POINTS,ROUND(TOTALP/NUMSUBJ,2) MEAN_SCORE,
     GRADECODEM,GRADECODEP ,
     POSTREAM_MARKS,POSFORM_MARKS,POSTREAM_POINTS,POSFORM_POINTS
     from SASTMN WHERE ADMNO='{$admno}' AND EXAM='{$exam}'  AND TOTAL>0
     ");

     if($progress){
     	if(is_array($progress)){
     		foreach ($progress as $term_progress){
     			$formcode       = valueof($term_progress, 'FORMCODE');
     			$termcode       = valueof($term_progress, 'TERMCODE');
     			$total          = valueof($term_progress, 'TOTAL');
     			$points         = valueof($term_progress, 'POINTS');
     			$gradecode      = valueof($term_progress, 'GRADECODE'.$prefix_meangrade_gradecode);
     			$mean_score     = valueof($term_progress, 'MEAN_SCORE');
     			$position       = valueof($term_progress, $positioning_col);

     			$terms_progress[$formcode][$termcode] = array(
     			'total'=>round($total,0),
     			'points'=>round($points,0),
     			'gradecode'=>$gradecode,
     			'mean_score'=>round($mean_score,2),
     			'position'=>$position,
     			);

     			$terms_progress_per_term[$termcode][$formcode] = round($mean_score,2);

     		}
     	}
     }

 	 $fullname        = valueof($student_mean,'FULLNAME');
 	 $streamcode      = valueof($student_mean,'STREAMCODE');
 	 $mean_total      = valueof($student_mean,'TOTAL','','number_format');
 	 $numsubj         = valueof($student_mean,'NUMSUBJ');
 	 $mean_total_raw  = valueof($student_mean,'TOTAL',0);
 	 $mean_totalmax   = valueof($student_mean,'TOTALMAX','','number_format');
 	 $mean_totalmax_raw   = valueof($student_mean,'TOTALMAX');
     $termmean        = $numsubj>0 ?  ($mean_total_raw*100)/$mean_totalmax_raw: 0;//bodmas
     $termmean        = $termmean>0 ?  round($termmean,2): 0;//bodmas

 	 $mean_totalp     = valueof($student_mean,'TOTALP','','number_format');
 	 $mean_totalm     = valueof($student_mean,'TOTALM','','number_format');
 	 $gradecodep      = valueof($student_mean,'GRADECODEP');
 	 $gradecodem      = valueof($student_mean,'GRADECODEM');
 	 $gradecode_mean  = valueof($student_mean,'GRADECODE'.$prefix_meangrade_gradecode);

 	 $position_class  = valueof($student_mean,'POSTREAM_'.trim($prefix_meangrade_pos_col));
 	 $position_form   = valueof($student_mean,'POSFORM_'.trim($prefix_meangrade_pos_col));
 	 $kcpe_point      = valueof($student_mean,'KCPEPOINT');
 	 $kcpegrade       = valueof($student_mean,'KCPEGRADE');
 	 $kcpemean        = valueof($student_mean,'KCPEMEAN');
     $kcpemean        = $kcpemean*1;
 	 $kcpemean        = round($kcpemean,1);
 	 $kcpepoint       = valueof($student_mean,'KCPEPOINT');
 	 $kcperank        = valueof($student_mean,'KCPERANK');
 	 $kcpemarks_raw   = valueof($student_mean,'KCPEMARKS');
 	 $kcpemarks   = $kcpemarks_raw*1;
 	 $comments_principle  = valueof($student_mean,'COMMENTS','&nbsp;');
 	 $comments_clsteacher = valueof($student_mean,'COMMENTS2','&nbsp;');
 	 $value_added         = $termmean - $kcpemean;
 	 $value_added_symbol  = $value_added > 0 ? '+' : '-';
     $classTeacherVars    = valueof($classTeachers, $streamcode);
     $classTeacher        = valueof($classTeacherVars, 'SLTCODE').' '.valueof($classTeacherVars, 'FULLNAME');
     $photo_name          = $admno.'.jpg';
     $photo_name_default  = 'profile-default.gif';
	 $photo               = ROOT.'public/photos/'.$photo_name;
	 $photo_default       = ROOT.'public/photos/'.$photo_name_default;
	 $rand                = mt_rand();
	 $student_photo       = '';


	 if(file_exists($photo) && is_readable($photo)){
	 	$student_photo = '<img src="../../public/photos/'.$photo_name.'?cache_reload='.$rand.'" border="0">';
	 }else{
	 	$student_photo = '<img src="../../public/photos/'.$photo_name_default.'?cache_reload='.$rand.'" border="0">';
	 }

 	 $num_stream_students = isset($num_streams_students[$streamcode]) ? $num_streams_students[$streamcode] : 0;

 	 foreach ($array_student_subjects_data as $subjectcode=>$subject){
 	  $subjectname = valueof($subject, 'SUBJECTNAME');
 	  $codeoffc    = valueof($subject, 'CODEOFFC');
 	  $subjects[$subjectcode] = array($codeoffc,$subjectname);
 	 }

 	 $num_cats   =  0;
 	 $num_exams  =  0;

       if(isset($subject_config['cats']) && sizeof($subject_config['cats'])>0){
        	foreach ($subject_config['cats'] as $cat_index => $cat){
        		++$num_cats;
        	}
        }

        if(isset($subject_config['papers']) && sizeof($subject_config['papers'])>0){
        	foreach ($subject_config['papers'] as $cat_index => $paper){
        		++$num_exams;
        	}
        }



     $exams_count = $num_cats + $num_exams;
     $cat_exam_col_span_add  = $exams_count>1 ? 9 : 8;
     $cat_exam_col_span = $num_cats + $num_exams + $cat_exam_col_span_add;

     //echo "\$exams_count={$exams_count} <br>";//remove

     echo '<table id="marks" cellpadding="1" cellspacing="0" width="100%" border="0" class="report-form">';
      echo '<thead>';

 	 	echo  "<tr>";
 	 	 echo "<td colspan=\"{$cat_exam_col_span}\">

 	 	   <table width=\"100%\" border=0 cellspacing=\"0\" cellpadding=\"1\" class=\"plain top\">

 	 	    <tr>
 	 	     <td rowspan=\"2\" valign=\"top\" ><img src=\"{$school->logo_path}\" ></td>
 	 	     <td valign=\"top\"  colspan=\"1\">

 	 	     <table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"1\" class=\"plain top\">

 	 	      <tr>
 	 	       <td ><span class=\"title\" style=\"color:{$header_color}\" >{$school->name}</span></td>
 	 	      </tr>

 	 	    <tr>
 	 	     <td  colspan=\"1\" style=\"color:{$header_color}\" ><b>Address : {$school->address}</b></td>
 	 	    </tr>

 	 	    <tr>
 	 	     <td colspan=\"1\" style=\"color:{$header_color}\" ><b>Tel :{$school->telephone}</b></td>
 	 	    </tr>

 	 	    <tr>
 	 	     <td colspan=\"1\" style=\"color:{$header_color}\" ><b>School Motto :{$school->motto}</b></td>
 	 	    </tr>

 	 	    <tr>
 	 	     <td colspan=\"1\" style=\"color:{$header_color}\" ><b>&nbsp;</b></td>
 	 	    </tr>

 	 	     </table>

 	 	    </td>
 	 	     <td valign=\"top\" align=\"right\" rowspan=\"2\" >{$student_photo}</td>
 	 	    </tr>

 	 	    <tr>
 	 	     <td colspan=\"1\" align=\"center\"><h3 style=\"padding:0;margin:0;\">REPORT FORM</h3></td>
 	 	    </tr>

 	 	   </table>

 	 	 </td>";

 	 	echo "</tr>";

 	    echo  "<tr>";
 	 	 echo "<td colspan=\"{$cat_exam_col_span}\" style=\"padding-top:2px;padding-bottom:2px;\">";
          echo "<table width=\"100%\" border=0 cellspacing=\"0\" cellpadding=\"1\" class=\"plain\">";
           echo '<tr>';
            echo '<td>ADM NO </td>';
            echo '<td>'.$admno.'</td>';
            echo '<td>Name </td>';
            echo '<td>'.$fullname.'</td>';
            echo '<td>Date Printed</td>';
            echo '<td>'.$dateNow.'</td>';
           echo '</tr>';
          echo '</table>';
         echo '</td>';
        echo '</tr>';


 	    echo  "<tr>";
 	 	 echo "<td colspan=\"{$cat_exam_col_span}\" style=\"padding-top:2px;padding-bottom:2px;\">";
          echo "<table width=\"100%\" border=0 cellspacing=\"0\" cellpadding=\"1\" class=\"plain\">";
           echo '<tr>';
            echo '<td>Form</td>';
            echo '<td>'.$streamcode.'</td>';
            echo '<td>Term</td>';
            echo '<td>'.$termcode.'</td>';
            echo '<td>Year</td>';
            echo '<td>'.$yearcode.'</td>';
            echo '<td width=\"60%\">&nbsp;</td>';
           echo '</tr>';
          echo '</table>';
         echo '</td>';
        echo '</tr>';

 	 	$kcpe_num_students ='';
 	 	$kcpe_num_students = $num_stream_students;

 	    echo  "<tr>";
 	 	 echo "<td colspan=\"{$cat_exam_col_span}\" style=\"padding-top:2px;padding-bottom:2px;\">";
          echo "<table width=\"100%\" border=0 cellspacing=\"0\" cellpadding=\"1\" class=\"plain\">";
           echo '<tr>';
            echo '<td>Rank (Class)</td>';
            echo '<td><u>'.$position_class.'</u> out of <u>'.$num_stream_students.'</u></td>';
            echo '<td>Rank (Form)</td>';
            echo '<td><u>'.$position_form.'</u> out of <u>'.$num_form_students.'</u></td>';
            echo '<td>KCPE MARKS</td>';
            echo '<td>'.$kcpemarks.'&nbsp;'.$kcpegrade.'</td>';
            echo '<td>KCPE Rank</td>';
            echo '<td>'.$kcperank.' out of '.$num_form_students.'</td>';
           echo '</tr>';
          echo '</table>';
         echo '</td>';
        echo '</tr>';


       echo '<tr>';
        echo '<td   class=\"header\" ><b>SUBJECT</b></td>';
        echo '<td   class=\"header\" ><b>CODE</b></td>';

        if(isset($subject_config['cats']) && sizeof($subject_config['cats'])>0){
        	foreach ($subject_config['cats'] as $cat_index => $cat){
        		$name = valueof($cat,'name','','Camelize');
        		$max  = valueof($cat,'max');
               echo "<td   class=\"header\"><b>{$name}</b></td>";
        	}
        }

        if(isset($subject_config['papers']) && sizeof($subject_config['papers'])>0){
        	foreach ($subject_config['papers'] as $cat_index => $paper){
        		$name = valueof($paper,'name','','Camelize');
        		$max  = valueof($paper,'max');
               echo "<td  class=\"header\"><b>{$name}</b></td>";
        	}
        }

       if($exams_count>1){
       echo "<td class=\"header right\">Total</td>";
       }

       echo "<td class=\"header right\">Grade</td>";
       echo "<td class=\"header right\">PTS</td>";
       echo "<td class=\"header right\">SUBJECT <BR>POSITION</td>";
       echo "<td class=\"header right\" nowrap>REMARKS</td>";
       echo "<td class=\"header \" nowrap  colspan=\"2\" >SUBJ <br>TEACHER</td>";

       echo "</tr>";

       echo '<tr>';
        echo '<td  class="header left right">&nbsp;</td>';
        echo '<td  class="header left right">&nbsp;</td>';

        if(isset($subject_config['cats']) && sizeof($subject_config['cats'])>0){
        	foreach ($subject_config['cats'] as $cat_index => $cat){
        		$name = valueof($cat,'name');
        		$max  = valueof($cat,'max');
               echo "<td  class=\"header right\"><b>{$max}</b></td>";
        	}
        }

        if(isset($subject_config['papers']) && sizeof($subject_config['papers'])>0){
        	foreach ($subject_config['papers'] as $cat_index => $paper){
        		$name = valueof($paper,'name');
        		$max  = valueof($paper,'max');
               echo "<td  class=\"header right\"><b>{$max}</b></td>";
        	}
        }

       if($exams_count>1){
       echo "<td class=\"header\" colspan=\"1\"><b>%</b></td>";
       }

       echo "<td class=\"header\" colspan=\"1\">&nbsp;</td>";
       echo "<td class=\"header\" colspan=\"1\">&nbsp;</td>";
       echo "<td class=\"header\" colspan=\"1\">&nbsp;</td>";
       echo "<td class=\"header\" colspan=\"1\">&nbsp;</td>";
       echo "<td class=\"header\" colspan=\"2\" >&nbsp;</td>";
       echo "</tr>";
      echo '</thead>';

      if(count($subjects)>0) {

       echo '<tbody>';

       $student= array();
       $count = 1;


        if($exam=='TOTAL'){
		$gradecodecol_prefix = 'GRADECODE';
		$pointscol_prefix    = 'POINTS';
		$commentscol_prefix  = 'COMMENTS';
		}else{
		$gradecodecol_prefix = "{$exam}GRADECODE";
		$pointscol_prefix    = "{$exam}POINTS";
		$commentscol_prefix  = "{$exam}COMMENTS";
		}

		$position_col        = strtoupper("{$exam}POS");

       foreach ($subjects as $subjectcode=>$subject_vars ) {


        $subjectcode_official  =  valueof($subject_vars,0);
        $subjectname           =  valueof($subject_vars,1);
        $subject               =  isset($array_student_subjects_data[$subjectcode]) ? $array_student_subjects_data[$subjectcode] : array();
        $num_subject_students  = valueof($num_subjects_students, $subjectcode);

       	$avgcat      = valueof($subject,'AVGCAT');
       	$avgexam     = valueof($subject,'AVGEXAM');
       	$total       = valueof($subject,'TOTAL','-');
        $total       = ($total)>0 ? round($total,0) :'-';
        $subj_done   = ($total)>0 ? true : false;
        $subj_done   = true;
        $gradecode   = valueof($subject, $gradecodecol_prefix);
       	$points      = valueof($subject, $pointscol_prefix);
       	$position    = valueof($subject, $position_col);
       	$comments    = valueof($subject, $commentscol_prefix);
       	$streamcode  = valueof($subject,'STREAMCODE');
       	$teacher     = isset($subject_teachers[$streamcode][$subjectcode]) ? $subject_teachers[$streamcode][$subjectcode] : '-';

        echo '<tr>';
         echo "<td class=\"input grid\" >{$subjectname}</td>";
         echo "<td class=\"input grid\" >{$subjectcode_official}</td>";

         if(isset($subject_config['cats']) && sizeof($subject_config['cats'])>0){
         foreach ($subject_config['cats'] as $cat_index => $cat){

          $value  = valueof($subject,"CAT{$cat_index}{$subject_scorecol_postfix}");

          if(!is_null($value)){
           $value  = round($value,0);
	      }else{
           $value  = '-';
	      }

          echo "<td class=\"input grid\"  align=\"center\">{$value}</td>";
          }
         }

         if(isset($subject_config['papers']) && sizeof($subject_config['papers'])>0){
         foreach ($subject_config['papers'] as $paper_index => $paper){

          $value = valueof($subject,"PAPER{$paper_index}{$subject_scorecol_postfix}");

          if(!is_null($value)){
           $value  = round($value,0);
	      }else{
           $value  = '-';
	      }

          echo "<td class=\"input\" align=\"center\">{$value}</td>";
          }
         }

         if($exams_count>1){
         echo "<td class=\"input grid\" >{$total}</td>";
	     }
         echo "<td class=\"input grid\" >{$gradecode}</td>";
         echo "<td class=\"input grid\" >{$points}</td>";

         if($subj_done){
         echo "<td class=\"input grid\" >{$position}/{$num_subject_students}</td>";
         echo "<td class=\"input grid\" nowrap>{$comments}</td>";
         echo "<td class=\"input grid\" nowrap  colspan=\"2\" >{$teacher}</td>";
	     }else{
         echo "<td class=\"input grid\" >&nbsp;</td>";
         echo "<td class=\"input grid\" >&nbsp;</td>";
         echo "<td class=\"input grid\" colspan=\"2\">&nbsp;</td>";
	     }

        echo '</tr>';

        ++$count;
       }//each subject

        echo '<tr>';
        echo '<td  class="header left right">&nbsp;</td>';
        echo '<td  class="header left right">&nbsp;</td>';

        if(isset($subject_config['cats']) && sizeof($subject_config['cats'])>0){
        	foreach ($subject_config['cats'] as $cat_index => $cat){
        		$name = valueof($cat,'name');
        		$max  = valueof($cat,'max');
               echo "<td  class=\"header right\">&nbsp;</td>";
        	}
        }

        if(isset($subject_config['papers']) && sizeof($subject_config['papers'])>0){
        	foreach ($subject_config['papers'] as $cat_index => $paper){
        		$name = valueof($paper,'name');
        		$max  = valueof($paper,'max');
               echo "<td  class=\"header right\">&nbsp;</td>";
        	}
        }

       if($exams_count>1){
       echo "<td  class=\"header right\">&nbsp;</td>";
       }

       echo "<td  class=\"header right\">&nbsp;</td>";
       echo "<td  class=\"header right\">&nbsp;</td>";
       echo "<td  class=\"header right\">&nbsp;</td>";
       echo "<td  class=\"header right\">&nbsp;</td>";
       echo "<td  class=\"header right\"  colspan=\"2\" >&nbsp;</td>";
       echo "</tr>";

        echo '<tr>';
        echo '<td  class="header left right">&nbsp;</td>';

        if($exams_count>1){
        echo '<td  class="header left right">&nbsp;</td>';
	    }

        if(isset($subject_config['cats']) && sizeof($subject_config['cats'])>0){
        	foreach ($subject_config['cats'] as $cat_index => $cat){
        		$name = valueof($cat,'name');
        		$max  = valueof($cat,'max');
               echo "<td  class=\"header right\">&nbsp;</td>";
        	}
        }

        if(isset($subject_config['papers']) && sizeof($subject_config['papers'])>0){
        	foreach ($subject_config['papers'] as $cat_index => $paper){
        		$name = valueof($paper,'name');
        		$max  = valueof($paper,'max');
               echo "<td  class=\"header\">&nbsp;</td>";
        	}
        }

       echo "<td  class=\"header right\" colspan=\"2\">Total {$mean_total}/{$mean_totalmax}</td>";
       echo "<td class=\"header\" colspan=\"2\">Points {$mean_totalp}/{$numsubjects_max_points}  </td>";
       echo "<td class=\"\" colspan=\"1\">Mean Grade {$gradecode_mean}</td>";
       echo "<td class=\"header\" colspan=\"2\">&nbsp;</td>";
       echo "</tr>";

       /**
        * graphs
        */

       echo  "<tr>";
 	 	 echo "<td colspan=\"{$cat_exam_col_span}\">";
          echo "<table width=\"100%\" border=0 cellspacing=\"0\" cellpadding=\"1\" class=\"plain\">";

           echo '<tr>';
            echo "<td width=\"40%\">&nbsp;</td>";
            echo "<td width=\"60%\" align=\"center\"><b>PROGRESSIVE SUMMARY</b></td>";
           echo '</tr>';

           echo '<tr>';
            echo "<td width=\"40%\" valign=\"top\">".graph_mini($admno, $kcpe_point, $terms_progress_per_term)."</td>";
            echo "<td width=\"60%\" valign=\"top\">".progressive_summary( $admno, $terms_progress )."</td>";
           echo '</tr>';

          echo '</table>';
         echo '</td>';
        echo '</tr>';


       /**
        * mean
        */

       echo  "<tr>";
 	 	 echo "<td colspan=\"{$cat_exam_col_span}\" style=\"padding-top:2px;padding-bottom:2px;\">";
          echo "<table width=\"100%\" border=0 cellspacing=\"0\" cellpadding=\"1\" class=\"plain\">";
           echo '<tr>';
            echo '<td style="font-weight:bold">KCPE Mean (%)</td>';
            echo '<td>'.$kcpemean.'</td>';
            echo '<td style="font-weight:bold">Term Mean (%)</td>';
            echo '<td>'.$termmean.'</td>';
            echo '<td style="font-weight:bold">Value Added</td>';
            echo '<td>'.$value_added.' ('.$value_added_symbol.'VE)</td>';
           echo '</tr>';

          echo '</table>';
         echo '</td>';
        echo '</tr>';

       /**
        * class teacher comments
        */
       $cat_exam_col_span_20 = ceil(20/100 * $cat_exam_col_span);
       $cat_exam_col_span_80 = floor(80/100 * $cat_exam_col_span);

 	   echo  "<tr>";
 	 	echo "<td colspan=\"{$cat_exam_col_span}\" >&nbsp;</td>";
       echo '</tr>';

 	   echo  "<tr>";
 	 	echo "<td colspan=\"{$cat_exam_col_span_20}\"   class=\"bold\">CLASS TEACHER NAME</td>";
 	 	echo "<td  colspan=\"{$cat_exam_col_span_80}\"  class=\"bold\">{$classTeacher}</td>";
       echo '</tr>';

 	   echo  "<tr>";
 	 	echo "<td colspan=\"{$cat_exam_col_span_20}\"  class=\"bold\">COMMENTS</td>";
 	 	echo "<td  colspan=\"{$cat_exam_col_span_80}\" ><i>{$comments_clsteacher}</i></td>";
       echo '</tr>';

       /**
        * principle  comments
        */

// 	   echo  "<tr>";
// 	 	echo "<td colspan=\"{$cat_exam_col_span}\" >&nbsp;</td>";
//       echo '</tr>';

 	   echo  "<tr>";
 	 	echo "<td colspan=\"{$cat_exam_col_span}\"   >";
         echo '<table class="plain" width="100%" cellpadding="2" cellspacing="0">';
          echo '<tr>';
           echo "<td valign=\"top\" class=\"bold\" nowrap>PRINCIPAL  NAME</td>";
 	 	   echo "<td valign=\"top\" class=\"bold\" width=\"50%\">{$principal_name} </td>";
 	 	   echo "<td valign=\"top\" align=\"right\"  class=\"bold\">Signature</td>";
 	 	   echo "<td valign=\"top\" rowspan=\"2\">{$signature}</td>";
          echo '</tr>';
 	      echo  "<tr>";
 	 	   echo "<td >COMMENTS</td>";
 	 	   echo "<td  colspan=\"2\" ><i>{$comments_principle}</i></td>";
          echo '</tr>';
         echo '</table>';
        echo '</td>';
       echo '</tr>';

 	   echo  "<tr>";
 	 	echo "<td colspan=\"{$cat_exam_col_span}\" >&nbsp;</td>";
       echo '</tr>';

       /**
        * parent &fee  bal
        */
       echo  "<tr>";
 	 	 echo "<td colspan=\"{$cat_exam_col_span}\" style=\"padding-top:2px;padding-bottom:2px;\">";

          echo "<table width=\"100%\" border=0 cellspacing=\"0\" cellpadding=\"1\" class=\"plain\">";
           echo '<tr>';
            echo '<td width=\"200px\">';

             echo "<table width=\"100%\" border=0 cellspacing=\"0\" cellpadding=\"1\" class=\"plain\">";
              echo '<tr>';
               echo '<td class=\"bold\" width=\"10%\">Parents Signature</td>';
               echo '<td>'.str_repeat('.',60).'</td>';
              echo '</tr>';
              echo '<tr>';
               echo '<td style="font-weight:bold" nowrap>Next Term Begins</td>';
               echo '<td>'.textDate($next_term_starts, true).'</td>';
              echo '</tr>';
             echo '</table>';
             echo '</td>';

             echo '<td  width="33%" style="text-align:right;color:#666">Official school stamp</td>';
             echo '<td  width="33%">&nbsp;</td>';

            echo '</tr>';
          echo '</table>';

         echo '</td>';
        echo '</tr>';



       /**
        * school vision
        */
 	   echo  "<tr>";
 	 	echo "<td colspan=\"{$cat_exam_col_span}\" style=\"padding-top:2px;padding-bottom:2px;text-align:left;font-size:12px;\"><i>School Vision : {$school->vision}</i></td>";
       echo '</tr>';


       /**
        * school rubber stamp
        */
 	   echo  "<tr>";
 	 	echo "<td colspan=\"{$cat_exam_col_span}\" style=\"padding-top:2px;padding-bottom:2px;text-align:center;font-size:12px;font-weight:bold\">Not valid Without An Official School Rubber Stamp</td>";
       echo '</tr>';


       echo '</tbody>';
      }

     echo '</table>';

 }

  function graph_mini($admno, $kcpe_point, $terms_progress_per_term) {
    global $server_root,$server_name;

    //print_pre($terms_progress_per_term);
    //exit();


$graph_title = 'Performance';
$forms = range(1,4);
$terms = range(1,3);
$data  = array();

foreach($forms as $form){
 foreach($terms as $term){
	$data[$form][$term] =  null;
	//$data[$form][$term] =  rand(1,12);
 }
}

foreach($terms_progress_per_term as $termcode=>$formdata){
 foreach($formdata as $formcode=>$points){
	$data[$formcode][$termcode] = $points;
 }
}

unset($data[4][3]);

               //KCPE   F1            F2           F3           F4
$data1y = array( $kcpe_point,  $data[1][1] ,$data[2][1] ,$data[3][1] ,$data[4][1] );//T1
$data2y = array( null, $data[1][2] ,$data[2][2] ,$data[3][2] ,$data[4][2] );//T2
$data3y = array( null, $data[1][3] ,$data[2][3] ,$data[3][3] , null );//T3

$avg_f1  = ($data1y[0]+$data2y[0]+$data3y[0]) ? round(($data1y[0]+$data2y[0]+$data3y[0])/3,0) : null;
$avg_f2  = ($data1y[1]+$data2y[1]+$data3y[1]) ? round(($data1y[1]+$data2y[1]+$data3y[1])/3,0) : null;
$avg_f3  = ($data1y[2]+$data2y[2]+$data3y[2]) ? round(($data1y[2]+$data2y[2]+$data3y[2])/3,0) : null;
$avg_f4  = ($data1y[3]+$data2y[3]) ? round(($data1y[3]+$data2y[3])/2,0) : null;

$l1datay = array(
 $kcpe_point,
 $avg_f1,
 $avg_f2,
 $avg_f3,
 $avg_f4
);


// Create the graph. These two calls are always required
$graph = new Graph(400,220,'auto');
$graph->SetScale("textlin");

$theme_class=new UniversalTheme;
$graph->SetTheme($theme_class);

//$graph->yaxis->SetTickPositions(array(0,2,4,6,8,10,12,15), array(0,2,4,6,8,10,12,15));//check custom
$graph->SetBox(false);

$graph->ygrid->SetFill(false);
$graph->xaxis->SetTickLabels(array('KCPE','Form 1','Form 2','Form 3','Form 4'));
$graph->yaxis->HideLine(false);
$graph->yaxis->HideTicks(false,false);

// Create the bar plots
$b1plot = new BarPlot($data1y);
$b2plot = new BarPlot($data2y);
$b3plot = new BarPlot($data3y);

// Create the linear error plot - erax

/*
$l1plot=new LinePlot($l1datay);
$l1plot->SetColor('black');//not working :(
$l1plot->SetWeight(1);
$l1plot->SetLegend('Average');
*/

// Create the grouped bar plot
$gbplot = new GroupBarPlot(array($b1plot,$b2plot,$b3plot));
$gbplot->SetWidth(40);

// ...and add it to the graPH
$graph->Add($gbplot);
//$graph->Add($l1plot);// - erax

$b1plot->SetColor("white");
$b1plot->SetFillColor("#cc1111");
$b1plot->value->Show();
//$b1plot->value->SetFormat('%d');
//$b1plot->value->SetFont(FF_FONT1,FS_NORMAL,8);
$b1plot->value->SetFont(FF_ARIAL,FS_NORMAL,8);
$b1plot->value->SetAngle(90);
$b1plot->SetValuePos('top');//center

$b2plot->SetColor("white");
$b2plot->SetFillColor("#1111cc");
$b2plot->value->Show();
//$b2plot->value->SetFormat('%d');
//$b2plot->value->SetFont(FF_FONT1,FS_NORMAL,8);
$b2plot->value->SetFont(FF_ARIAL,FS_NORMAL,8);
$b2plot->value->SetAngle(90);
$b2plot->SetValuePos('top');

$b3plot->SetColor("white");
$b3plot->SetFillColor("#29EA23");
$b3plot->value->Show();
//$b3plot->value->SetFormat('%d');
//$b3plot->value->SetFont(FF_FONT1,FS_NORMAL,8);
$b3plot->value->SetFont(FF_ARIAL,FS_NORMAL,8);
$b3plot->value->SetAngle(90);
$b3plot->SetValuePos('top');

//$graph->title->Set($graph_title);

$file_name        =  create_slug( "{$admno} {$graph_title}", $slug='_');
$graph_path       =  ROOT.'tmp_files/'.$file_name.'.png';
$graph_path_web   =  'tmp_files/'.$file_name.'.png';

$graph->Stroke($graph_path);

return  "<img src=\"{$server_root}/{$graph_path_web}\">";

}

  function progressive_summary($admno, $terms_progress){
  	global $db;

     $forms = range(1,4);
     $terms = range(1,3);
     $cols = array();

     $cols['total']      = 'Marks';
     $cols['points']     = 'POINTS';
     $cols['mean_score'] = 'MEAN SCORE';
     $cols['gradecode']  = 'MEAN GRADE';
     $cols['position']   = 'POSITION';

     if(count($cols)){

     	  $table = "<table width=\"100%\"  cellspacing=\"0\" cellpadding=\"3\" class=\"summary\"  >";

           $table .= '<tr>';
           $table .= '<td style="border-top:1px solid #fff;border-left:1px solid #fff">&nbsp;</td>';

           foreach ($forms as $formcode){
            $table .= '<td colspan="3" align="center">FORM '.$formcode.'</td>';
           }
           $table .= '</tr>';

           $table .= '<tr>';
           $table .= '<td>&nbsp;</td>';
           foreach ($forms as $formcode){
            foreach ($terms as $termcode){
            $table .= '<td nowrap align="center">T'.$termcode.'</td>';
            }
           }
           $table .= '</tr>';

           foreach ($cols as $colname=>$coldesc){
           $table .= '<tr>';
           $table .= '<td nowrap>'.$coldesc.'</td>';
           foreach ($forms as $formcode){
            foreach ($terms as $termcode){
            $cell_value = isset($terms_progress[$formcode][$termcode][$colname]) ? $terms_progress[$formcode][$termcode][$colname] : null;
            $table .= '<td nowrap>'.$cell_value.'</td>';
            }
           }
           $table .= '</tr>';
           }

          $table .= '</table>';

          return 	$table;
     }else{
     	return null;
     }

  }

?>
<html>
  <head>
   <title>Report Form</title>
  <script language="JavaScript" type="text/javascript">
    setTimeout("window.print();", 10000);
</script>
<style>
 body {
 padding : 0px;
 margin : 2px;
 font-size:12px;
 font-family:"Times New Roman";
 }
    table.top {
        font-family:"Times New Roman";
        font-size:12px;
        empty-cells: show;
        border:1px solid #000;
        border-collapse:collapse;
        border-spacing: 0.5rem;
        empty-cells:show;
        vertical-align: top;
    }

     table.top td{
       font-family:"Times New Roman";
       font-size:14px;
       vertical-align: top;
     }

    table.report-form {
        font-family:"Times New Roman";
        font-size:12px;
        empty-cells: show;
        border:1px solid #000;
        border-collapse:collapse;
        border-spacing: 0.5rem;
        empty-cells:show;
    }

    table.report-form td {
        border:1px solid #555;
        vertical-align: top;
    }

   table.plain {
        font-family:"Times New Roman";
        font-size:12px;
        empty-cells: show;
        border:1px solid #000;
        border-collapse:collapse;
        border-spacing: 0.5rem;
        empty-cells:show;
        vertical-align: middle;
    }

    table.plain td {
        border:1px solid #fff;
        vertical-align: middle;
    }

   table.summary {
        font-family:"Times New Roman";
        font-size:12px;
        empty-cells: show;
        border:1px solid #000;
        border-collapse:collapse;
        border-spacing: 0.5rem;
        empty-cells:show;
    }

    table.summary td {
        border:1px solid #555;
    }

    td.inputx {
      padding:2px;
    }

    td.grid {
      padding:2px;
    }

    td.abottom {
        vertical-align:bottom;
        font-size:12px;
    }

    td.bold {
        font-weight:bolder;
    }

    span.title{
     font-size:14px;
     font-weight:bold;
    }

    @media all {
        .page-break  { display: none; }
    }

    @media print {
        .page-break  {
            display: block;
            page-break-before: always;
            margin:0px;
            padding:0px;
        }
    }

    @media screen {
        .page-break  {
            display: block;
            page-break-before: always;
            margin:5px;
            padding:5px;
        }
    }

</style>

  </head>
 <body>
<?php
if($exam!='TOTAL'){
switch ($exam){
       	case 'CAT1':
       	 $exam_configmax_col = 'CATMAX1';
       	break;
       	case 'CAT2':
       	 $exam_configmax_col = 'CATMAX2';
       	break;
       	case 'CAT3':
       	 $exam_configmax_col = 'CATMAX3';
       	break;
       	case 'CAT4':
       	 $exam_configmax_col = 'CATMAX4';
       	break;
       	case 'CAT5':
       	 $exam_configmax_col = 'CATMAX5';
       	break;
       	case 'PAPER1':
       	 $exam_configmax_col = 'EXAMMAX1';
       	break;
       	case 'PAPER2':
       	 $exam_configmax_col = 'EXAMMAX2';
       	break;
       	case 'PAPER3':
       	 $exam_configmax_col = 'EXAMMAX3';
       	break;
       	case 'PAPER4':
       	 $exam_configmax_col = 'EXAMMAX4';
       	break;
       	case 'PAPER5':
       	 $exam_configmax_col = 'EXAMMAX5';
       	break;
       }
}

 if(strlen($formstream)>1){
  $formcode            = substr($formstream,0,1);
  $formstream_code_col = "STREAMCODE";
 }else{
  $formcode            = $formstream;
  $formstream_code_col = "FORMCODE";
 }

$setup = $db->GetRow("
SELECT * FROM SATSSG
WHERE FORMCODE='{$formcode}'
and YEARCODE='{$yearcode}'
and TERMCODE='{$termcode}'
");

if(empty($setup)){
	die("Missing Global Exam Setup for Form <b>{$formcode}</b> Year <b>{$yearcode}</b> Term <b>{$termcode}</b>");
}

$dateY = date('Y');

$principal = $db->GetRow("
SELECT C.SLTCODE,T.TCHNO, C.FULLNAME FROM SAPRC T
INNER JOIN SATEACHERS C ON C.TCHNO = T.TCHNO
WHERE T.YEAREND>={$dateY} AND T.YEAREND>={$dateY}
");

if(empty($principal)){
	die("Missing Set Up for Current Serving Principal");
}

$next_term = $db->GetRow("
SELECT * FROM SATCAL WHERE ISNEXT=1
");

if(empty($next_term)){
	die("Setup Upcoming Term Details");
}

$next_term_starts = valueof($next_term,'DATESTART');

$principal_name = valueof($principal,'SLTCODE') .'. '. valueof($principal,'FULLNAME');

$principal_signature  = valueof($principal,'TCHNO').'.jpg';
$principal_signature_default   = 'default.jpg';
$signature           = ROOT.'public/signatures/'.$principal_signature;
$signature_default   = ROOT.'public/signatures/'.$principal_signature_default;
$rand                = mt_rand();
$student_photo       = '';

 if(file_exists($signature) && is_readable($signature)){
	$signature = '<img src="../../public/signatures/'.$principal_signature.'?cache_reload='.$rand.'" border="0">';
 }else{
	//$signature = '<img src="../../public/signatures/'.$principal_signature_default.'?cache_reload='.$rand.'" border="0">';
	$signature = '';
 }

$exam_options  = $db->GetRow("SELECT * FROM SAXOPTS");
$header_color  =  valueof($exam_options,'RFHC','black');
$numsubjects   =  valueof($setup,'NUMSUBJECTS');

 if($exam=='TOTAL'){
  $exam_configmax =  100;
 }else{
  $exam_configmax =  valueof($setup, $exam_configmax_col,100);
 }


$numsubjects_max_score =  $numsubjects * $exam_configmax;
$numsubjects_max_points =  $numsubjects * 12;


     $subject_config = array();

     for ($c=1;$c<=5;++$c){
      if(isset($setup["CATMAX{$c}"])){
      	if($setup["CATMAX{$c}"]>0){

			if(empty($exam)){
      		$subject_config['cats'][$c]['max']   = $setup["CATMAX{$c}"];
            $subject_config['cats'][$c]['name']  = isset($setup["CATNAME{$c}"]) && !empty($setup["CATNAME{$c}"]) ? $setup["CATNAME{$c}"] : "CAT {$c}";
		    }else{
			 $exam_loop = "CAT{$c}";
			 if($setup["CATMAX{$c}"]>0 && ($exam==$exam_loop || $exam=='TOTAL') ){
			 $subject_config['cats'][$c]['max']   = $setup["CATMAX{$c}"];
             $subject_config['cats'][$c]['name']  = isset($setup["CATNAME{$c}"]) && !empty($setup["CATNAME{$c}"]) ? $setup["CATNAME{$c}"] : "CAT {$c}";
			 }
			}
      	}
      }
    }

    for ($x=1;$x<=5;++$x){
      if(isset($setup["EXAMMAX{$x}"])){
      	if($setup["EXAMMAX{$x}"]>0){
			if(empty($exam)){
      		$subject_config['papers'][$x]['max']   = $setup["EXAMMAX{$x}"];
            $subject_config['papers'][$x]['name']  = isset($setup["EXAMNAME{$x}"]) && !empty($setup["EXAMNAME{$x}"]) ? $setup["EXAMNAME{$x}"] : "Paper {$x}";
            }else{
			 $exam_loop = "PAPER{$x}";
			 if($setup["EXAMMAX{$x}"]>0 && ($exam==$exam_loop  || $exam=='TOTAL') ){
			  $subject_config['papers'][$x]['max']   = $setup["EXAMMAX{$x}"];
              $subject_config['papers'][$x]['name']  = isset($setup["EXAMNAME{$x}"]) && !empty($setup["EXAMNAME{$x}"]) ? $setup["EXAMNAME{$x}"] : "Paper {$x}";
			 }
			}
      	}
      }
    }


$exam_index = substr($exam,-1,1);
$exam_type  = str_replace($exam_index, '',$exam);
$examname   = isset($setup["{$exam_type}NAME{$exam_index}"]) ? $setup["{$exam_type}NAME{$exam_index}"] : $exam;

$data_sql   = ("
select ADMNO,SUBJECTCODE,SUBJECTNAME ,CODEOFFC,STREAMCODE,
CAT1,CAT2,CAT3,CAT4,CAT5,AVGCAT,
PAPER1,PAPER2,PAPER3,PAPER4,PAPER5,
CAT1GRADECODE,CAT2GRADECODE,CAT3GRADECODE,CAT4GRADECODE,CAT5GRADECODE,
PAPER1GRADECODE,PAPER2GRADECODE,PAPER3GRADECODE,PAPER4GRADECODE,PAPER5GRADECODE,
CAT1POINTS,CAT2POINTS,CAT3POINTS,CAT4POINTS,CAT5POINTS,
CAT1P,CAT2P,CAT3P,CAT4P,CAT5P,
PAPER1POINTS,PAPER2POINTS,PAPER3POINTS,PAPER4POINTS,PAPER5POINTS,
PAPER1P,PAPER2P,PAPER3P,PAPER4P,PAPER5P,
TOTAL,TOTALP,GRADECODE,POINTS,STATUSCODE,PICKED,COMMENTS,
CAT1COMMENTS,CAT2COMMENTS,CAT3COMMENTS,CAT4COMMENTS,CAT5COMMENTS,
PAPER1COMMENTS,PAPER2COMMENTS,PAPER3COMMENTS,PAPER4COMMENTS,PAPER5COMMENTS,
CAT1POS,CAT2POS,CAT3POS,CAT4POS,CAT5POS,
PAPER1POS,PAPER2POS,PAPER3POS,PAPER4POS,PAPER5POS,TOTALPOS

from VIEWSTUDENTSUBJECTS
WHERE YEARCODE='{$yearcode}'
AND TERMCODE='{$termcode}'
AND {$formstream_code_col}='{$formstream}'
AND {$exam}>0
");

$data_sql   .= !empty($admno) ?" AND ADMNO='{$admno}' " : '';

$data_sql   .= 'ORDER BY ADMNO,SORTPOS, SUBJECTCODE';

$data       = $db->GetArray($data_sql);

$err        = $db->ErrorMsg();

if(!empty($err)){
	die("Database Error : ".$db->ErrorMsg() );
}else if(!$data){
	die("No Records Found for Form <b>{$formcode}</b> Year <b>{$yearcode}</b> Term <b>{$termcode}</b>");
}else if(sizeof($data)==0 ) {
	die("No Records Found for Form <b>{$formcode}</b> Year <b>{$yearcode}</b> Term <b>{$termcode}</b>");
}

$db->debug=0;

$student_means = $db->GetAssoc("
SELECT ADMNO, FULLNAME, FORMCODE, STREAMCODE,
TOTAL,TOTALMAX,NUMSUBJ,TOTALP,TOTALM,GRADECODEP,GRADECODEM,
KCPEGRADE, KCPEMEAN, KCPEPOINT, KCPEMARKS,KCPERANK,
POSTREAM_MARKS,POSFORM_MARKS,POSTREAM_POINTS,POSFORM_POINTS,
COMMENTS,COMMENTS2
FROM VIEWSTUDENTSMEAN
WHERE YEARCODE='{$yearcode}'
AND FORMCODE='{$formcode}'
AND TERMCODE='{$termcode}'
AND EXAM='{$exam}'
order by {$positioning_col} asc
");

//order by POSFORM_MARKS asc
//print_pre($student_means);
$err        = $db->ErrorMsg();


if(!empty($err)){
	die("Database Error : ".$db->ErrorMsg() );
}else if(!$student_means){
	die("First Grade  <b>{$examname}</b> exam for Form <b>{$formcode}</b> Year <b>{$yearcode}</b> Term <b>{$termcode}</b>");
}else if(sizeof($student_means)==0 ) {
	die("First Grade  <b>{$examname}</b> exam for Form <b>{$formcode}</b> Year <b>{$yearcode}</b> Term <b>{$termcode}</b>");
}


$num_subjects_students = $db->GetAssoc("
SELECT SUBJECTCODE,COUNT(ADMNO) NUM FROM VIEWSTUDENTSUBJECTS
WHERE FORMCODE='{$formcode}'
and YEARCODE='{$yearcode}'
and TERMCODE='{$termcode}'
GROUP BY SUBJECTCODE
");


$num_streams_students = $db->GetAssoc("
SELECT STREAMCODE,COUNT(DISTINCT(ADMNO)) NUM FROM VIEWSTUDENTSUBJECTS
WHERE FORMCODE='{$formcode}'
and YEARCODE='{$yearcode}'
and TERMCODE='{$termcode}'
GROUP BY STREAMCODE
");

$num_form_students  = 0;

if(is_array($num_streams_students)){
 $num_form_students = array_sum($num_streams_students);
}

$subject_teachers = array();

$teachers  =  $db->GetArray("
 SELECT A.STREAMCODE,A.SUBJECTCODE, T.INITIALS
 FROM SATSA A
 INNER JOIN  SATEACHERS T ON T.TCHNO=A.TCHNO
left join SASTREAMS R ON R.STREAMCODE = A.STREAMCODE
left join SAFORMS F ON F.FORMCODE = R.FORMCODE
 WHERE A.YEARCODE='{$yearcode}' AND F.FORMCODE='{$formcode}'
 ");

if($teachers){
 if(is_array($teachers) && sizeof($teachers)>0){
 	foreach ($teachers as $teacher){
 	  $initials     = valueof($teacher,'INITIALS','***');
 	  $streamcode   = valueof($teacher,'STREAMCODE');
 	  $subjectcode  = valueof($teacher,'SUBJECTCODE');
 	  $subject_teachers[$streamcode][$subjectcode]  = $initials;
 	}
 }
}


 $data_array = array();


 $skip_cols   =  array();
 $skip_cols[] = 'ADMNO';
 $skip_cols[] = 'SUBJECTCODE';

 foreach ($data as $data_part ){
 	$admno = valueof($data_part,'ADMNO');
 	$subjectcode = valueof($data_part,'SUBJECTCODE');

 	foreach ($data_part as $k=>$v){
 	 if(!in_array($k,$skip_cols)){
 	  $data_array[$admno][$subjectcode][$k] = $v;
 	 }
 	}

 }

 $classTeachers  =  $db->CacheGetAssoc(1200,"
 SELECT C.STREAMCODE,T.SLTCODE,T.FULLNAME
 FROM SACT C
 INNER JOIN  SATEACHERS T ON T.TCHNO=C.TCHNO
left join SASTREAMS R ON R.STREAMCODE = C.STREAMCODE
left join SAFORMS F ON F.FORMCODE = R.FORMCODE
 WHERE C.YEARCODE='{$yearcode}' AND R.FORMCODE='{$formcode}'
 ");


 foreach ($data_array as $admno=>$student_subjects ){
   print_report_form( $admno, $data_array[$admno] );
   echo " \r\n<div class=\"page-break\"></div>  \r\n";

 }

 ?>

 <?php
 ?>
 </body>
  </html>

