<?php


// TODO: Rename exams to readable format
// TODO: Add headers to the second page
// TODO: Add columns for grade for means


$examprev = filter_input(INPUT_GET, ui::fi('examcompare'));
$form = filter_input(INPUT_GET, ui::fi('form'));
$examcurrent = filter_input(INPUT_GET, ui::fi('examcurrent'));
$examcurrentall = explode(':', $examcurrent);
$year = array_map('trim', explode(' ', $examcurrentall[1]));
$cats = array_map('trim', explode(':', $examcurrent));
$catcurrent = $cats[0];
$yearcurrent = substr($year[0], 1);
$termcurrent = substr($year[1], 1);

$exampevall = explode(':', $examprev);
$yearsprev = array_map('trim', explode(' ', $exampevall[1]));
$catsprev = array_map('trim', explode(':', $examprev));
$catprev = $catsprev[0];
$yearprev = substr($yearsprev[0], 1);
$termprev = substr($yearsprev[1], 1);
$grade = 'GRADECODE';

$sql = "SELECT DISTINCT a.`STREAMCODE`,a.`SUBJECTCODE`,sateachers.`FULLNAME`,
COUNT(a.$catcurrent$grade) AS entry,
SUM($catcurrent$grade = 'A') A,
SUM($catcurrent$grade = 'A-') AS 'A-',
SUM($catcurrent$grade = 'B+')AS 'B+',
SUM($catcurrent$grade = 'B') AS 'B',
SUM($catcurrent$grade = 'B-') AS 'B-',
SUM($catcurrent$grade = 'C+') AS 'C+',
SUM($catcurrent$grade = 'C')AS 'C',
SUM($catcurrent$grade = 'C-') AS 'C-',
SUM($catcurrent$grade = 'D+') AS 'D+',
SUM($catcurrent$grade = 'D') AS 'D',
SUM($catcurrent$grade = 'D-') AS 'D-',
SUM($catcurrent$grade = 'E') AS 'E'
 FROM sastudsub a
 LEFT JOIN satsa ON satsa.`SUBJECTCODE`=a.SUBJECTCODE
INNER JOIN sateachers
        ON sateachers.TCHNO = satsa.TCHNO
 WHERE satsa.yearcode={$yearcurrent} AND satsa.streamcode=a.`STREAMCODE`
 AND a.formcode='{$form}'
AND a.termcode='{$termcurrent}'
AND a.$catcurrent > 0
GROUP BY `STREAMCODE`,`SUBJECTCODE`
   ";

$comparedto = "SELECT DISTINCT a.`STREAMCODE`,a.`SUBJECTCODE`,sateachers.`FULLNAME`,
COUNT(a.$catprev$grade) AS entry,
SUM($catprev$grade = 'A') A,
SUM($catprev$grade = 'A-') AS 'A-',
SUM($catprev$grade = 'B+')AS 'B+',
SUM($catprev$grade = 'B') AS 'B',
SUM($catprev$grade = 'B-') AS 'B-',
SUM($catprev$grade = 'C+') AS 'C+',
SUM($catprev$grade = 'C')AS 'C',
SUM($catprev$grade = 'C-') AS 'C-',
SUM($catprev$grade = 'D+') AS 'D+',
SUM($catprev$grade = 'D') AS 'D',
SUM($catprev$grade = 'D-') AS 'D-',
SUM($catprev$grade = 'E') AS 'E'
 FROM sastudsub a
 LEFT JOIN satsa ON satsa.`SUBJECTCODE`=a.SUBJECTCODE
INNER JOIN sateachers
        ON sateachers.TCHNO = satsa.TCHNO
 WHERE satsa.yearcode={$yearprev} AND satsa.streamcode=a.`STREAMCODE`
 AND a.formcode='{$form}'
AND a.termcode='{$termprev}'
AND a.$catprev > 0
GROUP BY `STREAMCODE`,`SUBJECTCODE`
   ";

$exams = $db->Execute($sql);
$examstwo = $db->Execute($comparedto);
$subject_grades_data = $db->GetAssoc(" SELECT GRADECODE,CEIL(MIN) POINTS  FROM SAMGRD ORDER BY MAX DESC");

if (empty($examstwo)) {
    echo "<p> No data to be compared with. Select Different Exams</p>";
    die();
}
?>


<html>
<head>
    <title>Merit List</title>
    <script language="JavaScript" type="text/javascript">
        setTimeout("window.print();", 10000);
    </script>
    <style>
        body {
            padding: 0px;
            margin: 0px;
            font-size: 12px;
            font-weight: bold;
            font-family: "Times New Roman";
        }

        table.data {
            font-family: "Times New Roman";
            font-size: 12px;
            empty-cells: show;
            border: 1px solid #000;
            border-collapse: collapse;
            border-spacing: 0.5rem;
            empty-cells: show;
        }

        table.data td {
            border: 1px solid #666;
            font-size: 12px;
        }

        table.data td.header {
            background-color: #EDECEB;
            font-size: bold;
        }

        table.data td.abottom {
            vertical-align: bottom;
            font-size: 10px;
        }

        span.title {
            font-size: 14px;
            font-weight: bold;
        }

        @media all {
            .page-break {
                display: none;
            }
        }

        @media print {
            .page-break {
                display: block;
                page-break-before: always;
                margin: 0px;
                padding: 0px;
            }
        }

        @media screen {
            .page-break {
                display: block;
                page-break-before: always;
                margin: 5px;
                padding: 5px;
            }
        }

    </style>

</head>
<body>
<table class="data" width="100%" cellspacing="0" cellpadding="2">
    <tbody>
    <tr>
        <td colspan="25">

            <table style="border:0" class="data" width="100%" cellspacing="0" cellpadding="1">

                <tbody>
                <tr>
                    <td style="border:0" rowspan="6"><img src="../../public/images/school_logo.jpg"></td>
                </tr>

                <tr>
                    <td style="border:0" colspan="2" valign=""><span class="title">ST. BHAKITA SIAKAGO GIRLS' HIGH SCHOOL</span>
                    </td>
                </tr>

                <tr>
                    <td style="border:0" colspan="2"><b>Address : P.O BOX 91 - 60104</b></td>
                </tr>

                <tr>
                    <td style="border:0" colspan="2"><b>Tel :0723 476 839 / 0720</b></td>
                </tr>

                <tr>
                    <td style="border:0" colspan="2"><b>Motto :KNOWLEDGE IS LIGHT</b></td>
                </tr>

                <tr>
                    <td style="border:0" colspan="2">&nbsp;</td>
                </tr>

                <tr>
                    <td style="border:0"><b>FORM <?php echo mb_strtoupper($form) ?></b></td>
                    <td style="border:0"><b>PERFORMANCE COMPARISON PER CLASS </b><br>
                        <span style="color: grey;font-weight: bold;" <b>  <?php echo $examcurrent ?> COMPARED
                            TO <?php echo $examprev ?></b></span>
                    </td>
                </tr>

                </tbody>
            </table>

        </td>
    </tr>
    <tr>
        <td class="bold" valign="top"><b>STREAM</b></td>
        <td class="bold" valign="top"><b>ENTRY</b></td>
        <td class="bold" valign="top"><b>A</b></td>
        <td class="bold" valign="top"><b>A-</b></td>
        <td class="bold" valign="top"><b>B+</b></td>
        <td class="bold" valign="top"><b>B</b></td>
        <td class="bold" valign="top"><b>B-</b></td>
        <td class="bold" valign="top"><b>C+</b></td>
        <td class="bold" valign="top"><b>C</b></td>
        <td class="bold" valign="top"><b>C-</b></td>
        <td class="bold" valign="top"><b>D+</b></td>
        <td class="bold" valign="top"><b>D</b></td>
        <td class="bold" valign="top"><b>D-</b></td>
        <td class="bold" valign="top"><b>E</b></td>
        <td class="bold" valign="top"><b>CURRENT MEAN</b>
            <br>
            <p style="margin: 0 !important; padding: 0 !important;"><?php echo $examcurrent ?></p>
        </td>
        <td class="bold" valign="top"><b>PREV MEAN</b>
            <br>
            <p style="margin: 0 !important; padding: 0 !important;"><?php echo $examprev ?></p></td>
<!--        <td class="bold" valign="top"><b>GD</b></td>-->
        <td class="bold" valign="top"><b>INDX</b></td>
        <td class="bold" valign="top"><b>TEACHER</b></td>
    </tr>


    <!--    <tr>-->
    <!--        <td>1N</td>-->
    <!--    </tr>-->
    <!--    <tr>-->
    <!--        <td>1E</td>-->
    <!--    </tr>-->

    <?php

    $result = array();
    $sums = array();
    $means = array();
    $hak = array();
    $allcodes = array(
        'A',
        'A-',
        'B+',
        'B',
        'B-',
        'C+',
        'C',
        'C-',
        'D+',
        'D',
        'D-',
        'E',
    );
    $comparedtoes = array();
    $meansschp = '';
    foreach ($examstwo as $eleent => $mang) {
        $comparedtoes[$mang['SUBJECTCODE']][] = $mang;
    }


    foreach ($exams as $element => $k) {

        $result[$k['SUBJECTCODE']][] = $k;

    }


    foreach ($comparedtoes as $comparedtoe => $values) {
        foreach ($values as $key => $value) {
            $sump = valueof($value, 'entry');
            $asump = valueof($value, 'A');
            $amsump = valueof($value, 'A-');
            $bpsump = valueof($value, 'B+');
            $bsump = valueof($value, 'B');
            $bmsump = valueof($value, 'B-');
            $cpsump = valueof($value, 'C+');
            $csump = valueof($value, 'C');
            $cmsump = valueof($value, 'C-');
            $dpsump = valueof($value, 'D+');
            $dsump = valueof($value, 'D;');
            $dmsump = valueof($value, 'D-');
            $esump = valueof($value, 'E;');
            $meansschp = round(($asump * 12 + $amsump * 11 + $bpsump * 10 + $bsump * 9 + $bmsump * 8 + $cpsump * 7 + $csump * 6 + $cmsump * 5 + $dpsump * 4 + $dsump * 3 + $dmsump * 2 + $esump * 1) / $sump, 3);

        }
        $meansall[$value['STREAMCODE']][$value['SUBJECTCODE']][] = $meansschp;
    }
    $sss = array();

    foreach ($result as $subjects => $k) {

        if (array_key_exists($subjects, $comparedtoes)) {
            $sss = $comparedtoes[$subjects];
        }

        $sump= 0;
        $asump= 0;
        $amsump= 0;
        $bpsump= 0;
        $bsump= 0;
        $bmsump= 0;
        $cpsump= 0;
        $cmsump= 0;
        $csump= 0;
        $dpsump= 0;
        $dsump= 0;
        $dmsump= 0;
        $esump= 0;
        foreach ($sss as $ki => $va) {
            $sump += $va['entry'];
            $asump += $va['A'];
            $amsump += $va['A-'];
            $bpsump += $va['B+'];
            $bsump += $va['B'];
            $bmsump += $va['B-'];
            $cpsump += $va['C+'];
            $csump += $va['C'];
            $cmsump += $va['C-'];
            $dpsump += $va['D+'];
            $dsump += $va['D'];
            $dmsump += $va['D-'];
            $esump += $va['E'];

        }
        $meansschpxz = round(($asump * 12 + $amsump * 11 + $bpsump * 10 + $bsump * 9 + $bmsump * 8 + $cpsump * 7 + $csump * 6 + $cmsump * 5 + $dpsump * 4 + $dsump * 3 + $dmsump * 2 + $esump * 1) / $sump, 3);

        $sum = 0;
        $asum = 0;
        $amsum = 0;
        $bpsum = 0;
        $bsum = 0;
        $bmsum = 0;
        $cpsum = 0;
        $cmsum = 0;
        $csum = 0;
        $dpsum = 0;
        $dsum = 0;
        $dmsum = 0;
        $esum = 0;
        foreach ($k as $key => $value) {
            $sum += $value['entry'];
            $asum += $value['A'];
            $amsum += $value['A-'];
            $bpsum += $value['B+'];
            $bsum += $value['B'];
            $bmsum += $value['B-'];
            $cpsum += $value['C+'];
            $csum += $value['C'];
            $cmsum += $value['C-'];
            $dpsum += $value['D+'];
            $dsum += $value['D'];
            $dmsum += $value['D-'];
            $esum += $value['E'];

        }

        $meanssch = round(($asum * 12 + $amsum * 11 + $bpsum * 10 + $bsum * 9 + $bmsum * 8 + $cpsum * 7 + $csum * 6 + $cmsum * 5 + $dpsum * 4 + $dsum * 3 + $dmsum * 2 + $esum * 1) / $sum, 3);
        $meangradesch = get_mean_grade_code($meanssch);
        echo "
         <tr >
            <td colspan=\"14\"><b>$subjects</b></td>
         </tr>
        ";

        foreach ($k as $l => $s) {
            foreach ($sss as $w) {
                if (valueof($s, 'STREAMCODE') === valueof($w, 'STREAMCODE') && valueof($s, 'SUBJECTCODE') === valueof($w, 'SUBJECTCODE')) {
                    $sap = valueof($w, 'entry');
                    $ap = valueof($w, 'A');
                    $sasp = valueof($w, 'STREAMCODE');
                    $amp = valueof($w, 'A-');
                    $bp = valueof($w, 'B+');
                    $bmp = valueof($w, 'B');
                    $bcp = valueof($w, 'B-');
                    $cp = valueof($w, 'C+');
                    $cmp = valueof($w, 'C-');
                    $ccp = valueof($w, 'C');
                    $dp = valueof($w, 'D+');
                    $dcp = valueof($w, 'D');
                    $dmp = valueof($w, 'D-');
                    $ep = valueof($w, 'E');
                    $teacherp = valueof($w, 'FULLNAME');
                    $meansp = round(($ap * 12 + $amp * 11 + $bp * 10 + $bmp * 9 + $bcp * 8 + $cp * 7 + $cmp * 5 + $ccp * 6 + $dp * 4 + $dcp * 3 + $dmp * 2 + $ep * 1) / $sap, 3);
                }
            }

            $sa = valueof($s, 'entry');
            $a = valueof($s, 'A');
            $sas = valueof($s, 'STREAMCODE');
            $am = valueof($s, 'A-');
            $b = valueof($s, 'B+');
            $bm = valueof($s, 'B');
            $bc = valueof($s, 'B-');
            $c = valueof($s, 'C+');
            $cm = valueof($s, 'C-');
            $cc = valueof($s, 'C');
            $d = valueof($s, 'D+');
            $dc = valueof($s, 'D');
            $dm = valueof($s, 'D-');
            $e = valueof($s, 'E');
            $teacher = valueof($s, 'FULLNAME');
            $means = round(($a * 12 + $am * 11 + $b * 10 + $bm * 9 + $bc * 8 + $c * 7 + $cm * 5 + $cc * 6 + $d * 4 + $dc * 3 + $dm * 2 + $e * 1) / $sa, 3);
            $meangrade = get_mean_grade_code($means);
            $index =  $means - $meansp ;
            echo "
             <tr>
            <td >$sas </td>
            <td >$sa </td>
            <td >$a</td>
            <td >$am</td>
            <td >$b</td>
            <td >$bm</td>
            <td >$bc</td>
            <td >$c</td>
            <td >$cc</td>
            <td >$cm  </span></td>
            <td >$d</td>
            <td >$dc</td>
            <td >$dm</td>
            <td >$e</td>
            <td >$means
             <span style='float: right;'><b>$meangradesch</b></span>
             </td>
            <td style='color: blue'><b>$meansp</b></td>
            <td >$index</td>
            <td >$teacher</td>
             </tr>
       ";

        }

        $measn = $meansschpxz - $meanssch;
        echo "
        <tr style=\"border-bottom: solid 2px black; border-top: solid 2px black;\">
        <td></td>
        <td style='color: blue;'><b><b>$sum</b></b></td>
        <td style='color: blue'><b>$asum</b></td>
        <td style='color: blue'><b>$amsum</b></td>
        <td style='color: blue'><b>$bpsum</b></td>
        <td style='color: blue'><b>$bsum</b></td>
        <td style='color: blue'><b>$bmsum</b></td>
        <td style='color: blue'><b>$cpsum</b></td>
        <td style='color: blue'><b>$csum</b></td>
        <td style='color: blue'><b>$cmsum</b></td>
        <td style='color: blue'><b>$dpsum</b></td>
        <td style='color: blue'><b>$dsum</b></td>
        <td style='color: blue'><b>$dmsum</b></td>
        <td style='color: blue'><b>$esum</b></td>
        <td style='color: blue'><b>$meanssch</b></td>
        <td style='color: blue'><b>$meansschpxz</b></td>
        <td style='color: blue'><b>$measn</b></td>
        <td></td>
    </tr>";
    }

    ?>

    </tbody>
</table>

</body>
</html>

<?php

function get_mean_grade_code($points)
{
    global $db;
    return $db->CacheGetOne(1200, "select GRADECODE from SAMGRD WHERE MIN<={$points} AND MAX>={$points}");
}

?>


