<?php


$dateNow     = textDate(null, true);
$school      = new school();

$yearcode    = filter_input(INPUT_GET , ui::fi('year'));
$termcode    = filter_input(INPUT_GET , ui::fi('term'));
$formstream  = filter_input(INPUT_GET , ui::fi('form'));
$subjectcode = filter_input(INPUT_GET , ui::fi('subject'));
$sortcol     = filter_input(INPUT_GET , ui::fi('sortcol'));
$sortcol     = strtoupper($sortcol);

$subjectname =  $db->GetOne("SELECT SUBJECTNAME FROM SASUBJECTS WHERE SUBJECTCODE='{$subjectcode}'");

if(strlen($formstream)>1){
 $formcode            = $formstream;
 $formstream_code_col = "STREAMCODE";
}else{
 $formcode            = substr($formstream,0,1);
 $formstream_code_col = "FORMCODE";
 }


$subject_grades = array();

$data  = $db->Execute("
        SELECT * FROM VIEWSTUDENTSUBJECTS 
        WHERE YEARCODE='{$yearcode}'
        AND TERMCODE='{$termcode}' 
        AND SUBJECTCODE='{$subjectcode}' 
        AND {$formstream_code_col}='{$formstream}' 
        ORDER BY STREAMCODE, {$sortcol}
        ");

$teacher  =  $db->GetOne("select FULLNAME from VIEWTSA 
WHERE YEARCODE='{$yearcode}'
AND {$formstream_code_col}='{$formstream}' 
AND SUBJECTCODE='{$subjectcode}' 
 ");

 ?>
 <html>
  <head>
   <title>Mark Sheet</title>
  <script language="JavaScript" type="text/javascript">
    //setTimeout("window.print();", 10000);
</script>
<style>
 body {
 padding : 10px;
 margin : 10px;
 font-size:9px;
 }
    table {
        font-family:Verdana;
        font-size:9px;
        empty-cells: show;
        border:1px solid #000;
        border-collapse:collapse;
        border-spacing: 0.5rem;
        empty-cells:show;
    }

    td {
        border:1px solid #666;
    }

    td.abottom {
        vertical-align:bottom;
        font-size:10px;
    }

    td.bold {
        font-weight:bold;
    }

    span.title{
     font-size:14px;
     font-weight:bold;
    }
    
    @media all {
        .page-break  { display: none; }
    }

    @media print {
        .page-break  { 
            display: block; 
            page-break-before: always;
            margin:0px;
            padding:0px;
        }
    }

    @media screen {
        .page-break  { 
            display: block; 
            page-break-before: always;
            margin:5px;
            padding:5px;
        }
    }

</style>

  </head>
 <body>
 <?php
 if($data) {
 	
 	 	  echo "
 	 	  <table width=\"100%\" border=0 cellspacing=\"0\" cellpadding=\"3\" class=\"data\">
 	 	  <tr>
 	 	   <td colspan=\"9\">
 	 	 
 	 	   <table width=\"100%\"  style=\"border:0\"  cellspacing=\"0\" cellpadding=\"1\" class=\"data\">
 	 	   
 	 	    <tr>
 	 	     <td rowspan=\"6\" style=\"border:0\" ><img src=\"{$school->logo_path}\" ></td>
 	 	    </tr>
 	 	    
 	 	    <tr>
 	 	     <td valign=\"top\"  style=\"border:0\"  colspan=\"2\"><span class=\"title\">{$school->name}</span></td>
 	 	    </tr>
 	 	    
 	 	    <tr>
 	 	     <td  colspan=\"2\" style=\"border:0\" ><b>Address : {$school->address}</b></td>
 	 	    </tr>
 	 	    
 	 	    <tr>
 	 	     <td colspan=\"2\" style=\"border:0\" ><b>Tel :{$school->telephone}</b></td>
 	 	    </tr>
 	 	    
 	 	    <tr>
 	 	     <td colspan=\"2\" style=\"border:0\" ><b>Vision :{$school->vision}</b></td>
 	 	    </tr>
 	 	    
 	 	    <tr>
 	 	     <td colspan=\"2\" style=\"border:0\" >&nbsp;</td>
 	 	    </tr>
 	 	    
 	 	    <tr>
 	 	     <td  colspan=\"3\"  style=\"border:0\" align=\"center\"><b>MARK SHEET : Form {$formstream} - Term {$termcode} {$yearcode}</b></td>
 	 	    </tr>
 	 	    
 	 	    <tr>
 	 	     <td  colspan=\"3\"  style=\"border:0\" align=\"right\"><b>Date : </b>{$dateNow}</td>
 	 	    </tr>
 	 	    
 	 	   </table>
 	 	 </td>
 	 	 </tr>";
 	 	
 	   if($data->RecordCount()>0) {
 	   	$count= 1;
 	   		
 	 	 
 	 	 echo "<tr>";
 	 	  echo "<td  colspan=\"3\">
 	 	  SUBJECT : {$subjectname} <br>
 	 	  TEACHER : {$teacher}
 	 	  </td>";
 	 	  echo "<td>CAT 1</td>";
 	 	  echo "<td>CAT 2</td>";
 	 	  echo "<td>CAT 3</td>";
 	 	  echo "<td>EXAM 1</td>";
 	 	  echo "<td>EXAM 2</td>";
 	 	  echo "<td>EXAM 3</td>";
 	 	 echo "</tr>";
 	 	 
 	 	 
 	 	 
 		while (!$data->EOF) {
       	 	
       	 	$admno        = valueof($data->fields, 'ADMNO');	
       	 	$fullname     = valueof($data->fields, 'FULLNAME');	
       	 	$subjectcode  = valueof($data->fields, 'SUBJECTCODE');	
       	 	$codeoffc     = valueof($data->fields, 'CODEOFFC');	
       	 	$catcode      = valueof($data->fields, 'CATCODE');	
       	 	$formcode     = valueof($data->fields, 'FORMCODE');
       	 	$streamcode   = valueof($data->fields, 'STREAMCODE');
       	 	$sbccode      = valueof($data->fields, 'SBCCODE');
       	 	
 	 	 echo "<tr>";
 	 	  echo "<td>{$count}</td>";
 	 	  echo "<td>{$admno}</td>";
 	 	  echo "<td>{$fullname}</td>";
 	 	  echo "<td>&nbsp;</td>";
 	 	  echo "<td>&nbsp;</td>";
 	 	  echo "<td>&nbsp;</td>";
 	 	  echo "<td>&nbsp;</td>";
 	 	  echo "<td>&nbsp;</td>";
 	 	  echo "<td>&nbsp;</td>";
 	 	 echo "</tr>";
 	 	 
 	 	 ++$count;
    
       	  $data->MoveNext();
       	 }
       	 --$count;
       	  	
 	 	 echo "<tr>";
 	 	  echo "<td colspan=\"4\"><b>Total :{$count}</b></td>";
 	 	 echo "</tr>";
 	 	 
 	   }
 	  }
 	 	
 	 	echo "</table>";
 	 	
 ?>
 </body>
  </html>
 
