<?php

 $school = new school();
 
 $forms = $db->GetAssoc('SELECT FORMCODE,FORMNAME FROM SAFORMS ORDER BY FORMCODE');
 $streams_data = $db->GetAssoc('SELECT STREAMCODE,STREAMNAME,FORMCODE FROM SASTREAMS');
 $years = $db->GetAssoc('SELECT YEARCODE,YEARNAME FROM SAYEAR ORDER BY YEARCODE');
 $terms = $db->GetAssoc('SELECT TERMCODE,TERMNAME FROM SATERMS ORDER BY TERMCODE');
 $exams = $db->GetAssoc('SELECT TERMCODE,TERMNAME FROM SATERMS ORDER BY TERMCODE');
 
 $forms = array();
 if(isset($streams_data)){
  if(sizeof($streams_data)>0){	
   foreach ($streams_data as $streamcode=>$stream_data){
   	$streamname = valueof($stream_data, 'STREAMNAME');
   	$formcode   = valueof($stream_data, 'FORMCODE');
   	$forms[$formcode] = "Form {$formcode}";
   	$forms[$streamcode] = "Form {$formcode}-{$streamname}";
   }
  }
 }
 
 
 $sortCols = array();
 $sortCols['admno']    = 'Admission Number';
 $sortCols['fullname'] = 'Student Name';

?>

	<form id="<?php echo ui::fi('ff'); ?>" method="post" novalidate>
		<table cellpadding="2" cellspacing="0" width="100%">
		
			<tr>
				<td  width="130px">Year:</td>
				<td>
					<?php echo ui::form_select_fromArray('year', $years, $school->active_yearcode," onchange=\"{$cfg['appname']}.list_subjects();\" ");  ?>
				</td>
			</tr>
			
			<tr>
				<td>Term:</td>
				<td>
					<?php echo ui::form_select_fromArray('term', $terms,$school->active_termcode,"onchange=\"{$cfg['appname']}.list_subjects();\"");  ?>
				</td>
			</tr>
			
			<tr>
				<td>Form</td>
				<td>
				<?php echo ui::form_select_fromArray('form', $forms,'',"onchange=\"{$cfg['appname']}.list_subjects();\"");  ?>
				</td>
			</tr>
			
			<tr>
				<td>Subject:</td>
				<td>
				   <div id="<?php echo ui::fi('div_subjects'); ?>">
					<?php echo ui::form_select_fromArray('subject', array(0=>'...'));  ?>
					</div>
				</td>
			</tr>
			
	    	<tr>
				<td>Student Sort Column</td>
				<td>
					<?php echo ui::form_select_fromArray('sortcol', $sortCols, 'admno' );  ?>
				</td>
	    	</tr>
	    		
			<tr>
				<td>&nbsp;</td>
				<td>
				 <a href="javascript:void(0)" class="easyui-linkbutton" onclick="<?php echo $cfg['appname']; ?>.print_report();"><i class="fa fa-print"></i> Open Report</a>
				</td>
			</tr>
			
		</table>
     </form>
   
	<script>
		
		var <?php echo $cfg['appname']; ?> = {
		clearForm:function (){
			$('#<?php echo ui::fi('ff'); ?>').form('clear');
		},
		list_subjects:function (){
            $.post('./endpoints/crud/',   'modvars=<?php echo $vars; ?>&function=list_subjects&'+$('#<?php echo ui::fi('ff'); ?>').serialize()   , function( html ){
  		 	$('#<?php echo ui::fi('div_subjects'); ?>').html(html);
  		 });
		},
        print_report:function (){
         var fdata = $('#<?php echo ui::fi('ff'); ?>').serialize();
  	     var <?php echo ui::fi('w'); ?>=window.open('./endpoints/print/?modvars=<?php echo $vars; ?>&'+fdata,'<?php echo ui::fi('pw'); ?>','height=800,width=830,toolbar=no,menubar=no,directories=no,location=no,scrollbars=yes,status=no,resizable=no,fullscreen=no,top=0,left=0');
          <?php echo ui::fi('w'); ?>.focus();
        },
	}
			
	</script>
