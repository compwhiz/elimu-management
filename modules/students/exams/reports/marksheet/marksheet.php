<?php

  class marksheet {

	public function __construct(){
		global $cfg;
		
	}
	
	public function list_subjects(){
		global $db;
		
	 $yearcode     = filter_input(INPUT_POST , ui::fi('year'));	
	 $termcode     = filter_input(INPUT_POST , ui::fi( 'term'));
	 $formstream   = filter_input(INPUT_POST , ui::fi('form'));

    if(strlen($formstream)>1){
     $formcode            = $formstream;
     $formstream_code_col = "STREAMCODE";
    }else{
     $formcode            = substr($formstream,0,1);
     $formstream_code_col = "FORMCODE";
     }

	  $subjects = $db->GetArray("
       SELECT DISTINCT SUBJECTCODE 
        FROM SASTUDSUB 
        WHERE {$formstream_code_col}='{$formcode}'
        AND YEARCODE='{$yearcode}'
        AND TERMCODE='{$termcode}'
       ");
     
	  $subjects_array = array();
	  
	  if($subjects){
	  	if(count($subjects)>0){
	  	  foreach ($subjects as $subject){
	  	  	$subjectcode  = valueof($subject,'SUBJECTCODE');
	  	  	$subjects_array[$subjectcode] = $subjectcode;
	  	  }
	  	}
	  }
	  
	  return ui::form_select_fromArray('subject',$subjects_array, ''," onchange=\"\" ");
	  
	}
	
  }
