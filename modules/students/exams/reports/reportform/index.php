<?php

$school = new school();

$forms = $db->GetAssoc('SELECT FORMCODE,FORMNAME FROM SAFORMS ORDER BY FORMCODE');
$streams_data = $db->GetAssoc('SELECT STREAMCODE,STREAMNAME,FORMCODE FROM SASTREAMS');
$years = $db->GetAssoc('SELECT YEARCODE,YEARNAME FROM SAYEAR ORDER BY YEARCODE');
$terms = $db->GetAssoc('SELECT TERMCODE,TERMNAME FROM SATERMS ORDER BY TERMCODE');
$exams = array();
$forms = array();

if (isset($streams_data)) {
    if (sizeof($streams_data) > 0) {
        foreach ($streams_data as $streamcode => $stream_data) {
            $streamname = valueof($stream_data, 'STREAMNAME');
            $formcode = valueof($stream_data, 'FORMCODE');
            $forms[$formcode] = "Form {$formcode}";
            $forms[$streamcode] = "Form {$formcode}-{$streamname}";
        }
    }
}

?>
<form id="<?php echo ui::fi('ff'); ?>" method="post" novalidate onsubmit="return false;">
    <table cellpadding="2" cellspacing="0" width="100%">

        <tr>
            <td width="100px">Student</td>
            <td><?php echo ui::form_input('text', 'find_admno', 20, '', '', '', '', '', '', ""); ?>
                <a href="javascript:void(0)" class="easyui-linkbutton"
                   onclick="<?php echo $cfg['appname']; ?>.loadProfile();">
                    <i class="fa fa-chevron-circle-down"></i> View</a></td>
            <td>
                &nbsp;
            </td>
        </tr>

        <tr>
            <td colspan="3">
                <div id="<?php echo ui::fi('div_stage'); ?>">&nbsp;</div>
            </td>
        </tr>


    </table>
</form>

<script>

    var <?php echo $cfg['appname']; ?> =
    {
        clearForm:function () {
            $('#<?php echo ui::fi('ff'); ?>').form('clear');
            $('#<?php echo ui::fi('find_admno'); ?>').val();
            $('.combo-text').val();
        }
    ,
        loadProfile :function () {
            var find_admno = $('#<?php echo ui::fi('find_admno'); ?>').combogrid('getValue');
            $.post('./endpoints/crud/', 'modvars=<?php echo $vars; ?>&function=get_profile&admno=' + find_admno, function (html) {
                $('#<?php echo ui::fi('div_stage'); ?>').html(html);
            });
        }
    ,
        list_exams:function () {
            $.post('./endpoints/crud/', 'modvars=<?php echo $vars; ?>&function=list_exams&' + $('#<?php echo ui::fi('ff'); ?>').serialize(), function (html) {
                $('#<?php echo ui::fi('div_exams'); ?>').html(html);
            });
        }
    ,
        print_report:function () {
            var fdata = $('#<?php echo ui::fi('ff'); ?>').serialize();
            var <?php echo ui::fi('w'); ?>=
            window.open('./endpoints/print/?modvars=<?php echo $vars; ?>&' + fdata, '<?php echo ui::fi('pw'); ?>', 'height=800,width=830,toolbar=no,menubar=no,directories=no,location=no,scrollbars=yes,status=no,resizable=no,fullscreen=no,top=0,left=0');
            <?php echo ui::fi('w'); ?>.
            focus();
        }
    ,
    }

    <?php
    echo ui::ComboGrid($combogrid_array, 'find_admno', "{$cfg['appname']}.loadProfile");
    ?>

</script>
	
