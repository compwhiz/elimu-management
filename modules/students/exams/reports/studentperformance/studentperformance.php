<?php

  class studentperformance {
	private $id;
	private $datasrc;
	private $primary_key;
	private $admno;
	
	public function __construct(){
		global $cfg;
	  $this->admno        = filter_input(INPUT_POST , 'admno');
	}
	
	public function get_profile(){
		global $db,$cfg;
		
         $student    = new student( $this->admno );
         
         if(!empty($student->fullname)){
			 
			 $success = 1;
			 $message = 'ok';
			 
			 $data['admno']       =  $student->admno;
			 $data['fullname']    =  $student->fullname;
			 $data['streamname']  =  $student->streamcode;
			 $data['formname']    =  $student->formcode;
			 
	     }else{
		     $success = 1;
			 $message = 'Student Not Found';
	     }
	     
          $data['success']    =  $success;
          $data['message']    =  $message;
         
         return json_encode($data);
	}
	
	
	
}
