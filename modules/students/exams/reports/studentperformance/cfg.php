<?php
/**
* auto created config file for modules/students/exams/reports/studentperformance
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-01-22 06:56:47
*/

if(!defined('MAKE_FIELDS_UNIQUE')){
 define('MAKE_FIELDS_UNIQUE' , true);
}

$scriptname  = @end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'Student Performance';//user-readable formart
 $cfg['appname']       = 'studentperformance';//lower cased one word
 $cfg['datasrc']       = 'SASTUDSUB';//where to get data
 $cfg['datatbl']       = 'SASTUDSUB';//base data src [for updates & deletes]
 $cfg['form_width']    = 400;
 $cfg['form_height']   = 200;
 $cfg['window_width']    = 500;
 $cfg['window_height']   = 400;
 
 $cfg['pkcol']         = 'ADMNO';//the primary key
 
 $cfg['tblbns']['chk_button_add']    = 1;
 $cfg['tblbns']['chk_button_edit']   = 1;
 $cfg['tblbns']['chk_button_delete'] = 1;
 $cfg['tblbns']['chk_button_import'] = 1;
 $cfg['tblbns']['chk_button_export'] = 1;
  
$combogrid_array   = array();
$combogrid_array['find_admno']['columns']['STREAMCODE']  = array( 'field'=>'stream', 'title'=>'Stream', 'width'=>80);
$combogrid_array['find_admno']['columns']['ADMNO']       = array( 'field'=>'admno', 'title'=>'Admission No', 'width'=> 80 , 'isIdField' => true );
$combogrid_array['find_admno']['columns']['FULLNAME']    = array( 'field'=>'fullname', 'title'=>'Full Name', 'width'=> 150, 'isTextField'=>true);

$combogrid_array['find_admno']['source'] ='VIEWSP';
	
                      

