<?php
/**
* auto created config file for modules/students/exams/reports/persubjects/gradenposition
* @author kenmsh@gmail.com
* 
* @version 2.0
* @since 2016-02-27 09:37:45
*/

$scriptname  = @end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'Graden';//user-readable formart
 $cfg['appname']       = 'graden';//lower cased one word
 $cfg['datasrc']       = '';//where to get data
 $cfg['datatbl']       = '';//base data src [for updates & deletes]
 $cfg['form_width']    = 400;
 $cfg['form_height']   = 200;
 $cfg['window_width']    = 400;
 $cfg['window_height']   = 350;
 
 $cfg['pkcol']         = '0';//the primary key
 
 $cfg['tblbns']['chk_button_add']    = 1;
 $cfg['tblbns']['chk_button_edit']   = 1;
 $cfg['tblbns']['chk_button_delete'] = 1;
 $cfg['tblbns']['chk_button_import'] = 1;
 $cfg['tblbns']['chk_button_export'] = 1;
 
