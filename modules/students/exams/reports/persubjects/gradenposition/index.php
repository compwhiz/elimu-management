<?php

/**
* auto created index file for modules/students/exams/reports/positionpersubject
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-02-18 16:20:46
*/
 

 $school          = new school();
 $user            = new user();
 $terms           = $db->GetAssoc('SELECT TERMCODE,TERMNAME FROM SATERMS ORDER BY TERMCODE');
 $subjects_array  =  $db->GetAssoc("SELECT SUBJECTCODE,SUBJECTNAME FROM SASUBJECTS order by SUBJECTCODE");

 $my_subjects     =  $db->GetArray("
	SELECT SUBJECTCODE,STREAMCODE,YEARCODE FROM SATSA
	ORDER BY YEARCODE,STREAMCODE ASC
   ");


 $my_list    = array();
 $my_streams = array();
 $streams = array();
 $years = array();
 $subjects = array();
 $exams = array();
 $display = array();

 if( sizeof($my_subjects)>0){
 	foreach ($my_subjects as $my_subject){
 		$subjectcode  = valueof($my_subject, 'SUBJECTCODE');
 		$streamcode   = valueof($my_subject, 'STREAMCODE');
 		$yearcode     = valueof($my_subject, 'YEARCODE');

 		$years[$yearcode] = $yearcode;
 		$subjects[$subjectcode] = valueof($subjects_array,$subjectcode);
 	}
 }

$display['g'] = 'By Grade';
$display['s'] = 'By Score';
 ?>
	<form id="<?php echo ui::fi('ff'); ?>" method="post" onsubmit="return false;">
	  <table cellpadding="2" cellspacing="0" width="100%">

	  <tr>
			<td width="180px">Year:</td>
			<td>
			 <?php echo ui::form_select_fromArray('yearcode', $years, $school->active_yearcode," onchange=\"{$cfg['appname']}.list_streams();\" ");  ?>
			</td>
		</tr>

		<tr>
			<td>Term:</td>
			<td>
			<?php echo ui::form_select_fromArray('termcode', $terms,'',"onchange=\"{$cfg['appname']}.list_streams();\"");  ?>
			</td>
		</tr>

		<tr>
			<td >Form</td>
			<td>
			 <div id="<?php echo ui::fi('div_streams'); ?>">
			 <?php echo ui::form_select_fromArray('streamcode', $streams,'',"onchange=\"{$cfg['appname']}.list_exams();\"");  ?>
			 </div>
			</td>
		</tr>

	    <tr>
	     <td>Exam:</td>
	     <td>
	      <div id="<?php echo ui::fi('div_exams'); ?>">
	       <?php echo ui::form_select_fromArray('exam', $exams,'',"onchange=\"\"");  ?>
	      </div>
	     </td>
	    </tr>

	    <tr>
	     <td>Display:</td>
	     <td>
	       <?php echo ui::form_select_fromArray('display', $display,'',"");  ?>
	     </td>
	    </tr>

		<tr>
			<td>&nbsp;</td>
			<td>
			 <button onclick="<?php echo $cfg['appname']; ?>.print_report();return false;"><i class="fa check-square"></i> Open Report</button>
			</td>
		</tr>

	 </table>
	</form>
	
	<script>
		
	 var <?php echo $cfg['appname']; ?> = {
		 clearForm:function (){
			$('#<?php echo ui::fi('ff'); ?>').form('clear');
		},
		list_streams:function (){
         $.post('./endpoints/crud/',   'modvars=<?php echo $vars; ?>&function=list_streams&'+$('#<?php echo ui::fi('ff'); ?>').serialize()   , function( html ){
	     $('#<?php echo ui::fi('div_subjects'); ?>').html('&nbsp;');
	     $('#<?php echo ui::fi('div_exams'); ?>').html('&nbsp;');
  	     $('#<?php echo ui::fi('div_streams'); ?>').html(html);
  	     });
        },
        list_exams:function (){
            $.post('./endpoints/crud/',   'module=<?php echo $vars; ?>&function=list_exams&'+$('#<?php echo ui::fi('ff'); ?>').serialize()   , function( html ){
  		 	$('#<?php echo ui::fi('div_exams'); ?>').html(html);
  		 	//$('#<?php echo ui::fi('div_subjects'); ?>').html('&nbsp;');
  		 });
		},
        list_subjects:function (){
          $.post('./endpoints/crud/',   'modvars=<?php echo $vars; ?>&function=list_subjects&'+$('#<?php echo ui::fi('ff'); ?>').serialize()   , function( html ){
  	      $('#<?php echo ui::fi('div_subjects'); ?>').html(html);
  	      });
        },
         print_report:function (){
         var fdata      = $('#<?php echo ui::fi('ff'); ?>').serialize();
         var exam_name  = $("#<?php echo ui::fi('exam'); ?> option:selected").text();
  	     var <?php echo ui::fi('w'); ?>=window.open('./endpoints/print/?modvars=<?php echo $vars; ?>&exam_name='+exam_name+'&'+fdata,'<?php echo ui::fi('pw'); ?>','height=900,width=1200,toolbar=no,menubar=no,directories=no,location=no,scrollbars=yes,status=no,resizable=no,fullscreen=no,top=0,left=0');
          <?php echo ui::fi('w'); ?>.focus();
        },
	  }
			
	</script>
	
