<?php

require_once ( ROOT.'lib/jpgraph/jpgraph.php');
require_once ( ROOT.'lib/jpgraph/jpgraph_bar.php');

$dateNow     = textDate(null, true);

$yearcode    = filter_input(INPUT_GET , ui::fi('year'));
$termcode    = filter_input(INPUT_GET , ui::fi('term'));
$exam        = filter_input(INPUT_GET , ui::fi('exam'));
$exam_name   = filter_input(INPUT_GET , ui::fi('exam_name'));
$formstream  = filter_input(INPUT_GET , ui::fi('form'));
$positioning = 'P';

 if(strlen($formstream)>1){
  $formcode            = substr($formstream,0,1);
  $formstream_code_col = "STREAMCODE";
  $class_name          = $formstream;
 }else{
  $formcode            = $formstream;
  $formstream_code_col = "FORMCODE";
  $class_name          = "ALL";
 }

$school = new school();

$examdesc_joined = "TERM {$termcode} {$yearcode} {$exam_name} Subject Grade Count FORM {$formstream}  ";
$examdesc        = strtoupper($examdesc_joined);

$subject_grades = array();

$data  = $db->Execute("
        SELECT * FROM VIEWSTUDENTSUBJECTS
        WHERE YEARCODE='{$yearcode}'
        AND TERMCODE='{$termcode}'
        AND {$formstream_code_col}='{$formstream}'
        AND PICKED=1
        ORDER BY STREAMCODE, ADMNO, SORTPOS,CODEOFFC
        ");

$subjects       = $db->GetAssoc("select SUBJECTCODE,SUBJECTNAME from SASUBJECTS ORDER BY  CODEOFFC");
$subject_grades_data = $db->GetAssoc(" SELECT GRADECODE,CEIL(MIN) POINTS  FROM SAMGRD ORDER BY MAX DESC");

if($subject_grades_data && sizeof($subject_grades_data)>0){
 foreach ($subject_grades_data as $subject_gradecode => $subject_grade_points){
  $subject_grades[$subject_gradecode] = $subject_grade_points;
 }
}

$where_am_i   = str_replace($_SERVER['DOCUMENT_ROOT'],'', ROOT);
$server_name  = $_SERVER['SERVER_NAME'];
$server_port  = $_SERVER['SERVER_PORT'];

$x = pathinfo(realpath($_SERVER['DOCUMENT_ROOT']));
$y = pathinfo(realpath(ROOT));
$z = '';

if(isset($x['dirname']) && isset($y['dirname'])){
 if($x != $y){
  $z = $y['filename'];
 }
}

$server_root =  "http://{$server_name}:{$server_port}/{$z}/";

 ?>
 <html>
  <head>
   <title>Grade Analysis</title>
  <script language="JavaScript" type="text/javascript">
    //setTimeout("window.print();", 10000);
</script>
<style>
 body {
 padding : 10px;
 margin : 10px;
 font-size:9px;
 }
    table {
        font-family:Verdana;
        font-size:10px;
        empty-cells: show;
        border:1px solid #000;
        border-collapse:collapse;
        border-spacing: 0.5rem;
        empty-cells:show;
    }

    td {
        border:1px solid #777;
    }

    td.abottom {
        vertical-align:bottom;
        font-size:10px;
    }

    td.bold {
        font-weight:bold;
    }

    span.title{
     font-size:14px;
     font-weight:bold;
    }

</style>

  </head>
 <body>
 <?php
 if($data) {
 	 $graph_data     = array();
 	 $display_points = array();
     $display_grades = array();
     if(strstr($exam,'CAT') || strstr($exam,'PAPER')){
        $pointscol    = "{$exam}POINTS";
        $gradecodecol = "{$exam}GRADECODE";
       }else{
       	$pointscol    = "POINTS";
       	$gradecodecol = "GRADECODE";
       }

    $gradecodecol = strtoupper($gradecodecol);

 	$subjects_data = array();
 	$distinct_subjects = array();
 	$totals = array();
 	if($data->RecordCount()>0) {
 		while (!$data->EOF) {

       	 	$admno        = valueof($data->fields, 'ADMNO');
       	 	$fullname     = valueof($data->fields, 'FULLNAME');
       	 	$subjectcode  = valueof($data->fields, 'SUBJECTCODE');
       	 	$codeoffc     = valueof($data->fields, 'CODEOFFC');
       	 	$catcode      = valueof($data->fields, 'CATCODE');
       	 	$formcode     = valueof($data->fields, 'FORMCODE');
       	 	$streamcode   = valueof($data->fields, 'STREAMCODE');
       	 	$sbccode      = valueof($data->fields, 'SBCCODE');

       	 	if(!array_key_exists($subjectcode, $distinct_subjects)){
       	 		$distinct_subjects[$subjectcode] = $codeoffc;
       	 	}

       	 	$subjects_gradecode    = valueof($data->fields, $gradecodecol);
       	 	$subjects_points       = valueof($data->fields, $pointscol);

       	 	$subjects_data[$subjectcode][$subjects_gradecode][]     = $subjects_points;

            if(!isset($totals[$subjectcode])){
       	 	$totals[$subjectcode] = $subjects_points;
		    }else{
       	 	$totals[$subjectcode] +=$subjects_points;
		    }

       	  $data->MoveNext();
       	 }
 	}

 	 arsort($totals);

 	 if(sizeof($distinct_subjects)>0 && sizeof($subjects_data)>0 && sizeof($subject_grades)>0){
 	 	$page_rows    = 1;
 	 	$pageno       = 1;
 	 	$page_num_lines       = 30;
 	 	$header_td_colspan    = count($subject_grades) + 7;

 	 	$header = "<table cellpadding=\"2\" cellspacing=\"0\" width=\"100%\">";
 	 	 $header .= "<tr>";
 	 	  $header .= "<td colspan=\"{$header_td_colspan}\">

 	 	   <table width=\"100%\"  style=\"border:0\"  cellspacing=\"0\" cellpadding=\"1\" class=\"data\">

 	 	    <tr>
 	 	     <td  style=\"border:0\" rowspan=\"6\"><img src=\"{$school->logo_path}\" ></td>
 	 	    </tr>

 	 	    <tr>
 	 	     <td  style=\"border:0\" valign=\"top\"  colspan=\"2\"><span class=\"title\">{$school->name}</span></td>
 	 	    </tr>

 	 	    <tr>
 	 	     <td  style=\"border:0\"  colspan=\"2\"><b>Address : {$school->address}</b></td>
 	 	    </tr>

 	 	    <tr>
 	 	     <td  style=\"border:0\"  colspan=\"2\"><b>Tel :{$school->telephone}</b></td>
 	 	    </tr>


 	 	    <tr>
 	 	     <td  style=\"border:0\" colspan=\"2\"><b>Motto :{$school->motto}</b></td>
 	 	    </tr>

 	 	    <tr>
 	 	     <td  style=\"border:0\" colspan=\"2\">&nbsp;</td>
 	 	    </tr>

 	 	    <tr>
 	 	     <td  style=\"border:0\" colspan=\"2\">&nbsp;</td>
 	 	     <td style=\"border:0;text-align:right;\" colspan=\"1\" ><b>Date : </b>{$dateNow}</td>
 	 	    </tr>

 	 	     <tr>
 	 	     <td  style=\"border:0;text-align:center;font-weight:bolder;font-size:14px;\" colspan=\"3\"><b>{$examdesc}</b></td>
 	 	    </tr>

 	 	   </table>

 	 	 </td>";
 	 	$header .= "</tr>";


 	 	$header .= "<tr>";
 	 	 $header .="<td class=\"bold\">SUBJECT</td>";
 	 	 $header .="<td  class=\"bold\"nowrap>CODE</td>";
 	 	 $header .="<td  class=\"bold\"nowrap>CLS</td>";

 	 	 if(count($subject_grades)>0){
 	 	 	foreach ($subject_grades as $subject_gradecode=> $subject_grade_points){
 	 	 		$header .="<td class=\"bold\">{$subject_gradecode}</td>";
 	 	 	}
 	 	 }

 	 	 $header .="<td class=\"bold\">&nbsp;</td>";
 	 	 $header .="<td  class=\"bold\"nowrap>ENTRY</td>";
 	 	 $header .="<td class=\"bold\">MIN POINTS</td>";
 	 	 $header .="<td class=\"bold\" nowrap>MG</td>";
 	 	$header .="</tr>";

 	 	echo $header;

 	 	$count = 1;

 	 	//_______________________________________________________________________________________________________________________________
 	 	foreach ($totals as $subjectcode=>$total_points){

       	  $subject_code_official  = valueof($distinct_subjects, $subjectcode);
       	  $subjectname            = valueof($subjects, $subjectcode);

       	  $subject_grade_points_all = array();
       	  $count_subject_gradecode_students_all            = 0;
       	  $sum_subject_grade_points            = 0;

 	 	 if(count($subject_grades)>0){
 	 	 	foreach ($subject_grades as $subject_gradecode=> $subject_grade_points){

 	 	 		if(isset($subjects_data[$subjectcode][$subject_gradecode])){

 	 	 		$sum_subject_grade_points += $subject_grade_points;

 	 	 		$count_subject_gradecode_students      = count($subjects_data[$subjectcode][$subject_gradecode]);

 	 	 		$count_subject_gradecode_students_all += $count_subject_gradecode_students;
 	 	 		$subject_grade_points_all[] =  $subject_grade_points * $count_subject_gradecode_students;
 	 	 		$x[$subjectcode][$subject_grade_points][] = $count_subject_gradecode_students;

 	 	 	}
 	 	 }

 	 	 $subject_grade_points_all_sum = array_sum($subject_grade_points_all);
 	 	 
 	 	 $sum_subject_grade_points_avg = $count_subject_gradecode_students_all > 0 ? round( $total_points / $count_subject_gradecode_students_all, 4) : null;
 	 	 //$sum_subject_grade_points_avg = $count_subject_gradecode_students_all > 0 ? round($subject_grade_points_all_sum /  $count_subject_gradecode_students_all, 4) : null;
 	 	 $get_grade_code               = get_grade_code($sum_subject_grade_points_avg);

 	 	 $graph_data[$subjectcode]     =  $sum_subject_grade_points_avg;
 	 	 $display_points[$subjectcode] =  $sum_subject_grade_points_avg;
 	 	 $display_grades[$subjectcode] =  $get_grade_code;

 	  }//each subject
     }

 	  arsort($display_points);

 	  //_______________________________________________________________________________________________________________________________

 	 	foreach ($display_points as $subjectcode=>$subject_points){

       	  $total_points           = valueof($totals, $subjectcode);
       	  $subject_grade          = valueof($display_grades, $subjectcode);
       	  $subject_code_official  = valueof($distinct_subjects, $subjectcode);
       	  $subjectname            = valueof($subjects, $subjectcode);

       	  $count_subject_gradecode_students_all            = 0;

 	 	echo "<tr>";
 	 	 echo "<td>{$subjectname}</td>";
 	 	 echo "<td>{$subject_code_official}</td>";
 	 	 echo "<td>{$class_name}</td>";

 	 	 if(count($subject_grades)>0){
 	 	 	foreach ($subject_grades as $subject_gradecode=> $subject_grade_points){

 	 	 		if(isset($subjects_data[$subjectcode][$subject_gradecode])){

 	 	 		$count_subject_gradecode_students      = count($subjects_data[$subjectcode][$subject_gradecode]);

 	 	 		$count_subject_gradecode_students_all +=$count_subject_gradecode_students;

 	 	 		echo "<td>{$count_subject_gradecode_students}</td>";
 	 	 		}else{
 	 	 		echo "<td>&nbsp;</td>";
 	 	 		}
 	 	 	}
 	 	 }

 	 	 echo "<td>&nbsp;</td>";
 	 	 echo "<td>{$count_subject_gradecode_students_all}</td>";
 	 	 echo "<td>{$subject_points}</td>";
 	 	 echo "<td>{$subject_grade}</td>";
 	 	echo "</tr>";


 	  }//each subject


 	 	echo "<tr>";
 	 	 echo "<td colspan=\"{$header_td_colspan}\">";
 	 	  echo form_summary($yearcode, $termcode, $formcode, $exam, "m");
 	 	 echo "</td>";
 	 	echo "</tr>";

 	 	echo "<tr>";
 	 	 echo "<td colspan=\"{$header_td_colspan}\" >";
 	 	  echo '&nbsp;';
 	 	 echo "</td>";
 	 	echo "</tr>";

 	 	//echo "<tr>";
 	 	 //echo "<td colspan=\"{$header_td_colspan}\" style=\"border:0;text-align:center;font-weight:bolder;font-size:14px;\">";
 	 	  //echo 'Subjects Graphical Representation';
 	 	 //echo "</td>";
 	 	//echo "</tr>";

 	 	echo "<tr>";
 	 	 echo "<td colspan=\"{$header_td_colspan}\">";
 	 	  echo get_subject_graph($yearcode, $termcode, $formcode, $graph_data);
 	 	 echo "</td>";
 	 	echo "</tr>";

 	 	echo "</table>";
 	 }
 }

 //graph


 function get_subject_graph($yearcode, $termcode, $formcode,$graph_data){
  global $server_root,$subjects;

    $data  = array();

    if(is_array($subjects)){
     foreach($subjects as $subjectcode => $subjectname){
	  if(isset($graph_data[$subjectcode])){
	   $data[$subjectcode] = $graph_data[$subjectcode];
      }
	 }
	}

	arsort($graph_data);

	unset($graph_data);

	$graph_title     = 'Subjects Graphical Representation';
	$graph_img_name = "{$graph_title} {$yearcode} {$termcode} {$formcode}" . rand(1,100000);

	$datay  = array_values($data);
	$labels = array_keys($data);

	$graph = new Graph(800,400);
	$graph->SetScale('textlin');

	$graph->SetShadow();

	$graph->SetMargin(70,30,20,50);

	$bplot = new BarPlot($datay);

	$bplot->SetFillColor('orange');
	$graph->Add($bplot);

	$bplot->value->Show();
	$bplot->value->SetFormat('%01.3f');
	$bplot->SetValuePos('top');

	$graph->xaxis->SetTickLabels($labels);
	$graph->ygrid->SetFill(false);


	$graph->title->Set( $graph_title );
	$graph->xaxis->title->Set('Subjects');
	$graph->yaxis->title->Set('Mean Points');

	$graph->title->SetFont(FF_FONT1,FS_BOLD);
	$graph->yaxis->title->SetFont(FF_FONT1,FS_BOLD);
	$graph->xaxis->title->SetFont(FF_FONT1,FS_BOLD);

	$file_name        =  create_slug( $graph_img_name, $slug='_');
	$graph_path       =  ROOT.'tmp_files/'.$file_name.'.png';
	$graph_path_web   =  'tmp_files/'.$file_name.'.png';

	//@unlink($graph_path_web);

	$graph->Stroke($graph_path);

	return  "<img src=\"{$server_root}/{$graph_path_web}\">";

 }


 ?>
 </body>
</html>

