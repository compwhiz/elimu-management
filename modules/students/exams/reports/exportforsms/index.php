<?php

 $forms = $db->GetAssoc('SELECT FORMCODE,FORMNAME FROM SAFORMS ORDER BY FORMCODE');
 $streams_data = $db->GetAssoc('SELECT STREAMCODE,STREAMNAME,FORMCODE FROM SASTREAMS');
 $years = $db->GetAssoc('SELECT YEARCODE,YEARNAME FROM SAYEAR ORDER BY YEARCODE');
 $terms = $db->GetAssoc('SELECT TERMCODE,TERMNAME FROM SATERMS ORDER BY TERMCODE');
 $exams = $db->GetAssoc('SELECT TERMCODE,TERMNAME FROM SATERMS ORDER BY TERMCODE');

 $school = new school();
 
 $forms  = array();
 if(isset($streams_data)){
  if(sizeof($streams_data)>0){	
   foreach ($streams_data as $streamcode=>$stream_data){
   	$streamname = valueof($stream_data, 'STREAMNAME');
   	$formcode   = valueof($stream_data, 'FORMCODE');
   	$forms[$formcode] = "Form {$formcode}";
   	$forms[$streamcode] = "Form {$formcode}-{$streamname}";
   }
  }
 }
?>
        <form id="<?php echo ui::fi('ff'); ?>" method="post" novalidate>
		<table cellpadding="2" cellspacing="0" width="100%">
		
			<tr>
				<td  width="130px">Year:</td>
				<td>
					<?php echo ui::form_select_fromArray('year', $years, $school->active_yearcode," onchange=\"{$cfg['appname']}.list_exams();\" ");  ?>
				</td>
			</tr>
			
			<tr>
				<td>Term:</td>
				<td>
					<?php echo ui::form_select_fromArray('term', $terms, $school->active_termcode,"onchange=\"{$cfg['appname']}.list_exams();\"");  ?>
				</td>
			</tr>
			
			<tr>
				<td>Form</td>
				<td>
				<?php echo ui::form_select_fromArray('form', $forms,'',"onchange=\"{$cfg['appname']}.list_exams();\"");  ?>
				</td>
			</tr>
			
			<tr>
				<td>Exam:</td>
				<td>
				   <div id="<?php echo ui::fi('div_exams'); ?>">
					<?php echo ui::form_select_fromArray('exam', $exams);  ?>
					</div>
				</td>
			</tr>
			
			<tr>
				<td>Student *(Optional):</td>
				<td>
					<?php echo ui::form_input( 'text', 'find_admno', 20, '', '', '', '', '', '' , ""); ?>
				</td>
			</tr>
			
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			
			<tr>
				<td>&nbsp;</td>
				<td>
				 <a href="javascript:void(0)"  id="btnSendSMSExamSingle" class="easyui-linkbutton" onclick="<?php echo $cfg['appname']; ?>.pre_run();" style="min-height:30px;vertical-align:center" ><i class="fa fa-paper-plane"></i> Download SMS file</a>
				 &nbsp;
				 <a href="javascript:void(0)" class="easyui-linkbutton"  onclick="<?php echo $cfg['appname']; ?>.clearForm()"><i class="fa fa-undo"></i> Reset</a>
				</td>
			</tr>
			
		</table>
     </form>
     <script>
		
	  var <?php echo $cfg['appname']; ?> = {
		clearForm:function (){
			$('#<?php echo ui::fi('ff'); ?>').form('clear');
			$('#<?php echo ui::fi('find_admno'); ?>').val();
			$('.combo-text').val();
		},
		list_exams:function (){
		 $.post('./endpoints/crud/',   'modvars=<?php echo $vars; ?>&function=list_exams&'+$('#<?php echo ui::fi('ff'); ?>').serialize()   , function( html ){
  		  $('#<?php echo ui::fi('div_exams'); ?>').html(html);
  		 });
		},
        pre_run:function (){
			 $.messager.progress();
			 $('#btnSendSMSExam').linkbutton('disable');
			 var fdata = $('#<?php echo ui::fi('ff'); ?>').serialize()  + '&modvars=<?php echo $vars; ?>&function=pre_run';
		     $.post('./endpoints/crud/', fdata, function(data) {
             if (data.success === 1) {
                 <?php echo $cfg['appname']; ?>.send();
              } else {
				$.messager.progress('close');
				$('#btnSendSMSExam').linkbutton('enable');
                $.messager.alert('Error',data.message,'error');
                <?php echo $cfg['appname']; ?>.print_report_nophone();
             }
            }, "json");
		},
		send:function (){
		 var fdata = $('#<?php echo ui::fi('ff'); ?>').serialize()  + '&modvars=<?php echo $vars; ?>&function=send';
		 $.post('./endpoints/crud/', fdata, function(data) {
		       $.messager.progress('close');
		       $('#btnSendSMSExam').linkbutton('enable');
		       if (data.success === 1) {
                 if(typeof data.file_path=='string'){
				   top.location = data.file_path;
                   /*
                   var <?php echo ui::fi('w'); ?>=window.open(data.file_path,'height=800,width=1000,toolbar=no,menubar=no,directories=no,location=no,scrollbars=yes,status=no,resizable=no,fullscreen=no,top=0,left=0');
                   <?php echo ui::fi('w'); ?>.focus();
                   */
				 }
               } else {
                $.messager.alert('Error',data.message,'error');
               }
         }, "json");
		},
	    print_report_nophone:function (){
         var fdata = $('#<?php echo ui::fi('ff'); ?>').serialize();
  	     var <?php echo ui::fi('w'); ?>=window.open('./endpoints/print/?modvars=<?php echo $vars; ?>&'+fdata,'<?php echo ui::fi('pw'); ?>','height=800,width=1000,toolbar=no,menubar=no,directories=no,location=no,scrollbars=yes,status=no,resizable=no,fullscreen=no,top=0,left=0');
          <?php echo ui::fi('w'); ?>.focus();
        },
	  }
        
    <?php
      echo ui::ComboGrid($combogrid_array,'find_admno');
	?>
			
	</script>
