<?php

/**
* auto created config file for modules/students/exams/reports/exportforsms
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-08-20 13:02:30
*/

 final class exportforsms {
 private $id;
 private $datasrc;
 private $primary_key;

 public function __construct(){
  global $cfg;

     $this->id           = filter_input(INPUT_POST , 'id');
     $this->datasrc      = valueof($cfg,'datasrc');
     $this->primary_key  = valueof($cfg,'pkcol');
 }

 public function list_exams(){
		global $db;
	
	 $formcode     = filter_input(INPUT_POST , ui::fi('form'));	
	 $examcode     = filter_input(INPUT_POST , ui::fi('exam'));	
	 $yearcode     = filter_input(INPUT_POST , ui::fi('year'));	
	 $termcode     = filter_input(INPUT_POST , ui::fi('term'));
		
	 if(strlen($formcode)>1){
	 	$formcode = substr($formcode,0,1);
	 }
	 
	  $exams = $db->GetRow("
       SELECT S.CATNAME1 , S.CATNAME2 , S.CATNAME3 , S.CATNAME4 , S.CATNAME5,
        S.EXAMNAME1 , S.EXAMNAME2 , S.EXAMNAME3 , S.EXAMNAME4 , S.EXAMNAME5
        FROM SATSSG S
        WHERE S.FORMCODE='{$formcode}'
        AND S.YEARCODE='{$yearcode}'
        AND S.TERMCODE='{$termcode}'
       ");
	  
     $exams_array = array();
      
       if($exams){
       	if(sizeof($exams)>=1){
       		
       		for ($c=1;$c<=5;++$c){
       		 if(isset($exams["CATNAME{$c}"])){
       		 	 $exams_array["CAT{$c}"] = $exams["CATNAME{$c}"];
       		 }
       		}
       		
       		for ($e=1;$e<=5;++$e){
       		 if(isset($exams["EXAMNAME{$e}"])){
       		 	 $exams_array["PAPER{$e}"] = $exams["EXAMNAME{$e}"];
       		 }
       		}
       		
       		$exams_array['TOTAL'] = 'Combined';
       		
       	}
       }
       
	  return ui::form_select_fromArray('exam',$exams_array, $examcode," onchange=\"\" ");
	  
	}
 
 public function pre_run() {
		global $db;

    return json_response(1,"ok");
    
	$formcode     = filter_input(INPUT_POST , ui::fi('form'));
	$examcode     = filter_input(INPUT_POST , ui::fi('exam'));
	$yearcode     = filter_input(INPUT_POST , ui::fi('year'));
	$termcode     = filter_input(INPUT_POST , ui::fi('term'));

	$col     = 'FORMCODE';

	if(strlen($formcode)>1){
	 $formcode = substr($formcode,0,1);
	 $col     = 'STREAMCODE';
	}


    $missing_phones  = $db->Execute("select ADMNO,FULLNAME,STREAMCODE from VIEWSP where {$col}='{$formcode}' AND (MOBILEPHONE IS NULL OR MOBILEPHONE='' OR {$db->length}(MOBILEPHONE)<10)");
    $num             = @$missing_phones->RecordCount();

    if($num>0){
	 return json_response(0,"{$num} Student Are Missing Phone");
    }else{
	 return json_response(1,"ok");
	}


  }

 public function send(){
   global $db;

	$formstream   = filter_input(INPUT_POST , ui::fi('form'));
	$exam         = filter_input(INPUT_POST , ui::fi('exam'));
	$exam_name    = filter_input(INPUT_POST , ui::fi('exam'));
	$yearcode     = filter_input(INPUT_POST , ui::fi('year'));
	$termcode     = filter_input(INPUT_POST , ui::fi('term'));
	$admno        = filter_input(INPUT_POST , ui::fi('find_admno'));

	$fsCol     = 'FORMCODE';

	if(strlen($formstream)>1){
	 $formcode = substr($formstream,0,1);
	 $fsCol     = 'STREAMCODE';
	}

    $sql_extra    =  !empty($admno) ? "  and ADMNO='{$admno}' " : '';
    $positioning  = 'p';
    $disp_score   = 'p';

    if(strlen($formstream)>1){
     $formstream_code_col = "STREAMCODE";
     $positioning_col     = "POSTREAM";
     $formcode            = substr($formstream,0,1);

     switch($positioning){
	 case 'p':
      $positioning_col        = 'POSTREAM_POINTS';
      $display_col_name       = 'TOTALP';
      $display_col_title      = 'PNTS';
      $display_col_gradecode  = 'GRADECODEP';
      $positioning_by_descr   = ' POINTS ';
     break;
     case 'm':
     default:
      $positioning_col        = 'POSTREAM_MARKS';
      $display_col_name       = 'TOTALM';
      $display_col_title      = 'AVE';
      $display_col_gradecode  = 'GRADECODEM';
      $positioning_by_descr   = ' MARKS ';
     break;
   }

  }else{
   $formstream_code_col = "FORMCODE";
   $positioning_col     = "POSFORM";
   $formcode            = $formstream;

   switch($positioning){
	case 'p':
     $positioning_col        = 'POSFORM_POINTS';
     $display_col_name       = 'TOTALP';
     $display_col_title      = 'PNTS';
     $display_col_gradecode  = 'GRADECODEP';
     $positioning_by_descr   = ' POINTS ';
    break;
    case 'm':
    default:
     $positioning_col        = 'POSFORM_MARKS';
     $display_col_name       = 'TOTALM';
     $display_col_title      = 'AVE';
     $display_col_gradecode  = 'GRADECODEM';
     $positioning_by_descr   = ' MARKS ';
    break;
  }
 }

  switch ($disp_score){
	case 'r':
	  $score_column_postfix = 'R';
	 break;
	 case 'p':
	 default:
	  $score_column_postfix = 'P';
	 break;
 }

 $exam_column      = $exam.$score_column_postfix;
 $examdesc_joined  = "Form {$formstream} {$exam_name} T{$termcode} {$yearcode}";
 $examdesc_slugged = create_slug("Form {$formstream} {$exam_name} {$yearcode}T{$termcode}");
 
 $examdesc         = ($examdesc_joined);

 if(strstr($exam,'CAT') || strstr($exam,'PAPER')){
  $gradecode_col = "{$exam}GRADECODE";
 }else{
  $gradecode_col = "GRADECODE";
 }

 $students  = $db->Execute("
        SELECT ADMNO,FULLNAME,MOBILEPHONE,STREAMCODE,SUBJECTCODE,PICKED,{$exam} SCORE,{$gradecode_col} GRADECODE FROM VIEWSTUDENTSUBJECTS
        WHERE YEARCODE='{$yearcode}'
        AND TERMCODE='{$termcode}'
        AND {$formstream_code_col}='{$formstream}'
        {$sql_extra}
        and {$exam}>0
        ORDER BY STREAMCODE, SORTPOS,CODEOFFC
        ");

   if(!$students){
    return json_response(0, "An Error Occured. Try again later");
   }

   if($students->RecordCount()==0){
    return json_response(0, "No Student To Display");
   }

	$SMSGateway   =  new sms();

    if(!empty($SMSGateway->error)){
      return json_response(0, $SMSGateway->error);
	}

 	$subjects_data = array();
 	$distinct_subjects = array();
 	$distinct_subjects_official_codes = array();
 	$summary_grades = array();
 	$summary_streams = array();
 	$summary_points = array();
 	$summary = array();

 foreach($students as $student){

	$admno         = valueof($student, 'ADMNO');
	$fullname      = valueof($student, 'FULLNAME');
	$streamcode    = valueof($student, 'STREAMCODE');
	$mobilephone   = valueof($student, 'MOBILEPHONE');

	if(!strstr($mobilephone,'+254') || substr($mobilephone,0,4)!='+254'  ){
    $mobilephone = '+254'.$mobilephone;
	}

	//$mobilephone = "+254722323391";

	//if( strlen($mobilephone)==13){
	if( 1>0 ){
	$subjectcode  = valueof($student, 'SUBJECTCODE');
	$gradecode    = valueof($student, 'GRADECODE');
	$picked       = valueof($student, 'PICKED');

	if($picked!=1){
     $subjectcode = "*{$subjectcode}";
	}

	$subjects_data[$admno]['mobilephone']  = $mobilephone;
	$subjects_data[$admno]['streamcode']   = $streamcode;
	$subjects_data[$admno]['fullname']     = $fullname;
	$subjects_data[$admno]['subjects'][]   = "{$subjectcode}={$gradecode}";

    }
 }

  $num_sent_to  = 0;

  
  $file      = fopen( ROOT ."tmp_files/{$examdesc_slugged}.csv","w");
  $file_path =  "./tmp_files/{$examdesc_slugged}.csv";
  fprintf($file, chr(0xEF).chr(0xBB).chr(0xBF));

  //fputcsv($file, array('Phone', 'Message') );

  foreach($subjects_data as $admno=> $student ){
   $mobilephone  = valueof($student, 'mobilephone');
   $streamcode   = valueof($student, 'streamcode');
   $fullname     = valueof($student, 'fullname');
   $results      = valueof($student, 'subjects');
   $results_str  = implode( ', ', $results);
   $sms_msg      = centerTrim("{$fullname} Results for {$examdesc}. {$results_str}");

   fputcsv($file, array($mobilephone ,$sms_msg ) );
   
   ++$num_sent_to;
  }
 
   fclose($file);

    $s = $num_sent_to>1 ? '' : 's';

	return json_response(1, "Processed {$num_sent_to} Student{$s}", array('file_path' => $file_path) );

  }
	

}
