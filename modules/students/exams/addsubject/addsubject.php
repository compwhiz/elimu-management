<?php

class addsubject
{
    private $id;
    private $page;
    private $rows;
    private $admno;

    public function __construct()
    {
        global $cfg;

        $this->datasrc = valueof($cfg, 'datasrc');
        $this->primary_key = valueof($cfg, 'pkcol');

        $this->id = filter_input(INPUT_POST, 'id');
        $this->admno = filter_input(INPUT_POST, 'admno');

    }

    public function get_profile()
    {
        global $db, $cfg;

        $student = new student($this->admno);
        $school = new school();
        $streamcode = $student->streamcode;
        $streamcode_alpha = preg_replace("/[^A-Za-z]/", '', $streamcode);

        $streams = $db->GetAssoc("SELECT STREAMCODE,STREAMNAME,FORMCODE FROM SASTREAMS WHERE FORMCODE<='{$student->formcode}' AND STREAMCODE LIKE '%{$streamcode_alpha}%'");
        $years = $db->GetAssoc('SELECT YEARCODE,YEARNAME FROM SAYEAR ORDER BY YEARCODE');
        $terms = $db->GetAssoc('SELECT TERMCODE,TERMNAME FROM SATERMS ORDER BY TERMCODE');
        $exams = $db->GetAssoc('SELECT TERMCODE,TERMNAME FROM SATERMS ORDER BY TERMCODE');
        $subjects = $db->GetAssoc("SELECT SUBJECTCODE,SUBJECTNAME FROM SASUBJECTS WHERE OFFERED=1 order by SUBJECTCODE");

        $forms = array();
        if (isset($streams)) {
            if (sizeof($streams) > 0) {
                foreach ($streams as $streamcode => $stream_data) {
                    $streamname = valueof($stream_data, 'STREAMNAME');
                    $formcode = valueof($stream_data, 'FORMCODE');
                    $forms[$streamcode] = "Form {$streamname}";
                }
            }
        }

        asort($years);
        asort($forms);
        asort($terms);
        ?>
        <table cellpadding="2" cellspacing="0" width="100%">

            <tr>
                <td width="100px">Year:</td>
                <td><?php echo ui::form_select_fromArray('yearcode', $years, $school->active_yearcode, " onchange=\"{$cfg['appname']}.list_subjects();\" "); ?></td>
                <td>Term:</td>
                <td><?php echo ui::form_select_fromArray('termcode', $terms, $school->active_termcode, "onchange=\"{$cfg['appname']}.list_subjects();\""); ?></td>
            </tr>

            <tr>
                <td>Form</td>
                <td><?php echo ui::form_select_fromArray('form', $forms, $student->streamcode, "onchange=\"{$cfg['appname']}.list_subjects();\""); ?></td>
                <td>Subject</td>
                <td><?php echo ui::form_select_fromArray('subject', $subjects, '', "onchange=\"{$cfg['appname']}.list_subjects();\"", false, 150); ?></td>
            </tr>


            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>
                    <button onclick="<?php echo $cfg['appname']; ?>.add_subject();return false;">Add Subject</button>
                </td>
            </tr>

            <tr>
                <td colspan="4">
                    <div id="div_subjects"></div>
                </td>
            </tr>

        </table>

        <script>
            <?php echo $cfg['appname']; ?>.list_subjects();
        </script>

        <?php
    }

    public function list_subjects()
    {
        global $db, $cfg;

        $formcode = filter_input(INPUT_POST, ui::fi('form'));
        $yearcode = filter_input(INPUT_POST, ui::fi('yearcode'));
        $termcode = filter_input(INPUT_POST, ui::fi('termcode'));
        $streamcode = filter_input(INPUT_POST, ui::fi('form'));
        $formcode = filter_input(INPUT_POST, ui::fi('form'));

        if (strlen($streamcode) > 1) {
            $formcode = substr($streamcode, 0, 1);
        }

        $data_sql = ("
select SUBJECTCODE,ID, SUBJECTNAME ,CAT1,CAT2,CAT3,CAT4,CAT5,AVGCAT,PAPER1,PAPER2,PAPER3,PAPER4,PAPER5,
CAT1GRADECODE,CAT2GRADECODE,CAT3GRADECODE,CAT4GRADECODE,CAT5GRADECODE,
PAPER1GRADECODE,PAPER2GRADECODE,PAPER3GRADECODE,PAPER4GRADECODE,PAPER5GRADECODE,

AVGEXAM,TOTAL,GRADECODE,POINTS,STATUSCODE
from VIEWSTUDENTSUBJECTS 
where ADMNO='{$this->admno}' 
and FORMCODE='{$formcode}'
and YEARCODE='{$yearcode}' 
and TERMCODE='{$termcode}' 
ORDER BY ADMNO,SORTPOS, SUBJECTCODE
");
        $data = $db->GetAssoc($data_sql);

        $subjects = array();

        if ($data) {
            if (sizeof($data) > 0) {
                foreach ($data as $subjectcode => $subject) {
                    $subjectname = valueof($subject, 'SUBJECTNAME');
                    $subjects[$subjectcode] = $subjectname;

                }
            }
        }

        $setup = $db->GetRow("
       SELECT * FROM SATSSG
        WHERE FORMCODE='{$formcode}'
        and YEARCODE='{$yearcode}' 
        and TERMCODE='{$termcode}' 
       ");

        $subject_config = array();

        for ($c = 1; $c <= 5; ++$c) {
            if (isset($setup["CATMAX{$c}"])) {
                if ($setup["CATMAX{$c}"] > 0) {
                    $subject_config['cats'][$c]['max'] = $setup["CATMAX{$c}"];
                    $subject_config['cats'][$c]['name'] = isset($setup["CATNAME{$c}"]) && !empty($setup["CATNAME{$c}"]) ? $setup["CATNAME{$c}"] : "CAT {$c}";
                }
            }
        }

        for ($x = 1; $x <= 5; ++$x) {
            if (isset($setup["EXAMMAX{$x}"])) {
                if ($setup["EXAMMAX{$x}"] > 0) {
                    $subject_config['papers'][$x]['max'] = $setup["EXAMMAX{$x}"];
                    $subject_config['papers'][$x]['name'] = isset($setup["EXAMNAME{$x}"]) && !empty($setup["EXAMNAME{$x}"]) ? $setup["EXAMNAME{$x}"] : "Paper {$x}";
                }
            }
        }

        echo '<table id="marks" cellpadding="4" cellspacing="0" width="100%" border="0" class="datagrid-htable datagrid-btable datagrid-ftable">';
        echo '<thead>';

        echo '<tr>';
        echo '<td   class="panel-header" >No.</td>';
        echo '<td   class="panel-header" >Subject</td>';

        if (isset($subject_config['cats']) && sizeof($subject_config['cats']) > 0) {
            foreach ($subject_config['cats'] as $cat_index => $cat) {
                $name = valueof($cat, 'name', '', 'Camelize');
                $max = valueof($cat, 'max');
                echo "<td   class=\"panel-header\">{$name}</td>";
            }
            echo "<td  class=\"panel-header\">&nbsp;</td>";
        }

        if (isset($subject_config['papers']) && sizeof($subject_config['papers']) > 0) {
            foreach ($subject_config['papers'] as $cat_index => $paper) {
                $name = valueof($paper, 'name', '', 'Camelize');
                $max = valueof($paper, 'max');
                echo "<td  class=\"panel-header\">{$name}</td>";
            }
        }

        echo "<td class=\"panel-header\" colspan=\"4\">&nbsp;</td>";
        echo "</tr>";

        echo '<tr>';
        echo '<td  class="header  top  left right">&nbsp;</td>';
        echo '<td  class="header top   right">&nbsp;</td>';

        if (isset($subject_config['cats']) && sizeof($subject_config['cats']) > 0) {
            foreach ($subject_config['cats'] as $cat_index => $cat) {
                $name = valueof($cat, 'name');
                $max = valueof($cat, 'max');
                echo "<td  class=\"header top  right\">{$max}</td>";
            }
        }
        echo "<td  class=\"header top   right\">CT.Avg</td>";

        if (isset($subject_config['papers']) && sizeof($subject_config['papers']) > 0) {
            foreach ($subject_config['papers'] as $cat_index => $paper) {
                $name = valueof($paper, 'name');
                $max = valueof($paper, 'max');
                echo "<td  class=\"header top   right\">{$max}</td>";
            }
        }

        echo "<td class=\"header top right\">PP.Avg</td>";
        echo "<td class=\"header top  right\">Total</td>";
        echo "<td class=\"header top  right\">Grade</td>";
        echo "<td class=\"header top  right\">Options</td>";
        echo "</tr>";

        echo '</thead>';

        if (count($subjects) > 0) {

            echo '<tbody>';

            $student = array();
            $count = 1;

            foreach ($subjects as $subjectcode => $subjectname) {
                $subjectname = empty($subjectname) ? 'missing' : $subjectname;
                $subject = isset($data[$subjectcode]) ? $data[$subjectcode] : array();

                $id = valueof($subject, 'ID');

                $cat1 = valueof($subject, 'CAT1');
                $cat1 = round($cat1, 0);
                $cat2 = valueof($subject, 'CAT2');
                $cat3 = valueof($subject, 'CAT3');
                $cat4 = valueof($subject, 'CAT4');
                $cat5 = valueof($subject, 'CAT5');
                $avgcat = valueof($subject, 'AVGCAT');

                $paper1 = valueof($subject, 'PAPER1');
                $paper2 = valueof($subject, 'PAPER2');
                $paper3 = valueof($subject, 'PAPER3');
                $paper4 = valueof($subject, 'PAPER4');
                $paper5 = valueof($subject, 'PAPER5');
                $avgexam = valueof($subject, 'AVGEXAM');
                $total = valueof($subject, 'TOTAL');
                $gradecode = valueof($subject, 'GRADECODE');

                $divAvgCat = "divavgcat_{$id}";
                $divAvgExam = "divavgexam_{$id}";
                $divGradeCode = "divgrade_{$id}";
                $divTotal = "divtotal_{$id}";
                $divStatus = "divstatus_{$id}";

                $link_delete = ui::href("{$cfg['appname']}.delete_subject({$id},'{$subjectname}','{$yearcode}','{$termcode}');", '<i class="fa fa-trash"></i> ', "Delete" . $subjectname, 'delete' . $subjectname, 'delete', 'delete');
                //$link_delete = ui::href("{$cfg['appname']}.delete_subject({$id},'{$subjectname}','{$yearcode}','{$termcode}');",'<i class="fa fa-trash"></i> ',"Delete ".$subjectname,'delete'.$subjectname,'delete','delete');

                echo '<tr>';
                echo "<td class=\"input left\" >{$count}</td>";
                echo "<td class=\"input\" nowrap>{$subjectname}</td>";

                if (isset($subject_config['cats']) && sizeof($subject_config['cats']) > 0) {
                    foreach ($subject_config['cats'] as $cat_index => $cat) {

                        $value = valueof($subject, "CAT{$cat_index}");
                        $value = round($value, 1);
                        $name = valueof($cat, 'name');
                        $max = valueof($cat, 'max');

                        echo "<td class=\"input\"  >{$value}</td>";

                    }
                } else {
//         	echo "<td class=\"input\"  >&nbsp;</td>";
                }

                echo "<td class=\"input\" ><div id=\"{$divAvgCat}\">{$avgcat}</div></td>";

                if (isset($subject_config['papers']) && sizeof($subject_config['papers']) > 0) {
                    foreach ($subject_config['papers'] as $paper_index => $paper) {

                        $value = valueof($subject, "PAPER{$paper_index}");
                        $value = round($value, 1);
                        $name = valueof($paper, 'name');
                        $max = valueof($paper, 'max');

                        echo "<td class=\"input\" >{$value}</td>";

                    }
                }

                echo "<td class=\"input\" ><div id=\"{$divAvgExam}\">{$avgexam}</div></td>";
                echo "<td class=\"input\" ><div id=\"{$divTotal}\">{$total}</div></td>";
                echo "<td class=\"input\" ><div id=\"{$divGradeCode}\">{$gradecode}</div></td>";
                echo "<td class=\"input\">{$link_delete}</td>";
                echo '</tr>';

//        exit();

                ++$count;
            }//each subject

            echo '</tbody>';
        }

        echo '</table>';


        //echo "
        //<script>
//$('#marks').tableNav();
//$('#marks input').eq(1).click();
//</script>
//";
    }

    public function add_subject()
    {
        global $db, $cfg;

        $admno = filter_input(INPUT_POST, ui::fi('find_admno'));
        $yearcode = filter_input(INPUT_POST, ui::fi('yearcode'));
        $termcode = filter_input(INPUT_POST, ui::fi('termcode'));
        $subjectcode = filter_input(INPUT_POST, ui::fi('subject'));
        $streamcode = filter_input(INPUT_POST, ui::fi('form'));
        $formcode = filter_input(INPUT_POST, ui::fi('form'));
        $user = new user();

        if (strlen($streamcode) > 1) {
            $formcode = substr($streamcode, 0, 1);
        }

        $subject = new ADODB_Active_Record('SASTUDSUB', array('ADMNO', 'SUBJECTCODE', 'FORMCODE', 'YEARCODE', 'TERMCODE'));

        $subject->Load("
	ADMNO='{$admno}'
	AND SUBJECTCODE='{$subjectcode}'
	AND FORMCODE='{$formcode}'
	AND YEARCODE='{$yearcode}'
	AND TERMCODE='{$termcode}'
	");

        if (!empty($subject->_original)) {
            $deleted = valueof($subject, 'deleted');
            if (!$deleted) {
                return json_encode(array('success' => 0, 'message' => "{$subjectcode} Already Exists in {$yearcode}/{$termcode} F{$formcode}"));
            }
        }

        if (empty($subject->_original)) {
            $subject->id = generateID($subject->_tableat, 'ID', $db);
            $subject->admno = $admno;
            $subject->subjectcode = $subjectcode;
            $subject->formcode = $formcode;
            $subject->streamcode = $streamcode;
            $subject->yearcode = $yearcode;
            $subject->termcode = $termcode;
        }

        $subject->deleted = null;
        // TODO: Check why $record variable is not initialised.
        $record->audituser = $user->userid;
        $record->auditdate = date('Y-m-d');
        $record->audittime = time();

        if ($subject->Save()) {
            return json_encode(array('success' => 1, 'message' => 'Subject ' . $subjectcode . ' Saved.'));
        } else {
            $error = @$db->ErrorMsg();
            return json_response(0, "save failed : {$error}");
        }

    }

    public function delete()
    {
        global $db;

        $id = filter_input(INPUT_POST, 'id');
        $subjectname = filter_input(INPUT_POST, 'subjectname');
        $delete = $db->Execute("UPDATE SASTUDSUB SET DELETED=1  WHERE ID={$id}");

        if ($delete) {
            return json_response(1, "Subject {$subjectname} Deleted");
        } else {
            $error = @$db->ErrorMsg();
            return json_response(0, "Delete Failed : {$error}");
        }

    }

    private function get_global_set_up($formcode, $yearcode, $termcode)
    {
        global $db;

        $global_setup = $db->GetRow("
       SELECT * FROM SATSSG
        WHERE FORMCODE='{$formcode}'
        AND YEARCODE='{$yearcode}'
        AND TERMCODE='{$termcode}'
       ");

        return $global_setup;
    }
}
