<?php


//  Code to use when importing from MS-Access
//INSERT INTO sastudsub (ADMNO,SUBJECTCODE,FORMCODE,STREAMCODE,YEARCODE,TERMCODE,CAT1,CAT2,PAPER1)
//SELECT ADMISSIONNO,'COM',FORM,CLASS,YEAR,TERM,COMC,COMC2,COMEX FROM analysis WHERE ADMISSIONNO IN (SELECT DISTINCT admno FROM sastudents)
/**
 * @todo
 * make table id unique
 * css refs uniq
 */
 $school          =  new school();
 $user            =  new user();
 $terms           =  $db->GetAssoc('SELECT TERMCODE,TERMNAME FROM SATERMS ORDER BY TERMCODE');
 $subjects_array  =  $db->GetAssoc("SELECT SUBJECTCODE,SUBJECTNAME FROM SASUBJECTS order by SUBJECTCODE");

 $my_subjects     =  $db->GetArray("
	SELECT SUBJECTCODE,STREAMCODE,YEARCODE FROM SATSA
	where TCHNO='{$user->userid}'
	ORDER BY YEARCODE,STREAMCODE ASC
   ");


 $my_list    = array();
 $my_streams = array();
 $streams = array();
 $years = array();
 $subjects = array();

 if( sizeof($my_subjects)>0){
 	foreach ($my_subjects as $my_subject){
 		$subjectcode  = valueof($my_subject, 'SUBJECTCODE');
 		$streamcode   = valueof($my_subject, 'STREAMCODE');
 		$yearcode     = valueof($my_subject, 'YEARCODE');

 		$years[$yearcode] = $yearcode;
 		$subjects[$subjectcode] = valueof($subjects_array,$subjectcode);
 	}
 }

$table_id =  ui::fi('tblMarks');
?>
<style>
table#<?php echo $table_id;?> {font-size:12px;font-family:Verdana}
table.tblSubSetup {font-size:12px;font-family:Verdana}
table.tblSubSetup td.label{ background-color:#95B8E7;}
table.tblSubSetup td.cat{ background-color:#E2EDFF;}
table.tblSubSetup td.exam{ background-color:#FFE48D;}

table#<?php echo $table_id;?> a.delete { color: #555;}
table#<?php echo $table_id;?> a.delete:hover { color: #D00;}
table#<?php echo $table_id;?> a.comments { color: #555;}
table#<?php echo $table_id;?> a.comments:hover { color: #0D0;}

table#<?php echo $table_id;?> a.active-sort { color: #00F;}
table#<?php echo $table_id;?> a.active-sort:hover { color: #D00;}
table#<?php echo $table_id;?> a.inactive-sort { color: #555;}
table#<?php echo $table_id;?> a.inactive-sort:hover { color: #00F;}

table#<?php echo $table_id;?> td.input input {
    width: 50px;
    box-sizing: border-box;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
}

table#<?php echo $table_id;?> td.header {
 border-top:1px solid #999;
 border-bottom:1px dotted #999;
 padding-left: 5px;
}

table#<?php echo $table_id;?> td.input {
 border-bottom:1px dotted #999;
 border-right:1px dotted #999;
 padding-left: 5px;
}

table#<?php echo $table_id;?> td.left {
 border-left:1px dotted #999;
}

table#<?php echo $table_id;?> td.right {
 border-right:1px dotted #999;
}

#loading {
width: 100%;
position: absolute;
}


#pagination
{
text-align:center;
margin-left:100px;

}
li.nav{
list-style: none;
float: left;
margin-left: 5px;
padding:4px;
border:solid 1px #dddddd;
color:#0063DC;
font-size:12px;
}
li.nav:hover
{
color:#FF0084;
cursor: pointer;
}

li.pagination li.page-active{
	border: 4px solid #c2e1f5;
}

</style>


    <div id="content<?php echo MNUID; ?>"  class="easyui-panel" title="Select Subject Below" fit="true" >
	  <form id="ff<?php echo MNUID; ?>" method="post" onsubmit="return false;">
	  <table cellpadding="2" cellspacing="0" width="100%">

	  <tr>
			<td width="180px">Year:</td>
			<td>
			 <?php echo ui::form_select_fromArray('yearcode', $years, $school->active_yearcode," onchange=\"{$cfg['appname']}.list_streams();\" ");  ?>
			</td>
		</tr>

		<tr>
			<td>Term:</td>
			<td>
			<?php echo ui::form_select_fromArray('termcode', $terms,'',"onchange=\"{$cfg['appname']}.list_streams();\"");  ?>
			</td>
		</tr>

		<tr>
			<td >Stream</td>
			<td>
			 <div id="div_streams<?php echo MNUID; ?>">
			 <?php echo ui::form_select_fromArray('streamcode', $streams,'',"onchange=\"{$cfg['appname']}.list_subjects();\"");  ?>
			 </div>
			</td>
		</tr>

		<tr>
			<td>Subject:</td>
			<td>
			   <div id="div_subjects<?php echo MNUID; ?>">
				<?php echo ui::form_select_fromArray('subjectcode', $subjects);  ?>
				</div>
			</td>
		</tr>

		<tr>
			<td>&nbsp;</td>
			<td>
			 <button onclick="<?php echo $cfg['appname']; ?>.subject_setup();return false;"><i class="fa check-square"></i> Open Subject</button>
			</td>
		</tr>

	 </table>
	</form>
  </div>

    <div id="pager" ></div>
    <div id="loading" ></div>

    <div id="dlg<?php echo MNUID; ?>" class="easyui-dialog" title="Subject Setup" data-options="iconCls:'icon-save'" closed="true" style="width:580px;height:320px;padding:10px">
		<div id="div_setup<?php echo MNUID; ?>"></div>
	</div>


	<div id="<?php echo ui::fi('dlg_comments'); ?>" class="easyui-dialog" style="width:350px;height:160px;padding:2px" closed="true" buttons="#<?php echo ui::fi('dlg-buttons'); ?>">
	 <form id="<?php echo ui::fi('ff_comments') ?>" method="post" novalidate onsubmit="return false;">
	 <div id="<?php echo ui::fi('div_comments'); ?>">&nbsp;</div>
	 </form>
	</div>

	<div id="<?php echo ui::fi('dlg-buttons'); ?>">
		<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="<?php echo $cfg['appname']; ?>.save_subject_comment();" >Save Comments</a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#<?php echo ui::fi('dlg_comments'); ?>').dialog('close')">Cancel</a>
	</div>

    <script>

   var <?php echo $cfg['appname']; ?> = {
   load_stage:function (){
	   ui.cc('load_stage', 'modvars=<?php echo $vars; ?>', 'content<?php echo MNUID; ?>');
   },
   list_streams:function (){
    $.post('./endpoints/crud/',   'modvars=<?php echo $vars; ?>&function=list_streams&'+$('#ff<?php echo MNUID; ?>').serialize()   , function( html ){
	$('#div_subjects<?php echo MNUID; ?>').html('&nbsp;');
  	$('#div_streams<?php echo MNUID; ?>').html(html);
  	});
   },
   list_subjects:function (){
    $.post('./endpoints/crud/',   'modvars=<?php echo $vars; ?>&function=list_subjects&'+$('#ff<?php echo MNUID; ?>').serialize()   , function( html ){
  	$('#div_subjects<?php echo MNUID; ?>').html(html);
  	});
   },
   subject_setup:function (  ){
   	   var data = '&modvars=<?php echo $vars; ?>&'+ $('#ff<?php echo MNUID; ?>').serialize();
       ui.cc('subject_setup',data , 'div_setup<?php echo MNUID; ?>');
  		$('#dlg<?php echo MNUID; ?>').dialog({
             title:  "Subject Setup",
             closed: false,
             resizable: true,
             modal: true,
         });
  	},
  	subject_resetup:function (subjectcode, streamcode, yearcode, termcode, enable_enter_marks){
	  var data = 'modvars=<?php echo $vars; ?>&subjectcode='+subjectcode+'&streamcode='+streamcode+'&yearcode='+yearcode+'&termcode='+termcode;
       ui.cc('subject_resetup',data , 'div_setup<?php echo MNUID; ?>');
  		$('#dlg<?php echo MNUID; ?>').dialog({
             title:  "Subject Setup",
             closed: false,
             resizable: true,
             modal: true,
         });
	},
  	save_subject_setup:function (subjectcode, streamcode, yearcode, termcode, enable_enter_marks){
  	var fdata = $('#frmSubSetup<?php echo MNUID; ?>').serialize() + '&modvars=<?php echo $vars; ?>&function=save_subject_setup&subjectcode='+subjectcode+'&streamcode='+streamcode+'&yearcode='+yearcode+'&termcode='+termcode;
  	$.post("./endpoints/crud/",   fdata  , function(data){
       	if(data.success===1){
         if(enable_enter_marks){
          $("#btnEntry").removeAttr( "disabled");
         }
         $.messager.show({title: 'Success',msg: data.message});
        }else{
            $("#btnSave").removeAttr( "disabled");
            $.messager.alert('Settings Save',data.message,'error');
        }
        }, "json" );
    },
    preload_subject:function (subjectcode, streamcode, yearcode, termcode){
  	    $('#dlg<?php echo MNUID; ?>').dialog("close");
  	    $('#content<?php echo MNUID; ?>').panel({title:""+subjectcode+"- "+streamcode+"  "+yearcode+" Term "+termcode+""})
  		 $.post('./endpoints/crud/',   'modvars=<?php echo $vars; ?>&function=preload&subjectcode='+subjectcode+'&streamcode='+streamcode+'&yearcode='+yearcode+'&termcode='+termcode  , function( pager_content ){
  		 	$('#pager').html(pager_content);
  		 	<?php echo $cfg['appname']; ?>.open_subject();
  		 });
     },
    open_subject:function (){
 	 $("#content<?php echo MNUID; ?>").load("./endpoints/crud/?modvars=<?php echo $vars;
 	 ?>&function=data&rows=100&page=1");
 	 $('#button_print').linkbutton('enable');
 	 $('#button_config').linkbutton('enable');
     },
     paginate:function (lid,pageNum,sortcol,sortorder){
		$("#content<?php echo MNUID; ?>").load("./endpoints/crud/?modvars=<?php echo $vars;
		?>&function=data&sortcol="+sortcol+"&sortorder="+sortorder+"&rows=100&page=" + pageNum);
     },
     pagesort:function (sortcol,sortorder,pageNum){
		$("#content<?php echo MNUID; ?>").load("./endpoints/crud/?modvars=<?php echo $vars;
		?>&function=data&sortcol="+sortcol+"&sortorder="+sortorder+"&rows=100&page=" + pageNum);
     },
     print_subject:function (r){
  	  var w=window.open('./endpoints/print/?modvars=<?php echo $vars; ?>&r='+r,'<?php echo ui::fi('pw'); ?>','height=800,width=830,toolbar=no,menubar=no,directories=no,location=no,scrollbars=yes,status=no,resizable=no,fullscreen=no,top=0,left=0');
          w.focus();
     },
	  makeTableNav:function (){
         $('#tbl').tableNav();
         return $('#tbl input').eq(0).click();
	  },
      clearForm:function (){
		$('#ff<?php echo MNUID; ?>').form('clear');
	   },
	  save:function (id,column,textboxId,max,origVal,name,divAvgCat,divAvgExam,divTotal,divGradeCode,divStatus){
			 var inputval = $('input#'+textboxId).val();

			 if(typeof inputval<0){
			 	$.messager.alert(name,'Numbers Only','error');
			 	$('input#'+textboxId).val(origVal);
			 	return;
			 }

			 inputval = inputval*1;
			 max  = max*1;
			 origVal  = origVal*1;

			 if(inputval>max){
			 	$.messager.alert('Error','Score Greator for '+name+' than Max '+max,'error');
			 	$('input#'+textboxId).val(origVal);
			 	return;
			 }

			  /*if(inputval != origVal){*/
			 if(inputval>=0){

			  var fdata = '&id='+ id +'&max='+ max +'&score='+ inputval + '&column='+ column + '&modvars=<?php echo $vars; ?>&function=save';
		      $.post('./endpoints/crud/', fdata, function(data) {

               if (data.success === 1) {
                 $('#'+divAvgCat).html(data.avgcat);
                 $('#'+divAvgExam).html(data.avgexam);
                 $('#'+divTotal).html(data.total);
                 $('#'+divGradeCode).html(data.gradecode);
                } else {
				 $.messager.alert('Error',data.message,'error');
             }
            }, "json");
		   }
		},
		subject_comment:function (id,subjectode,yearcode,termcode){
			 $('#<?php echo ui::fi('dlg_comments'); ?>').dialog({
             title: 'Custom Comments for '+subjectode+' '+yearcode+'/T'+termcode+'',
             closed: false,
             resizable: true,
             modal: true,
            });
            ui.fc('modvars=<?php echo $vars; ?>&function=get_custom_comments&id='+id,'<?php echo ui::fi('div_comments'); ?>');
	   },
	   get_exam_custom_comment:function (id){
         ui.fc('modvars=<?php echo $vars; ?>&function=get_exam_custom_comment&id='+id+'&exam='+$('#<?php echo ui::fi('exam'); ?>').val(),'<?php echo ui::fi('exam_custom_comments'); ?>');
	   },
	   save_subject_comment:function (id){
         var fdata = $('#<?php  echo ui::fi('ff_comments')?>').serialize() + '&modvars=<?php echo $vars; ?>&function=save_subject_comment';
  	     $.post("./endpoints/crud/",   fdata  , function(data){
       	 if(data.success===1){
          $.messager.show({title: 'Success',msg: data.message});
		  $('#<?php echo ui::fi('dlg_comments'); ?>').dialog('close')
         }else{
          $.messager.alert('Save Comments',data.message,'error');
         }
         }, "json" );

		},
	}

</script>
