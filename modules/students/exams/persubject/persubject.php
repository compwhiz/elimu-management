<?php

  class persubject{
	private $id;
	private $page;
	private $rows;
	private $admno;

	public function __construct(){
		global $cfg;

		$this->id           = filter_input(INPUT_POST , 'id');
		$this->datasrc      = valueof($cfg,'datasrc');
		$this->primary_key  = valueof($cfg,'pkcol');
	}

	public function load_stage(){
		global $db,$cfg;
		$school          =  new school();
		$user            =  new user();
		$terms           =  $db->GetAssoc('SELECT TERMCODE,TERMNAME FROM SATERMS ORDER BY TERMCODE');
		$subjects_array  =  $db->GetAssoc("SELECT SUBJECTCODE,SUBJECTNAME FROM SASUBJECTS order by SUBJECTCODE");

		$my_subjects     =  $db->GetArray("
		SELECT SUBJECTCODE,STREAMCODE,YEARCODE FROM SATSA
		where TCHNO='{$user->userid}'
		ORDER BY YEARCODE,STREAMCODE ASC
		");

		$years    = array();
		$subjects = array();
        $streams  = array();

		if( sizeof($my_subjects)>0){
		 foreach ($my_subjects as $my_subject){
			$subjectcode_loop  = valueof($my_subject, 'SUBJECTCODE');
			$streamcode_loop   = valueof($my_subject, 'STREAMCODE');
			$yearcode_loop     = valueof($my_subject, 'YEARCODE');

			$years[$yearcode_loop]       = $yearcode_loop;
			$streams[$streamcode_loop]   = $streamcode_loop;
			$subjects[$subjectcode_loop] = valueof($subjects_array, $subjectcode_loop);

		 }
		}


       $yearcode     = isset($_SESSION['marks']['yearcode']) ? $_SESSION['marks']['yearcode'] : $school->active_yearcode;
       $termcode     = isset($_SESSION['marks']['termcode']) ? $_SESSION['marks']['termcode'] : $school->active_termcode;
       $streamcode   = isset($_SESSION['marks']['streamcode']) ? $_SESSION['marks']['streamcode'] : null;
       $subjectcode  = isset($_SESSION['marks']['subjectcode']) ? $_SESSION['marks']['subjectcode'] : null;
       $subjects     = isset($_SESSION['marks']['subjectcode']) ? array($subjectcode=>$subjectcode) : array();

       if( isset($_SESSION['marks']['yearcode']) && isset($_SESSION['marks']['streamcode']) ){

       $streams   =  array();
       $records   =  $db->GetArray("
		SELECT distinct STREAMCODE FROM SATSA
		WHERE TCHNO='{$user->userid}'
		AND YEARCODE='{$yearcode}'
		ORDER BY STREAMCODE ASC
       ");

	    if($records){
	  	 if(count($records)>0){
	  	  foreach ($records as $record){
	  	  	$streamcode_loop           = valueof($record,'STREAMCODE');
	  	  	$streams[$streamcode_loop] = $streamcode_loop;
	  	  }
	  	 }
	    }

       }

	?>
	<form id="ff<?php echo MNUID; ?>" method="post" onsubmit="return false;">
	  <table cellpadding="2" cellspacing="0" width="100%">

	  <tr>
			<td width="180px">Year:</td>
			<td>
			 <?php echo ui::form_select_fromArray('yearcode', $years, $yearcode," onchange=\"{$cfg['appname']}.list_streams();\" ");  ?>
			</td>
		</tr>

		<tr>
			<td>Term:</td>
			<td>
			<?php echo ui::form_select_fromArray('termcode', $terms, $termcode,"onchange=\"{$cfg['appname']}.list_streams();\"");  ?>
			</td>
		</tr>

		<tr>
			<td >Stream</td>
			<td>
			 <div id="div_streams<?php echo MNUID; ?>">
			 <?php echo ui::form_select_fromArray('streamcode', $streams, $streamcode,"onchange=\"{$cfg['appname']}.list_subjects();\"");  ?>
			 </div>
			</td>
		</tr>

		<tr>
			<td>Subject:</td>
			<td>
			   <div id="div_subjects<?php echo MNUID; ?>">
				<?php echo ui::form_select_fromArray('subjectcode', $subjects, $subjectcode);  ?>
				</div>
			</td>
		</tr>

		<tr>
			<td>&nbsp;</td>
			<td>
			 <button onclick="<?php echo $cfg['appname']; ?>.subject_setup();return false;">Open Subject</button>
			</td>
		</tr>

	 </table>
	</form>
		<?php
	}

	public function list_streams(){
		global $db,$cfg;

	 $subjectcode  =  filter_input(INPUT_POST , ui::fi('subjectcode'));
     $streamcode   =  filter_input(INPUT_POST , ui::fi('streamcode'));
     $yearcode     =  filter_input(INPUT_POST , ui::fi('yearcode'));
     $termcode     =  filter_input(INPUT_POST , ui::fi('termcode'));

	 $streams_array = array();
     $user          = new user();

     $records   =  $db->GetArray("
		SELECT distinct STREAMCODE FROM SATSA
		WHERE TCHNO='{$user->userid}'
		AND YEARCODE='{$yearcode}'
		ORDER BY STREAMCODE ASC
     ");

	  if($records){
	  	if(count($records)>0){
	  	  foreach ($records as $record){
	  	  	$streamcode  = valueof($record,'STREAMCODE');
	  	  	$streams_array[$streamcode] = $streamcode;
	  	  }
	  	}
	  }

	  return ui::form_select_fromArray('streamcode',$streams_array, ''," onchange=\"{$cfg['appname']}.list_subjects();\" ");

	}

	public function list_subjects(){
		global $db;
     $streamcode   =  filter_input(INPUT_POST , ui::fi('streamcode'));
     $yearcode     =  filter_input(INPUT_POST , ui::fi('yearcode'));
     $termcode     =  filter_input(INPUT_POST , ui::fi('termcode'));

     $user         =  new user();

     $subjects      =  $db->GetArray("
		SELECT DISTINCT SUBJECTCODE FROM SATSA
		WHERE TCHNO='{$user->userid}'
		AND YEARCODE='{$yearcode}'
		AND STREAMCODE='{$streamcode}'
		ORDER BY SUBJECTCODE ASC
     ");

	  $subjects_array = array();

	  if($subjects){
	  	if(count($subjects)>0){
	  	  foreach ($subjects as $subject){
	  	  	$subjectcode  = valueof($subject,'SUBJECTCODE');
	  	  	$subjects_array[$subjectcode] = $subjectcode;
	  	  }
	  	}
	  }

	  return ui::form_select_fromArray('subjectcode',$subjects_array, ''," onchange=\"\" ");

	}

	public function preload(){
		global $db,$cfg;

	   $user         =  new user();

	   $subjectcode  =  filter_input(INPUT_POST ,('subjectcode'));
       $streamcode   = filter_input(INPUT_POST , ('streamcode'));
	   $yearcode     = filter_input(INPUT_POST , ('yearcode'));
	   $termcode     = filter_input(INPUT_POST , ('termcode'));

       $formcode     =  $db->GetOne("SELECT FORMCODE FROM SASTREAMS WHERE STREAMCODE='{$streamcode}'");
       $gsyscode     =  self::get_grading_system($formcode, $yearcode, $termcode);
       $setup        =  self::get_set_up($subjectcode, $streamcode, $yearcode, $termcode);

       $count_cats    = 0;
       $count_exams   = 0;

       $_SESSION['marks'] = array();

       for ($c=1;$c<=5;++$c){
        if(isset($setup["CATMAX{$c}"])){
      	 if($setup["CATMAX{$c}"]>0){
      		$_SESSION['marks']["catmax{$c}"]   = $setup["CATMAX{$c}"];
      		++$count_cats;
      	 }
        }
       }

     for ($x=1;$x<=5;++$x){
      if(isset($setup["EXAMMAX{$x}"])){
      	if($setup["EXAMMAX{$x}"]>0){
      		$_SESSION['marks']["exammax{$x}"]   = $setup["EXAMMAX{$x}"];
      		++$count_exams;
      	}
      }
     }

       $_SESSION['marks']['subjectcode']  = $subjectcode;
       $_SESSION['marks']['formcode']     = $formcode;
       $_SESSION['marks']['gsyscode']     = $gsyscode;
       $_SESSION['marks']['streamcode']   = $streamcode;
       $_SESSION['marks']['yearcode']     = $yearcode;
       $_SESSION['marks']['termcode']     = $termcode;
       $_SESSION['marks']['count_cats']   = $count_cats;
       $_SESSION['marks']['count_exams']  = $count_exams;
       $_SESSION['marks']['contc']        = valueof($setup, 'CONTC');
       $_SESSION['marks']['contct']        = valueof($setup, 'CONTCT');
       $_SESSION['marks']['conte']        = valueof($setup, 'CONTE');

	}

	public function data(){
		global $db,$cfg;

	$this->page         = filter_input(INPUT_GET , 'page' , FILTER_VALIDATE_INT);
	$this->rows         = filter_input(INPUT_GET , 'rows' , FILTER_VALIDATE_INT);
	$this->id           = filter_input(INPUT_GET , 'id');
	$_sortcol           = filter_input(INPUT_GET , 'sortcol');
	$_sortorder         = filter_input(INPUT_GET , 'sortorder');

	$sortcol            = !empty($_sortcol)	? strtoupper($_sortcol) : 'ADMNO';
	$sortorder          = !empty($_sortorder)	? strtoupper($_sortorder) : 'ASC';

    $sort_asc_col1  = $_sortcol=='admno' && $_sortorder=='desc' ? 'active-sort' : 'inactive-sort';
    $sort_desc_col1 = $_sortcol=='admno' && $_sortorder=='asc' ? 'active-sort' : 'inactive-sort';

    $sort_asc_col2  = $_sortcol=='fullname' && $_sortorder=='desc' ? 'active-sort' : 'inactive-sort';
    $sort_desc_col2 = $_sortcol=='fullname' && $_sortorder=='asc' ? 'active-sort' : 'inactive-sort';


	$page    =  $this->page>0 ? $this->page : 1;
	$rows    =  $this->rows>0 ? $this->rows : 10;
	$offset  = ($page-1)*$rows;
	$count_start    = ($page*$rows);

    $subjectcode  =  isset($_SESSION['marks']['subjectcode']) ? $_SESSION['marks']['subjectcode'] : null;
    $formcode     =  isset($_SESSION['marks']['formcode']) ? $_SESSION['marks']['formcode'] : null;
    $streamcode   =  isset($_SESSION['marks']['streamcode']) ? $_SESSION['marks']['streamcode'] : null;
    $yearcode     =  isset($_SESSION['marks']['yearcode']) ? $_SESSION['marks']['yearcode'] : null;
    $termcode     =  isset($_SESSION['marks']['termcode']) ? $_SESSION['marks']['termcode'] : null;

    $data_sql   = ("
select ID, ADMNO, FULLNAME ,CAT1,CAT2,CAT3,CAT4,CAT5,AVGCAT,PAPER1,PAPER2,PAPER3,PAPER4,PAPER5,AVGEXAM,TOTAL,GRADECODE,POINTS,STATUSCODE
from VIEWSTUDENTSUBJECTS
where YEARCODE='{$yearcode}'
AND TERMCODE='{$termcode}'
AND FORMCODE='{$formcode}'
AND STREAMCODE='{$streamcode}'
AND SUBJECTCODE='{$subjectcode}'
ORDER BY  {$sortcol} {$sortorder}
");
     $data = $db->SelectLimit($data_sql,$rows, $offset);
     $students  = array();

     if ($data) {
	  $numRecords =  $data->RecordCount();
      if ( $numRecords>0) {
      	while (!$data->EOF){
      	 $students[] = $data->fields;
      	 $data->MoveNext();
      	}
      }
     }

   $setup = $db->GetRow("
       SELECT * FROM SATSS
        WHERE SUBJECTCODE='{$subjectcode}'
        AND STREAMCODE='{$streamcode}'
        AND YEARCODE='{$yearcode}'
        AND TERMCODE='{$termcode}'
       ");

     $subject_config = array();

     for ($c=1;$c<=5;++$c){
      if(isset($setup["CATMAX{$c}"])){
      	if($setup["CATMAX{$c}"]>0){
      		$subject_config['cats'][$c]['max']   = $setup["CATMAX{$c}"];
            $subject_config['cats'][$c]['name']  = isset($setup["CATNAME{$c}"]) && !empty($setup["CATNAME{$c}"]) ? $setup["CATNAME{$c}"] : "CAT {$c}";
      	}
      }
     }

     for ($x=1;$x<=5;++$x){
      if(isset($setup["EXAMMAX{$x}"])){
      	if($setup["EXAMMAX{$x}"]>0){
      		$subject_config['papers'][$x]['max']   = $setup["EXAMMAX{$x}"];
            $subject_config['papers'][$x]['name']  = isset($setup["EXAMNAME{$x}"]) && !empty($setup["EXAMNAME{$x}"]) ? $setup["EXAMNAME{$x}"] : "Paper {$x}";
      	}
      }
     }

     $table_id =  ui::fi('tblMarks');

     echo '<table id="'.$table_id.'" cellpadding="0" cellspacing="0" width="100%" border="0" class="datagrid-htable datagrid-btable datagrid-ftable">';
      echo '<thead>';

       echo '<tr>';
        echo '
        <td  class="panel-header" colspan="3">
        <a  href="javascript:void(0);"  class="marks-menu"  onclick="'.$cfg['appname'].'.load_stage();"><i class="fa fa-edit"></i> Open</a> |
        <a  href="javascript:void(0);"  class="marks-menu"  onclick="'.$cfg['appname'].'.subject_resetup(\''.$subjectcode.'\', \''.$streamcode.'\', \''.$yearcode.'\', \''.$termcode.'\', 1);"><i class="fa fa-cog"></i> Subject Setup</a> |
        <a  href="javascript:void(0);"  class="marks-menu"  onclick="'.$cfg['appname'].'.print_subject();"><i class="fa fa-print"></i> Print</a>
        </td>
        ';

        if(isset($subject_config['cats']) && sizeof($subject_config['cats'])>0){
        	foreach ($subject_config['cats'] as $cat_index => $cat){
        		$name = valueof($cat,'name','','Camelize');
        		$max  = valueof($cat,'max');
               echo "<td  class=\"panel-header\">{$name}</td>";
        	}
        	 echo "<td  class=\"panel-header\">&nbsp;</td>";
        }

        if(isset($subject_config['papers']) && sizeof($subject_config['papers'])>0){
        	foreach ($subject_config['papers'] as $cat_index => $paper){
        		$name = valueof($paper,'name','','Camelize');
        		$max  = valueof($paper,'max');
               echo "<td  class=\"panel-header\">{$name}</td>";
        	}
        }


       echo "<td class=\"panel-header\" colspan=\"4\">&nbsp;</td>";
       echo "</tr>";

       echo '<tr>';
        echo '<td  class="header left right">No</td>';

        echo '<td  class="header right">
             <a  href="javascript:void(0);" class="'.$sort_asc_col1.'" title="Sort by No. Descending"  class="marks-menu"  onclick="'.$cfg['appname'].'.pagesort(\'admno\',\'desc\','.$page.');"><i class="fa fa-chevron-down"></i></a>
             Adm.No
             <a  href="javascript:void(0);" class="'.$sort_desc_col1.'"  title="Sort by No. Ascending"   class="marks-menu"  onclick="'.$cfg['appname'].'.pagesort(\'admno\',\'asc\','.$page.');"><i class="fa fa-chevron-up"></i></a>
        </td>';

        echo '<td  class="header right">
             <a  href="javascript:void(0);" class="'.$sort_asc_col2.'" title="Sort by Name Descending"  class="marks-menu"  onclick="'.$cfg['appname'].'.pagesort(\'fullname\',\'desc\','.$page.');"><i class="fa fa-chevron-down"></i></a>
             Name
             <a  href="javascript:void(0);" class="'.$sort_desc_col2.'" title="Sort by Name Ascending"   class="marks-menu"  onclick="'.$cfg['appname'].'.pagesort(\'fullname\',\'asc\','.$page.');"><i class="fa fa-chevron-up"></i></a>
        </td>';

        if(isset($subject_config['cats']) && sizeof($subject_config['cats'])>0){
        	foreach ($subject_config['cats'] as $cat_index => $cat){
        		$name = valueof($cat,'name');
        		$max  = valueof($cat,'max');
               echo "<td  class=\"header right\"><b>{$max}</b></td>";
        	}
        	 echo "<td  class=\"header right\">CT.Avg</td>";
        }

        if(isset($subject_config['papers']) && sizeof($subject_config['papers'])>0){
        	foreach ($subject_config['papers'] as $cat_index => $paper){
        		$name = valueof($paper,'name');
        		$max  = valueof($paper,'max');
               echo "<td  class=\"header right\"><b>{$max}</b></td>";
        	}
        	echo "<td  class=\"header right\">PP.Avg</td>";
        }

       echo "<td class=\"header right\">Total</td>";
       echo "<td class=\"header right\">Grade</td>";
       echo "<td class=\"header right\">Comment</td>";
       echo "</tr>";

      echo '</thead>';

      if(count($students)>0){
       echo '<tbody>';

       if($page==1){
        $count = 1;
       }else{
        $count = $offset+1;
       }

       foreach ($students as $student){

       	$id          = valueof($student,'ID');
       	$admno       = valueof($student,'ADMNO');
       	$fullname    = valueof($student,'FULLNAME','**missing**');

       	$cat1        = valueof($student,'CAT1');
       	$cat1        = round($cat1,0);
       	$cat2        = valueof($student,'CAT2');
       	$cat3        = valueof($student,'CAT3');
       	$cat4        = valueof($student,'CAT4');
       	$cat5        = valueof($student,'CAT5');
       	$avgcat      = valueof($student,'AVGCAT');

       	$paper1      = valueof($student,'PAPER1');
       	$paper2      = valueof($student,'PAPER2');
       	$paper3      = valueof($student,'PAPER3');
       	$paper4      = valueof($student,'PAPER4');
       	$paper5      = valueof($student,'PAPER5');
       	$avgexam     = valueof($student,'AVGEXAM');
       	$total       = valueof($student,'TOTAL');
       	$gradecode   = valueof($student,'GRADECODE');

       	$divAvgCat     = "divavgcat_{$id}";
       	$divAvgExam    = "divavgexam_{$id}";
       	$divGradeCode  = "divgrade_{$id}";
       	$divTotal      = "divtotal_{$id}";
       	$divStatus     = "divstatus_{$id}";

       	$link_comment = ui::href("{$cfg['appname']}.subject_comment({$id},'{$subjectcode}','{$yearcode}','{$termcode}');",'<i class="fa fa-commenting-o"></i> ',"Comments for ".$fullname,'comments'.$fullname,'comments','comments');

        echo '<tr>';
         echo "<td class=\"input left\" >{$count}</td>";
         echo "<td class=\"input\" >{$admno}</td>";
         echo "<td class=\"input\" >{$fullname}</td>";

         if(isset($subject_config['cats']) && sizeof($subject_config['cats'])>0){
         foreach ($subject_config['cats'] as $cat_index => $cat){

          $value         = valueof($student,"CAT{$cat_index}");
          $value         = round($value,1);
          $name          = valueof($cat,'name');
          $max           = valueof($cat,'max');
       	  $textbox_catid = "cat{$cat_index}_{$id}";
       	  $column        = "cat{$cat_index}";

       	  $textbox_cat   = "<input type=\"text\" value=\"{$value}\" id=\"{$textbox_catid}\" size=\"5\" onfocus=\"\" onblur=\"{$cfg['appname']}.save({$id},'{$column}','{$textbox_catid}','{$max}','{$value}','{$name}','{$divAvgCat}','{$divAvgExam}','{$divTotal}','{$divGradeCode}','{$divStatus}');\"  />";

          echo "<td class=\"input\"  >{$textbox_cat}</td>";

          }
         }

         echo "<td class=\"input\" ><div id=\"{$divAvgCat}\">{$avgcat}</div></td>";

         if(isset($subject_config['papers']) && sizeof($subject_config['papers'])>0){
         foreach ($subject_config['papers'] as $paper_index => $paper){

          $value            = valueof($student,"PAPER{$paper_index}");
          $value            = round($value,1);
          $name             = valueof($paper,'name');
          $max              = valueof($paper,'max');
       	  $textbox_paperid  = "paper{$paper_index}_{$id}";
       	  $column           = "paper{$paper_index}";

       	  $textbox_paper    = "<input type=\"text\" value=\"{$value}\" id=\"{$textbox_paperid}\" size=\"5\" onfocus=\"\" onblur=\"{$cfg['appname']}.save({$id},'{$column}','{$textbox_paperid}','{$max}','{$value}','{$name}','{$divAvgCat}','{$divAvgExam}','{$divTotal}','{$divGradeCode}','{$divStatus}');\"   />";

          echo "<td class=\"input\" >{$textbox_paper}</td>";

          }
         }

         echo "<td class=\"input\" ><div id=\"{$divAvgExam}\">{$avgexam}</div></td>";
         echo "<td class=\"input\" ><div id=\"{$divTotal}\">{$total}</div></td>";
         echo "<td class=\"input\" ><div id=\"{$divGradeCode}\">{$gradecode}</div></td>";
         echo "<td class=\"input\" >{$link_comment}</td>";
        echo '</tr>';

        ++$count;
       }

       echo '</tbody>';
      }

     echo '</table>';


      $data_pager_sql   = ("
select count(ADMNO) NUM
from VIEWSTUDENTSUBJECTS
where YEARCODE='{$yearcode}'
AND TERMCODE='{$termcode}'
AND FORMCODE='{$formcode}'
AND STREAMCODE='{$streamcode}'
AND SUBJECTCODE='{$subjectcode}'
");
     $numRecords = $db->GetOne($data_pager_sql);

      echo '<table width="100%" cellpadding="2" cellspacing="0" border="0">';
	   echo '<tr>';
	   echo '<td>';
	   echo '<ul id="pagination">';

				$pages = $numRecords/$this->rows;
				$pages = ceil($pages);

				for($i=1; $i<=$pages; $i++){
					$li_color = $i==$page ? "#000" : "#0063DC";
					$li_class = $i==$page ? "page-active" : "#page-inactive";
					echo "<li class=\"nav {$li_class}\" id=\"li_{$i}\" style=\"color:{$li_color}\" onclick=\"{$cfg['appname']}.paginate(this.id,{$i},'{$_sortcol}','{$_sortorder}');\">Page {$i}</li>";
				}

	   echo '</ul>';
	  echo '</td>';
	 echo '</tr>';
	echo '</table>';

     echo "
     <script>
$('#{$table_id}').tableNav();
$('#{$table_id} input').eq(1).click();
</script>
";
	}

	private function get_grading_system($formcode, $yearcode, $termcode){
		global $db;

	  return $db->GetOne("
       SELECT GSYSCODE FROM SATSSG
        WHERE FORMCODE='{$formcode}'
        AND YEARCODE='{$yearcode}'
        AND TERMCODE='{$termcode}'
       ");

	}

	private function get_global_set_up($formcode, $yearcode, $termcode){
		global $db;

	  $global_setup = $db->GetRow("
       SELECT * FROM SATSSG
        WHERE FORMCODE='{$formcode}'
        AND YEARCODE='{$yearcode}'
        AND TERMCODE='{$termcode}'
       ");

	  return $global_setup;
	}

	private function get_set_up($subjectcode,$streamcode,$yearcode,$termcode){
		global $db;

	  $setup = $db->GetRow("
       SELECT * FROM SATSS
        WHERE SUBJECTCODE='{$subjectcode}'
        AND STREAMCODE='{$streamcode}'
        AND YEARCODE='{$yearcode}'
        AND TERMCODE='{$termcode}'
       ");

	  return $setup;
	}

	public  function subject_setup() {
		global $db,$cfg;

	   $subjectcode  =  filter_input(INPUT_POST , ui::fi('subjectcode'));
       $streamcode   =  filter_input(INPUT_POST , ui::fi('streamcode'));
       $yearcode     =  filter_input(INPUT_POST , ui::fi('yearcode'));
       $termcode     =  filter_input(INPUT_POST , ui::fi('termcode'));

       return self::call_setup($subjectcode, $streamcode, $yearcode, $termcode);
    }

	public  function subject_resetup() {
		global $db,$cfg;

	   $subjectcode  =  filter_input(INPUT_POST , ('subjectcode'));
       $streamcode   =  filter_input(INPUT_POST , ('streamcode'));
       $yearcode     =  filter_input(INPUT_POST , ('yearcode'));
       $termcode     =  filter_input(INPUT_POST , ('termcode'));

	   return self::call_setup($subjectcode, $streamcode, $yearcode, $termcode);
    }

	public  function call_setup($subjectcode, $streamcode, $yearcode, $termcode) {
		global $db,$cfg;

       $formcode     =  $db->CacheGetOne(1200,"SELECT FORMCODE FROM SASTREAMS WHERE STREAMCODE='{$streamcode}'");

       $setup        = self::get_set_up($subjectcode, $streamcode, $yearcode, $termcode);
       $global_setup = self::get_global_set_up($formcode, $yearcode, $termcode);

       $setup_contc = valueof($setup,'CONTC');
       $setup_conte = valueof($setup,'CONTE');

       $global_setup_contc = valueof($global_setup,'CONTC');
       $global_setup_conte = valueof($global_setup,'CONTE');

       $saved = sizeof($setup)>0 ? true : false;

       if( ($global_setup_contc!=$setup_contc) || ($global_setup_conte!=$setup_conte) ){
		$setup['CONTC'] = $global_setup_contc;
		$setup['CONTE'] = $global_setup_conte;
		$saved =  false;
	   }

       if(!$saved){
       	 $btn_enter_marks_status     = 'disabled';
       	 $enable_enter_marks         = 1;
       }else{
       	 $btn_enter_marks_status     = '';
       	 $enable_enter_marks         = 0;
       }

       if( !isset($setup['YEARCODE']) || (isset($setup['YEARCODE']) && empty($setup['YEARCODE'])) ){
       	$setup = $global_setup;
       }

       if(empty($setup)){
       	 if($termcode>1){//pick previous term settings
       	 	for ($s=3;$s>=1;--$s){
       	 		$setup = self::get_set_up($subjectcode, $streamcode, $yearcode, $s);
       	 		if(!empty($setup)){
       	 			break;
       	 		}
       	 	}
       	 }else{//pick previous year term 3 settings
       	 	$yearcode_check  = ($yearcode-1);
       	 	$termcode_check  = 3;
       	 	$setup = self::get_set_up($subjectcode, $streamcode, $yearcode_check, $termcode_check);
       	 }
       }// give up & user enters new subject setup

       //subject cols
       $subjectcols = $db->GetRow("SELECT NUMCATS,NUMEXAMS FROM SAXPF WHERE SUBJECTCODE='{$subjectcode}' AND FORMCODE='{$formcode}'");
       $cats        = valueof($subjectcols,'NUMCATS',3);
       $exams       = valueof($subjectcols,'NUMEXAMS',3);
       $all         = $cats+$exams;

       echo '<form id="frmSubSetup'.MNUID.'" name="frmSubSetup"  method="POST"  enctype="multipart/form-data" onsubmit="return false;"  > ';
       echo "<table class=\"tblSubSetup\" width=\"100%\" border=\"0\" cellpadding=\"0\"  cellspacing=\"0\"  >";
        echo "<tr>";

         echo "<td class=\"header label\">&nbsp;</td>";

        for ($c=1;$c<=$cats;++$c){
         echo "<td class=\"header cat\">Cat{$c}</td>";
        }

        for ($c=1;$c<=$exams;++$c){
         echo "<td class=\"header exam\" style=\"padding:2px;\">Exam{$c}</td>";
        }

        echo "</tr>";

        echo "<tr>";

        //-----------------------------------max
        echo "<td  class=\"left label input\">Out of </td>";

        for ($c=1;$c<=$cats;++$c){
         $input_id    = "cat[{$c}]";
         $input_value = valueof($setup,"CATMAX{$c}");
         echo "<td  class=\"input cat\" style=\"padding:2px;\"><input  class=\"easyui-numberspinner\" type=\"text\" id=\"{$input_id}\"  name=\"{$input_id}\" style=\"width:60px;\" value=\"{$input_value}\" ></input></td>";
        }

        for ($c=1;$c<=$exams;++$c){
          $input_id = "exam[{$c}]";
          $input_value = valueof($setup,"EXAMMAX{$c}");
         echo "<td class=\"input exam\" style=\"padding:2px;\"><input  class=\"easyui-numberspinner\" type=\"text\" id=\"{$input_id}\"  name=\"{$input_id}\" style=\"width:60px;\" value=\"{$input_value}\" ></input></td>";
        }

        echo "</tr>";

        //-----------------------------------title
        echo "<td  class=\"left label input\">Title</td>";

        for ($c=1;$c<=$cats;++$c){
         $input_id = "title_cat[{$c}]";
         $input_value = valueof($setup,"CATNAME{$c}");
         echo "<td class=\"input cat\" style=\"padding:2px;\"><input  class=\"easyui-numberspinner\" type=\"text\" id=\"{$input_id}\"  name=\"{$input_id}\" style=\"width:65px;\" value=\"{$input_value}\" ></input></td>";
        }

        for ($c=1;$c<=$exams;++$c){
          $input_id = "title_exam[{$c}]";
          $input_value = valueof($setup,"EXAMNAME{$c}");
         echo "<td class=\"input exam\" style=\"padding:2px;\"><input  class=\"easyui-numberspinner\" type=\"text\" id=\"{$input_id}\"  name=\"{$input_id}\" style=\"width:65px;\" value=\"{$input_value}\" ></input></td>";
        }

        echo "</tr>";


       echo "<tr>";
        echo "<td  class=\"left label input\">Contributions</td>";
        echo "<td  class=\"input cat\">".ui::form_input('hidden','contc',0, valueof($setup, 'CONTC')).valueof($setup, 'CONTC')."%</td>";
        echo "<td colspan='2' class=\"input cat\">".ui::form_input('hidden','contct',0, valueof($setup, 'CONTCT')).valueof($setup, 'CONTCT')."%</td>";
        echo "<td colspan=\"{$exams}\" class=\"input exam\">".ui::form_input('hidden','conte',0, valueof($setup, 'CONTE')).valueof($setup, 'CONTE')."%</td>";
       echo "</tr>";

       echo "<tr>";
        echo "<td>&nbsp;</td>";
        echo "<td colspan=\"{$all}\" >&nbsp;</td>";
       echo "</tr>";

       echo "<tr>";
        echo "<td>&nbsp;</td>";
        echo "<td colspan=\"{$all}\" >
        <button type=\"submit\" id=\"btnSave\" onclick=\"{$cfg['appname']}.save_subject_setup('{$subjectcode}', '{$streamcode}', '{$yearcode}','{$termcode}',{$enable_enter_marks});\">Save Setup</button>
        ";

        echo " or <button type=\"submit\" id=\"btnEntry\" onclick=\"{$cfg['appname']}.preload_subject('{$subjectcode}', '{$streamcode}', '{$yearcode}','{$termcode}');\" {$btn_enter_marks_status}>Enter Marks</button>";

        echo "</td>";
       echo "</tr>";

       echo "</table>";
       echo "</form>";

	}

	public function save_subject_setup(){
      global $db;

	   $user         =  new user();
       $subjectcode  =  filter_input(INPUT_POST , ('subjectcode'));
       $streamcode   =  filter_input(INPUT_POST , ('streamcode'));
       $yearcode     =  filter_input(INPUT_POST , ('yearcode'));
       $termcode     =  filter_input(INPUT_POST , ('termcode'));

       $record = new ADODB_Active_Record('SATSS', array('SUBJECTCODE','STREAMCODE','YEARCODE','TERMCODE'));

       $record->Load("
        SUBJECTCODE='{$subjectcode}'
        AND STREAMCODE='{$streamcode}'
        AND YEARCODE='{$yearcode}'
        AND TERMCODE='{$termcode}'
       ");

       if(empty($record->_original)){
       	$record->id	          = generateID($record->_table);
       	$record->tchno	      = $user->userid;
       	$record->subjectcode  = $subjectcode;
       	$record->streamcode	  = $streamcode;
       	$record->yearcode	  = $yearcode;
       	$record->termcode	  = $termcode;
       }

        $record->catmax1	  = isset($_POST['cat'][1]) && is_numeric($_POST['cat'][1]) ? $_POST['cat'][1] : null;
        $record->catmax2	  = isset($_POST['cat'][2]) && is_numeric($_POST['cat'][2]) ? $_POST['cat'][2] : null;
        $record->catmax3	  = isset($_POST['cat'][3]) && is_numeric($_POST['cat'][3]) ? $_POST['cat'][3] : null;
        $record->catmax4	  = isset($_POST['cat'][4]) && is_numeric($_POST['cat'][4]) ? $_POST['cat'][4] : null;
        $record->catmax5	  = isset($_POST['cat'][5]) && is_numeric($_POST['cat'][5]) ? $_POST['cat'][5] : null;
        $record->contc   	  = filter_input(INPUT_POST , ui::fi('contc'));

        $record->exammax1	  = isset($_POST['exam'][1]) && is_numeric($_POST['exam'][1]) ? $_POST['exam'][1] : null;
        $record->exammax2	  = isset($_POST['exam'][2]) && is_numeric($_POST['exam'][2]) ? $_POST['exam'][2] : null;
        $record->exammax3	  = isset($_POST['exam'][3]) && is_numeric($_POST['exam'][3]) ? $_POST['exam'][3] : null;
        $record->exammax4	  = isset($_POST['exam'][4]) && is_numeric($_POST['exam'][4]) ? $_POST['exam'][4] : null;
        $record->exammax5	  = isset($_POST['exam'][5]) && is_numeric($_POST['exam'][5]) ? $_POST['exam'][5] : null;
        $record->conte   	  = filter_input(INPUT_POST ,  ui::fi('conte'));

        $record->catname1	  = isset($_POST['title_cat'][1]) && is_string($_POST['title_cat'][1]) && $record->catmax1>0 ? $_POST['title_cat'][1] : null;
        $record->catname2	  = isset($_POST['title_cat'][2]) && is_string($_POST['title_cat'][2]) && $record->catmax2>0? $_POST['title_cat'][2] : null;
        $record->catname3	  = isset($_POST['title_cat'][3]) && is_string($_POST['title_cat'][3]) && $record->catmax3>0 ? $_POST['title_cat'][3] : null;
        $record->catname4	  = isset($_POST['title_cat'][4]) && is_string($_POST['title_cat'][4]) && $record->catmax4>0 ? $_POST['title_cat'][4] : null;
        $record->catname5	  = isset($_POST['title_cat'][5]) && is_string($_POST['title_cat'][5]) && $record->catmax5>0 ? $_POST['title_cat'][5] : null;

        $record->examname1	  = isset($_POST['title_exam'][1]) && is_string($_POST['title_exam'][1]) && $record->exammax1>0  ? $_POST['title_exam'][1] : null;
        $record->examname2	  = isset($_POST['title_exam'][2]) && is_string($_POST['title_exam'][2]) && $record->exammax2>0  ? $_POST['title_exam'][2] : null;
        $record->examname3	  = isset($_POST['title_exam'][3]) && is_string($_POST['title_exam'][3])  && $record->exammax3>0 ? $_POST['title_exam'][3] : null;

        $record->examname4	  = isset($_POST['title_exam'][4]) && is_string($_POST['title_exam'][4]) && $record->exammax4>0  ? $_POST['title_exam'][4] : null;
        $record->examname5	  = isset($_POST['title_exam'][5]) && is_string($_POST['title_exam'][5]) && $record->exammax5>0  ? $_POST['title_exam'][5] : null;

        $record->catmute1	  = isset($_POST['silent_cat'][1]) && is_string($_POST['silent_cat'][1]) && $record->catmax1>0 ? $_POST['silent_cat'][1] : null;
        $record->catmute2	  = isset($_POST['silent_cat'][2]) && is_string($_POST['silent_cat'][2]) && $record->catmax2>0? $_POST['silent_cat'][2] : null;
        $record->catmute3	  = isset($_POST['silent_cat'][3]) && is_string($_POST['silent_cat'][3]) && $record->catmax3>0 ? $_POST['silent_cat'][3] : null;
        $record->catmute4	  = isset($_POST['silent_cat'][4]) && is_string($_POST['silent_cat'][4]) && $record->catmax4>0 ? $_POST['silent_cat'][4] : null;
        $record->catmute5	  = isset($_POST['silent_cat'][5]) && is_string($_POST['silent_cat'][5]) && $record->catmax5>0 ? $_POST['silent_cat'][5] : null;

        $record->exammute1	  = isset($_POST['silent_exam'][1]) && is_string($_POST['silent_exam'][1]) && $record->exammax1>0  ? $_POST['silent_exam'][1] : null;
        $record->exammute2	  = isset($_POST['silent_exam'][2]) && is_string($_POST['silent_exam'][2]) && $record->exammax2>0  ? $_POST['silent_exam'][2] : null;
        $record->exammute3	  = isset($_POST['silent_exam'][3]) && is_string($_POST['silent_exam'][3])  && $record->exammax3>0  ? $_POST['silent_exam'][3] : null;
        $record->exammute4	  = isset($_POST['silent_exam'][4]) && is_string($_POST['silent_exam'][4]) && $record->exammax4>0  ? $_POST['silent_exam'][4] : null;
        $record->exammute5	  = isset($_POST['silent_exam'][5]) && is_string($_POST['silent_exam'][5]) && $record->exammax5>0  ? $_POST['silent_exam'][5] : null;

        $record->audituser	  = $user->userid;
        $record->auditdate	  = date('Y-m-d');
        $record->audittime	  = time();

      if( $record->Save() ){
         return json_response(1,"{$subjectcode} for {$streamcode} {$yearcode} saved");
       }else{
       	 $error = @$db->ErrorMsg();
         return json_response(0,"save failed : {$error}");
       }

	}

	public function save(){
        global $db, $cfg;

        $id = filter_input(INPUT_POST, 'id');
        $max = filter_input(INPUT_POST, 'max');
        $score = filter_input(INPUT_POST, 'score');
        $column = filter_input(INPUT_POST, 'column');
        $user = new user();

        $count_cats = isset($_SESSION['marks']['count_cats']) ? $_SESSION['marks']['count_cats'] : null;
        $count_exams = isset($_SESSION['marks']['count_exams']) ? $_SESSION['marks']['count_exams'] : null;

        $streamcode = isset($_SESSION['marks']['streamcode']) ? $_SESSION['marks']['streamcode'] : null;
        $formcode = isset($_SESSION['marks']['formcode']) ? $_SESSION['marks']['formcode'] : null;
        $gradesyscode = isset($_SESSION['marks']['gsyscode']) ? $_SESSION['marks']['gsyscode'] : null;
        $yearcode = isset($_SESSION['marks']['yearcode']) ? $_SESSION['marks']['yearcode'] : null;
        $termcode = isset($_SESSION['marks']['termcode']) ? $_SESSION['marks']['termcode'] : null;
        $subjectcode = isset($_SESSION['marks']['subjectcode']) ? $_SESSION['marks']['subjectcode'] : null;

        $catmax1 = isset($_SESSION['marks']['catmax1']) ? $_SESSION['marks']['catmax1'] : null;
        $catmax2 = isset($_SESSION['marks']['catmax2']) ? $_SESSION['marks']['catmax2'] : null;
        $catmax3 = isset($_SESSION['marks']['catmax3']) ? $_SESSION['marks']['catmax3'] : null;
        $catmax4 = isset($_SESSION['marks']['catmax4']) ? $_SESSION['marks']['catmax4'] : null;
        $catmax5 = isset($_SESSION['marks']['catmax5']) ? $_SESSION['marks']['catmax5'] : null;

        $exammax1 = isset($_SESSION['marks']['exammax1']) ? $_SESSION['marks']['exammax1'] : null;
        $exammax2 = isset($_SESSION['marks']['exammax2']) ? $_SESSION['marks']['exammax2'] : null;
        $exammax3 = isset($_SESSION['marks']['exammax3']) ? $_SESSION['marks']['exammax3'] : null;
        $exammax4 = isset($_SESSION['marks']['exammax4']) ? $_SESSION['marks']['exammax4'] : null;
        $exammax5 = isset($_SESSION['marks']['exammax5']) ? $_SESSION['marks']['exammax5'] : null;

        $contc = isset($_SESSION['marks']['contc']) ? $_SESSION['marks']['contc'] : null;
        $contct = isset($_SESSION['marks']['contct']) ? $_SESSION['marks']['contct'] : null;
        $conte = isset($_SESSION['marks']['conte']) ? $_SESSION['marks']['conte'] : null;
        $cont = $contc + $conte + $contct;
        $special_subjects = array('CHM', 'PHY', 'BIO');
        $subject_is_special = false;

        if ($formcode == 3 || $formcode == 4) {
            if (array_search($subjectcode, $special_subjects) || in_array($subjectcode, $special_subjects)) {
                $subject_is_special = true;
            }
        }

        if (!isset($_SESSION['marks']) || (isset($_SESSION['marks']) && sizeof($_SESSION['marks']) == 0)) {

            return json_response(0, "Please Re Login",
                array(
                    'goto' => './?act=logout')
            );

        }

        if (empty($formcode)) {
            return json_response(0, " missing variable 'formcode'");
        }

        if (empty($gradesyscode)) {
            return json_response(0, " missing grading system for {$streamcode}");
        }

        if ($contc == 100 && $conte == 100 && $contct == 100) {

        } else {
            if ($cont != 100) {
                return json_response(0, "CAT & EXAM contributions should total 100");
            }
        }

        $record = new ADODB_Active_Record('SASTUDSUB', array('ID'));
        $record->Load("ID={$id}");

        if (empty($record->_original)) {
            return json_response(0, "save failed : record missing in database");
        }

        if (!empty($column)) {
            $record->$column = $score;
        }

        if (!empty($column)) {

            switch ($column) {
                case 'cat1':
                    $avg_this = ($score / $catmax1) * $contc;
                    $perc_this = ($avg_this / $contc) * 100;
                    break;
                case 'cat2':
                    // TODO: $contct is undefined
                    $avg_this = ($score / $catmax2) * $contct;
                    $perc_this = ($avg_this / $contct) * 100;
                    break;
                case 'cat3':
                    $avg_this = ($score / $catmax3) * $contc;
                    $perc_this = ($avg_this / $contc) * 100;
                    break;
                case 'cat4':
                    $avg_this = ($score / $catmax4) * $contc;
                    $perc_this = ($avg_this / $contc) * 100;
                    break;
                case 'cat5':
                    $avg_this = ($score / $catmax5) * $contc;
                    $perc_this = ($avg_this / $contc) * 100;
                    break;
                case 'paper1':
                    $avg_this = ($score / $exammax1) * $conte;
                    // TODO: Check if its divided by 100 or user supplied input
                    $perc_this = ($avg_this / $conte) * 100;
                    break;
                case 'paper2':
                    $avg_this = ($score / $exammax2) * $conte;
                    $perc_this = ($avg_this / $conte) * 100;
                    break;
                case 'paper3':
                    $avg_this = ($score / $exammax3) * $conte;
                    $perc_this = ($avg_this / $conte) * 100;
                    break;
                case 'paper4':
                    $avg_this = ($score / $exammax4) * $conte;
                    $perc_this = ($avg_this / $conte) * 100;
                    break;
                case 'paper5':
                    $avg_this = ($score / $exammax5) * $conte;
                    $perc_this = ($avg_this / $conte) * 100;
                    break;
            }

            $avg_this = round($avg_this, 2);
            $perc_this = round($perc_this, 2);

            $exam_raw_column = "{$column}r";
            $exam_per_column = "{$column}p";
            $gradecode_column = "{$column}gradecode";
            $points_column = "{$column}points";
            $comments_column = "{$column}comments";
            $record->$column = $score;

            $Get_Grade_Exam = self::Get_Grade($gradesyscode, $perc_this);

            if (is_array($Get_Grade_Exam)) {
                $record->$exam_raw_column = $avg_this;
                $record->$exam_per_column = $perc_this;
                $record->$gradecode_column = valueof($Get_Grade_Exam, 'GRADECODE');
                $record->$points_column = valueof($Get_Grade_Exam, 'POINTS');
            }

            $subject_Comments = self::Get_Comments($record->subjectcode, $record->$gradecode_column);
            $record->$comments_column = $subject_Comments;

        }

        if ($contc == 100 && $conte == 100 && $contct == 100) {

            if ($count_cats > 0) {
                $catcont1 = $record->cat1 > 0 && $catmax1 > 0 ? $record->cat1 : 0;
                $catcont2 = $record->cat2 > 0 && $catmax2 > 0 ? $record->cat2 : 0;
                $catcont3 = $record->cat3 > 0 && $catmax3 > 0 ? $record->cat3 : 0;
                $catcont4 = $record->cat4 > 0 && $catmax4 > 0 ? $record->cat4 : 0;
                $catcont5 = $record->cat5 > 0 && $catmax5 > 0 ? $record->cat5 : 0;
                $avgcat = ($catcont1 + $catcont2 + $catcont3 + $catcont4 + $catcont5) / $count_cats;
                $record->avgcat = round($avgcat, 0);
            }

            if ($count_exams > 0) {

                $examcont1 = $record->paper1 > 0 && $exammax1 > 0 ? $record->paper1 : 0;
                $examcont2 = $record->paper2 > 0 && $exammax2 > 0 ? $record->paper2 : 0;
                $examcont3 = $record->paper3 > 0 && $exammax3 > 0 ? $record->paper3 : 0;
                $examcont4 = $record->paper4 > 0 && $exammax4 > 0 ? $record->paper4 : 0;
                $examcont5 = $record->paper5 > 0 && $exammax5 > 0 ? $record->paper5 : 0;

                if (!$subject_is_special) {
                    $avgexam = ($examcont1 + $examcont2 + $examcont3 + $examcont4 + $examcont5) / $count_exams;
                    $record->avgexam = round($avgexam, 0);
                    $total = (($catcont1 + $catcont2 + $catcont3 + $catcont4 + $catcont5) + ($examcont1 + $examcont2 + $examcont3 + $examcont4 + $examcont5)) / ($count_cats + $count_exams);
                } else {
                    $exammaxs = ($exammax1 + $exammax2);
                    $examcont_avg = ($examcont1 + $examcont2) / ($exammax1 + $exammax2);
                    $kamaa = ($examcont_avg * 60) + $examcont3;
                    $njooro = $kamaa * ($conte / 100);
                    $avgexam = $njooro;
                    $record->avgexam = round($avgexam, 0);
                    $total = (($catcont1 + $catcont2 + $catcont3 + $catcont4 + $catcont5) + ($record->avgexam)) / ($count_cats + 1);

                }

            }

            $record->total = round($total);

        } else {//else not 100-100

            if ($count_cats > 0) {
                $catcont1 = $record->cat1 > 0 && $catmax1 > 0 ? $record->cat1 / $catmax1 * $contc : 0;
                $catcont2 = $record->cat2 > 0 && $catmax2 > 0 ? $record->cat2 / $catmax2 * $contct : 0;
                $catcont3 = $record->cat3 > 0 && $catmax3 > 0 ? $record->cat3 / $catmax3 * $contc : 0;
                $catcont4 = $record->cat4 > 0 && $catmax4 > 0 ? $record->cat4 / $catmax4 * $contc : 0;
                $catcont5 = $record->cat5 > 0 && $catmax5 > 0 ? $record->cat5 / $catmax5 * $contc : 0;
                $avgcat = ($catcont1 + $catcont2 + $catcont3 + $catcont4 + $catcont5) / $count_cats;
                $record->avgcat = round($avgcat, 0);
            }

            if ($count_exams > 0) {

                if ((!$subject_is_special) || ($count_exams == 1)) {
                    $examcont1 = $record->paper1 > 0 && $exammax1 > 0 ? $record->paper1 / $exammax1 * $conte : 0;
                    $examcont2 = $record->paper2 > 0 && $exammax2 > 0 ? $record->paper2 / $exammax2 * $conte : 0;
                    $examcont3 = $record->paper3 > 0 && $exammax3 > 0 ? $record->paper3 / $exammax3 * $conte : 0;
                    $examcont4 = $record->paper4 > 0 && $exammax4 > 0 ? $record->paper4 / $exammax4 * $conte : 0;
                    $examcont5 = $record->paper5 > 0 && $exammax5 > 0 ? $record->paper5 / $exammax5 * $conte : 0;

                    $avgexam = ($examcont1 + $examcont2 + $examcont3 + $examcont4 + $examcont5) / $count_exams;
                    $record->avgexam = round($avgexam, 0);
//	   $total            = $record->avgcat + $record->avgexam;
                    $total = $catcont1 + $catcont2 + $examcont1;

                } else {

                    $examcont1 = $record->paper1 > 0 && $exammax1 > 0 ? $record->paper1 : 0;
                    $examcont2 = $record->paper2 > 0 && $exammax2 > 0 ? $record->paper2 : 0;
                    $examcont3 = $record->paper3 > 0 && $exammax3 > 0 ? $record->paper3 : 0;
                    $examcont4 = $record->paper4 > 0 && $exammax4 > 0 ? $record->paper4 : 0;
                    $examcont5 = $record->paper5 > 0 && $exammax5 > 0 ? $record->paper5 : 0;

                    $exammaxs = ($exammax1 + $exammax2);
                    $examcont_avg = ($examcont1 + $examcont2) / ($exammax1 + $exammax2);
                    $kamaa = ($examcont_avg * 60) + $examcont3;
                    $njooro = $kamaa * ($conte / 100);
                    $avgexam = $njooro;
                    $record->avgexam = round($avgexam, 0);
                    $total = $record->avgcat + $record->avgexam;

                }

                $record->total = round($total);

            }

        }

        $Get_Grade = self::Get_Grade($gradesyscode, $record->total);

        if (is_array($Get_Grade)) {
            $record->gradecode = valueof($Get_Grade, 'GRADECODE');
            $record->points = valueof($Get_Grade, 'POINTS');
        }

        $subject_Comments = self::Get_Comments($record->subjectcode, $record->gradecode);
        $record->comments = $subject_Comments;

        $record->audituser = $user->userid;
        $record->auditdate = date('Y-m-d');
        $record->audittime = time();


        if ($record->Save()) {
            return json_encode(array('success' => 1, 'message' => 'ok', 'avgcat' => $record->avgcat, 'avgexam' => $record->avgexam, 'total' => $record->total, 'gradecode' => $record->gradecode, 'points' => 7, 'gradesyscode' => $gradesyscode));
        } else {
            $error = @$db->ErrorMsg();
            return json_response(0, "save failed : {$error}");
        }
    }
    public function Get_Grade( $gradesyscode='KN',$total ){
    	global $db;
      $total         = $total>0 ? $total : 0;
      $gradesyscode  = isset($_SESSION['marks']['gsyscode']) ? $_SESSION['marks']['gsyscode'] : null;
      return $db->GetRow("SELECT GRADECODE,POINTS,COMMENTS FROM SAXGRD WHERE GSYSCODE='{$gradesyscode}' AND MIN<={$total} AND MAX>={$total}");
    }

	private  function Get_Comments($subjectcode, $gradecode ){
	  	 global $db;
	  	 return $db->CacheGetOne(120,"SELECT COMMENTS FROM SASUBREMARKS WHERE SUBJECTCODE='{$subjectcode}' AND GRADECODE='{$gradecode}'");
	}

	public function remove(){
		global $db, $cfg;
		json_response(0,'failed');
	}

    public  function get_custom_comments(){
	  	 global $db,$cfg;

	  	 $id     = filter_input(INPUT_POST , 'id', FILTER_VALIDATE_INT);
	     $record = new ADODB_Active_Record('SASTUDSUB', array('ID'));
	     $record->Load("ID={$id}");

	 $setup = $db->GetRow("
       SELECT * FROM SATSSG
        WHERE FORMCODE='{$record->formcode}'
        and YEARCODE='{$record->yearcode}'
        and TERMCODE='{$record->termcode}'
       ");

     $comments = array();

     for ($c=1;$c<=5;++$c){
      if(isset($setup["CATMAX{$c}"])){
      	if($setup["CATMAX{$c}"]>0){
      		$comments["cat{$c}comments"]   = "CAT {$c} Comments";
      	}
      }
     }

     for ($x=1;$x<=5;++$x){
      if(isset($setup["EXAMMAX{$x}"])){
      	if($setup["EXAMMAX{$x}"]>0){
      		$comments["paper{$x}comments"]   = "PAPER {$x} Comments";
      	}
      }
     }

         $comments["comments"]   = "Combined";
	     $subject_Comments       = $record->comments;

	     $f_id       = ui::fi('id');
	     $f_exam     = 'exam';
	     $f_comment  = ui::fi('custom_comments');
	     $div_exam   = ui::fi('exam_custom_comments');


	     $select   = ui::form_select_fromArray($f_exam,$comments,'comments' ,'onchange="'.$cfg['appname'].'.get_exam_custom_comment('.$id.')"', '', 200);
	     $textarea = "<textarea name=\"".$f_comment."\" id=\"".$f_comment."\" cols=\"30\" rows=\"2\" data-options=\"\" class=\"\">".$subject_Comments."</textarea>";

		  echo $input = ui::form_input('hidden', 'id', 30, $id);
		  echo '<table cellpadding="0" cellspacing="0" width="100%" border="0" >';

		  echo '<tr>';
			echo '<td>Exam</td>';
			echo '<td>'.$select.'</td>';
		  echo '</tr>';


		  echo '<tr>';
			echo '<td>Comment</td>';
			echo '<td><div id="'.$div_exam.'">'.$textarea.'</div></td>';
		  echo '</tr>';

		  echo '</table>';

	}

    public  function get_exam_custom_comment(){
	  	 global $db;

	  	 $id       = filter_input(INPUT_POST , 'id', FILTER_VALIDATE_INT);
	  	 $exam     = filter_input(INPUT_POST , 'exam' );
	     $record   = new ADODB_Active_Record('SASTUDSUB', array('ID'));

	     $record->Load("ID={$id}");

	     $subject_Comments  = isset($record->$exam) ? $record->$exam : $record->comments;

	     $f_comment         = ui::fi('custom_comments');

	     echo "<textarea name=\"".$f_comment."\" id=\"".$f_comment."\" cols=\"30\" rows=\"2\" data-options=\"\" class=\"\">".$subject_Comments."</textarea>";

	}

    public  function save_subject_comment(){
	  	 global $db;

	  	 $id         = filter_input(INPUT_POST , ui::fi('id'), FILTER_VALIDATE_INT);
	  	 $exam       = filter_input(INPUT_POST , ui::fi('exam'), FILTER_SANITIZE_STRING);
	  	 $comments   = filter_input(INPUT_POST , ui::fi('custom_comments'), FILTER_SANITIZE_STRING);

	     $record     = new ADODB_Active_Record('SASTUDSUB', array('ID'));
	     $record->Load("ID={$id}");

	     if(isset($record->$exam)){
		  $record->$exam = $comments;
		 }else{
	      $record->comments = $comments;
	     }

	     if($record->Save()){
		  return json_response(1,'Comments Saved');
		 }else{
		  return json_response(1,'Failed to Save Comments');
		 }

	}

}
