<?php

/**
* auto created index file for modules/students/exams/datacleanup
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-03-18 09:12:50
*/


 $school          = new school();
 $user            = new user();
 $terms           = $db->GetAssoc('SELECT TERMCODE,TERMNAME FROM SATERMS ORDER BY TERMCODE');
 $subjects_array  =  $db->GetAssoc("SELECT SUBJECTCODE,SUBJECTNAME FROM SASUBJECTS order by SUBJECTCODE");

 $my_subjects     =  $db->GetArray("
	SELECT SUBJECTCODE,STREAMCODE,YEARCODE FROM SATSA
	ORDER BY YEARCODE,STREAMCODE ASC
   ");


 $my_list    = array();
 $my_streams = array();
 $streams = array();
 $years = array();
 $subjects = array();

 if( sizeof($my_subjects)>0){
 	foreach ($my_subjects as $my_subject){
 		$subjectcode  = valueof($my_subject, 'SUBJECTCODE');
 		$streamcode   = valueof($my_subject, 'STREAMCODE');
 		$yearcode     = valueof($my_subject, 'YEARCODE');

 		$years[$yearcode] = $yearcode;
 		$subjects[$subjectcode] = valueof($subjects_array,$subjectcode);
 	}
 }

$table_id =  ui::fi('tblMarks');
$form_id  =  ui::fi('fd');
?>
<style>
table#<?php echo $table_id;?> {font-size:12px;font-family:Verdana}
table.tblSubSetup {font-size:12px;font-family:Verdana}
table.tblSubSetup td.label{ background-color:#95B8E7;}
table.tblSubSetup td.cat{ background-color:#E2EDFF;}
table.tblSubSetup td.exam{ background-color:#FFE48D;}

table#<?php echo $table_id;?> a.delete { font-style:none;color: #555;}
table#<?php echo $table_id;?> a.delete:hover { color: #D00;}
table#<?php echo $table_id;?> a.comments { color: #555;}
table#<?php echo $table_id;?> a.comments:hover { color: #0D0;}

table#<?php echo $table_id;?> a.active-sort { color: #00F;}
table#<?php echo $table_id;?> a.active-sort:hover { color: #D00;}
table#<?php echo $table_id;?> a.inactive-sort { color: #555;}
table#<?php echo $table_id;?> a.inactive-sort:hover { color: #00F;}

td.input input {
    width: 50px;
    box-sizing: border-box;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
}

td.header {
 border-top:1px solid #999;
 border-bottom:1px dotted #999;
 padding-left: 5px;
}

td.input {
 border-bottom:1px dotted #999;
 border-right:1px dotted #999;
 padding-left: 5px;
}

td.left {
 border-left:1px dotted #999;
}

td.right {
 border-right:1px dotted #999;
}

#loading {
width: 100%;
position: absolute;
}

#pagination
{
text-align:center;
margin-left:100px;

}
li.nav{
list-style: none;
float: left;
margin-left: 5px;
padding:4px;
border:solid 1px #dddddd;
color:#0063DC;
font-size:12px;
}
li.nav:hover
{
color:#FF0084;
cursor: pointer;
}

li.pagination li.page-active{
	border: 4px solid #c2e1f5;
}

a.marks-menu{
text-decoration:none;
padding:4px;
}
</style>


    <div id="content<?php echo MNUID; ?>"  class="easyui-panel" title="Select Subject Below" fit="true" >
	  <form id="ff<?php echo MNUID; ?>" method="post" onsubmit="return false;">
	  <table cellpadding="2" cellspacing="0" width="100%">

	  <tr>
			<td width="180px">Year:</td>
			<td>
			 <?php echo ui::form_select_fromArray('yearcode', $years, $school->active_yearcode," onchange=\"{$cfg['appname']}.list_streams();\" ");  ?>
			</td>
		</tr>

		<tr>
			<td>Term:</td>
			<td>
			<?php echo ui::form_select_fromArray('termcode', $terms,'',"onchange=\"{$cfg['appname']}.list_streams();\"");  ?>
			</td>
		</tr>

		<tr>
			<td >Stream</td>
			<td>
			 <div id="div_streams<?php echo MNUID; ?>">
			 <?php echo ui::form_select_fromArray('streamcode', $streams,'',"onchange=\"{$cfg['appname']}.list_subjects();\"");  ?>
			 </div>
			</td>
		</tr>

		<tr>
			<td>Subject:</td>
			<td>
			   <div id="div_subjects<?php echo MNUID; ?>">
				<?php echo ui::form_select_fromArray('subjectcode', $subjects);  ?>
				</div>
			</td>
		</tr>

		<tr>
			<td>&nbsp;</td>
			<td>
			 <button onclick="<?php echo $cfg['appname']; ?>.subject_setup();return false;"><i class="fa check-square"></i> View Class List</button>
			</td>
		</tr>

	 </table>
	</form>
  </div>

    <div id="pager" ></div>
    <div id="loading" ></div>

    <script>

   var <?php echo $cfg['appname']; ?> = {
   load_stage:function (){
	   ui.cc('load_stage', 'modvars=<?php echo $vars; ?>', 'content<?php echo MNUID; ?>');
   },
   list_streams:function (){
    $.post('./endpoints/crud/',   'modvars=<?php echo $vars; ?>&function=list_streams&'+$('#ff<?php echo MNUID; ?>').serialize()   , function( html ){
	$('#div_subjects<?php echo MNUID; ?>').html('&nbsp;');
  	$('#div_streams<?php echo MNUID; ?>').html(html);
  	});
   },
   list_subjects:function (){
    $.post('./endpoints/crud/',   'modvars=<?php echo $vars; ?>&function=list_subjects&'+$('#ff<?php echo MNUID; ?>').serialize()   , function( html ){
  	$('#div_subjects<?php echo MNUID; ?>').html(html);
  	});
   },
   subject_setup:function ( ){
	   var subjectcode = $('#<?php echo ui::fi('subjectcode'); ?>').val();
	   var streamcode = $('#<?php echo ui::fi('streamcode'); ?>').val();
	   var yearcode = $('#<?php echo ui::fi('yearcode'); ?>').val();
	   var termcode = $('#<?php echo ui::fi('termcode'); ?>').val();
   	   this.preload_subject( subjectcode, streamcode, yearcode, termcode);
  	},
    preload_subject:function (subjectcode, streamcode, yearcode, termcode){
  	    $('#dlg<?php echo MNUID; ?>').dialog("close");
  	    $('#content<?php echo MNUID; ?>').panel({title:""+subjectcode+"- "+streamcode+"  "+yearcode+" Term "+termcode+""})
  		 $.post('./endpoints/crud/',   'modvars=<?php echo $vars; ?>&function=preload&subjectcode='+subjectcode+'&streamcode='+streamcode+'&yearcode='+yearcode+'&termcode='+termcode  , function( pager_content ){
  		 	$('#pager').html(pager_content);
  		 	<?php echo $cfg['appname']; ?>.open_subject();
  		 });
     },
    open_subject:function (){
 	 $("#content<?php echo MNUID; ?>").load("./endpoints/crud/?modvars=<?php echo $vars; ?>&function=data&rows=15&page=1");
 	 $('#button_print').linkbutton('enable');
 	 $('#button_config').linkbutton('enable');
     },
     paginate:function (lid,pageNum,sortcol,sortorder){
		$("#content<?php echo MNUID; ?>").load("./endpoints/crud/?modvars=<?php echo $vars; ?>&function=data&sortcol="+sortcol+"&sortorder="+sortorder+"&rows=15&page=" + pageNum);
     },
     pagesort:function (sortcol,sortorder,pageNum){
		$("#content<?php echo MNUID; ?>").load("./endpoints/crud/?modvars=<?php echo $vars; ?>&function=data&sortcol="+sortcol+"&sortorder="+sortorder+"&rows=15&page=" + pageNum);
     },
     clearForm:function (){
		$('#ff<?php echo MNUID; ?>').form('clear');
	 },
	 delete_students:function (sortcol,sortorder,pageNum){
	  if(confirm("Are you Sure you want to Delete the Selected Students from this class list ?")){
	   $.post("./endpoints/crud/?sortcol="+sortcol+"&sortorder="+sortorder+"&rows=15&page=" + pageNum,   'modvars=<?php echo $vars; ?>&function=delete_students&'+$('#<?php echo $form_id; ?>').serialize()   , function( html ){
  	   $('#content<?php echo MNUID; ?>').html(html);
  	   });
	  }
	 },
	}

</script>
