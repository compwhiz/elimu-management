<?php

/**
* auto created config file for modules/students/exams/datacleanup
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-03-18 09:12:50
*/

 final class datacleanup {
 private $id;
	private $page;
	private $rows;
	private $admno;

	public function __construct(){
		global $cfg;

		$this->id           = filter_input(INPUT_POST , 'id');
		$this->datasrc      = valueof($cfg,'datasrc');
		$this->primary_key  = valueof($cfg,'pkcol');
	}

	public function load_stage(){
		global $db,$cfg;
		$school          =  new school();
		$user            =  new user();
		$terms           =  $db->GetAssoc('SELECT TERMCODE,TERMNAME FROM SATERMS ORDER BY TERMCODE');
		$subjects_array  =  $db->GetAssoc("SELECT SUBJECTCODE,SUBJECTNAME FROM SASUBJECTS order by SUBJECTCODE");

		$my_subjects     =  $db->GetArray("
		SELECT SUBJECTCODE,STREAMCODE,YEARCODE FROM SATSA
		ORDER BY YEARCODE,STREAMCODE ASC
		");

		$years = array();
		$subjects = array();
        $streams = array();

		if( sizeof($my_subjects)>0){
		 foreach ($my_subjects as $my_subject){
			$subjectcode_loop  = valueof($my_subject, 'SUBJECTCODE');
			$streamcode_loop   = valueof($my_subject, 'STREAMCODE');
			$yearcode_loop     = valueof($my_subject, 'YEARCODE');

			$years[$yearcode_loop]       = $yearcode_loop;
			$streams[$streamcode_loop]   = $streamcode_loop;
			$subjects[$subjectcode_loop] = valueof($subjects_array, $subjectcode_loop);

		 }
		}


       $yearcode     = isset($_SESSION['marks']['yearcode']) ? $_SESSION['marks']['yearcode'] : $school->active_yearcode;
       $termcode     = isset($_SESSION['marks']['termcode']) ? $_SESSION['marks']['termcode'] : $school->active_termcode;
       $streamcode   = isset($_SESSION['marks']['streamcode']) ? $_SESSION['marks']['streamcode'] : null;
       $subjectcode  = isset($_SESSION['marks']['subjectcode']) ? $_SESSION['marks']['subjectcode'] : null;
       $subjects     = isset($_SESSION['marks']['subjectcode']) ? array($subjectcode=>$subjectcode) : array();

       if( isset($_SESSION['marks']['yearcode']) && isset($_SESSION['marks']['streamcode']) ){

       $streams   =  array();
       $records   =  $db->GetArray("
		SELECT distinct STREAMCODE FROM SATSA
		WHERE YEARCODE='{$yearcode}'
		ORDER BY STREAMCODE ASC
       ");

	    if($records){
	  	 if(count($records)>0){
	  	  foreach ($records as $record){
	  	  	$streamcode_loop           = valueof($record,'STREAMCODE');
	  	  	$streams[$streamcode_loop] = $streamcode_loop;
	  	  }
	  	 }
	    }

       }

	?>
	<form id="ff<?php echo MNUID; ?>" method="post" onsubmit="return false;">
	  <table cellpadding="2" cellspacing="0" width="100%">

	  <tr>
			<td width="180px">Year:</td>
			<td>
			 <?php echo ui::form_select_fromArray('yearcode', $years, $yearcode," onchange=\"{$cfg['appname']}.list_streams();\" ");  ?>
			</td>
		</tr>

		<tr>
			<td>Term:</td>
			<td>
			<?php echo ui::form_select_fromArray('termcode', $terms, $termcode,"onchange=\"{$cfg['appname']}.list_streams();\"");  ?>
			</td>
		</tr>

		<tr>
			<td >Stream</td>
			<td>
			 <div id="div_streams<?php echo MNUID; ?>">
			 <?php echo ui::form_select_fromArray('streamcode', $streams, $streamcode,"onchange=\"{$cfg['appname']}.list_subjects();\"");  ?>
			 </div>
			</td>
		</tr>

		<tr>
			<td>Subject:</td>
			<td>
			   <div id="div_subjects<?php echo MNUID; ?>">
				<?php echo ui::form_select_fromArray('subjectcode', $subjects, $subjectcode);  ?>
				</div>
			</td>
		</tr>

		<tr>
			<td>&nbsp;</td>
			<td>
			 <button onclick="<?php echo $cfg['appname']; ?>.subject_setup();return false;">View Class List</button>
			</td>
		</tr>

	 </table>
	</form>
		<?php
	}

	public function list_streams(){
		global $db,$cfg;

	 $subjectcode  =  filter_input(INPUT_POST , ui::fi('subjectcode'));
     $streamcode   =  filter_input(INPUT_POST , ui::fi('streamcode'));
     $yearcode     =  filter_input(INPUT_POST , ui::fi('yearcode'));
     $termcode     =  filter_input(INPUT_POST , ui::fi('termcode'));

	 $streams_array = array();
     $user          = new user();

     $records   =  $db->GetArray("
		SELECT distinct STREAMCODE FROM SASTUDSUB
		WHERE YEARCODE='{$yearcode}'
		ORDER BY STREAMCODE ASC
     ");


	  if($records){
	  	if(count($records)>0){
	  	  foreach ($records as $record){
	  	  	$streamcode  = valueof($record,'STREAMCODE');
	  	  	$streams_array[$streamcode] = $streamcode;
	  	  }
	  	}
	  }

	  return ui::form_select_fromArray('streamcode',$streams_array, ''," onchange=\"{$cfg['appname']}.list_subjects();\" ");

	}

	public function list_subjects(){
		global $db;
     $streamcode   =  filter_input(INPUT_POST , ui::fi('streamcode'));
     $yearcode     =  filter_input(INPUT_POST , ui::fi('yearcode'));
     $termcode     =  filter_input(INPUT_POST , ui::fi('termcode'));

     $user         =  new user();

     $subjects      =  $db->GetArray("
		SELECT DISTINCT SUBJECTCODE FROM SASTUDSUB
		WHERE YEARCODE='{$yearcode}'
		AND STREAMCODE='{$streamcode}'
		ORDER BY SUBJECTCODE ASC
     ");

	  $subjects_array = array();

	  if($subjects){
	  	if(count($subjects)>0){
	  	  foreach ($subjects as $subject){
	  	  	$subjectcode  = valueof($subject,'SUBJECTCODE');
	  	  	$subjects_array[$subjectcode] = $subjectcode;
	  	  }
	  	}
	  }

	  return ui::form_select_fromArray('subjectcode',$subjects_array, ''," onchange=\"\" ");

	}

	public function preload(){
		global $db,$cfg;

	   $user         =  new user();

	   $subjectcode  = filter_input(INPUT_POST , ('subjectcode'));
       $streamcode   = filter_input(INPUT_POST , ('streamcode'));
	   $yearcode     = filter_input(INPUT_POST , ('yearcode'));
	   $termcode     = filter_input(INPUT_POST , ('termcode'));

       $formcode     =  $db->GetOne("SELECT FORMCODE FROM SASTREAMS WHERE STREAMCODE='{$streamcode}'");
       $gsyscode     =  self::get_grading_system($formcode, $yearcode, $termcode);
       $setup        =  self::get_set_up($subjectcode, $streamcode, $yearcode, $termcode);

       $count_cats    = 0;
       $count_exams   = 0;

       $_SESSION['marks'] = array();

       for ($c=1;$c<=5;++$c){
        if(isset($setup["CATMAX{$c}"])){
      	 if($setup["CATMAX{$c}"]>0){
      		$_SESSION['marks']["catmax{$c}"]   = $setup["CATMAX{$c}"];
      		++$count_cats;
      	 }
        }
       }

     for ($x=1;$x<=5;++$x){
      if(isset($setup["EXAMMAX{$x}"])){
      	if($setup["EXAMMAX{$x}"]>0){
      		$_SESSION['marks']["exammax{$x}"]   = $setup["EXAMMAX{$x}"];
      		++$count_exams;
      	}
      }
     }

       $_SESSION['marks']['subjectcode']  = $subjectcode;
       $_SESSION['marks']['formcode']     = $formcode;
       $_SESSION['marks']['gsyscode']     = $gsyscode;
       $_SESSION['marks']['streamcode']   = $streamcode;
       $_SESSION['marks']['yearcode']     = $yearcode;
       $_SESSION['marks']['termcode']     = $termcode;
       $_SESSION['marks']['count_cats']   = $count_cats;
       $_SESSION['marks']['count_exams']  = $count_exams;
       $_SESSION['marks']['contc']        = valueof($setup, 'CONTC');
       $_SESSION['marks']['conte']        = valueof($setup, 'CONTE');

	}

	public function data(){
		global $db,$cfg;

	$this->page         = filter_input(INPUT_GET , 'page' , FILTER_VALIDATE_INT);
	$this->rows         = filter_input(INPUT_GET , 'rows' , FILTER_VALIDATE_INT);
	$this->id           = filter_input(INPUT_GET , 'id');
	$_sortcol           = filter_input(INPUT_GET , 'sortcol');
	$_sortorder         = filter_input(INPUT_GET , 'sortorder');

	if(empty($this->rows)){
     $this->rows = 10;
	}
	
	if(empty($this->page)){
     $this->page = 1;
	}

	$sortcol            = !empty($_sortcol)	? strtoupper($_sortcol) : 'ADMNO';
	$sortorder          = !empty($_sortorder)	? strtoupper($_sortorder) : 'ASC';

    $sort_asc_col1  = $_sortcol=='admno' && $_sortorder=='desc' ? 'active-sort' : 'inactive-sort';
    $sort_desc_col1 = $_sortcol=='admno' && $_sortorder=='asc' ? 'active-sort' : 'inactive-sort';

    $sort_asc_col2  = $_sortcol=='fullname' && $_sortorder=='desc' ? 'active-sort' : 'inactive-sort';
    $sort_desc_col2 = $_sortcol=='fullname' && $_sortorder=='asc' ? 'active-sort' : 'inactive-sort';

	$page    =  $this->page>0 ? $this->page : 1;
	$rows    =  $this->rows>0 ? $this->rows : 10;
	$offset  = ($page-1)*$rows;
	$count_start    = ($page*$rows);

    $subjectcode  =  isset($_SESSION['marks']['subjectcode']) ? $_SESSION['marks']['subjectcode'] : null;
    $formcode     =  isset($_SESSION['marks']['formcode']) ? $_SESSION['marks']['formcode'] : null;
    $streamcode   =  isset($_SESSION['marks']['streamcode']) ? $_SESSION['marks']['streamcode'] : null;
    $yearcode     =  isset($_SESSION['marks']['yearcode']) ? $_SESSION['marks']['yearcode'] : null;
    $termcode     =  isset($_SESSION['marks']['termcode']) ? $_SESSION['marks']['termcode'] : null;

    $data_sql   = ("
select ID, ADMNO, FULLNAME ,CAT1,CAT2,CAT3,CAT4,CAT5,AVGCAT,PAPER1,PAPER2,PAPER3,PAPER4,PAPER5,AVGEXAM,TOTAL,GRADECODE,POINTS,STATUSCODE
from VIEWSTUDENTSUBJECTS
where YEARCODE='{$yearcode}'
AND TERMCODE='{$termcode}'
AND FORMCODE='{$formcode}'
AND STREAMCODE='{$streamcode}'
AND SUBJECTCODE='{$subjectcode}'
ORDER BY {$sortcol} {$sortorder}
");
     $data = $db->SelectLimit($data_sql,$rows, $offset);
     $students  = array();

     if ($data) {
	  $numRecords =  $data->RecordCount();
      if ( $numRecords>0) {
      	while (!$data->EOF){
      	 $students[] = $data->fields;
      	 $data->MoveNext();
      	}
      }
     }

   $setup = $db->GetRow("
       SELECT * FROM SATSS
        WHERE SUBJECTCODE='{$subjectcode}'
        AND STREAMCODE='{$streamcode}'
        AND YEARCODE='{$yearcode}'
        AND TERMCODE='{$termcode}'
       ");

     $subject_config = array();

     for ($c=1;$c<=5;++$c){
      if(isset($setup["CATMAX{$c}"])){
      	if($setup["CATMAX{$c}"]>0){
      		$subject_config['cats'][$c]['max']   = $setup["CATMAX{$c}"];
            $subject_config['cats'][$c]['name']  = isset($setup["CATNAME{$c}"]) && !empty($setup["CATNAME{$c}"]) ? $setup["CATNAME{$c}"] : "CAT {$c}";
      	}
      }
     }

     for ($x=1;$x<=5;++$x){
      if(isset($setup["EXAMMAX{$x}"])){
      	if($setup["EXAMMAX{$x}"]>0){
      		$subject_config['papers'][$x]['max']   = $setup["EXAMMAX{$x}"];
            $subject_config['papers'][$x]['name']  = isset($setup["EXAMNAME{$x}"]) && !empty($setup["EXAMNAME{$x}"]) ? $setup["EXAMNAME{$x}"] : "Paper {$x}";
      	}
      }
     }

     $table_id =  ui::fi('tblMarks');
     $form_id  =  ui::fi('fd');

     echo '<form id="'.$form_id.'"  method="POST"  enctype="multipart/form-data" onsubmit="return false;"  > ';
     echo '<table id="'.$table_id.'" cellpadding="0" cellspacing="0" width="100%" border="0" class="datagrid-htable datagrid-btable datagrid-ftable">';
      echo '<thead>';

       echo '<tr>';
        echo '
        <td  class="panel-header" colspan="3">
        <a  href="javascript:void(0);"  class="marks-menu"  onclick="'.$cfg['appname'].'.load_stage();"><i class="fa fa-edit"></i> Open</a> |
        <a  href="javascript:void(0);"  class="easyui-linkbutton" id="btnDeleteClassStudents"  class="delete"  onclick="'.$cfg['appname'].'.delete_students(\'admno\',\'desc\','.$page.');"><i class="fa fa-remove"></i> Delete Selected</a>
        </td>
        ';

        if(isset($subject_config['cats']) && sizeof($subject_config['cats'])>0){
        	foreach ($subject_config['cats'] as $cat_index => $cat){
        		$name = valueof($cat,'name','','Camelize');
        		$max  = valueof($cat,'max');
               echo "<td  class=\"panel-header\">{$name}</td>";
        	}
        	 echo "<td  class=\"panel-header\">&nbsp;</td>";
        }

        if(isset($subject_config['papers']) && sizeof($subject_config['papers'])>0){
        	foreach ($subject_config['papers'] as $cat_index => $paper){
        		$name = valueof($paper,'name','','Camelize');
        		$max  = valueof($paper,'max');
               echo "<td  class=\"panel-header\">{$name}</td>";
        	}
        }

       echo "<td class=\"panel-header\" colspan=\"4\">&nbsp;</td>";
       echo "</tr>";

       echo '<tr>';
        echo '<td  class="header left right">No</td>';
        echo '<td  class="header left right">Delete</td>';

        echo '<td  class="header right">
             <a  href="javascript:void(0);" class="'.$sort_asc_col1.'" title="Sort by No. Descending"  class="marks-menu"  onclick="'.$cfg['appname'].'.pagesort(\'admno\',\'desc\','.$page.');"><i class="fa fa-chevron-down"></i></a>
             Adm.No
             <a  href="javascript:void(0);" class="'.$sort_desc_col1.'"  title="Sort by No. Ascending"   class="marks-menu"  onclick="'.$cfg['appname'].'.pagesort(\'admno\',\'asc\','.$page.');"><i class="fa fa-chevron-up"></i></a>
        </td>';

        echo '<td  class="header right">
             <a  href="javascript:void(0);" class="'.$sort_asc_col2.'" title="Sort by Name Descending"  class="marks-menu"  onclick="'.$cfg['appname'].'.pagesort(\'fullname\',\'desc\','.$page.');"><i class="fa fa-chevron-down"></i></a>
             Name
             <a  href="javascript:void(0);" class="'.$sort_desc_col2.'" title="Sort by Name Ascending"   class="marks-menu"  onclick="'.$cfg['appname'].'.pagesort(\'fullname\',\'asc\','.$page.');"><i class="fa fa-chevron-up"></i></a>
        </td>';

        if(isset($subject_config['cats']) && sizeof($subject_config['cats'])>0){
        	foreach ($subject_config['cats'] as $cat_index => $cat){
        		$name = valueof($cat,'name');
        		$max  = valueof($cat,'max');
               echo "<td  class=\"header right\"><b>{$max}</b></td>";
        	}
        	 echo "<td  class=\"header right\">CT.Avg</td>";
        }

        if(isset($subject_config['papers']) && sizeof($subject_config['papers'])>0){
        	foreach ($subject_config['papers'] as $cat_index => $paper){
        		$name = valueof($paper,'name');
        		$max  = valueof($paper,'max');
               echo "<td  class=\"header right\"><b>{$max}</b></td>";
        	}
        	echo "<td  class=\"header right\">PP.Avg</td>";
        }

       echo "<td class=\"header right\">Total</td>";
       echo "<td class=\"header right\">Grade</td>";
       echo "</tr>";

      echo '</thead>';

      if(count($students)>0){
       echo '<tbody>';

       if($page==1){
        $count = 1;
       }else{
        $count = $offset+1;
       }

       foreach ($students as $student){

       	$id          = valueof($student,'ID');
       	$admno       = valueof($student,'ADMNO');
       	$fullname    = valueof($student,'FULLNAME','**missing**');

       	$cat1        = valueof($student,'CAT1');
       	$cat1        = round($cat1,0);
       	$cat2        = valueof($student,'CAT2');
       	$cat3        = valueof($student,'CAT3');
       	$cat4        = valueof($student,'CAT4');
       	$cat5        = valueof($student,'CAT5');
       	$avgcat      = valueof($student,'AVGCAT');

       	$paper1      = valueof($student,'PAPER1');
       	$paper2      = valueof($student,'PAPER2');
       	$paper3      = valueof($student,'PAPER3');
       	$paper4      = valueof($student,'PAPER4');
       	$paper5      = valueof($student,'PAPER5');
       	$avgexam     = valueof($student,'AVGEXAM');
       	$total       = valueof($student,'TOTAL');
       	$gradecode   = valueof($student,'GRADECODE');

       	$divAvgCat     = "divavgcat_{$id}";
       	$divAvgExam    = "divavgexam_{$id}";
       	$divGradeCode  = "divgrade_{$id}";
       	$divTotal      = "divtotal_{$id}";
       	$divStatus     = "divstatus_{$id}";

       	$checkbox_id  = "delete[{$id}]";
       	$checked      =  'checked';
       	$checkbox     =  ui::form_checkbox($checkbox_id,$checked,'','',1);

        echo '<tr>';
         echo "<td class=\"input left\" >{$count}</td>";
         echo "<td class=\"input left\" >{$checkbox}</td>";
         echo "<td class=\"input\" >{$admno}</td>";
         echo "<td class=\"input\" >{$fullname}</td>";

         if(isset($subject_config['cats']) && sizeof($subject_config['cats'])>0){
         foreach ($subject_config['cats'] as $cat_index => $cat){

          $value         = valueof($student,"CAT{$cat_index}");
          $value         = round($value,1);
          $name          = valueof($cat,'name');
          $max           = valueof($cat,'max');
       	  $textbox_catid = "cat{$cat_index}_{$id}";
       	  $column        = "cat{$cat_index}";

       	  $textbox_cat   = "<input type=\"text\" class=\"easyui-numberbox\" value=\"{$value}\" id=\"{$textbox_catid}\" size=\"5\" readonly disabled />";

          //echo "<td class=\"input\"  >{$textbox_cat}</td>";
          echo "<td class=\"input\"  >{$value}</td>";

          }
         }

         //echo "<td class=\"input\" ><div id=\"{$divAvgCat}\">{$avgcat}</div></td>";

         if(isset($subject_config['papers']) && sizeof($subject_config['papers'])>0){
         foreach ($subject_config['papers'] as $paper_index => $paper){

          $value            = valueof($student,"PAPER{$paper_index}");
          $value            = round($value,1);
          $name             = valueof($paper,'name');
          $max              = valueof($paper,'max');
       	  $textbox_paperid  = "paper{$paper_index}_{$id}";
       	  $column           = "paper{$paper_index}";

       	  $textbox_paper    = "<input type=\"text\" class=\"easyui-numberbox\" value=\"{$value}\" id=\"{$textbox_paperid}\" size=\"5\"  readonly disabled  />";

          //echo "<td class=\"input\" >{$textbox_paper}</td>";
          echo "<td class=\"input\" >{$value}</td>";

          }
         }

         //echo "<td class=\"input\" ><div id=\"{$divAvgExam}\">{$avgexam}</div></td>";
         echo "<td class=\"input\" ><div id=\"{$divTotal}\">{$total}</div></td>";
         echo "<td class=\"input\" ><div id=\"{$divGradeCode}\">{$gradecode}</div></td>";
        echo '</tr>';

        ++$count;
       }

       echo '</tbody>';
      }

     echo '</table>';
     echo '</form>';


      $data_pager_sql   = ("
select count(ADMNO) NUM
from VIEWSTUDENTSUBJECTS
where YEARCODE='{$yearcode}'
AND TERMCODE='{$termcode}'
AND FORMCODE='{$formcode}'
AND STREAMCODE='{$streamcode}'
AND SUBJECTCODE='{$subjectcode}'
");
     $numRecords = $db->GetOne($data_pager_sql);

      echo '<table width="100%" cellpadding="2" cellspacing="0" border="0">';
	   echo '<tr>';
	   echo '<td>';
	   echo '<ul id="pagination">';

				$pages = $numRecords/$this->rows;
				$pages = ceil($pages);

				for($i=1; $i<=$pages; $i++){
					$li_color = $i==$page ? "#000" : "#0063DC";
					$li_class = $i==$page ? "page-active" : "#page-inactive";
					echo "<li class=\"nav {$li_class}\" id=\"li_{$i}\" style=\"color:{$li_color}\" onclick=\"{$cfg['appname']}.paginate(this.id,{$i},'{$_sortcol}','{$_sortorder}');\">Page {$i}</li>";
				}

	   echo '</ul>';
	  echo '</td>';
	 echo '</tr>';
	echo '</table>';

       echo "
     <script>
$('#{$table_id}').tableNav();
$('#{$table_id} input').eq(1).click();
</script>
";
	}

	private function get_grading_system($formcode, $yearcode, $termcode){
		global $db;

	  return $db->GetOne("
       SELECT GSYSCODE FROM SATSSG
        WHERE FORMCODE='{$formcode}'
        AND YEARCODE='{$yearcode}'
        AND TERMCODE='{$termcode}'
       ");

	}

	private function get_global_set_up($formcode, $yearcode, $termcode){
		global $db;

	  $global_setup = $db->GetRow("
       SELECT * FROM SATSSG
        WHERE FORMCODE='{$formcode}'
        AND YEARCODE='{$yearcode}'
        AND TERMCODE='{$termcode}'
       ");

	  return $global_setup;
	}

	private function get_set_up($subjectcode,$streamcode,$yearcode,$termcode){
		global $db;

	  $setup = $db->GetRow("
       SELECT * FROM SATSS
        WHERE SUBJECTCODE='{$subjectcode}'
        AND STREAMCODE='{$streamcode}'
        AND YEARCODE='{$yearcode}'
        AND TERMCODE='{$termcode}'
       ");

	  return $setup;
	}

	public  function subject_setup() {
		global $db,$cfg;

	   $subjectcode  =  filter_input(INPUT_POST , ui::fi('subjectcode'));
       $streamcode   =  filter_input(INPUT_POST , ui::fi('streamcode'));
       $yearcode     =  filter_input(INPUT_POST , ui::fi('yearcode'));
       $termcode     =  filter_input(INPUT_POST , ui::fi('termcode'));

       return self::call_setup($subjectcode, $streamcode, $yearcode, $termcode);
    }

	public  function subject_resetup() {
		global $db,$cfg;

	   $subjectcode  =  filter_input(INPUT_POST , ('subjectcode'));
       $streamcode   =  filter_input(INPUT_POST , ('streamcode'));
       $yearcode     =  filter_input(INPUT_POST , ('yearcode'));
       $termcode     =  filter_input(INPUT_POST , ('termcode'));

	   return self::call_setup($subjectcode, $streamcode, $yearcode, $termcode);
    }

	public  function call_setup($subjectcode, $streamcode, $yearcode, $termcode) {
		global $db,$cfg;

       $formcode     =  $db->CacheGetOne(1200,"SELECT FORMCODE FROM SASTREAMS WHERE STREAMCODE='{$streamcode}'");

       $setup        = self::get_set_up($subjectcode, $streamcode, $yearcode, $termcode);
       $global_setup = self::get_global_set_up($formcode, $yearcode, $termcode);

       $setup_contc = valueof($setup,'CONTC');
       $setup_conte = valueof($setup,'CONTE');

       $global_setup_contc = valueof($global_setup,'CONTC');
       $global_setup_conte = valueof($global_setup,'CONTE');

       $saved = sizeof($setup)>0 ? true : false;

       if( ($global_setup_contc!=$setup_contc) || ($global_setup_conte!=$setup_conte) ){
		$setup['CONTC'] = $global_setup_contc;
		$setup['CONTE'] = $global_setup_conte;
		$saved =  false;
	   }

       if(!$saved){
       	 $btn_enter_marks_status     = 'disabled';
       	 $enable_enter_marks         = 1;
       }else{
       	 $btn_enter_marks_status     = '';
       	 $enable_enter_marks         = 0;
       }

       if( !isset($setup['YEARCODE']) || (isset($setup['YEARCODE']) && empty($setup['YEARCODE'])) ){
       	$setup = $global_setup;
       }

       if(empty($setup)){
       	 if($termcode>1){//pick previous term settings
       	 	for ($s=3;$s>=1;--$s){
       	 		$setup = self::get_set_up($subjectcode, $streamcode, $yearcode, $s);
       	 		if(!empty($setup)){
       	 			break;
       	 		}
       	 	}
       	 }else{//pick previous year term 3 settings
       	 	$yearcode_check  = ($yearcode-1);
       	 	$termcode_check  = 3;
       	 	$setup = self::get_set_up($subjectcode, $streamcode, $yearcode_check, $termcode_check);
       	 }
       }// give up & user enters new subject setup

       //subject cols
       $subjectcols = $db->GetRow("SELECT NUMCATS,NUMEXAMS FROM SAXPF WHERE SUBJECTCODE='{$subjectcode}' AND FORMCODE='{$formcode}'");
       $cats        = valueof($subjectcols,'NUMCATS',3);
       $exams       = valueof($subjectcols,'NUMEXAMS',3);
       $all         = $cats+$exams;

       echo '<form id="frmSubSetup'.MNUID.'" name="frmSubSetup"  method="POST"  enctype="multipart/form-data" onsubmit="return false;"  > ';
       echo "<table class=\"tblSubSetup\" width=\"100%\" border=\"0\" cellpadding=\"0\"  cellspacing=\"0\"  >";
        echo "<tr>";

         echo "<td class=\"header label\">&nbsp;</td>";

        for ($c=1;$c<=$cats;++$c){
         echo "<td class=\"header cat\">Cat{$c}</td>";
        }

        for ($c=1;$c<=$exams;++$c){
         echo "<td class=\"header exam\" style=\"padding:2px;\">Exam{$c}</td>";
        }

        echo "</tr>";

        echo "<tr>";

        //-----------------------------------max
        echo "<td  class=\"left label input\">Out of </td>";

        for ($c=1;$c<=$cats;++$c){
         $input_id    = "cat[{$c}]";
         $input_value = valueof($setup,"CATMAX{$c}");
         echo "<td  class=\"input cat\" style=\"padding:2px;\"><input  class=\"easyui-numberspinner\" type=\"text\" id=\"{$input_id}\"  name=\"{$input_id}\" style=\"width:60px;\" value=\"{$input_value}\" ></input></td>";
        }

        for ($c=1;$c<=$exams;++$c){
          $input_id = "exam[{$c}]";
          $input_value = valueof($setup,"EXAMMAX{$c}");
         echo "<td class=\"input exam\" style=\"padding:2px;\"><input  class=\"easyui-numberspinner\" type=\"text\" id=\"{$input_id}\"  name=\"{$input_id}\" style=\"width:60px;\" value=\"{$input_value}\" ></input></td>";
        }

        echo "</tr>";

        //-----------------------------------title
        echo "<td  class=\"left label input\">Title</td>";

        for ($c=1;$c<=$cats;++$c){
         $input_id = "title_cat[{$c}]";
         $input_value = valueof($setup,"CATNAME{$c}");
         echo "<td class=\"input cat\" style=\"padding:2px;\"><input  class=\" \" type=\"text\" id=\"{$input_id}\"  name=\"{$input_id}\" style=\"width:65px;\" value=\"{$input_value}\" ></input></td>";
        }

        for ($c=1;$c<=$exams;++$c){
          $input_id = "title_exam[{$c}]";
          $input_value = valueof($setup,"EXAMNAME{$c}");
         echo "<td class=\"input exam\" style=\"padding:2px;\"><input  class=\" \" type=\"text\" id=\"{$input_id}\"  name=\"{$input_id}\" style=\"width:65px;\" value=\"{$input_value}\" ></input></td>";
        }

        echo "</tr>";


       echo "<tr>";
        echo "<td  class=\"left label input\">Contributions</td>";
        echo "<td colspan=\"{$cats}\" class=\"input cat\">".ui::form_input('hidden','contc',0, valueof($setup, 'CONTC')).valueof($setup, 'CONTC')."%</td>";
        echo "<td colspan=\"{$exams}\" class=\"input exam\">".ui::form_input('hidden','conte',0, valueof($setup, 'CONTE')).valueof($setup, 'CONTE')."%</td>";
       echo "</tr>";

       echo "<tr>";
        echo "<td>&nbsp;</td>";
        echo "<td colspan=\"{$all}\" >&nbsp;</td>";
       echo "</tr>";

       echo "<tr>";
        echo "<td>&nbsp;</td>";
        echo "<td colspan=\"{$all}\" >
        ";
        //<button type=\"submit\" id=\"btnSave\" onclick=\"{$cfg['appname']}.save_subject_setup('{$subjectcode}', '{$streamcode}', '{$yearcode}','{$termcode}',{$enable_enter_marks});\">Save Setup</button>

        echo " or <button type=\"submit\" id=\"btnEntry\" onclick=\"{$cfg['appname']}.preload_subject('{$subjectcode}', '{$streamcode}', '{$yearcode}','{$termcode}');\" {$btn_enter_marks_status}>Enter Marks</button>";

        echo "</td>";
       echo "</tr>";

       echo "</table>";
       echo "</form>";

	}

	public function delete_students(){
		global $db, $cfg;

    $subjectcode  =  isset($_SESSION['marks']['subjectcode']) ? $_SESSION['marks']['subjectcode'] : null;
    $formcode     =  isset($_SESSION['marks']['formcode']) ? $_SESSION['marks']['formcode'] : null;
    $streamcode   =  isset($_SESSION['marks']['streamcode']) ? $_SESSION['marks']['streamcode'] : null;
    $yearcode     =  isset($_SESSION['marks']['yearcode']) ? $_SESSION['marks']['yearcode'] : null;
    $termcode     =  isset($_SESSION['marks']['termcode']) ? $_SESSION['marks']['termcode'] : null;


    if(isset($_POST['delete'])){
     foreach($_POST['delete'] as $record_id => $state){
        $delete_sql   = ("
		 delete from SASTUDSUB
		 where YEARCODE='{$yearcode}'
		 AND TERMCODE='{$termcode}'
		 AND FORMCODE='{$formcode}'
		 AND STREAMCODE='{$streamcode}'
		 AND SUBJECTCODE='{$subjectcode}'
		 AND ID={$record_id}
		 ");

		 $db->Execute($delete_sql);
		 
		 
		 }
		}
		 

		return self::data();
	}

}


