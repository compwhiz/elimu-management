<?php

if(!defined('MAKE_FIELDS_UNIQUE')){
 define('MAKE_FIELDS_UNIQUE' , true);
}

$scriptname  = @end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'Edit Class List';//user-readable formart
 $cfg['appname']       = 'editclasslist';//lower cased
 $cfg['datasrc']       = 'SASTUDENTS';//where to get data
 $cfg['datatbl']       = 'SASTUDENTS';//base data src [for updates & deletes]
 $cfg['form_width']    = 400;
 $cfg['form_height']   = 280;
 
 $cfg['pkcol']         = 'code';//the primary key

