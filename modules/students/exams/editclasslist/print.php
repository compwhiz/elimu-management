<?php



//	$this->page         = filter_input(INPUT_GET , 'page' , FILTER_VALIDATE_INT);
//	$this->rows         = filter_input(INPUT_GET , 'rows' , FILTER_VALIDATE_INT);
//	$this->id           = filter_input(INPUT_GET , 'id');
//			
//	$page    =  $this->page>0 ? $this->page : 1;
//	$rows    =  $this->rows>0 ? $this->rows : 10;
//	$offset  = ($page-1)*$rows;
//	$count_start    = ($page*$rows);

    $subjectcode  =  isset($_SESSION['marks']['subjectcode']) ? $_SESSION['marks']['subjectcode'] : null; 
    $formcode     =  isset($_SESSION['marks']['formcode']) ? $_SESSION['marks']['formcode'] : null;
    $streamcode   =  isset($_SESSION['marks']['streamcode']) ? $_SESSION['marks']['streamcode'] : null;  
    $yearcode     =  isset($_SESSION['marks']['yearcode']) ? $_SESSION['marks']['yearcode'] : null;
    $termcode     =  isset($_SESSION['marks']['termcode']) ? $_SESSION['marks']['termcode'] : null;
    
    $data_sql   = ("
select ID, ADMNO, FULLNAME ,CAT1,CAT2,CAT3,CAT4,CAT5,AVGCAT,PAPER1,PAPER2,PAPER3,PAPER4,PAPER5,AVGEXAM,TOTAL,GRADECODE,POINTS,STATUSCODE
from VIEWSTUDENTSUBJECTS 
where YEARCODE='{$yearcode}' 
AND TERMCODE='{$termcode}' 
AND FORMCODE='{$formcode}'
AND STREAMCODE='{$streamcode}'
AND SUBJECTCODE='{$subjectcode}'
ORDER BY ADMNO
");
     $data = $db->Execute($data_sql);
     $students  = array();
     
     if ($data) {
      if ($data->RecordCount()>0) {
      	while (!$data->EOF){
      	 $students[] = $data->fields;	
      	 $data->MoveNext();
      	}
      }
     }
     
   $setup = $db->GetRow("
       SELECT * FROM SATSS
        WHERE SUBJECTCODE='{$subjectcode}'
        AND STREAMCODE='{$streamcode}'
        AND YEARCODE='{$yearcode}'
        AND TERMCODE='{$termcode}'
       ");
       
     $subject_config = array();
     
     $subject_config['cont']['cat']  = 40;
     $subject_config['cont']['exam'] = 60;
     
     for ($c=1;$c<=5;++$c){
      if(isset($setup["CATMAX{$c}"])){
      	if($setup["CATMAX{$c}"]>0){
      		$subject_config['cats'][$c]['max']   = $setup["CATMAX{$c}"];
            $subject_config['cats'][$c]['name']  = isset($setup["CATNAME{$c}"]) && !empty($setup["CATNAME{$c}"]) ? $setup["CATNAME{$c}"] : "CAT {$c}";
      	}
      }
     }
     
     for ($x=1;$x<=5;++$x){
      if(isset($setup["EXAMMAX{$x}"])){
      	if($setup["EXAMMAX{$x}"]>0){
      		$subject_config['papers'][$x]['max']   = $setup["EXAMMAX{$x}"];
            $subject_config['papers'][$x]['name']  = isset($setup["EXAMNAME{$x}"]) && !empty($setup["EXAMNAME{$x}"]) ? $setup["EXAMNAME{$x}"] : "Paper {$x}";
      	}
      }
     }
     
     echo '<table id="marks" cellpadding="0" cellspacing="0" width="100%" border="0" class="datagrid-htable datagrid-btable datagrid-ftable">';
      echo '<thead>';
      
       echo '<tr>';
        echo '<td  class="" colspan="3">&nbsp;</td>';
//        echo '<td  class=" right">&nbsp;</td>';
//        echo '<td  class=" right">&nbsp;</td>';
        if(isset($subject_config['cats']) && sizeof($subject_config['cats'])>0){
        	foreach ($subject_config['cats'] as $cat_index => $cat){
        		$name = valueof($cat,'name','','Camelize');
        		$max  = valueof($cat,'max');
               echo "<td  class=\" \"><b>{$name}</b></td>";
        	}
        	 echo "<td  class=\" \">&nbsp;</td>";
        }
        
        if(isset($subject_config['papers']) && sizeof($subject_config['papers'])>0){
        	foreach ($subject_config['papers'] as $cat_index => $paper){
        		$name = valueof($paper,'name','','Camelize');
        		$max  = valueof($paper,'max');
               echo "<td  class=\"\"><b>{$name}</b></td>";
        	}
        }
        
       
       echo "<td class=\"\" colspan=\"3\">&nbsp;</td>";  
       echo "</tr>";
       
       echo '<tr>';
        echo '<td  class="header left right">No</td>';
        echo '<td  class="header right">Adm.No</td>';
        echo '<td  class="header right">Name</td>';
        
        if(isset($subject_config['cats']) && sizeof($subject_config['cats'])>0){
        	foreach ($subject_config['cats'] as $cat_index => $cat){
        		$name = valueof($cat,'name');
        		$max  = valueof($cat,'max');
               echo "<td  class=\"header right\"><b>{$max}</b></td>";
        	}
        	 echo "<td  class=\"header right\">CT.Avg</td>";
        }
        
        if(isset($subject_config['papers']) && sizeof($subject_config['papers'])>0){
        	foreach ($subject_config['papers'] as $cat_index => $paper){
        		$name = valueof($paper,'name');
        		$max  = valueof($paper,'max');
               echo "<td  class=\"header right\"><b>{$max}</b></td>";
        	}
        	echo "<td  class=\"header\">PP.Avg</td>";
        }
        
       echo "<td class=\"header right\">Total</td>";  
       echo "<td class=\"header right\">Grade</td>";  
//       echo "<td class=\"header right\">&nbsp;</td>";  
       echo "</tr>";
       
      echo '</thead>';
      
      if(count($students)>0){
       echo '<tbody>';
      
       $count = 1;	
       
       foreach ($students as $student){
       	
       	$id          = valueof($student,'ID');
       	$admno       = valueof($student,'ADMNO');
       	$fullname    = valueof($student,'FULLNAME','**missing**');
       	
       	$cat1        = valueof($student,'CAT1');
       	$cat1        = round($cat1,0);
       	$cat2        = valueof($student,'CAT2');
       	$cat3        = valueof($student,'CAT3');
       	$cat4        = valueof($student,'CAT4');
       	$cat5        = valueof($student,'CAT5');
       	$avgcat      = valueof($student,'AVGCAT');
       	
       	$paper1      = valueof($student,'PAPER1');
       	$paper2      = valueof($student,'PAPER2');
       	$paper3      = valueof($student,'PAPER3');
       	$paper4      = valueof($student,'PAPER4');
       	$paper5      = valueof($student,'PAPER5');
       	$avgexam     = valueof($student,'AVGEXAM');
       	$total       = valueof($student,'TOTAL');
       	$gradecode   = valueof($student,'GRADECODE');
       	
       	$divAvgCat     = "divavgcat_{$id}";
       	$divAvgExam    = "divavgexam_{$id}";
       	$divGradeCode  = "divgrade_{$id}";
       	$divTotal      = "divtotal_{$id}";
       	$divStatus     = "divstatus_{$id}";
       	
        echo '<tr>';
         echo "<td class=\"input left\" >{$count}</td>";
         echo "<td class=\"input\" >{$admno}</td>";
         echo "<td class=\"input\" >{$fullname}</td>";
         
         if(isset($subject_config['cats']) && sizeof($subject_config['cats'])>0){
         foreach ($subject_config['cats'] as $cat_index => $cat){
         	
          $value = valueof($student,"CAT{$cat_index}");
          $value        = round($value,1);
          $name  = valueof($cat,'name');
          $max   = valueof($cat,'max');
          
       	  $textbox_catid = "cat{$cat_index}_{$id}";
       	  $column = "cat{$cat_index}";
       	  $textbox_cat   = "<input type=\"text\" value=\"{$value}\" id=\"{$textbox_catid}\" size=\"5\" onfocus=\"\" onblur=\"save({$id},'{$column}','{$textbox_catid}','{$max}','{$value}','{$name}','{$divAvgCat}','{$divAvgExam}','{$divTotal}','{$divGradeCode}','{$divStatus}');\"  />";
       	
          echo "<td class=\"input\"  >{$textbox_cat}</td>";
          
          }
         }
         
         echo "<td class=\"input\" ><div id=\"{$divAvgCat}\">{$avgcat}</div></td>";
         
         if(isset($subject_config['papers']) && sizeof($subject_config['papers'])>0){
         foreach ($subject_config['papers'] as $paper_index => $paper){
         	
          $value = valueof($student,"PAPER{$paper_index}");
          $value        = round($value,1);
          $name  = valueof($paper,'name');
          $max   = valueof($paper,'max');
       	  $textbox_paperid = "paper{$paper_index}_{$id}";
       	  $column = "paper{$paper_index}";
       	  $textbox_paper   = "<input type=\"text\" value=\"{$value}\" id=\"{$textbox_paperid}\" size=\"5\" onfocus=\"\" onblur=\"save({$id},'{$column}','{$textbox_paperid}','{$max}','{$value}','{$name}','{$divAvgCat}','{$divAvgExam}','{$divTotal}','{$divGradeCode}','{$divStatus}');\"   />";
       	
          echo "<td class=\"input\" >{$textbox_paper}</td>";
          
          }
         }
         
         echo "<td class=\"input\" ><div id=\"{$divAvgExam}\">{$avgexam}</div></td>";
         echo "<td class=\"input\" ><div id=\"{$divTotal}\">{$total}</div></td>";
         echo "<td class=\"input\" ><div id=\"{$divGradeCode}\">{$gradecode}</div></td>";
//         echo "<td class=\"input\" ><div id=\"{$divStatus}\">&nbsp;</div></td>";
        echo '</tr>';
        
        ++$count;
       }
       
       echo '</tbody>';
      }
      
     echo '</table>';
     