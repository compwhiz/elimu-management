<?php
/**
* auto created config file for modules/students/exams/setup/subjectremarks
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-01-08 07:06:48
*/

$scriptname  = @end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'Subject Remarks';//user-readable formart
 $cfg['appname']       = 'subjectremarks';//lower cased one word
 $cfg['datasrc']       = 'SASUBREMARKS';//where to get data
 $cfg['datatbl']       = 'SASUBREMARKS';//base data src [for updates & deletes]
 $cfg['form_width']    = 400;
 $cfg['form_height']   = 230;
 $cfg['window_width']    = 530;
 $cfg['window_height']   = 400;
 
 $cfg['pkcol']         = 'ID';//the primary key
 
 $cfg['tblbns']['chk_button_add']    = 1;
 $cfg['tblbns']['chk_button_edit']   = 1;
 $cfg['tblbns']['chk_button_delete'] = 1;
 $cfg['tblbns']['chk_button_import'] = 1;
 $cfg['tblbns']['chk_button_export'] = 1;
 

$cfg['columns']['qjia8'] = array(
                      'dbcol'=>'SUBJECTCODE',
                      'title'=>'Subject',
                      'width'=>50,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'SASUBJECTS|SUBJECTCODE|SUBJECTNAME|SUBJECTCODE',
                      );
                      



                      
$cfg['columns']['dszn8'] = array(
                      'dbcol'=>'GRADECODE',
                      'title'=>'Grade',
                      'width'=>50,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'SAXGRD|GRADECODE|GRADECODE|GRADECODE',
                      );
                      



                      
$cfg['columns']['eph8f'] = array(
                      'dbcol'=>'COMMENTS',
                      'title'=>'Comments',
                      'width'=>200,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      $combogrid_array   = array();
	 
$combogrid_array['qjia8']['columns']['SUBJECTCODE']  = array( 'field'=>'SUBJECTCODE', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['qjia8']['columns']['SUBJECTNAME']  = array( 'field'=>'SUBJECTNAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['qjia8']['source'] ='SASUBJECTS';
	 
$combogrid_array['dszn8']['columns']['GRADECODE']  = array( 'field'=>'GRADECODE', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['dszn8']['columns']['GRADECODE']  = array( 'field'=>'GRADECODE', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['dszn8']['source'] ='SAXGRD';
  