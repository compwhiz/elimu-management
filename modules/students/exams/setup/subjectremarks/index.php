<?php

/**
* auto created index file for modules/students/exams/setup/subjectremarks
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-01-08 07:06:48
*/

require_once('cfg.php');
//require_once(BASEPATH.'init.php');

$class     = CLASSFILE;
$classfile = CLASSFILE.'.php';

require_once("{$classfile}");	

 if(!class_exists($class)){
 	die('class--' .$classfile. 'not found');
 }
 
 $_class = new $class();
 $grid   = new grid();
 
 /*
 $module = array();
 $module['url']    =  DIR;
 $module['class']  =  $class;
 $module_packed    =  packvars($module);
 */
 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<?php
 $grid->draw_page_header( );
?>
<body>
<?php
 $grid->draw_grid( $module_packed );
 $grid->draw_form_simple(  );
// $_class->form();
?>
</body>
</html>