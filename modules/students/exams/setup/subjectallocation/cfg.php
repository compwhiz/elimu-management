<?php
/**
* auto created config file for modules/students/exams/setup/subjectallocation
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-01-19 10:17:29
*/

$scriptname  = @end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/';

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter);

$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] );

 $cfg                  = array();
 $cfg['apptitle']      = 'Subject Allocation';//user-readable formart
 $cfg['appname']       = 'subjectallocation';//lower cased one word
 $cfg['datasrc']       = 'VIEWTSA';//where to get data
 $cfg['datatbl']       = 'SATSA';//base data src [for updates & deletes]
 $cfg['form_width']    = 500;
 $cfg['form_height']   = 250;
 $cfg['window_width']    = 600;
 $cfg['window_height']   = 410;

 $cfg['pkcol']         = 'TCHNO';//the primary key

 $cfg['tblbns']['chk_button_add']    = 1;
 $cfg['tblbns']['chk_button_edit']   = 1;
 $cfg['tblbns']['chk_button_delete'] = 1;
 $cfg['tblbns']['chk_button_import'] = 1;
 $cfg['tblbns']['chk_button_export'] = 1;


$cfg['columns']['jkmfb'] = array(
                      'dbcol'=>'SLTCODE',
                      'title'=>'Saluation',
                      'width'=>70,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>4,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );





$cfg['columns']['eb4xs'] = array(
                      'dbcol'=>'TCHNO',
                      'title'=>'Teacher No',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'combogrid',
                      'selectsrc'=> 'SATEACHERS|TCHNO|FULLNAME|TCHNO',
                      );





$cfg['columns']['6pywd'] = array(
                      'dbcol'=>'FULLNAME',
                      'title'=>'Teacher Name',
                      'width'=>140,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>2,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );





$cfg['columns']['vaxeo'] = array(
                      'dbcol'=>'SUBJECTCODE',
                      'title'=>'Subject',
                      'width'=>80,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>3,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'combogrid',
                      'selectsrc'=> 'SASUBJECTS|SUBJECTCODE|SUBJECTNAME|SUBJECTCODE',
                      );





$cfg['columns']['jbigm'] = array(
                      'dbcol'=>'SUBJECTNAME',
                      'title'=>'Subject',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>2,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );





$cfg['columns']['wacoj'] = array(
                      'dbcol'=>'SHORTNAME',
                      'title'=>'Short Name',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>4,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );





$cfg['columns']['uxnyi'] = array(
                      'dbcol'=>'CODEOFFC',
                      'title'=>'Subject Code Off.',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>4,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );





$cfg['columns']['bqg9v'] = array(
                      'dbcol'=>'STREAMCODE',
                      'title'=>'Stream',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>3,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'SASTREAMS|STREAMCODE|STREAMNAME|STREAMCODE',
                      );





$cfg['columns']['mjec1'] = array(
                      'dbcol'=>'STREAMNAME',
                      'title'=>'Stream',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>2,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );





$cfg['columns']['k1mwz'] = array(
                      'dbcol'=>'FORMCODE',
                      'title'=>'Form',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>4,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );





$cfg['columns']['pjcky'] = array(
                      'dbcol'=>'YEARCODE',
                      'title'=>'Year',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'SAYEAR|YEARCODE|YEARNAME|YEARCODE',
                      );




                      $combogrid_array   = array();

$combogrid_array['eb4xs']['columns']['TCHNO']  = array( 'field'=>'TCHNO', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['eb4xs']['columns']['FULLNAME']  = array( 'field'=>'FULLNAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['eb4xs']['source'] ='SATEACHERS';

$combogrid_array['vaxeo']['columns']['SUBJECTCODE']  = array( 'field'=>'SUBJECTCODE', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['vaxeo']['columns']['SUBJECTNAME']  = array( 'field'=>'SUBJECTNAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['vaxeo']['source'] ='SASUBJECTS';

$combogrid_array['bqg9v']['columns']['STREAMCODE']  = array( 'field'=>'STREAMCODE', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['bqg9v']['columns']['STREAMNAME']  = array( 'field'=>'STREAMNAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['bqg9v']['source'] ='SASTREAMS';

$combogrid_array['pjcky']['columns']['YEARCODE']  = array( 'field'=>'YEARCODE', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['pjcky']['columns']['YEARNAME']  = array( 'field'=>'YEARNAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['pjcky']['source'] ='SAYEAR';
