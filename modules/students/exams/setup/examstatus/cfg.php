<?php
/**
* auto created config file for modules/students/setup/examstatus
* @author kenmsh@gmail.com
* 
* @version 2.0
* @since 2016-01-07 16:34:21
*/

$scriptname  = @end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'Exam Status';//user-readable formart
 $cfg['appname']       = 'examstatus';//lower cased one word
 $cfg['datasrc']       = 'SAXSTS';//where to get data
 $cfg['datatbl']       = 'SAXSTS';//base data src [for updates & deletes]
 $cfg['form_width']    = 400;
 $cfg['form_height']   = 200;
 $cfg['window_width']    = 500;
 $cfg['window_height']   = 400;
 
 $cfg['pkcol']         = 'STATUSCODE';//the primary key
 
 $cfg['tblbns']['chk_button_add']    = 1;
 $cfg['tblbns']['chk_button_edit']   = 1;
 $cfg['tblbns']['chk_button_delete'] = 1;
 $cfg['tblbns']['chk_button_import'] = 1;
 $cfg['tblbns']['chk_button_export'] = 1;
 

$cfg['columns']['j2rsm'] = array(
                      'dbcol'=>'STATUSCODE',
                      'title'=>'Code',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['earfd'] = array(
                      'dbcol'=>'STATUSNAME',
                      'title'=>'Name',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['xfolf'] = array(
                      'dbcol'=>'GRADED',
                      'title'=>'Graded',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'SAYN|YNCODE|YNNAME|YNCODE',
                      );
                      



                      $combogrid_array   = array();
	 
$combogrid_array['xfolf']['columns']['YNCODE']  = array( 'field'=>'YNCODE', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['xfolf']['columns']['YNNAME']  = array( 'field'=>'YNNAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['xfolf']['source'] ='SAYN';
  