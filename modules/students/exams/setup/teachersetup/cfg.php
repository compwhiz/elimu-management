<?php
/**
* auto created config file for modules/students/exams/setup/teachersetup
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-01-08 07:12:01
*/

$scriptname  = @end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'Teacher Setup';//user-readable formart
 $cfg['appname']       = 'teachersetup';//lower cased one word
 $cfg['datasrc']       = 'SATSS';//where to get data
 $cfg['datatbl']       = 'SATSS';//base data src [for updates & deletes]
 $cfg['form_width']    = 300;
 $cfg['form_height']   = 600;
 $cfg['window_width']    = 500;
 $cfg['window_height']   = 400;
 
 $cfg['pkcol']         = 'SUBJECTCODE';//the primary key
 
 $cfg['tblbns']['chk_button_add']    = 1;
 $cfg['tblbns']['chk_button_edit']   = 1;
 $cfg['tblbns']['chk_button_delete'] = 1;
 $cfg['tblbns']['chk_button_import'] = 0;
 $cfg['tblbns']['chk_button_export'] = 0;
 

$cfg['columns']['ljfzp'] = array(
                      'dbcol'=>'TCHNO',
                      'title'=>'Teacher',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'combogrid',
                      'selectsrc'=> 'SATEACHERS|TCHNO|FULLNAME|TCHNO',
                      );
                      



                      
$cfg['columns']['soemd'] = array(
                      'dbcol'=>'SUBJECTCODE',
                      'title'=>'Subject',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'SASUBJECTS|SUBJECTCODE|SUBJECTNAME|SUBJECTCODE',
                      );
                      



                      
$cfg['columns']['fw70m'] = array(
                      'dbcol'=>'STREAMCODE',
                      'title'=>'Stream',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'SASTREAMS|STREAMCODE|STREAMNAME|STREAMCODE',
                      );
                      



                      
$cfg['columns']['8bmox'] = array(
                      'dbcol'=>'YEARCODE',
                      'title'=>'Year',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'SAYEAR|YEARCODE|YEARNAME|YEARCODE',
                      );
                      



                      
$cfg['columns']['jlig8'] = array(
                      'dbcol'=>'TERMCODE',
                      'title'=>'Term',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'SATERMS|TERMCODE|TERMNAME|TERMCODE',
                      );
                      



                      
$cfg['columns']['emnry'] = array(
                      'dbcol'=>'CATMAX1',
                      'title'=>'Catmax1',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['pkzjh'] = array(
                      'dbcol'=>'CATMAX2',
                      'title'=>'Catmax2',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['97omn'] = array(
                      'dbcol'=>'CATMAX3',
                      'title'=>'Catmax3',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['a3zhg'] = array(
                      'dbcol'=>'CATMAX4',
                      'title'=>'Catmax4',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['rpxps'] = array(
                      'dbcol'=>'CATMAX5',
                      'title'=>'Catmax5',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['sumkq'] = array(
                      'dbcol'=>'CONTC',
                      'title'=>'Contc',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['dtyqi'] = array(
                      'dbcol'=>'EXAMMAX1',
                      'title'=>'Exammax1',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['sozj1'] = array(
                      'dbcol'=>'EXAMMAX2',
                      'title'=>'Exammax2',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['xvyms'] = array(
                      'dbcol'=>'EXAMMAX3',
                      'title'=>'Exammax3',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['4qjvf'] = array(
                      'dbcol'=>'EXAMMAX4',
                      'title'=>'Exammax4',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['onfyc'] = array(
                      'dbcol'=>'EXAMMAX5',
                      'title'=>'Exammax5',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['venit'] = array(
                      'dbcol'=>'CONTE',
                      'title'=>'Conte',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['zpw1r'] = array(
                      'dbcol'=>'CATNAME1',
                      'title'=>'Name',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['1c849'] = array(
                      'dbcol'=>'CATNAME2',
                      'title'=>'Name',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['so89z'] = array(
                      'dbcol'=>'CATNAME3',
                      'title'=>'Name',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['9dfuv'] = array(
                      'dbcol'=>'CATNAME4',
                      'title'=>'Name',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['mgzl6'] = array(
                      'dbcol'=>'CATNAME5',
                      'title'=>'Name',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['xrdc5'] = array(
                      'dbcol'=>'EXAMNAME1',
                      'title'=>'Name',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['lnihf'] = array(
                      'dbcol'=>'EXAMNAME2',
                      'title'=>'Name',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['ik7op'] = array(
                      'dbcol'=>'EXAMNAME3',
                      'title'=>'Name',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['rjgs0'] = array(
                      'dbcol'=>'EXAMNAME4',
                      'title'=>'Name',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['2aetv'] = array(
                      'dbcol'=>'EXAMNAME5',
                      'title'=>'Name',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['brupw'] = array(
                      'dbcol'=>'CATMUTE1',
                      'title'=>'Catmute1',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['kc8ae'] = array(
                      'dbcol'=>'CATMUTE2',
                      'title'=>'Catmute2',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>4,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['hyiac'] = array(
                      'dbcol'=>'CATMUTE3',
                      'title'=>'Catmute3',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>4,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['orhg7'] = array(
                      'dbcol'=>'CATMUTE4',
                      'title'=>'Catmute4',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>4,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['4fex3'] = array(
                      'dbcol'=>'CATMUTE5',
                      'title'=>'Catmute5',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>4,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['2vqpg'] = array(
                      'dbcol'=>'EXAMMUTE1',
                      'title'=>'Exammute1',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>4,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['way0j'] = array(
                      'dbcol'=>'EXAMMUTE2',
                      'title'=>'Exammute2',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>4,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['fnh5s'] = array(
                      'dbcol'=>'EXAMMUTE3',
                      'title'=>'Exammute3',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>4,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['bq8gw'] = array(
                      'dbcol'=>'EXAMMUTE4',
                      'title'=>'Exammute4',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>4,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['q7b9a'] = array(
                      'dbcol'=>'EXAMMUTE5',
                      'title'=>'Exammute5',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>4,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      $combogrid_array   = array();
	 
$combogrid_array['ljfzp']['columns']['TCHNO']  = array( 'field'=>'TCHNO', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['ljfzp']['columns']['FULLNAME']  = array( 'field'=>'FULLNAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['ljfzp']['source'] ='SATEACHERS';
	 
$combogrid_array['soemd']['columns']['SUBJECTCODE']  = array( 'field'=>'SUBJECTCODE', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['soemd']['columns']['SUBJECTNAME']  = array( 'field'=>'SUBJECTNAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['soemd']['source'] ='SASUBJECTS';
	 
$combogrid_array['fw70m']['columns']['STREAMCODE']  = array( 'field'=>'STREAMCODE', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['fw70m']['columns']['STREAMNAME']  = array( 'field'=>'STREAMNAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['fw70m']['source'] ='SASTREAMS';
	 
$combogrid_array['8bmox']['columns']['YEARCODE']  = array( 'field'=>'YEARCODE', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['8bmox']['columns']['YEARNAME']  = array( 'field'=>'YEARNAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['8bmox']['source'] ='SAYEAR';
	 
$combogrid_array['jlig8']['columns']['TERMCODE']  = array( 'field'=>'TERMCODE', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['jlig8']['columns']['TERMNAME']  = array( 'field'=>'TERMNAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['jlig8']['source'] ='SATERMS';
  