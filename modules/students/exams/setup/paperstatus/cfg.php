<?php
/**
* auto created config file for modules/students/exams/setup/paperstatus
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-01-16 14:28:02
*/

$scriptname  = @end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'Paper Status';//user-readable formart
 $cfg['appname']       = 'paperstatus';//lower cased one word
 $cfg['datasrc']       = 'SAXFMTST';//where to get data
 $cfg['datatbl']       = 'SAXFMTST';//base data src [for updates & deletes]
 $cfg['form_width']    = 400;
 $cfg['form_height']   = 250;
 $cfg['window_width']    = 500;
 $cfg['window_height']   = 400;
 
 $cfg['pkcol']         = 'FORMCODE';//the primary key
 
 $cfg['tblbns']['chk_button_add']    = 1;
 $cfg['tblbns']['chk_button_edit']   = 1;
 $cfg['tblbns']['chk_button_delete'] = 1;
 $cfg['tblbns']['chk_button_import'] = 1;
 $cfg['tblbns']['chk_button_export'] = 1;
 

$cfg['columns']['zcxgu'] = array(
                      'dbcol'=>'FORMCODE',
                      'title'=>'Form',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'SAFORMS|FORMCODE|FORMNAME|FORMCODE',
                      );
                      



                      
$cfg['columns']['gjp2w'] = array(
                      'dbcol'=>'PAPERSFROM',
                      'title'=>'From',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['zdu4c'] = array(
                      'dbcol'=>'PAPERSTO',
                      'title'=>'To',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['y2x4b'] = array(
                      'dbcol'=>'STATUSCODE',
                      'title'=>'Status',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      $combogrid_array   = array();
	 
$combogrid_array['zcxgu']['columns']['FORMCODE']  = array( 'field'=>'FORMCODE', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['zcxgu']['columns']['FORMNAME']  = array( 'field'=>'FORMNAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['zcxgu']['source'] ='SAFORMS';
  