<?php
/**
* auto created config file for modules/students/setup/globalsetup
* @author coderX
* @todo by-me-beer
* @version 1.0
* @since 2014-09-13 16:29:29
*/

$scriptname  = @end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'Global Setup';//user-readable formart
 $cfg['appname']       = 'globalsetup';//lower cased one word
 $cfg['datasrc']       = 'SATSSG';//where to get data
 $cfg['datatbl']       = 'SATSSG';//base data src [for updates & deletes]
 $cfg['form_width']    = 550;
 $cfg['form_height']   = 200;
 
 $cfg['pkcol']         = 'SUBJECTCODE';//the primary key
 
 $cfg['tblbns']['chk_button_add']    = 1;
 $cfg['tblbns']['chk_button_edit']   = 1;
 $cfg['tblbns']['chk_button_delete'] = 1;
 $cfg['tblbns']['chk_button_import'] = 0;
 $cfg['tblbns']['chk_button_export'] = 0;
 
                      
$cfg['columns']['hqhka'] = array(
                      'dbcol'=>'FORMCODE',
                      'title'=>'Form',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      
$cfg['columns']['w01lm'] = array(
                      'dbcol'=>'YEARCODE',
                      'title'=>'Year',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      
$cfg['columns']['qds6j'] = array(
                      'dbcol'=>'TERMCODE',
                      'title'=>'Term',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      
