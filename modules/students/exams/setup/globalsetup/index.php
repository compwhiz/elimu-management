<?php


?>

<style>
    table#tblSubSetup {
        font-size: 12px;
        font-family: Verdana
    }

    table#tblSubSetup td.header {
        background-color: #F0F7EA;
        border-bottom: 1px solid #999;
    }

    table#tblSubSetup td.label {
        background-color: #95B8E7;
    }

    table#tblSubSetup td.cat {
        background-color: #E2EDFF;
    }

    table#tblSubSetup td.exam {
        background-color: #FFE48D;
    }

    input.masked {
        border: 1px solid #F0F7EA;
        background-color: #999;
    }
</style>
<?php

if (isset($cfg['columns'])) {
    if (sizeof($cfg['columns']) > 0) {
        foreach ($cfg['columns'] as $col => $col_properties) {

            $visible = valueof($col_properties, 'visible', 1);
            $title = valueof($col_properties, 'title', $col);
            $width = valueof($col_properties, 'width', 50);
            $width = is_numeric($width) ? $width : 50;

            if ($visible == GRIDCOL_VISIBLE_GRIDONLY || $visible == GRIDCOL_VISIBLE_ALL) {
                $Headers[$col] = array('title' => $title, 'width' => $width);
            }

        }
    }
}

$form_width = isset($cfg['form_width']) ? $cfg['form_width'] : 500;
$form_height = isset($cfg['form_height']) ? $cfg['form_height'] : 200;

?>

<table id="dg<?php echo MNUID; ?>" style="width:<?php echo $form_width; ?>px;height:364px">
    <thead>
    <tr>
        <?php
        if (sizeof($Headers) > 0) {
            foreach ($Headers as $code => $properties) {

                $width = valueof($properties, 'width');
                $title = valueof($properties, 'title');

                echo "<th field=\"{$code}\" width=\"{$width}\" sortable=\"true\">{$title}</th>\r\n";
            }
        }
        ?>
    </tr>
    </thead>
</table>

<div id="toolbar<?php echo MNUID; ?>">
    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true"
       onclick="<?php echo $cfg['appname']; ?>.add()">New</a>
    <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true"
       onclick="<?php echo $cfg['appname']; ?>.edit()">Edit</a>
    <a href="#" class="easyui-linkbutton" iconCls="icon-redo" plain="true"
       onclick="<?php echo $cfg['appname']; ?>.copy()">Copy</a>
    <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true"
       onclick="<?php echo $cfg['appname']; ?>.remove()">Remove</a>
</div>

<div id="dlg<?php echo MNUID; ?>" class="easyui-dialog" style="width:650px;height:360px;padding:2px" closed="true"
     buttons="#dlg<?php echo MNUID; ?>-buttons">
    <?php
    $setup = array();
    $saved = sizeof($setup) > 0 ? true : false;

    $cats = 4;
    $exams = 3;

    $all = $cats + $exams;

    echo '<form id="frmSubSetup' . MNUID . '" name="frmSubSetup' . MNUID . '"  method="POST"  enctype="multipart/form-data" onsubmit="return false;"  > ';
    echo "<table id=\"tblSubSetup\" width=\"100%\" border=\"0\" cellpadding=\"4\"  cellspacing=\"0\"  >";

    echo "<tr>";
    echo "<td colspan=\"" . ($all + 1) . "\" class=\"header\">";

    echo "<table  width=\"100%\" border=\"0\" cellpadding=\"4\"  cellspacing=\"0\" >";
    echo "<tr>";
    echo "<td >Form</td>";
    echo "<td >" . dropdown::form('xgs_form') . "</td>";
    echo "<td >Grading</td>";
    echo "<td >" . dropdown::grading('xgs_grading') . "</td>";
    echo "</tr>";

    echo "<tr>";
    echo "<td >Year</td>";
    echo "<td >" . dropdown::year('xgs_year', date('Y')) . "</td>";
    echo "<td >Term</td>";
    echo "<td >" . dropdown::term('xgs_term') . "</td>";
    echo "</tr>";

    echo "<tr>";
    echo "<td >No.of Subjects</td>";
    echo "<td>" . ui::form_input('text', 'xgs_numsubjects', 5, '', '', '', ' data-options="required:true, increment:1,min:0,max:15" ', '', 'easyui-numberspinner easyui-validatebox', "") . "</td>";
    echo "<td >Minimum Subjects</td>";
    echo "<td>" . ui::form_input('text', 'xgs_minsubjects', 5, '', '', '', ' data-options="required:true, increment:1,min:0,max:15" ', '', 'easyui-numberspinner easyui-validatebox', "") . "</td>";
    echo "</tr>";

    echo "</table>";
    echo "</td>";

    echo "<tr>";

    echo "<td class=\"label\">&nbsp;</td>";

    for ($c = 1; $c <= $cats; ++$c) {
        echo "<td class=\"cat\">Cat{$c}</td>";
    }

    for ($c = 1; $c <= $exams; ++$c) {
        echo "<td class=\"exam\" style=\"padding:2px;\">Exam{$c}</td>";
    }

    echo "</tr>";

    echo "<tr>";

    //-----------------------------------max
    echo "<td  class=\"left label input\">Out of </td>";

    for ($c = 1; $c <= $cats; ++$c) {
        $input_id = "xgs_cat[{$c}]";
        $input_value = valueof($setup, "CATMAX{$c}");
        echo "<td  class=\"input cat\" style=\"padding:2px;\"><input  class=\"easyui-numberspinner\" type=\"text\" id=\"{$input_id}\"  name=\"{$input_id}\" style=\"width:60px;\" value=\"{$input_value}\" data-options=\"increment:1,min:0,max:500\"></input></td>";
    }

    for ($c = 1; $c <= $exams; ++$c) {
        $input_id = "xgs_exam[{$c}]";
        $input_value = valueof($setup, "EXAMMAX{$c}");
        echo "<td class=\"input exam\" style=\"padding:2px;\"><input  class=\"easyui-numberspinner\" type=\"text\" id=\"{$input_id}\"  name=\"{$input_id}\" style=\"width:60px;\" value=\"{$input_value}\"  data-options=\"increment:1,min:0,max:500\"></input></td>";
    }

    echo "</tr>";

    //-----------------------------------title
    echo "<td  class=\"left label input\">Title</td>";

    for ($c = 1; $c <= $cats; ++$c) {
        $input_id = "xgs_title_cat[{$c}]";
        $input_value = valueof($setup, "CATNAME{$c}");
        echo "<td class=\"input cat\" style=\"padding:2px;\"><input  class=\"textbox\" type=\"text\" id=\"{$input_id}\"  name=\"{$input_id}\" style=\"width:65px;\" value=\"{$input_value}\" ></input></td>";
    }

    for ($c = 1; $c <= $exams; ++$c) {
        $input_id = "xgs_title_exam[{$c}]";
        $input_value = valueof($setup, "EXAMNAME{$c}");
        echo "<td class=\"input exam\" style=\"padding:2px;\"><input  class=\"textbox\" type=\"text\" id=\"{$input_id}\"  name=\"{$input_id}\" style=\"width:65px;\" value=\"{$input_value}\" ></input></td>";
    }

    echo "</tr>";

    //-----------------------------------mute
    echo "<td  class=\"left label input\">Mute</td>";

    for ($c = 1; $c <= $cats; ++$c) {
        $input_id = "xgs_mute_cat[{$c}]";
        $input_value = valueof($setup, "CATMUTE{$c}", 0);
        $checked = $input_value == 1 ? 'checked' : '';
        echo "<td class=\"input cat\" style=\"padding:2px;\"><input type=\"checkbox\" id=\"{$input_id}\"  name=\"{$input_id}\" value=\"1\"  {$checked} ></td>";
    }

    for ($c = 1; $c <= $exams; ++$c) {
        $input_id = "xgs_mute_exam[{$c}]";
        $input_value = valueof($setup, "EXAMMUTE{$c}", 0);
        $checked = $input_value == 1 ? 'checked' : '';
        echo "<td class=\"input exam\" style=\"padding:2px;\"><input type=\"checkbox\" id=\"{$input_id}\"  name=\"{$input_id}\" value=\"1\" {$checked}  ></td>";
    }

    echo "</tr>";

    echo "<tr>";
    echo "<td  class=\"left label input\">Contributions</td>";
    echo "<td  class=\"input cat\">" . ui::numbers_select('xgs_contc_global', 0, 100, 10, valueof($setup, 'CONTC')) . "</td>";
    echo "<td colspan='3' class=\"input cat\">" . ui::numbers_select('xgs_second_contc_global', 0, 100, 10, valueof($setup, 'CONTC')) . "%</td>";
    echo "<td colspan=\"{$exams}\" class=\"input exam\">" . ui::numbers_select('xgs_conte_global', 0, 100, 10, valueof($setup, 'CONTE')) . "%</td>";
    echo "</tr>";

    echo "<tr>";
    echo "<td>&nbsp;</td>";
    echo "<td colspan=\"{$all}\" >&nbsp;</td>";
    echo "</tr>";

    echo "</table>";
    echo "</form>";
    ?>
</div>

<div id="dlg<?php echo MNUID; ?>-buttons">
    <a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="<?php echo $cfg['appname']; ?>.save();">Save</a>
    <a href="#" class="easyui-linkbutton" iconCls="icon-cancel"
       onclick="javascript:$('#dlg<?php echo MNUID; ?>').dialog('close')">Cancel</a>
</div>

<div id="dlg_copy<?php echo MNUID; ?>" class="easyui-dialog" style="width:400px;height:250px;padding:2px" closed="true"
     buttons="#dlg_copy<?php echo MNUID; ?>-buttons">
    <?php
    echo '<form id="frmSubSetupCopy' . MNUID . '" name="frmSubSetup' . MNUID . '"  method="POST"  enctype="multipart/form-data" onsubmit="return false;"  > ';
    echo "<table id=\"\" width=\"100%\" border=\"0\" cellpadding=\"4\"  cellspacing=\"0\"  >";

    echo "<tr>";
    echo "<td colspan=\"" . ($all + 1) . "\" class=\"header\">";

    echo "<table  width=\"100%\" border=\"0\" cellpadding=\"4\"  cellspacing=\"0\" >";

    echo "<tr>";
    echo "<td >&nbsp;</td>";
    echo "<td >Copy From</td>";
    echo "<td >Copy To</td>";
    echo "</tr>";

    echo "<tr>";
    echo "<td >Form</td>";
    echo "<td ><input type=\"text\" class=\"masked\"  id=\"xgs_form\"  name=\"xgs_form\" style=\"width:65px;\"  readonly></input></td>";
    //echo "<td ><input type=\"text\" class=\"masked\"  id=\"xgs_form_to\"  name=\"xgs_form_to\" style=\"width:65px;\"  readonly></input></td>";
    echo "<td >" . dropdown::form('xgs_form_to') . "</td>";
    echo "</tr>";

    echo "<tr>";
    echo "<td >Year</td>";
    echo "<td ><input type=\"text\" class=\"masked\"  id=\"xgs_year\"  name=\"xgs_year\" style=\"width:65px;\"  readonly></input></td>";
    echo "<td >" . dropdown::year('xgs_year_to', date('Y')) . "</td>";
    echo "</tr>";

    echo "<tr>";
    echo "<td >Term</td>";
    echo "<td ><input type=\"text\" class=\"masked\"  id=\"xgs_term\"  name=\"xgs_term\" style=\"width:65px;\"  readonly></input></td>";
    echo "<td >" . dropdown::term('xgs_term_to') . "</td>";
    echo "</tr>";

    echo "</table>";
    echo "</form>";

    ?>
</div>

<div id="dlg_copy<?php echo MNUID; ?>-buttons">
    <a href="#" class="easyui-linkbutton" iconCls="icon-ok"
       onclick="<?php echo $cfg['appname']; ?>.save_copy();">Save</a>
    <a href="#" class="easyui-linkbutton" iconCls="icon-cancel"
       onclick="javascript:$('#dlg_copy<?php echo MNUID; ?>').dialog('close')">Cancel</a>
</div>

<script>

    $(function () {
        var dg<?php echo MNUID; ?> = $('#dg<?php echo MNUID; ?>').datagrid({
            url: './endpoints/grid/',
            pagination: true,
            rownumbers: true,
            fitColumns: true,
            singleSelect: true,
            idField: 'id',
            toolbar: '#toolbar<?php echo MNUID; ?>',
            queryParams: {modvars: '<?php echo $vars; ?>', function: 'data'},
            remoteFilter: true,
            multiSort: true,
            fit: true,
            type: 'post',
            filterBtnIconCls: 'icon-filter',
        });
        dg<?php echo MNUID; ?>.datagrid('enableFilter');
    });


    var <?php echo $cfg['appname']; ?> =
    {
        add :function () {
            $('#dlg<?php echo MNUID; ?>').dialog({
                title: 'New <?php echo valueof($cfg, 'appname'); ?>',
                closed: false,
                resizable: true,
                modal: true,
            });

            $('#frmSubSetup<?php echo MNUID; ?>').form('clear');
        }
    ,
        edit:function () {
            var row = $('#dg<?php echo MNUID; ?>').datagrid('getSelected');
            if (row) {
                $('#dlg<?php echo MNUID; ?>').dialog('open').dialog('setTitle', 'Edit <?php echo valueof($cfg, 'appname'); ?>');
                var id = row.id;
                $('#frmSubSetup<?php echo MNUID; ?>').form('load', './endpoints/crud/?modvars=<?php echo $vars; ?>&function=data&id=' + id);
            }
        }
    ,
        copy:function () {
            var row = $('#dg<?php echo MNUID; ?>').datagrid('getSelected');
            if (row) {
                $('#dlg_copy<?php echo MNUID; ?>').dialog('open').dialog('setTitle', 'Copy Global Setup');
                var id = row.id;
                $('#frmSubSetupCopy<?php echo MNUID; ?>').form('load', './endpoints/crud/?modvars=<?php echo $vars; ?>&function=data_copy&id=' + id);
            }
        }
    ,
        save:function () {
            var fdata = $('#frmSubSetup<?php echo MNUID; ?>').serialize() + '&modvars=<?php echo $vars; ?>&function=save';

            console.log(fdata);
            $.post("./endpoints/crud/", fdata, function (data) {
                if (data.success === 1) {
                    $.messager.show({title: 'Success', msg: data.message});
                    $('#dg<?php echo MNUID; ?>').datagrid('reload');
                } else {
                    $("#btnSave").removeAttr("disabled");
                    $.messager.alert('Settings Save', data.message, 'error');
                }
            }, "json");

        }
    ,
        save_copy:function () {
            var fdata = $('#frmSubSetupCopy<?php echo MNUID; ?>').serialize() + '&modvars=<?php echo $vars; ?>&function=save_copy';
            $.post("./endpoints/crud/", fdata, function (data) {
                if (data.success === 1) {
                    $.messager.show({title: 'Success', msg: data.message});
                    $('#dg<?php echo MNUID; ?>').datagrid('reload');
                    $('#dlg_copy<?php echo MNUID; ?>').dialog('close');
                } else {
                    $("#btnSave").removeAttr("disabled");
                    $.messager.alert('Copy Settings', data.message, 'error');
                }
            }, "json");

        }
    ,
        remove:function () {
            var row = $('#dg<?php echo MNUID; ?>').datagrid('getSelected');
            if (row) {
                $.messager.confirm('Confirm', 'Are you sure you want to remove this <?php echo valueof($cfg, 'appname'); ?>?', function (r) {
                    if (r) {

                        var rdata = 'id=' + row.id + '&modvars=<?php echo $vars; ?>&function=remove';
                        $.post('./endpoints/crud/', rdata, function (data) {

                            if (data.success === 1) {
                                $('#dlg<?php echo MNUID; ?>').dialog('close');
                                $('#dg<?php echo MNUID; ?>').datagrid('reload');
                            } else {
                                $.messager.alert('Error', data.message, 'error');
                            }
                        }, "json");
                    }
                });
            }
        }
    }
</script>
