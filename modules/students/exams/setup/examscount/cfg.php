<?php
/**
* auto created config file for modules/students/exams/setup/examscount
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-01-11 10:07:14
*/

$scriptname  = @end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'Exams Count';//user-readable formart
 $cfg['appname']       = 'examscount';//lower cased one word
 $cfg['datasrc']       = 'SAXPF';//where to get data
 $cfg['datatbl']       = 'SAXPF';//base data src [for updates & deletes]
 $cfg['form_width']    = 400;
 $cfg['form_height']   = 220;
 $cfg['window_width']    = 510;
 $cfg['window_height']   = 401;
 
 $cfg['pkcol']         = 'FORMCODE';//the primary key
 
 $cfg['tblbns']['chk_button_add']    = 1;
 $cfg['tblbns']['chk_button_edit']   = 1;
 $cfg['tblbns']['chk_button_delete'] = 1;
 $cfg['tblbns']['chk_button_import'] = 1;
 $cfg['tblbns']['chk_button_export'] = 1;
 

$cfg['columns']['foazi'] = array(
                      'dbcol'=>'FORMCODE',
                      'title'=>'Form',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'SAFORMS|FORMCODE|FORMNAME|FORMCODE',
                      );
                      



                      
$cfg['columns']['lotks'] = array(
                      'dbcol'=>'SUBJECTCODE',
                      'title'=>'Subject',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'SASUBJECTS|SUBJECTCODE|SUBJECTNAME|SUBJECTCODE',
                      );
                      



                      
$cfg['columns']['aldzt'] = array(
                      'dbcol'=>'NUMCATS',
                      'title'=>'Num CATS',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'numberspinner',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['du0rp'] = array(
                      'dbcol'=>'NUMEXAMS',
                      'title'=>'Num EXAMS',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'numberspinner',
                      'selectsrc'=> '',
                      );
                      



                      $combogrid_array   = array();
	 
$combogrid_array['foazi']['columns']['FORMCODE']  = array( 'field'=>'FORMCODE', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['foazi']['columns']['FORMNAME']  = array( 'field'=>'FORMNAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['foazi']['source'] ='SAFORMS';
	 
$combogrid_array['lotks']['columns']['SUBJECTCODE']  = array( 'field'=>'SUBJECTCODE', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['lotks']['columns']['SUBJECTNAME']  = array( 'field'=>'SUBJECTNAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['lotks']['source'] ='SASUBJECTS';
  