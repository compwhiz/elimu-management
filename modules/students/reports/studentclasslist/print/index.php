<?php

//echo '<pre>';
// print_r($_GET);
//echo '</pre>';
//$db->debug=1;
$dateNow     = textDate(null, true);
$yearcode    = filter_input(INPUT_GET , 'year');
$termcode    = filter_input(INPUT_GET , 'term');
$formstream  = filter_input(INPUT_GET , 'form');
//$subjectcode = filter_input(INPUT_GET , 'subject');
//$subjectname =  $db->GetOne("SELECT SUBJECTNAME FROM SASUBJECTS WHERE SUBJECTCODE='{$subjectcode}'");

if(strlen($formstream)>1){
 $formcode            = $formstream;
 $formstream_code_col = "STREAMCODE";
}else{
 $formcode            = substr($formstream,0,1);
 $formstream_code_col = "FORMCODE";
 }

$school         =  $db->CacheGetRow(1200,"SELECT * FROM SASCHOOL");
$schooladdr     =  valueof($school, 'SCHADDRESS');
$schoolname     =  valueof($school, 'SCHNAME');
$schooltel      =  valueof($school, 'SCHTEL');
$schoolmotto    =  valueof($school, 'SCHMOTTO');
$schoolvision   =  valueof($school, 'SCHVISION');
$schoollogo     =  valueof($school, 'SCHLOGO','../../public/images/logo.gif');

//echo "\$yearcode={$yearcode} <br>";
//echo "\$termcode={$termcode} <br>";
//echo "\$formstream={$formstream} <br>";
//echo "\$formstream_code_col={$formstream_code_col} <br>";
//echo "\$formcode={$formcode} <br>";
//echo "\$subjectcode={$subjectcode} <br>";
//echo "\$subjectname={$subjectname} <br>";
//echo "\$schoolvision={$schoolvision} <br>";
//echo "\$schoollogo={$schoollogo} <br>";
//exit();
 
$subject_grades = array();

$data  = $db->Execute("
        SELECT  DISTINCT ADMNO,FULLNAME FROM VIEWSTUDENTSUBJECTS 
        WHERE YEARCODE='{$yearcode}'
        AND TERMCODE='{$termcode}'       
        AND {$formstream_code_col}='{$formstream}' 
        ORDER BY STREAMCODE, ADMNO
        ");

//print_pre($data);
//exit();

 ?>
 <html>
  <head>
   <title>Grade Analysis</title>
  <script language="JavaScript" type="text/javascript">
    setTimeout("window.print();", 10000);
</script>
<style>
 body {
 padding : 10px;
 margin : 10px;
 font-size:12px;
 }
    table {
        font-family:Verdana;
        font-size:12px;
        empty-cells: show;
        border:1px solid #000;
        border-collapse:collapse;
        border-spacing: 0.5rem;
        empty-cells:show;
    }

    td {
        border:1px solid #000;
    }

    td.abottom {
        vertical-align:bottom;
        font-size:10px;
    }

    td.bold {
        font-weight:bold;
    }

    span.title{
     font-size:14px;
     font-weight:bold;
    }
    
    @media all {
        .page-break  { display: none; }
    }

    @media print {
        .page-break  { 
            display: block; 
            page-break-before: always;
            margin:0px;
            padding:0px;
        }
    }

    @media screen {
        .page-break  { 
            display: block; 
            page-break-before: always;
            margin:5px;
            padding:5px;
        }
    }

</style>

  </head>
 <body>
 <?php
 if($data) {
 	
 	 	  echo "
 	 	  <table width=\"100%\" border=0 cellspacing=\"0\" cellpadding=\"3\" class=\"data\">
 	 	  <tr>
 	 	   <td colspan=\"4\">
 	 	 
 	 	   <table width=\"100%\" style=\"border:0;\" cellspacing=\"0\" cellpadding=\"1\" class=\"data\">
 	 	   
 	 	    <tr>
 	 	     <td rowspan=\"6\" style=\"border:0;\" ><img src=\"{$schoollogo}\" ></td>
 	 	    </tr>
 	 	    
 	 	    <tr>
 	 	     <td valign=\"top\"  colspan=\"2\" style=\"border:0;\" ><span class=\"title\">{$schoolname}</span></td>
 	 	    </tr>
 	 	    
 	 	    <tr>
 	 	     <td  colspan=\"2\" style=\"border:0;\" ><b>Address : {$schooladdr}</b></td>
 	 	    </tr>
 	 	    
 	 	    <tr>
 	 	     <td colspan=\"2\" style=\"border:0;\" ><b>Tel :{$schooltel}</b></td>
 	 	    </tr>
 	 	    
 	 	    <tr>
 	 	     <td colspan=\"2\" style=\"border:0;\" ><b>Motto :{$schoolmotto}</b></td>
 	 	    </tr>
 	 	    
 	 	    <tr>
 	 	     <td colspan=\"2\" style=\"border:0;\" >&nbsp;</td>
 	 	    </tr>
 	 	    
 	 	    <tr>
 	 	     <td  colspan=\"2\" style=\"border:0;\" ><b>CLASS LIST  Form {$formstream} - Term {$termcode} {$yearcode}</b></td>
 	 	     <td colspan=\"1\" style=\"border:0;\" ><b>Date : </b>{$dateNow}</td>
 	 	    </tr>
 	 	    
 	 	   </table>
 	 	 </td>
 	 	 </tr>";
 	 	
 	   if($data->RecordCount()>0) {
 	   	$count= 1;
 	   		
 	 	 echo "<tr>";
 	 	  echo "<td>No</td>";
 	 	  echo "<td>ADM. NO</td>";
 	 	  echo "<td>NAME</td>";
 	 	  echo "<td>&nbsp;</td>";
 	 	 echo "</tr>";
 	 	 
 		while (!$data->EOF) {
       	 	
       	 	$admno        = valueof($data->fields, 'ADMNO');	
       	 	$fullname     = valueof($data->fields, 'FULLNAME');	
       	 	//$subjectcode  = valueof($data->fields, 'SUBJECTCODE');	
       	 	//$codeoffc     = valueof($data->fields, 'CODEOFFC');	
       	 	//$catcode      = valueof($data->fields, 'CATCODE');	
       	 	//$formcode     = valueof($data->fields, 'FORMCODE');
       	 	//$streamcode   = valueof($data->fields, 'STREAMCODE');
       	 	//$sbccode      = valueof($data->fields, 'SBCCODE');
       	 	
 	 	 echo "<tr>";
 	 	  echo "<td>{$count}</td>";
 	 	  echo "<td>{$admno}</td>";
 	 	  echo "<td>{$fullname}</td>";
 	 	  echo "<td>&nbsp;</td>";
 	 	 echo "</tr>";
 	 	 
 	 	 ++$count;
    
       	  $data->MoveNext();
       	 }
 	   }
 	  }
 	 	
 	 	echo "</table>";
 	 	
 ?>
 </body>
  </html>
 
