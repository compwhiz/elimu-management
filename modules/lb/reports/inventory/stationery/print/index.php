<?php

$sctcode      = filter_input(INPUT_GET , ui::fi('sctcode'));
$reportname   = 'Library Stationery List';

$sql_where = array();

if(!empty($sctcode)){
$sql_where[] = " SCTCODE='{$sctcode}'";
}

$sql_where_str = sizeof($sql_where)>0 ? ' AND ' .implode(' AND ', $sql_where) : '';

$school       = new school(120);
$library      = new library(120);
$dateNow      = textDate(null, true);

$where_am_i   = str_replace($_SERVER['DOCUMENT_ROOT'],'', ROOT);
$server_name  = $_SERVER['SERVER_NAME'];
$server_port  = $_SERVER['SERVER_PORT'];

$x = pathinfo(realpath($_SERVER['DOCUMENT_ROOT']));
$y = pathinfo(realpath(ROOT));
$z = '';

if(isset($x['dirname']) && isset($y['dirname'])){
 if($x != $y){
  $z = $y['filename'];
 }
}

$server_root =  "http://{$server_name}:{$server_port}/{$z}/";

//cleanup
 $files = glob(ROOT.'tmp_files/lib*.png');
 foreach($files as $file){
  if(is_file($file)){
    unlink($file);
  }
 }
 	
require_once ( ROOT.'lib/jpgraph/jpgraph.php');
require_once ( ROOT.'lib/jpgraph/jpgraph_bar.php');
require_once ( ROOT.'lib/jpgraph/jpgraph_line.php');

$books = $db->Execute("select * from VIEWLIBSTATIONERY WHERE 1=1 {$sql_where_str}  ORDER BY SCTCODE ");


$html = <<<HTML
<table border="0" cellpadding="4" cellspacing="2" width="99%"  align="left" style="color:black;">\r\n
HTML;

$html .= <<<HTML
         <tr nobr="true" >
          <td  colspan="4"  style="font-weight:bold" align="center"><img src="{$school->logo_path}" ></td>\r\n
        </tr>
HTML;

$html .= <<<HTML
         <tr nobr="true" >
          <td  colspan="4"  style="font-weight:bold;border-bottom:1px solid black;" align="center">{$school->name}</td>\r\n
        </tr>
HTML;

$html .= <<<HTML
         <tr nobr="true" >
          <td  colspan="4"  style="font-weight:bold"  align="center">{$library->name}</td>\r\n
        </tr>
HTML;

$html .= <<<HTML
         <tr nobr="true" >
          <td  colspan="4"  style="font-weight:normal;border-bottom:1px solid black;"  align="center">{$reportname}</td>\r\n
        </tr>
HTML;

$html .= <<<HTML
         <tr nobr="true" >
          <td  colspan="4"  style="font-weight:bold">&nbsp;</td>\r\n
        </tr>
HTML;

if($books){
  if($books->RecordCount()>0){
    	  
$html .= <<<HTML
         <tr nobr="true" >
          <td colspan="4">
HTML;
	  
	 //show in table
	  //print_pre($books->fields);
//--------------------------------
	$html .= <<<HTML
<table border="0" cellpadding="2" cellspacing="0" width="100%"  align="left" style="color:black;">\r\n
HTML;
	  
	  
$html .= <<<HTML
         <tr nobr="true" >
          <td style="border:1px solid black;" width="150">Category</td>\r\n
          <td style="border:1px solid black;" width="80">Code</td>\r\n
          <td style="border:1px solid black;" width="310">Name</td>\r\n
          <td style="border:1px solid black;" width="50">Qty.</td>\r\n
        </tr>
HTML;
 $count =0;
  foreach($books as $book){
	$qty = valueof($book,'QTY',0,'number_format');
	
$html .= <<<HTML
         <tr nobr="true" >
          <td style="border:1px solid black;">{$book['SCTNAME']}</td>\r\n
          <td style="border:1px solid black;">{$book['STCODE']}</td>\r\n
          <td style="border:1px solid black;">{$book['STNAME']}</td>\r\n
          <td style="border:1px solid black;">{$qty}</td>\r\n
        </tr>
HTML;

++$count;
  }
  
$html .= <<<HTML
         <tr nobr="true" >
          <td style="border:1px solid black;" colspan="4">Total : {$count}</td>\r\n
        </tr>
HTML;

$html .= <<<HTML
          </table>
HTML;
//--------------------------
	  
$html .= <<<HTML
          </td>
        </tr>
HTML;
	  
  }else{
	  
$html .= <<<HTML
         <tr nobr="true" >
          <td  colspan="4"  style="font-weight:bold;color:red">No Stationery found matching your filter criteria</td>\r\n
        </tr>
HTML;
	  
  }
}else{
//error	
	  
$html .= <<<HTML
         <tr nobr="true" >
          <td  colspan="4"  style="font-weight:bold;color:red">An error occured</td>\r\n
        </tr>
HTML;
}

 $html .= <<<HTML
      </table>
HTML;

//exit($html);
html_to_pdf($reportname, $html);
