<?php

/**
* auto created index file for modules/lb/reports/circulation/overduelist
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-01-28 17:13:36
*/

?>
<form id="<?php echo ui::fi('ff'); ?>" method="post" novalidate onsubmit="return false;">
	<table cellpadding="5" cellspacing="0" width="100%">
	 
	 <tr>
	  <td>&nbsp;</td>
	  <td>All fields are optional</td>
	 </tr>
	 
	 <tr>
	  <td width="100px">Member Type</td>
	  <td><?php echo dropdown::lib_member_type( 'mtpcode'); ?></td>
	 </tr>
	 
	<tr>
	 <td>&nbsp;</td>
	 <td>
	 <a href="javascript:void(0)" class="easyui-linkbutton" onclick="<?php echo $cfg['appname']; ?>.print_report();"><i class="fa fa-print"></i> Open Report</a>
	 <a href="javascript:void(0)" class="easyui-linkbutton" onclick="<?php echo $cfg['appname']; ?>.clearForm();">Reset</a>
	 </td>
    </tr>
			
	 
	</table> 
	</form>
	
	<script>
		
	  var <?php echo $cfg['appname']; ?> = {
		clearForm:function (){
			$('#<?php echo ui::fi('ff'); ?>').form('clear');
		},
        print_report:function (){
         var fdata = $('#<?php echo ui::fi('ff'); ?>').serialize();
  	     var <?php echo ui::fi('w'); ?>=window.open('./endpoints/print/?modvars=<?php echo $vars; ?>&'+fdata,'p','height=800,width=830,toolbar=no,menubar=no,directories=no,location=no,scrollbars=yes,status=no,resizable=no,fullscreen=no,top=0,left=0');
          <?php echo ui::fi('w'); ?>.focus();
        },
	  }
       
			
</script>
