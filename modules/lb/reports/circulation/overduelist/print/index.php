<?php

$mtpcode         = filter_input(INPUT_GET , ui::fi('mtpcode'));
$date_now        = date('Y-m-d');
$date_now_fmt    = textDate(). ' , '. date('H:i');

$reportname   = 'Over Due Books List';
//$db->debug=1;
$sql_where = array();

$sql_where[] = " (DATEDUE<='{$date_now}')";

$sql_where[] = " (RETURNED IS NULL OR RETURNED=0)";

if(!empty($mtpcode)){
$sql_where[] = " MTPCODE='{$mtpcode}'";
}

$sql_where_str = sizeof($sql_where)>0 ? ' AND ' .implode(' AND ', $sql_where) : '';

$school       = new school(120);
$library      = new library(120);
$dateNow      = textDate(null, true);

$where_am_i   = str_replace($_SERVER['DOCUMENT_ROOT'],'', ROOT);
$server_name  = $_SERVER['SERVER_NAME'];
$server_port  = $_SERVER['SERVER_PORT'];

$x = pathinfo(realpath($_SERVER['DOCUMENT_ROOT']));
$y = pathinfo(realpath(ROOT));
$z = '';

if(isset($x['dirname']) && isset($y['dirname'])){
 if($x != $y){
  $z = $y['filename'];
 }
}

$server_root =  "http://{$server_name}:{$server_port}/{$z}/";

//cleanup
 $files = glob(ROOT.'tmp_files/lib*.png');
 foreach($files as $file){
  if(is_file($file)){
    unlink($file);
  }
 }
 	
require_once ( ROOT.'lib/jpgraph/jpgraph.php');
require_once ( ROOT.'lib/jpgraph/jpgraph_bar.php');
require_once ( ROOT.'lib/jpgraph/jpgraph_line.php');

$records = $db->Execute("select * from VIEWLIBTRANS WHERE 1=1 {$sql_where_str}  ORDER BY DATEDUE ");


$html = <<<HTML
<table border="0" cellpadding="4" cellspacing="2" width="99%"  align="left" style="color:black;">\r\n
HTML;

$html .= <<<HTML
         <tr nobr="true" >
          <td  colspan="4"  style="font-weight:bold" align="center"><img src="{$school->logo_path}" ></td>\r\n
        </tr>
HTML;

$html .= <<<HTML
         <tr nobr="true" >
          <td  colspan="4"  style="font-weight:bold;border-bottom:1px solid black;" align="center">{$school->name}</td>\r\n
        </tr>
HTML;

$html .= <<<HTML
         <tr nobr="true" >
          <td  colspan="4"  style="font-weight:bold"  align="center">{$library->name}</td>\r\n
        </tr>
HTML;

$html .= <<<HTML
         <tr nobr="true" >
          <td  colspan="4"  style="font-weight:normal;border-bottom:1px solid black;"  align="center">{$reportname} as at {$date_now_fmt}</td>\r\n
        </tr>
HTML;

$html .= <<<HTML
         <tr nobr="true" >
          <td  colspan="4"  style="font-weight:bold">&nbsp;</td>\r\n
        </tr>
HTML;

if($records){
  if($records->RecordCount()>0){
    	  
$html .= <<<HTML
         <tr nobr="true" >
          <td colspan="4">
HTML;
	  
	 //show in table
	  //print_pre($records->fields);
//--------------------------------
	$html .= <<<HTML
<table border="0" cellpadding="2" cellspacing="0" width="100%"  align="left" style="color:black;">\r\n
HTML;
	  
	  
$html .= <<<HTML
         <tr nobr="true" >
          <td style="border:1px solid black;" width="80">Member</td>\r\n
          <td style="border:1px solid black;" width="80">Type</td>\r\n
          <td style="border:1px solid black;" width="150">Name</td>\r\n
          <td style="border:1px solid black;" width="80">Code</td>\r\n
          <td style="border:1px solid black;" width="350">Book Title</td>\r\n
          <td style="border:1px solid black;" width="100">Issue Date</td>\r\n
          <td style="border:1px solid black;" width="100">Due Date</td>\r\n
          <td style="border:1px solid black;" width="50">Days</td>\r\n
        </tr>
HTML;

 $count = 0;
 $num_due = 0;
 $now = time();
  foreach($records as $record){
	  
	  $returned   = valueof($record, 'RETURNED',0);
	  
	  $dateissue  = valueof($record, 'DATEISSUE');
	  $datedue    = valueof($record, 'DATEDUE');
	  
	  $datetimeissue  = strtotime($dateissue);
	  $datetimedue    = strtotime($datedue);
	  
	  $datefmtissue  = date('d/m/Y',$datetimeissue);
	  $datefmtdue    = date('d/m/Y',$datetimedue);
	  
	  if(empty($returned) && ($now>$datetimedue)){
		$due            =  'Yes'; 
		++$num_due;
	  }else{
		$due            = 'No';
	  }
	  
	  $tz             = new DateTimeZone('Africa/Nairobi');
 	  $interval       = DateTime::createFromFormat('Y-m-d', $datedue, $tz)->diff(new DateTime('now', $tz));
	     
	 if($now>$datetimedue){
$html .= <<<HTML
         <tr nobr="true" >
          <td style="border:1px solid black;">{$record['MEMBERID']}</td>\r\n
          <td style="border:1px solid black;">{$record['MTPNAME']}</td>\r\n
          <td style="border:1px solid black;">{$record['FULLNAME']}</td>\r\n
          <td style="border:1px solid black;">{$record['BOOKCODE']}</td>\r\n
          <td style="border:1px solid black;">{$record['BOOKTITLE']}</td>\r\n
          <td style="border:1px solid black;">{$datefmtissue}</td>\r\n
          <td style="border:1px solid black;">{$datefmtdue}</td>\r\n
          <td style="border:1px solid black;">{$interval->d}</td>\r\n
        </tr>
HTML;

   ++$count;
  }
 }
  
$html .= <<<HTML
         <tr nobr="true" >
          <td style="border:1px solid black;" colspan="8">
          Total Due: <b>{$num_due}</b> 
          </td>\r\n
        </tr>
HTML;

$html .= <<<HTML
          </table>
HTML;
//--------------------------
	  
$html .= <<<HTML
          </td>
        </tr>
HTML;
	  
  }else{
	  
$html .= <<<HTML
         <tr nobr="true" >
          <td  colspan="4"  style="font-weight:bold;color:red">No Over Due Books</td>\r\n
        </tr>
HTML;
	  
  }
}else{
//error	
	  
$html .= <<<HTML
         <tr nobr="true" >
          <td  colspan="4"  style="font-weight:bold;color:red">An error occured</td>\r\n
        </tr>
HTML;
}

 $html .= <<<HTML
      </table>
HTML;

//exit($html);
html_to_pdf($reportname, $html, 'L');

