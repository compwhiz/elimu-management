<?php

/**
* auto created config file for modules/lb/transactions/lendreceive
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-01-24 16:54:06
*/

 final class lendreceive {
 private $id;
 private $datasrc;
 private $primary_key;
	
 public function __construct(){
  global $cfg;
		
     $this->id           = filter_input(INPUT_POST , 'id');
     $this->datasrc      = valueof($cfg,'datasrc');
     $this->primary_key  = valueof($cfg,'pkcol');
 }
	
 
    public function data(){
    global $db, $cfg;
   //$db->debug=1;
   
	$page     = isset($_POST['page']) ? intval($_POST['page']) : 1;
    $rows     = isset($_POST['rows']) ? intval($_POST['rows']) : 200;
    $offset   = ($page-1)*$rows;
    
    $sortbys = array();
    
    if( isset($_POST['sort']) && !empty($_POST['sort']) ) {
	 $sort_cols_array  = explode(',', $_POST['sort']);
	 $sort_order_array = explode(',', $_POST['order']);
	 
	 if(sizeof($sort_cols_array)>0){
		foreach($sort_cols_array as $sort_col_index => $sort_col_name){
		 $sort_col_name  = strtoupper($sort_col_name);
		 $sort_col_name  = str_replace("_".MNUID,'',$sort_col_name);
		 $sort_col_order = valueof($sort_order_array, $sort_col_index, 'asc');
		 $sortbys[]      = "{$sort_col_name} {$sort_col_order}";
		}
	 }
	 
	 if(sizeof($sortbys)>0){
	  $sortby = 'order by ' .implode(',', $sortbys);
	 }
   }else{
	 $sortby   =  'order by ID asc';  
   }
	
	 $filters_conditions_str =  '';
    
    /**
     * @ekariz 22OCT15 
     */
     
    if(isset($_POST['filterRules'])) {
		
	 $filterRules     = $_POST['filterRules'];
	 $filterRules_obj = json_decode($filterRules);
	 $filters_conditions = array();
	 
	 foreach($filterRules_obj as $filterRule){
	  $filterRuleCol = @$filterRule->field;
	  $filterRuleCol = str_replace("_".MNUID,'',$filterRuleCol);
	  $filterRuleOpr = @$filterRule->op;
	  $filterRuleVal = @$filterRule->value;
	  
	  $filterRuleDBCol = strtoupper($filterRuleCol);
	  
	  if(!is_null($filterRuleDBCol)){
	  
	   switch($filterRuleOpr){
         case 'none' : $filters_conditions[]="{$filterRuleDBCol} is not null";break;
         case 'contains' : $filters_conditions[]="{$filterRuleDBCol} like '%{$filterRuleVal}%'";break;
         case 'equal' : $filters_conditions[]="{$filterRuleDBCol} = '{$filterRuleVal}'";break;
         case 'notequal' : $filters_conditions[]="{$filterRuleDBCol} != '{$filterRuleVal}'";break;
         case 'beginwith' : $filters_conditions[]="{$filterRuleDBCol} like '{$filterRuleVal}%'";break;
         case 'endwith' : $filters_conditions[]="{$filterRuleDBCol} like '%{$filterRuleVal}'";break;
         case 'less' : $filters_conditions[]="{$filterRuleDBCol} < '{$filterRuleVal}'";break;
         case 'lessorequal' : $filters_conditions[]="{$filterRuleDBCol} <= '{$filterRuleVal}'";break;
         case 'greater' : $filters_conditions[]="{$filterRuleDBCol} > '{$filterRuleVal}'";break;
         case 'greaterorequal' : $filters_conditions[]="{$filterRuleDBCol} >= '{$filterRuleVal}'";break;
         default  : $filters_conditions[]="{$filterRuleDBCol} like '%{$filterRuleVal}%'";break;
        }
        
	  }
        
	 }
	 
	 if(sizeof($filters_conditions)>0){
		$filters_conditions_str = 'and ('. implode(' and ', $filters_conditions). ')';
	 }
	 
	}else{
	    $filters_conditions_str =  '';
	}
	
    $sql_where   = " WHERE 1=1 {$filters_conditions_str}";
	
	$numRecords    = $db->GetOne("select count(*) from VIEWLIBTRANS {$sql_where} ");
	
	$result["total"] = $numRecords;
	   
	$sql = "
	SELECT * FROM VIEWLIBTRANS
    {$sql_where}
    {$sortby}
    ";
	
	$rs = $db->SelectLimit($sql,$rows, $offset);
		
	$items = array();
	
	if($rs){
	 
	 while(!$rs->EOF){
	 	
	  foreach ($rs->fields as $dbCol=>$val){	
		  
	    $formCol = strtolower($dbCol);
	    
	    if($formCol=='returned'){
		 $val  = $val==1 ? 'Yes' : 'No';
		}
		
	    $rs->fields[$formCol]  = $val;
	    unset($rs->fields[$dbCol]);
	  }
	  
	  	
	  if(defined('MAKE_FIELDS_UNIQUE') && defined('MNUID')) {
		foreach($rs->fields as $k=>$v){
		  $field_id = "{$k}_".MNUID; 
		  if($k!='ID' && $k!='id'){
		   unset($rs->fields[$k]);
	      }
		  $rs->fields[$field_id] = $v;
		}
	  }
	  
	  array_push($items, $rs->fields);
	  
	  $rs->MoveNext();
	 }
	}
	
	$result["rows"] = $items;

	echo header('Content-type : text/json');
	echo json_encode($result);
		
 }
		
	public function get_member_opts(){
		global $db;
		
		$memberid   = 	filter_input(INPUT_POST , 'memberid', FILTER_SANITIZE_STRING);
		$member     = $db->GetRow("select * from VIEWLIBMEMBERS where MEMBERID='{$memberid}' ");
		$data       = array();
		
		if(!empty($member)) {
		$maxdays         = valueof($member,'MAXDAYS',1);
	    $data['maxdays'] = $maxdays;
	    $data['success'] = 1;
	    $data['message'] = 'ok';
	    }else{
		$data['success'] = 1;
	    $data['message'] = 'Member Not Found';
		}
		
	    return json_encode($data);
	
    }	
    
	public function data_return(){
		global $db;
		
		$id      = 	filter_input(INPUT_POST , 'id', FILTER_SANITIZE_STRING);
		$record  = $db->GetRow("select * from VIEWLIBTRANS where ID={$id} ");
	    
	    /**
	     * @todo: calculate fine, allow enter fines for damages
	     */
    
		$data = array();
		
		if( isset($record['RETURNED']) && $record['RETURNED']==1){
		 $success = 1;
		 $message = 'Book Already Returned';
		}else{
		
 	    $mtpcode        = valueof($record,'MTPCODE');
 	    $date_lend      = valueof($record,'DATEISSUE');
 	    $datetime_lend  = strtotime($date_lend);
		$date_due       = valueof($record,'DATEDUE');
		$datetime_due   = strtotime($date_due);
 	    $days_past      = null;
 	    $return_fine    = null;
 	    
		if( time() > $datetime_due) {
			
		 $tz             = new DateTimeZone('Africa/Nairobi');
 	     $interval       = DateTime::createFromFormat('Y-m-d', $date_due, $tz)->diff(new DateTime('now', $tz));
 	   
 	     if( $interval->d>0 ){
		 $days_past = $interval->d;
	     }
	    
	    }
	    
	    if($days_past>0){
		 $fine        = $db->GetOne("select AMOUNT from SLMTF where MTPCODE='{$mtpcode}' and TYPECODE='LR' ");// @todo and ISLATE=1
		 $return_fine = $days_past*$fine;
		}
     
		$data['return_member']    = valueof($record,'FULLNAME');
		$data['return_book']      = valueof($record,'BOOKTITLE');
		$data['return_date_lend'] = valueof($record,'DATEISSUE');
		$data['return_date_due']  = valueof($record,'DATEDUE');
		$data['return_days_past'] = $days_past;
		$data['return_fine']      = $return_fine;
		$data['return_damage_charge']      = 0;
		$success = 1;
		$message = 'OK';
	    }
	    
		if(defined('MAKE_FIELDS_UNIQUE') && defined('MNUID')) {
		foreach($data as $k=>$v){
		  $field_id = "{$k}_".MNUID; 
		  unset($data[$k]);
		  $data[$field_id] = $v;
		}
	   }
	   
	  $data['success'] = $success;
	  $data['message'] = $message;
		
	 return json_encode( $data );
		
	}
	
	public function save(){
		global $db,$cfg;

    $user         = new user();		
	$memberid     = filter_input(INPUT_POST, ui::fi('find_member'));
    $bookcode     = filter_input(INPUT_POST, ui::fi('find_book'));
    $days         = filter_input(INPUT_POST, ui::fi('days'));
    $datertn      = filter_input(INPUT_POST, ui::fi('datertn')); 
    $dateissue    = date('Y-m-d'); 
	
	//check if lend already
	$check  = $db->GetRow("select * from SLTRN where MEMBERID='{$memberid}'  and BOOKCODE='{$bookcode}' and (RETURNED is null or RETURNED=0) ");
	
	if(!empty($check)){
	 return json_response(0,"Member Already Lend Book {$bookcode} and Has not yet Returned");	
	}
	
    $book = new ADODB_Active_Record('SLTRN', array('ID'));
    
    if(empty($book->_original)){
     $book->id       = generateID('SLTRN');
     $book->transno  = generateUniqueCode('BLN',$book->id,6);
    }
	
	$book->memberid       =  $memberid;
	$book->bookcode       =  $bookcode;
	$book->dateissue	  =  $dateissue;
	$book->lenddays	      =  $days;
	$book->datedue	      =  $datertn;
	$book->userlnd        =  $user->userid;
	$book->audituser      =  $user->userid;
	$book->auditdate      =  date('Y-m-d'); 
	$book->audittime      =  time();

    if($book->Save()){
	 $set_avaible_qty = self::book_set_available_qty( $book->bookcode, -1);
     return json_response(1,'Book Lend');
    }else{
     return json_response(0,'Save Failed');
    }

  }
	
	public function save_receive(){
		global $db,$cfg;

    $user         = new user();		
	$id           = filter_input(INPUT_POST, 'id');
	$fine         = filter_input(INPUT_POST, ui::fi('return_fine'));
	$damage       = filter_input(INPUT_POST, ui::fi('return_damage_charge'));
	$remarks      = filter_input(INPUT_POST, ui::fi('return_remarks', FILTER_SANITIZE_STRING));
	$bill_total   = ($fine*1)+($damage*1);
	
    $book = new ADODB_Active_Record('SLTRN', array('ID'));
    $book->Load("ID={$id}");
    
    if(!empty($book->_original)) {
	
	$book->returned     =  1;
	$book->fine         =  $fine;
	$book->damage       =  $damage;
	$book->remarks      =  $remarks;
	$book->datertn      =  date('Y-m-d'); 
	$book->userrcv      =  $user->userid; 

     if($book->Save()) {
		 
	   $set_avaible_qty = self::book_set_available_qty( $book->bookcode, 1);
	   
	   if($bill_total>0){
		  $bookname  = $db->GetOne("select BOOKTITLE from SLBK where BOOKCODE='{$book->bookcode}' ");
		  $desc      = "Library Book Charges for {$bookname}";
		  $create_bill = self::create_bill( $book->memberid, $bill_total, $desc);
	   }
      
      return json_response(1,'Book Returned');
      
     }else{
      return json_response(0,'Book Return Failed');
     }

    }
    
  }
	
	public function create_bill($memberid, $amount, $desc){
		global $db,$cfg;

    $user               =  new user();		
    $record             =  new ADODB_Active_Record('SLBIL', array('ID'));
    $record->id         =  generateID($record->_tableat);
    $record->billno     =  generateUniqueCode('DN-L',$record->id,6);
	$record->memberid   =  $memberid;
	$record->datebill   =  date('Y-m-d');
	$record->amount	    =  $amount;
	$record->descr	    =  $desc;
	$record->audituser  =  $user->userid;
	$record->auditdate  =  date('Y-m-d'); 
	$record->audittime  =  time();

    if($record->Save()){
     return true;
    }else{
     return false;
    }

  }
	
	  
	public function book_set_available_qty($bookcode, $qty){
	 global $db;
	 
	    $available = $db->GetOne("select AVAILABLE from SLBK  where BOOKCODE='%s'", $bookcode);
	    
	    if(is_null($available)){
		 $db->Execute( sprintf("update SLBK set AVAILABLE=COPIES where BOOKCODE='%s'" , $bookcode) );
		}
		
	 	$sql = sprintf("update SLBK set AVAILABLE=AVAILABLE+%s where BOOKCODE='%s'", $qty , $bookcode);
	 	
	 	if($db->Execute($sql)){
			return true;
		}
		
		return false;
	}
	
	
	public function remove(){
		global $db, $cfg;
		json_response(0,'failed');
        $grid = new grid();
        return $grid->grid_remove_row();
	}
	
}
