<?php
/**
* auto created config file for modules/lb/transactions/lendreceive
* @author kenmsh@gmail.com
* 
* @version 2.0
* @since 2016-01-24 16:54:06
*/

if(!defined('MAKE_FIELDS_UNIQUE')){
 define('MAKE_FIELDS_UNIQUE' , true);
}

$scriptname  = @end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'Lend/receive';//user-readable formart
 $cfg['appname']       = 'lendreceive';//lower cased one word
 $cfg['datasrc']       = 'SLTRN';//where to get data
 $cfg['datatbl']       = 'SLTRN';//base data src [for updates & deletes]
 $cfg['form_width']    = 400;
 $cfg['form_height']   = 200;
 $cfg['window_width']    = 550;
 $cfg['window_height']   = 400;
 
 $cfg['pkcol']         = 'TRANSNO';//the primary key
 
 $cfg['tblbns']['chk_button_add']    = 1;
 $cfg['tblbns']['chk_button_edit']   = 1;
 $cfg['tblbns']['chk_button_delete'] = 1;
 $cfg['tblbns']['chk_button_import'] = 1;
 $cfg['tblbns']['chk_button_export'] = 1;
 
 
$combogrid_array   = array();

$combogrid_array['find_member']['columns']['MTPNAME']   = array( 'field'=>'mtpname', 'title'=>'Type', 'width'=>80);
$combogrid_array['find_member']['columns']['MEMBERID']  = array( 'field'=>'memberid', 'title'=>'Member ID', 'width'=> 80 , 'isIdField' => true );
$combogrid_array['find_member']['columns']['FULLNAME']  = array( 'field'=>'fullname', 'title'=>'Member Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['find_member']['source'] ='VIEWLIBMEMBERS';

$combogrid_array['find_book']['columns']['SUBJECTCODE']   = array( 'field'=>'subjectcode', 'title'=>'Subject', 'width'=>80);
$combogrid_array['find_book']['columns']['BOOKCODE']      = array( 'field'=>'bookcode', 'title'=>'Book Code', 'width'=> 100 , 'isIdField' => true );
$combogrid_array['find_book']['columns']['BOOKTITLE']     = array( 'field'=>'booktitle', 'title'=>'Book Title', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['find_book']['source'] ='SLBK';
	
       
 
