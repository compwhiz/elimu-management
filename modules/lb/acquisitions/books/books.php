<?php

/**
* auto created config file for modules/lb/acquisitions/books
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-01-25 17:47:15
*/

 final class books {
 private $id;
 private $datasrc;
 private $primary_key;
	
 public function __construct(){
  global $cfg;
		
     $this->id           = filter_input(INPUT_POST , 'id');
     $this->datasrc      = valueof($cfg,'datasrc');
     $this->primary_key  = valueof($cfg,'pkcol');
 }
	
    public function data(){
    global $db, $cfg;
   //$db->debug=1;
   
	$page     = isset($_POST['page']) ? intval($_POST['page']) : 1;
    $rows     = isset($_POST['rows']) ? intval($_POST['rows']) : 200;
    $offset   = ($page-1)*$rows;
    
    $sortbys = array();
    
    if( isset($_POST['sort']) && !empty($_POST['sort']) ) {
	 $sort_cols_array  = explode(',', $_POST['sort']);
	 $sort_order_array = explode(',', $_POST['order']);
	 
	 if(sizeof($sort_cols_array)>0){
		foreach($sort_cols_array as $sort_col_index => $sort_col_name){
		 $sort_col_name  = strtoupper($sort_col_name);
		 $sort_col_name  = str_replace("_".MNUID,'',$sort_col_name);
		 $sort_col_order = valueof($sort_order_array, $sort_col_index, 'asc');
		 $sortbys[]      = "{$sort_col_name} {$sort_col_order}";
		}
	 }
	 
	 if(sizeof($sortbys)>0){
	  $sortby = 'order by ' .implode(',', $sortbys);
	 }
   }else{
	 $sortby   =  'order by ID asc';  
   }
	
	 $filters_conditions_str =  '';
    
    /**
     * @ekariz 22OCT15 
     */
     
    if(isset($_POST['filterRules'])) {
		
	 $filterRules     = $_POST['filterRules'];
	 $filterRules_obj = json_decode($filterRules);
	 $filters_conditions = array();
	 
	 foreach($filterRules_obj as $filterRule){
	  $filterRuleCol = @$filterRule->field;
	  $filterRuleCol = str_replace("_".MNUID,'',$filterRuleCol);
	  $filterRuleOpr = @$filterRule->op;
	  $filterRuleVal = @$filterRule->value;
	  
	  $filterRuleDBCol = strtoupper($filterRuleCol);
	  
	  if(!is_null($filterRuleDBCol)){
	  
	   switch($filterRuleOpr){
         case 'none' : $filters_conditions[]="{$filterRuleDBCol} is not null";break;
         case 'contains' : $filters_conditions[]="{$filterRuleDBCol} like '%{$filterRuleVal}%'";break;
         case 'equal' : $filters_conditions[]="{$filterRuleDBCol} = '{$filterRuleVal}'";break;
         case 'notequal' : $filters_conditions[]="{$filterRuleDBCol} != '{$filterRuleVal}'";break;
         case 'beginwith' : $filters_conditions[]="{$filterRuleDBCol} like '{$filterRuleVal}%'";break;
         case 'endwith' : $filters_conditions[]="{$filterRuleDBCol} like '%{$filterRuleVal}'";break;
         case 'less' : $filters_conditions[]="{$filterRuleDBCol} < '{$filterRuleVal}'";break;
         case 'lessorequal' : $filters_conditions[]="{$filterRuleDBCol} <= '{$filterRuleVal}'";break;
         case 'greater' : $filters_conditions[]="{$filterRuleDBCol} > '{$filterRuleVal}'";break;
         case 'greaterorequal' : $filters_conditions[]="{$filterRuleDBCol} >= '{$filterRuleVal}'";break;
         default  : $filters_conditions[]="{$filterRuleDBCol} like '%{$filterRuleVal}%'";break;
        }
        
	  }
        
	 }
	 
	 if(sizeof($filters_conditions)>0){
		$filters_conditions_str = 'and ('. implode(' and ', $filters_conditions). ')';
	 }
	 
	}else{
	    $filters_conditions_str =  '';
	}
	
    $sql_where   = " WHERE 1=1 {$filters_conditions_str}";
	
	$numRecords    = $db->GetOne("select count(*) from VIEWLIBAQB {$sql_where} ");
	
	$result["total"] = $numRecords;
	   
	$sql = "
	SELECT * FROM VIEWLIBAQB
    {$sql_where}
    {$sortby}
    ";
	
	$rs = $db->SelectLimit($sql,$rows, $offset);
		
	$items = array();
	
	if($rs){
	 
	 while(!$rs->EOF){
	 	
	  foreach ($rs->fields as $dbCol=>$val){	
		  
	    $formCol = strtolower($dbCol);
		
	    $rs->fields[$formCol]  = $val;
	    unset($rs->fields[$dbCol]);
	  }
	  
	    if(isset($rs->fields['splcode'])){
		$rs->fields['find_spl'] = $rs->fields['splcode'];
	    }
	    
	    if(isset($rs->fields['bookcode'])){
		$rs->fields['find_book'] = $rs->fields['bookcode'];
	    }
			
	  if(defined('MAKE_FIELDS_UNIQUE') && defined('MNUID')) {
		foreach($rs->fields as $k=>$v){
		  $field_id = "{$k}_".MNUID; 
		  if($k!='ID' && $k!='id'){
		   unset($rs->fields[$k]);
	      }
		  $rs->fields[$field_id] = $v;
		}
		
	  }
	  
	  array_push($items, $rs->fields);
	  
	  $rs->MoveNext();
	 }
	}
	
	$result["rows"] = $items;

	echo header('Content-type : text/json');
	echo json_encode($result);
		
 }
		
	public function save(){
		global $db,$cfg;

    $user          = new user();		
	$splcode       = filter_input(INPUT_POST, ui::fi('find_spl'));
	$bookcode      = filter_input(INPUT_POST, ui::fi('find_book'));
	$dateacq       = filter_input(INPUT_POST, ui::fi('dateacq'));
	$qty           = filter_input(INPUT_POST, ui::fi('qty'));
	$unitcost      = filter_input(INPUT_POST, ui::fi('unitcost'));
	
	if(empty($qty)){
     return json_response(0,'Enter Quatity');
    }
    
	if(empty($bookcode)){
     return json_response(0,'Select Book Acquired');
    }
    
	if(empty($splcode)){
     return json_response(0,'Select Supplier');
    }
    
	if(empty($unitcost)){
     return json_response(0,'Enter Unit Cost');
    }
    
	$record  = new ADODB_Active_Record('SLAQB', array('BOOKCODE','SPLCODE','DATEACQ'));
	$record->Load("BOOKCODE='{$bookcode}' and SPLCODE='{$splcode}' and DATEACQ='{$dateacq}'  ");
    
    $new = false;
    if(empty($record->_original)){
     $record->id       = generateID($record->_tableat);
     $record->bookcode = $bookcode;
     $record->splcode  = $splcode;
     $record->dateacq  =  $dateacq;
     $new = true;
    }
	
	$record->qty	        =  $qty;
	$record->unitcost	    =  $unitcost;
	$record->audituser      =  $user->userid;
	$record->auditdate      =  date('Y-m-d'); 
	$record->audittime      =  time();

    if($record->Save()){
		
	  if($new){
	   self::book_edit_qty($record->bookcode, $record->qty);
	  }
      
      return json_response(1,'Acquistion Saved');
     }else{
      return json_response(0,'Save Failed');
     }

  }
	
	public function book_edit_qty($bookcode, $qty){
	 global $db;
	 	$sql = sprintf("update SLBK set COPIES=COPIES+%s where BOOKCODE='%s'", $qty , $bookcode);
	 	
	 	if($db->Execute($sql)){
			return true;
		}
		
		return false;
	}
	
	public function remove(){
		global $db, $cfg;
		json_response(0,'Record Deletion Not Allowed');
        $grid = new grid();
        return $grid->grid_remove_row();
	}
 
}
