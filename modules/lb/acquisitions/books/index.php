<?php

/**
* auto created index file for modules/lb/acquisitions/books
* @author kenmsh@gmail.com
* 
* @version 2.0
* @since 2016-01-25 17:47:15
*/

 
?>
<style>
		
#<?php echo ui::fi('dlg'); ?> .fitem label{
	display:inline-block;
	min-width:150px;
}

#<?php echo ui::fi('dlg'); ?> .fitem input{
	
}
</style>
	<table id="<?php echo ui::fi('dg',false); ?>" style="width:676px;height:354px;border:0" >
        <thead>
            <tr>
				<th field="<?php echo ui::fi('bookcode'); ?>"    width="80" sortable="true" >Book Code</th>
				<th field="<?php echo ui::fi('booktitle'); ?>"   width="100" sortable="true" >Book Title</th>
                <th field="<?php echo ui::fi('dateacq'); ?>"     width="85"  sortable="true" >Acquire Date</th>
                <th field="<?php echo ui::fi('qty'); ?>"         width="35" sortable="true" >Qty</th>
                <th field="<?php echo ui::fi('unitcost'); ?>"    width="35" sortable="true" >Cost</th>
                <th field="<?php echo ui::fi('splname'); ?>"     width="200"  >Supplier</th>
                
            </tr>
        </thead>
    </table>
    
    <div id="<?php echo ui::fi('tb'); ?>">
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="<?php echo $cfg['appname']; ?>.add()">New</a>
<!--
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-redo" plain="true" onclick="<?php echo $cfg['appname']; ?>.edit()">Edit</a>
-->
<!--
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="crud.remove(<?php echo MNUID; ?>,'<?php echo $cfg['apptitle']; ?>','<?php echo $vars; ?>')">Remove</a>
-->
    </div>
    
  <div 
     id="<?php echo ui::fi('dlg'); ?>" 
     class="easyui-dialog" 
     style="width:440px;height:280px;padding:0" 
     closed="true" 
     buttons="#<?php echo ui::fi('dlg-buttons'); ?>"
     modal="true"
     >
         
	<form id="<?php echo ui::fi('ff'); ?>" method="post" novalidate  onsubmit="return false;">
	
	    	<table cellpadding="5" cellspacing="0" width="100%">
				
	    		<tr>
				 <td>Book:</td>
				 <td colspan="3"><?php echo ui::form_input( 'text', 'find_book',20); ?></td>
				</tr>
				
	    		<tr>
				 <td>Supplier:</td>
				 <td colspan="3"><?php echo ui::form_input( 'text', 'find_spl',20); ?></td>
				</tr>
				
	    		<tr>
	    		 <td>Quantity</td>
	    		 <td><?php echo ui::form_input( 'text', 'qty', 5, '', '', '', 'data-options="required:true, min:0,max:10000,precision:0"', '', 'easyui-numberspinner' , ""); ?></td>
	    		</tr>
	    		
	    		<tr>
	    		 <td>Acquisition Date</td>
	    		 <td><?php echo ui::form_input( 'text', 'dateacq', 10, '', '', '', 'data-options="formatter:dateYmdFormatter,parser:dateYmdParser" placeholder="YYYY-MM-DD"', '', 'easyui-datebox' , ""); ?></td>
	    		</tr>
	    		
	    		<tr>
	    		 <td>Unit Cost</td>
	    		 <td><?php echo ui::form_input( 'text', 'unitcost', 5, '', '', '', 'data-options="required:true"', '', 'easyui-numberbox' , ""); ?></td>
	    		</tr>
	    		
	    		<tr>
	    		 <td>&nbsp;</td>
	    		 <td colspan="3" >&nbsp;</td>
	    		</tr>
	    		
	    	</table>
	
   </form>
  </div>
  
   <div id="<?php echo ui::fi('dlg-buttons'); ?>">
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" onclick="<?php echo $cfg['appname']; ?>.save();" >Save Acquisition</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#<?php echo ui::fi('dlg'); ?>').dialog('close');<?php echo $cfg['appname']; ?>.clearForm();" style="width:90px">Cancel</a>
   </div>
 
	<script>
		
	  $(function(){
			
	    $('#<?php echo ui::fi('dg',false); ?>').datagrid({
		     url:'./endpoints/crud/',
             pagination:'true',
             rownumbers:'true',
             fitColumns:'true',
             singleSelect:'true',
             idField:'id',
             toolbar:'#<?php echo ui::fi('tb'); ?>',
             queryParams:{ modvars:'<?php echo $vars; ?>',function:'data' },
             remoteFilter:true,
             collapsible:false,
             filterBtnIconCls:'icon-filter',
             multiSort:true,
             fit:true,
             type:'post'
		   });
	   });
	   
	   $('#<?php echo ui::fi('dg',false); ?>').datagrid('enableFilter');
	   
       var <?php echo $cfg['appname']; ?> = {
		clearForm :function (){
			$('#<?php echo ui::fi('ff'); ?>').form('clear');
		},
		add:function (){
			$('#<?php echo ui::fi('dlg'); ?>').dialog('open').dialog('setTitle','New <?php echo $cfg['apptitle']; ?>');
			$('#<?php echo ui::fi('ff'); ?>').form('clear');
			$('#<?php echo ui::fi('dateacq'); ?>').datebox('setValue', '<?php echo date('Y-m-d'); ?>');
		},
		edit:function (){
		var row = $('#<?php echo ui::fi('dg',false); ?>').datagrid('getSelected');
		 if (row){
			$('#<?php echo ui::fi('dlg'); ?>').dialog('open').dialog('setTitle','Edit <?php echo $cfg['apptitle']; ?>');
			$('#<?php echo ui::fi('ff'); ?>').form('clear');
			$('#<?php echo ui::fi('ff'); ?>').form('load',row);
		 }else{
			$.messager.alert('Error','Select a Record First','error'); 
		 }
		},
		save:function (){
			var validate =  $('#<?php echo ui::fi('ff'); ?>').form('validate');
			if(!validate){
			  $.messager.alert('Error','Fill In All Required Fields','error');
			 return;	
			}else{
			$.messager.confirm('Confirm','Confirm Acquisition',function(r){
			if (r){
			var fdata = $('#<?php echo ui::fi('ff'); ?>').serialize() +'&module=<?php echo $vars; ?>&function=save&';
		    $.post('./endpoints/crud/', fdata, function(data) {
             if (data.success === 1) {
                $.messager.show({title: 'Success',msg: data.message});
				$('#<?php echo ui::fi('dg',false); ?>').datagrid('reload');
				$('#<?php echo ui::fi('dlg'); ?>').dialog('close');
             } else {
                $.messager.alert('Error',data.message,'error');
             }
            }, "json");
	       }
	      });
		 }
		},
	  }
		
		
    <?php
      echo ui::ComboGrid($combogrid_array,'find_spl',"");
      echo ui::ComboGrid($combogrid_array,'find_book',"");
	?>
	</script>
