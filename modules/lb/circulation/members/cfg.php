<?php
/**
* auto created config file for modules/lb/circulation/members
* @author kenmsh@gmail.com
* 
* @version 2.0
* @since 2016-01-24 16:36:05
*/

$scriptname  = @end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'Members';//user-readable formart
 $cfg['appname']       = 'members';//lower cased one word
 $cfg['datasrc']       = 'VIEWLIBMEMBERS';//where to get data
 $cfg['datatbl']       = 'SLMBR';//base data src [for updates & deletes]
 $cfg['form_width']    = 400;
 $cfg['form_height']   = 300;
 $cfg['window_width']    = 650;
 $cfg['window_height']   = 400;
 
 $cfg['pkcol']         = 'MEMBERID';//the primary key
 
 $cfg['tblbns']['chk_button_add']    = 1;
 $cfg['tblbns']['chk_button_edit']   = 1;
 $cfg['tblbns']['chk_button_delete'] = 1;
 $cfg['tblbns']['chk_button_import'] = 1;
 $cfg['tblbns']['chk_button_export'] = 1;
 

$cfg['columns']['ld2zq'] = array(
                      'dbcol'=>'MEMBERID',
                      'title'=>'Member ID',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['e1rgx'] = array(
                      'dbcol'=>'FULLNAME',
                      'title'=>'Member',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['s6nps'] = array(
                      'dbcol'=>'MTPCODE',
                      'title'=>'Member Type',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>3,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'SLMTP|MTPCODE|MTPNAME|MTPCODE',
                      );
                      



                      
$cfg['columns']['vwyco'] = array(
                      'dbcol'=>'MTPNAME',
                      'title'=>'Member Type',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>2,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['lobtl'] = array(
                      'dbcol'=>'ISSTUD',
                      'title'=>'Isstud',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>4,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> '',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['dyzbr'] = array(
                      'dbcol'=>'MOBILE',
                      'title'=>'Mobile',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 0,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['es9te'] = array(
                      'dbcol'=>'BORROWMAX',
                      'title'=>'Borrow Max.',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'numberspinner',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['ibp8z'] = array(
                      'dbcol'=>'DATEJOIN',
                      'title'=>'Join Date',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'date',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['0rynj'] = array(
                      'dbcol'=>'DATEEXPIRE',
                      'title'=>'Expiry Date',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'date',
                      'selectsrc'=> '',
                      );
                      



                      $combogrid_array   = array();
	 
$combogrid_array['s6nps']['columns']['MTPCODE']  = array( 'field'=>'MTPCODE', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['s6nps']['columns']['MTPNAME']  = array( 'field'=>'MTPNAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['s6nps']['source'] ='SLMTP';
  