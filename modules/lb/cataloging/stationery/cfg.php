<?php
/**
* auto created config file for modules/lb/cataloging/stationery
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-01-25 13:42:59
*/

$scriptname  = @end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'Stationery';//user-readable formart
 $cfg['appname']       = 'stationery';//lower cased one word
 $cfg['datasrc']       = 'SLST';//where to get data
 $cfg['datatbl']       = 'SLST';//base data src [for updates & deletes]
 $cfg['form_width']    = 400;
 $cfg['form_height']   = 250;
 $cfg['window_width']    = 550;
 $cfg['window_height']   = 400;
 
 $cfg['pkcol']         = 'STCODE';//the primary key
 
 $cfg['tblbns']['chk_button_add']    = 1;
 $cfg['tblbns']['chk_button_edit']   = 1;
 $cfg['tblbns']['chk_button_delete'] = 1;
 $cfg['tblbns']['chk_button_import'] = 1;
 $cfg['tblbns']['chk_button_export'] = 1;
 

$cfg['columns']['dzzg8'] = array(
                      'dbcol'=>'SCTCODE',
                      'title'=>'Category',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'SLSCT|SCTCODE|SCTNAME|SCTCODE',
                      );
                      



                      
$cfg['columns']['qymsi'] = array(
                      'dbcol'=>'STCODE',
                      'title'=>'Stationery Code',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['naqrh'] = array(
                      'dbcol'=>'STNAME',
                      'title'=>'Stationery Name',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['fcx95'] = array(
                      'dbcol'=>'QTY',
                      'title'=>'Qty on Hand',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      $combogrid_array   = array();
	 
$combogrid_array['dzzg8']['columns']['SCTCODE']  = array( 'field'=>'SCTCODE', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['dzzg8']['columns']['SCTNAME']  = array( 'field'=>'SCTNAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['dzzg8']['source'] ='SLSCT';
  