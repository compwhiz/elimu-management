<?php

 
/**
* auto created config file for modules/lb/settings/library
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-01-23 14:17:10
*/

 final class library {
 private $id;
 private $datasrc;
 private $primary_key;
	
	
	public function __construct(){
		global $cfg;
		
		$this->id           = filter_input(INPUT_POST , 'id');
		$this->datasrc      = valueof($cfg,'datasrc');
		$this->primary_key  = valueof($cfg,'pkcol');
	}
	
	public function data(){
		global $db;
		
		$data = array();
		
        $library = $db->GetRow("SELECT * FROM SLCFG WHERE ID=1");
        
		$data['id']           = valueof( $library, 'ID'); 
		$data['libname']      = valueof( $library, 'LIBNAME'); 
		$data['timeopen']     = valueof( $library, 'TIMEOPEN'); 
		$data['timeclose']    = valueof( $library, 'TIMECLOSE'); 
		$data['blcfd']        = valueof( $library, 'BLCFD'); 
		$data['maxhlgn']      = valueof( $library, 'MAXHLGN'); 
		$data['mobile']       = valueof( $library, 'MOBILE'); 
		$data['addressphys']  = valueof( $library, 'ADDRESSPHYS'); 
		$data['email']        = valueof( $library, 'EMAIL'); 
		$data['logo']         = valueof( $library, 'LOGO');

	    if(defined('MAKE_FIELDS_UNIQUE') && defined('MNUID')) {
		foreach($data as $k=>$v){
		  $field_id = "{$k}_".MNUID; 
		  unset($data[$k]);
		  $data[$field_id] = $v;
		}
	   }

		return json_encode($data);
		
	}
	
	public function upload_logo(){
		global $db;
		
		$fileid  = ui::fi('library_logo');
		
		if(!isset($_FILES[$fileid])){
		 return  json_response(0,'Logo not Uploaded');	
		}
		
		$name      = $_FILES[$fileid]['name'];
		$tmp_name  = $_FILES[$fileid]['tmp_name'];
		$name      = $_FILES[$fileid]['name'];
		$type      = $_FILES[$fileid]['type'];
		$info      = pathinfo($tmp_name);
		
		$logo    = ROOT.'public/images/library_logo.jpg';
		
		if(file_exists($logo)){
			if(is_readable($logo)){
				@unlink($logo);
			}
		}
		
		if(!move_uploaded_file($tmp_name, $logo)){
		 return  json_response(0,'Upload Failed');		
		}
		
        $photo_small = ROOT.'public/images/library_logo_resized.jpg';
        
        $image = new simpleimage();
        $image->load($logo);
        $image->resize(160,110);
        $image->save($photo_small);
        $image->save($photo_small);
        
        rename($photo_small, $logo);
        
        $logo_info  = pathinfo($logo);
        $basename   = valueof($logo_info,'basename');
        
        $db->Execute("UPDATE SLCFG SET LOGO='{$basename}' WHERE ID=1");
        
		return  json_response(1,'Logo Uploaded');	
		
	}
	
	public function logo(){
		global $db;
		
		$logo_name       = $db->GetOne("SELECT LOGO FROM SLCFG WHERE ID=1");
		$logo            = ROOT.'public/images/'.$logo_name;
		
		if(file_exists($logo) && is_readable($logo) && is_file($logo)){
		 $logo_info = pathinfo($logo);
		 $image_name =  valueof($logo_info,'basename');	
		 $rand = mt_rand();	
		 $web_img = '<img src="./public/images/'.$image_name.'?cache_reload='.$rand.'" border="0">';
		 $message = "OK";
		}else{
		 $web_img = '<img src="./public/images/logo-default.gif" border="0">';
		 $message = "NO PHOTO";
		}
		
		return json_encode( array(
	     'success' => 1,
	     'message' => $message,
	     'logo' => $web_img,
	    ));
	 
	}
	
	public function save(){
		global $db,$cfg;

    $user              = new user();		
    
    $library = new ADODB_Active_Record('SLCFG', array('ID'));
    $library->Load("ID=1");
    
    if(empty($library->_original)){
     $library->id           = generateID('SLCFG');
    }
      
	$library->libname      =  filter_input(INPUT_POST, ui::fi('libname'));	
	$library->addressphys  =  filter_input(INPUT_POST, ui::fi('addressphys'));
	$library->postalcode   =  filter_input(INPUT_POST, ui::fi('postalcode'));
	$library->mobile       =  filter_input(INPUT_POST, ui::fi('mobile'));
	$library->email        =  filter_input(INPUT_POST, ui::fi('email' , FILTER_VALIDATE_EMAIL));
	$library->timeopen     =  filter_input(INPUT_POST, ui::fi('timeopen'));
	$library->timeclose    =  filter_input(INPUT_POST, ui::fi('timeclose'));
	$library->maxhlgn      =  filter_input(INPUT_POST, ui::fi('maxhlgn'));
	$library->blcfd        =  filter_input(INPUT_POST, ui::fi('blcfd'));
	$library->audituser    =  $user->userid;
	$library->auditdate    =  date('Y-m-d'); 
	$library->audittime    =  time();
    
    if($library->Save()){
     return json_response(1,'record saved');
    }else{
     $error = $db->ErrorMsg();	
     return json_response(0,"save failed : {$error}");
    }

	}
	
	public function remove(){
		global $db, $cfg;
		json_response(0,'failed');
	}
	
}
