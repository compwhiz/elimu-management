<?php

/**
* auto created config file for modules/students/setup/school
* @author coderX
* @todo by-me-beer
* @version 1.0
* @since 2014-09-29 23:35:58
*/

if(!defined('MAKE_FIELDS_UNIQUE')){
 define('MAKE_FIELDS_UNIQUE' , true);
}

$scriptname  = @end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'Library';//user-readable formart
 $cfg['appname']       = 'library';//lower cased one word
 $cfg['datasrc']       = 'SLCFG';//where to get data
 $cfg['datatbl']       = 'SLCFG';//base data src [for updates & deletes]
 $cfg['form_width']    = 400;
 $cfg['form_height']   = 200;
 
 $cfg['pkcol']         = 'ID';//the primary key
 

