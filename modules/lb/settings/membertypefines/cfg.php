<?php
/**
* auto created config file for modules/lb/settings/membertypefines
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-01-23 16:00:31
*/

$scriptname  = @end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'Member Type Fines';//user-readable formart
 $cfg['appname']       = 'membertypefines';//lower cased one word
 $cfg['datasrc']       = 'VIEWLIBFINES';//where to get data
 $cfg['datatbl']       = 'SLMTF';//base data src [for updates & deletes]
 $cfg['form_width']    = 400;
 $cfg['form_height']   = 230;
 $cfg['window_width']    = 500;
 $cfg['window_height']   = 400;
 
 $cfg['pkcol']         = 'MTPCODE';//the primary key
 
 $cfg['tblbns']['chk_button_add']    = 1;
 $cfg['tblbns']['chk_button_edit']   = 1;
 $cfg['tblbns']['chk_button_delete'] = 1;
 $cfg['tblbns']['chk_button_import'] = 1;
 $cfg['tblbns']['chk_button_export'] = 1;
 

$cfg['columns']['dyca1'] = array(
                      'dbcol'=>'MTPCODE',
                      'title'=>'Member Type',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>3,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'SLMTP|MTPCODE|MTPNAME|MTPCODE',
                      );
                      



                      
$cfg['columns']['ovcit'] = array(
                      'dbcol'=>'MTPNAME',
                      'title'=>'Member Type',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>2,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['qgxnw'] = array(
                      'dbcol'=>'TYPECODE',
                      'title'=>'Charge Type',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>3,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'SLFS|TYPECODE|TYPENAME|TYPECODE',
                      );
                      



                      
$cfg['columns']['dml8o'] = array(
                      'dbcol'=>'TYPENAME',
                      'title'=>'Charge Type',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>2,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['5dmpi'] = array(
                      'dbcol'=>'MUCODE',
                      'title'=>'Measure Unit',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'FINMU|MUCODE|MUNAME|MUCODE',
                      );
                      



                      
$cfg['columns']['drbwf'] = array(
                      'dbcol'=>'AMOUNT',
                      'title'=>'Charge',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'numberbox',
                      'selectsrc'=> '',
                      );
                      



                      $combogrid_array   = array();
	 
$combogrid_array['dyca1']['columns']['MTPCODE']  = array( 'field'=>'MTPCODE', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['dyca1']['columns']['MTPNAME']  = array( 'field'=>'MTPNAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['dyca1']['source'] ='SLMTP';
	 
$combogrid_array['qgxnw']['columns']['TYPECODE']  = array( 'field'=>'TYPECODE', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['qgxnw']['columns']['TYPENAME']  = array( 'field'=>'TYPENAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['qgxnw']['source'] ='SLFS';
	 
$combogrid_array['5dmpi']['columns']['MUCODE']  = array( 'field'=>'MUCODE', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['5dmpi']['columns']['MUNAME']  = array( 'field'=>'MUNAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['5dmpi']['source'] ='FINMU';
  