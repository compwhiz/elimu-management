<?php

/**
* auto created config file for modules/system/users
* @author kenmsh@gmail.com
* 
* @version 2.0
* @since 2016-01-16 11:21:23
*/

 final class users {
 private $id;
 private $datasrc;
 private $datatbl;
 private $primary_key;
	
 public function __construct(){
  global $cfg;
		
     $this->id           = filter_input(INPUT_POST , 'id');
     $this->datasrc      = valueof($cfg,'datasrc');
     $this->datatbl      = valueof($cfg,'datatbl');
     $this->primary_key  = valueof($cfg,'pkcol');
 }
	
 	
 public function save(){
  global $db,$cfg;
		
		$pk_col     =  isset($cfg['pkcol']) ? $cfg['pkcol'] : $this->primary_key;
		$user       =  new user();
		$pk_cols    =  array();
		$pk_values  =  array();
		$keycolums  =  $db->MetaPrimaryKeys( $this->datatbl );
		
	    if( sizeof($keycolums)>0 && isset($cfg['columns']) ) {
	     if(sizeof($cfg['columns'])>0){
		  foreach ($keycolums as $keycolum) {
		   if($keycolum=='ID'){
			 $pk_value    = 0;
			 $dbcol       = 'ID';
			 $pk_values[]  = "{$keycolum}='{$this->id}'";
			 $pk_cols[]    = "{$dbcol}";
		    }else{
		    foreach ($cfg['columns'] as $col => $col_properties){
		  	 $dbcol      = ($col_properties['dbcol']);
		  	 if($dbcol == $keycolum){
		  	 $pk_value     = filter_input(INPUT_POST , $col, FILTER_SANITIZE_STRING);
		  	 $pk_values[]  = "{$keycolum}='{$pk_value}'";
		  	 $pk_cols[]    = $dbcol;
		  	 }
		  	}
		   }
		  }
	     }
	    
	    if( sizeof($pk_values)>0 && sizeof($pk_cols)>0 ){
		 $record        = new ADODB_Active_Record( $this->datatbl, $pk_cols );
		 $pk_values_str = implode(' and ', $pk_values);
		 $record->Load("{$pk_values_str}");
	    }
	    
	    }else{
		 $record  = new ADODB_Active_Record( $this->datatbl, array( ID ));
		 $record->Load("ID={$this->id}");	
		}
		
		if(empty($record->_original)){
		 $record->id      = generateID($this->datatbl);
		 $record->$pk_col = $pk_value;
		}
		
	    if(isset($cfg['columns'])){
	     if(sizeof($cfg['columns'])>0){
		  foreach ($cfg['columns'] as $col => $col_properties){
		  	 $dbcol           = strtolower($col_properties['dbcol']);
		  	 $value           = filter_input(INPUT_POST , $col ,FILTER_SANITIZE_STRING);
		  	 $value           = centerTrim($value);
			 
		  	 if($dbcol=='password' && !empty($value) ) { 
			  $value = md5( strtolower($record->userid).$value.strtolower($record->userid));
			 }
			 
		  	 $record->$dbcol  = $value;
		  }
	     }
	    }	
		
		@$record->audituser  = $user->userid;
		@$record->auditdate  = $user->auditdate;
		@$record->audittime  = $user->audittime;
		@$record->auditip    = $user->auditip;
		
		if($record->Save()){
			echo json_response(1,'ok',array('id'=>$record->id));
		}else{
			echo json_response(0,'save failed '.$db->ErrorMsg());
		}
	
 }
	
	
 public function remove(){
  global $db, $cfg;
  
   $grid = new grid();
  return $grid->grid_remove_row();
 }
	
 	
 public function export(){
  global $db, $cfg;
  
   $grid = new grid();
  return $grid->export();
 }
 	
 public function import(){
  global $db, $cfg;
  
   $grid = new grid();
  return $grid->import();
 }
 
}
