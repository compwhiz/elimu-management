<?php

 $system_applications        = ui::form_select('appid','SYSAPPS','APPID','APPNAME','',"","onchange=\"{$cfg['appname']}.list_modules();\"  class=\"easyui-validatebox textbox\" ");
 $system_applications_modvars = ui::form_select('modid','SYSMODS','MODID','MODNAME','',''," class=\"easyui-validatebox textbox\" ",'','MODID');
 
?>

	
<form id="<?php echo ui::fi('ff'); ?>"   method="POST"  enctype="multipart/form-data" onsubmit="return false;"  >
<table width="100%" border="0" cellpadding="4"  cellspacing="1" >
 
<tr>
 <td width="150px">User Group</td>
 <td><?php print ui::form_select('groupcode','USERGROUPS','GROUPCODE','GROUPNAME','','','',0,'GROUPNAME'); ?></td>
</tr>
 
<tr>
 <td>Application</td>
 <td ><?php echo $system_applications; ?></td>
</tr>
 
<tr>
 <td>Modules</td>
 <td id="tdModules" >&nbsp;</td>
</tr>

<tr>
 <td valign="top">Menu</td>
 <td id="tdMenu" >
    <div style="width:200px;height:auto;border:1px solid #ccc;">
    <ul id="<?php echo ui::fi('tt'); ?>" class="easyui-tree" ></ul>
    </div></td>
</tr>

 <tr>
 <td>&nbsp;</td>
 <td ><a href="#" class="easyui-linkbutton" onclick="<?php echo $cfg['appname']; ?>.save();"><i class="fa fa-check-square"></i> Save</a> </td>
</tr>
 
</table>
</form>
 
	<script type="text/javascript">
		
	 var <?php echo $cfg['appname']; ?> = {
	 list_modules:function (){
        data = $('#<?php echo ui::fi('ff'); ?>').serialize() +'&modvars=<?php echo $vars; ?>';
        ui.cc('list_modules',data , 'tdModules');
     },
     list_menu:function (){
      data = $('#<?php echo ui::fi('ff'); ?>').serialize();
      $('#<?php echo ui::fi('tt'); ?>').html('&nbsp;');
      var t;
      $.ajax({
          type: 'POST',
          url:'./endpoints/crud/?',
          data: 'modvars=<?php echo $vars; ?>&function=list_menu&'+data,
          success: function(treeJson){
            t =$('#<?php echo ui::fi('tt'); ?>').tree({
            animate:true,
            checkbox:true,
            cascadeCheck:true,
            onlyLeafCheck:false,	
	        data: treeJson,
	        dnd:true,
	        onClick: function(node){
                       var isLeaf = true;
				      if(typeof node.children==='object'){
				       isLeaf = false;
				       if(typeof node.state==='string'){
				        if(node.state==='closed'){
                         $('#<?php echo ui::fi('tt'); ?>').tree('expand',node.target);
				        }else{
                          $('#<?php echo ui::fi('tt'); ?>').tree('collapse',node.target);
				        }
				       }
				      }
	          }
            });
          }
        });
        },
	    collapse:function (mnu){
			var node = $('#'+mnu).tree('getSelected');
			$('#'+mnu).tree('collapse',node.target);
		},
		expand:function (mnu){
			var node = $('#'+mnu).tree('getSelected');
			$('#'+mnu).tree('expand',node.target);
		},
		save:function (){
			 var nodes = $('#<?php echo ui::fi('tt'); ?>').tree('getChecked');
             var auths = '';
             
			 for(var i=0; i<nodes.length; i++){
			  auths += '&auths[]=';
			  auths += nodes[i].id;
			 }
			
			 $.messager.confirm('Authotizations', "Save Authotizations now?", function(r) {
				 
 	 	     if (r){
			 $.messager.progress();
			 var fdata = $('#<?php echo ui::fi('ff'); ?>').serialize() +'&modvars=<?php echo $vars; ?>&function=save&'+auths;
		     $.post('./endpoints/crud/', fdata, function(data) {
		     $.messager.progress('close');
             if (data.success === 1) {
                $.messager.show({title: 'Success',msg: data.message});
              } else {
                $.messager.alert('Error',data.message,'error');
             }
            }, "json");
		   }
		  });
		  
		},
	 }
	</script>
