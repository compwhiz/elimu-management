<?php

/**
* auto created config file for modules/system/authorizations
* @author kenmsh@gmail.com
* @todo by-me-beer
* @version 1.0
* @since 2014-08-07 00:40:21
*/

if(!defined('MAKE_FIELDS_UNIQUE')){
 define('MAKE_FIELDS_UNIQUE' , true);
}

$scriptname  = @end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'Authotizations';//user-readable formart
 $cfg['appname']       = 'authotizations';//lower cased one word
 $cfg['datasrc']       = 'SYSMNUAUTH';//where to get data
 $cfg['datatbl']       = 'SYSMNUAUTH';//base data src [for updates & deletes]
 $cfg['form_width']    = 400;
 $cfg['form_height']   = 200;
 
