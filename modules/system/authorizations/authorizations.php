<?php

/**
* auto created config file for modules/users/authotizations
* @author coderX
* @todo by-me-beer
* @version 1.0
* @since 2014-08-07 00:40:21
*/

 final class authorizations {
 private $id;
 private $datasrc;
 private $primary_key;
 
 private $menu_parents = array();
	
 public function __construct(){
  global $cfg;
		
     $this->id           = filter_input(INPUT_POST , 'id');
     $this->datasrc      = valueof($cfg,'datasrc');
     $this->primary_key  = valueof($cfg,'pkcol');
 }
 
 public function list_modules(){
	 global $db,$cfg;
	 $appid =  filter_input(INPUT_POST , ui::fi('appid'));
	 return  ui::form_select('modid','SYSMODS','MODID','MODNAME'," APPID='{$appid}' ",'',"onchange=\"{$cfg['appname']}.list_menu();\"",'','MODID');
 }
 
 public function list_menu(){

    $groupcode  = filter_input(INPUT_POST , ui::fi('groupcode'));
    $appid      = filter_input(INPUT_POST , ui::fi('appid'));
    $modid      = filter_input(INPUT_POST , ui::fi('modid'));
    
    header('Content-type: text/json');
    
    $auths = new auths($appid, $modid, $groupcode);
    
    echo $auths->list_menu();  
 
 }
	
 public function list_menu_children( $parentid ){
		
      $num_children = count( $this->menu_children[$parentid] );
      
     if($num_children>0){
     	$count = 1;
     	$tree = "";
     	
     	 foreach ($this->menu_children[$parentid] as $childid=>$child){
     	 	
     	   $childname      = valueof($child,0);
     	   $modpath        = valueof($child,1);
     	   
     	    if(!array_key_exists($childid, $this->menu_children)){
     	     $state = 'open';	
     	    }else{
     	     $state = 'closed';	
     	    }
     	    
		   $vars['appid']    = $this->_appid;
		   $vars['modid']    = $this->_modid;
		   $vars['mnuid']    = $childid;
		   $vars['modpath']  = $modpath;
		   $vars_str         = packvars($vars);
     	      
     	   $tree .= "{\r\n";
     	    $tree .=  "\"id\":{$childid},\r\n";
     	    $tree .=  "\"text\":\"{$childname}\",\r\n";
     	    $tree .= "\"state\":\"{$state}\", \r\n";
     	    $tree .=  "\"attributes\":{ \"url\":\"./endpoints/mod/?vars={$vars_str}\", \"modpath\":\"0\" }\r\n";
     	    //children
     	    if(array_key_exists($childid, $this->menu_children)){
     	    	$tree .= ",\r\n";//terminate  attributes
     	    	$tree .=  "\"children\":[\r\n";
     	         $tree .=  self::list_menu_children( $childid );
     	        $tree .=  "]\r\n";
     	    }
     	    
     	     $tree .=  "}";//close leaf
     	   
     	   if($count<$num_children) {
     	   	$tree .= ",\r\n";
     	   }
     	   
     	   ++$count;
         }
         
      }
      
     return $tree;
     
	}
	
 public function save(){
  global $db,$cfg;
    
    $groupcode  = filter_input(INPUT_POST , ui::fi('groupcode'));
    $appid      = filter_input(INPUT_POST , ui::fi('appid'));
    $modid      = filter_input(INPUT_POST , ui::fi('modid'));
   
     //clear
     $db->Execute("DELETE FROM SYSMNUAUTH WHERE GROUPCODE='{$groupcode}' AND APPID='{$appid}' AND MODID='{$modid}'");
     
     if(!isset($_POST['auths'])){
     	return json_response(0,"Authorizations Cleared for Group {$groupcode} in Module {$modid}");
     }
     
     $allow_access = array();
     
     
     $auths_sql_in = implode(',',$_POST['auths']);
     $menu         = $db->GetAssoc("SELECT MNUID,PARENTID FROM SYSMODMNU WHERE MNUID IN ({$auths_sql_in})");
	 $menu_parents = $db->GetArray("SELECT DISTINCT PARENTID FROM SYSMODMNU WHERE APPID='{$appid}' AND MODID='{$modid}' ORDER BY PARENTID ");
	 
	 if(count($menu_parents)>0){
	 	foreach ($menu_parents as $menu_parent){
	 		$this->menu_parents[] = valueof($menu_parent, 'PARENTID');
	 	}
	 }
	 
     foreach ($menu as $menuid => $parentid){
     	
     	if(!array_search($menuid, $allow_access)){
     	 $allow_access[] = $menuid;
     	}
     	 
     	if($parentid>0){
     		if(!array_search($parentid, $allow_access) && !in_array($parentid, $allow_access)){
     	     $allow_access[] = $parentid;
     		}
     	}
     }
     
     
     foreach ($allow_access as $allow_access_menu_id){
     	$sysmnuauth = new ADODB_Active_Record('SYSMNUAUTH', array('APPID'));
     	$sql = "INSERT INTO SYSMNUAUTH (GROUPCODE,APPID ,MODID ,MNUID) VALUES('{$groupcode}','{$appid}','{$modid}',{$allow_access_menu_id})";
     	$db->Execute($sql);
     }
     
     return json_response(1,'menu saved');
     
 }
	
 public function remove(){
  global $db, $cfg;
  
   $grid = new grid();
  return $grid->grid_remove_row();
 }
 	
 public function export(){
  global $db, $cfg;
  
   $grid = new grid();
  return $grid->export();
 }
 	
 public function import(){
  global $db, $cfg;
  
   $grid = new grid();
  return $grid->import();
 }
 
}
