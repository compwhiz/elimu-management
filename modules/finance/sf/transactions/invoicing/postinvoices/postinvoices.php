<?php

/**
* auto created config file for /modules/finance/sf/transactions/invoicing/postinvoices
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-01-06 17:07:14
*/

  class postinvoices {
	private $id;
	private $datasrc;
	private $primary_key;
	private $admno;
	
	public function __construct(){
		global $cfg;
		
		$this->id           = filter_input(INPUT_POST , 'id');
		$this->datasrc      = valueof($cfg,'datasrc');
		$this->primary_key  = valueof($cfg,'pkcol');
	}
	
	public function list_invoices(){
		global $db;
			
		 $batches  = $db->GetAssoc("
		 SELECT H.ID, H.INVDESC, H.INVDATE, COUNT(DISTINCT(D.ADMNO)) NUM , SUM(D.AMOUNT) AMOUNT
		 FROM FININVH H
		 inner join FININVD D on D.YEARCODE = H.YEARCODE AND D.TERMCODE = H.TERMCODE AND D.FORMCODE = H.FORMCODE  
		 WHERE  (D.POSTED IS NULL OR  D.POSTED=0)
		 GROUP BY H.ID, H.INVDESC, H.INVDATE
		 ");
		 
		 $invoices = array();
		 
		 if(count($batches)){
			foreach ($batches as $batchid=> $batch){
				$invoices[$batchid]  = $batch['INVDESC'] .' ('. $batch['INVDATE'] .') '. $batch['NUM'] .' students' ;
			}
		 }
		 
		 return ui::form_select_fromArray('batchid', $invoices , '' , "",'', 400); 
	}


	public function post_invoices(){
		global $db,$cfg;
      
    $user              = new user();
	  $batchid           = filter_input(INPUT_POST , 'batchid');
    $statement_header  = $db->GetRow(" SELECT * FROM FININVH WHERE ID='{$batchid}'");

    $batchid     = valueof($statement_header, 'ID');
    $batchdesc   = valueof($statement_header, 'INVDESC');
    $invdate     = valueof($statement_header, 'INVDATE');
      
      $invoice_header_lines = $db->GetArray("
        SELECT H.YEARCODE,H.TERMCODE,H.FORMCODE,H.ID, H.INVDESC, H.INVDATE, D.ADMNO, D.INVDESC,D.ENTRYTYPE, D.FYRPRCODE, D.POSTED, 
        COUNT(DISTINCT(D.ID)) NUM , SUM(D.AMOUNT) AMOUNT
		FROM FININVH H
		inner join FININVD D on D.YEARCODE = H.YEARCODE AND D.TERMCODE = H.TERMCODE AND D.FORMCODE = H.FORMCODE  
		WHERE  H.ID={$batchid} AND (D.POSTED IS NULL OR  D.POSTED=0) 
		GROUP BY H.YEARCODE,H.TERMCODE,H.FORMCODE,H.ID, H.INVDESC, H.INVDATE, D.ADMNO, D.INVDESC,D.ENTRYTYPE, D.FYRPRCODE, D.POSTED
    ");
      
     if(count($invoice_header_lines)>0){
     	foreach ($invoice_header_lines as $invoice_header_line) {
     		
        $statement_header = new ADODB_Active_Record('FINSTH', array('ADMNO','BATCHID','SRCCODE'));
    
        $statement_header->Load("
         ADMNO='".valueof($invoice_header_line, 'ADMNO')."' 
         AND BATCHID='".valueof($invoice_header_line, 'ID')."' 
         AND SRCCODE='".valueof($invoice_header_line, 'ENTRYTYPE')."' 
        ");
      
       $statement_header_saved = true;
    
      if(empty($statement_header->_original)) {
      	
       $statement_header->id         = generateID( $statement_header->_tableat );
       $statement_header->admno      = valueof($invoice_header_line, 'ADMNO');
       $statement_header->yearcode   = valueof($invoice_header_line, 'YEARCODE');
       $statement_header->termcode   = valueof($invoice_header_line, 'TERMCODE');
       $statement_header->formcode   = valueof($invoice_header_line, 'FORMCODE');
       $statement_header->batchid    = $batchid;
       $statement_header->entryid    = 1;
       $statement_header->srccode    = valueof($invoice_header_line, 'ENTRYTYPE');
       $statement_header->amount     = valueof($invoice_header_line, 'AMOUNT');
       $statement_header->auditdate  = valueof($invoice_header_line, 'INVDATE');
       $statement_header->docno      = $batchid;
       $statement_header->docdesc    = $batchdesc;
       $statement_header->doctype    = valueof($invoice_header_line, 'ENTRYTYPE');
       $statement_header->accountdebit = 0;
       $statement_header->accountbank  = 0;
       $statement_header->bkcode       = 0;
       $statement_header->fyrprcode    = 0;
       $statement_header->auditdate    = date('Y-m-d');
       $statement_header->audittime    = time();
       $statement_header->audituser    = $user->userid;
       
        if(!$statement_header->Save()){
     	   $statement_header_saved = false;
        }
          
      }//header save once
         
         
         if($statement_header_saved){
         
                $invoice_batch_detail_lines  = $db->GetAssoc("
                SELECT * FROM FININVD
                WHERE ADMNO='".valueof($invoice_header_line, 'ADMNO')."' 
                AND YEARCODE='".valueof($invoice_header_line, 'YEARCODE')."' 
                AND TERMCODE='".valueof($invoice_header_line, 'TERMCODE')."' 
                AND FORMCODE='".valueof($invoice_header_line, 'FORMCODE')."' 
                AND HEADERID=".valueof($invoice_header_line, 'ID')."
               ");
      
                
               if(count($invoice_batch_detail_lines)>0){
                 foreach ($invoice_batch_detail_lines as $invoice_batch_detail_line){	
               
                //--------------------------------------------------------------------------------------------------------------------
              
                $statement_line = new ADODB_Active_Record('FINSTD', array('ADMNO','BATCHID','YEARCODE','TERMCODE','FORMCODE','DOCNO','FITEMCODE'));
    	
                $statement_line->Load("
                ADMNO='".valueof($invoice_batch_detail_line, 'ADMNO')."' 
                AND YEARCODE='".valueof($invoice_header_line, 'YEARCODE')."' 
                AND TERMCODE='".valueof($invoice_header_line, 'TERMCODE')."' 
                AND FORMCODE='".valueof($invoice_header_line, 'FORMCODE')."' 
                AND DOCNO='".valueof($invoice_batch_detail_line, 'INVNO')."' 
                AND FITEMCODE='".valueof($invoice_batch_detail_line, 'FITEMCODE')."' 
               ");
      
                
               $statement_line_saved = true;
    
                if(empty($statement_line->_original)){
                 	
                 $statement_line->admno      = valueof($invoice_batch_detail_line, 'ADMNO');
                 $statement_line->yearcode   = valueof($invoice_header_line, 'YEARCODE');
                 $statement_line->termcode   = valueof($invoice_header_line, 'TERMCODE');
                 $statement_line->formcode   = valueof($invoice_header_line, 'FORMCODE');
                 $statement_line->batchid    = valueof($invoice_batch_detail_line, 'HEADERID');
                 $statement_line->entryid    = generateID( $statement_line->_tableat,'ENTRYID' );
                 $statement_line->fitemcode  = valueof($invoice_batch_detail_line, 'FITEMCODE');
                 $statement_line->fitemname  = valueof($invoice_batch_detail_line, 'FITEMNAME');
                 $statement_line->docno      = valueof($invoice_batch_detail_line, 'INVNO');
                 $statement_line->docdesc    = valueof($invoice_batch_detail_line, 'INVDESC');
                 $statement_line->doctype    = $statement_header->doctype;
                 $statement_line->amount     = valueof($invoice_batch_detail_line, 'AMOUNT');
                 $statement_line->auditdate  = date('Y-m-d');
                 $statement_line->audittime  = time();
                 $statement_line->audituser  = $user->userid;
     
                 if($statement_line->Save()){
                 	
                  $db->Execute("UPDATE FININVD SET POSTED=1 
     	             WHERE ADMNO='{$statement_line->admno}' 
                   AND HEADERID={$statement_line->batchid}
                   AND INVNO='{$statement_line->docno}' 
                   AND FITEMCODE='{$statement_line->fitemcode}' 
                   ");
                  
                 }else{	
     	            $statement_line_saved = false;
                 }
     
                 
                 if($statement_line_saved){
                	// ++$students_invoiced;
                 }
                 
                }
                //--------------------------------------------------------------------------------------------------------------------
              }
            }
                
         }
         
     	}
      }
      
		  return json_response(1,'Processed');
      
		  json_response(0,'failed');	
		
      
  }
	
    public function delete(){
		global $db, $cfg;
		
		$id  = filter_input(INPUT_POST , 'id');
		$del = $db->Execute("UPDATE FINFSTR SET ACTIVE=0 WHERE ID={$id}");
		
		return  self::list_items();
		
		if($del){
		 json_response(1,'deleted');
		}else{
		  json_response(0,'failed');	
		}
		
	}
	

}
