<?php
/**
* auto created config file for /modules/finance/sf/transactions/invoicing/postinvoices
* @author kenmsh@gmail.com
* 
* @version 2.0
* @since 2016-01-06 17:07:14
*/

$scriptname  = @end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'Post Invoices';//user-readable formart
 $cfg['appname']       = 'postinvoices';//lower cased one word
 $cfg['datasrc']       = 'FININVH';//where to get data
 $cfg['datatbl']       = 'FININVH';//base data src [for updates & deletes]
 $cfg['form_width']    = 400;
 $cfg['form_height']   = 200;
 $cfg['window_width']    = 500;
 $cfg['window_height']   = 400;
 
 $cfg['pkcol']         = 'YEARCODE';//the primary key
 
 $cfg['tblbns']['chk_button_add']    = 1;
 $cfg['tblbns']['chk_button_edit']   = 1;
 $cfg['tblbns']['chk_button_delete'] = 1;
 $cfg['tblbns']['chk_button_import'] = 1;
 $cfg['tblbns']['chk_button_export'] = 1;
 

$cfg['columns']['aby0h'] = array(
                      'dbcol'=>'INVDESC',
                      'title'=>'Invdesc',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['bjxyu'] = array(
                      'dbcol'=>'INVDATE',
                      'title'=>'Invdate',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'date',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['nf8zo'] = array(
                      'dbcol'=>'YEARCODE',
                      'title'=>'Code',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['wxy1i'] = array(
                      'dbcol'=>'TERMCODE',
                      'title'=>'Code',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['fcxz6'] = array(
                      'dbcol'=>'FORMCODE',
                      'title'=>'Code',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      $combogrid_array   = array();
  