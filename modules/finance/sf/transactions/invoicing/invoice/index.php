<?php

/**
* auto created index file for /modules/finance/sf/transactions/invoicing/invoice
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-01-06 17:08:32
*/

 $forms   = $db->GetAssoc('SELECT FORMCODE,FORMNAME FROM SAFORMS');
 $streams_data = $db->GetAssoc('SELECT STREAMCODE,STREAMNAME,FORMCODE FROM SASTREAMS');
 $streams = $db->GetAssoc('SELECT STREAMCODE,STREAMNAME,FORMCODE FROM SASTREAMS');
 $years   = $db->GetAssoc('SELECT YEARCODE,YEARNAME FROM SAYEAR ORDER BY YEARCODE');
 $terms   = $db->GetAssoc('SELECT TERMCODE,TERMNAME FROM SATERMS ORDER BY TERMCODE');
 

 $forms = array();
 if(isset($streams_data)){
  if(sizeof($streams_data)>0){	
   foreach ($streams_data as $streamcode=>$stream_data){
   	$streamname = valueof($stream_data, 'STREAMNAME');
   	$formcode   = valueof($stream_data, 'FORMCODE');
   	$forms[$formcode] = "Form {$formcode}";
   	$forms[$streamcode] = "Form {$formcode}-{$streamname}";
   }
  }
 }
 
  $school = new school();
?>

 <style type="text/css">
 table.main{
  font-family: arial;
  font-size: 12px;	
 }
 </style>	
	<form id="<?php echo ui::fi('ff'); ?>" method="post" novalidate>
		
	    	<table cellpadding="2" cellspacing="0" width="100%" class="main">
	    	
	    		<tr>
	    			<td  width="180px">Invoice Date:</td>
	    			<td><input id="<?php echo ui::fi('invdate'); ?>" name="<?php echo ui::fi('invdate'); ?>" type="text" class="easyui-datebox" data-options="formatter:dateYmdFormatter,parser:dateYmdParser" placeholder="YYYY-MM-DD" value="<?php echo date('Y-m-d'); ?>"></td>
	    			<td>Description:</td>
	    			<td><input  class="easyui-validatebox textbox" type="text"  id="<?php echo ui::fi('invdesc'); ?>"  name="<?php echo ui::fi('invdesc'); ?>" size="30" ></input></td>
	    		</tr>
	    		
	    		<tr>
	    			<td>Year:</td>
	    			<td><?php echo ui::form_select_fromArray('year', $years, $school->active_yearcode," onchange=\"{$cfg['appname']}.list_all();\"");  ?></td>
	    			<td>Term:</td>
	    			<td><?php echo ui::form_select_fromArray('term', $terms,$school->active_termcode,"onchange=\"{$cfg['appname']}.list_all();\"");  ?></td>
	    		</tr>
	    		
	    		<tr>
	    			<td width="180px">Stream</td>
	    			<td><?php echo ui::form_select_fromArray('form', $forms,'',"onchange=\"{$cfg['appname']}.list_all();\"");  ?></td>
	    			<td>&nbsp;</td>
	    			<td><a href="javascript:void(0)" class="easyui-linkbutton"  onclick="<?php echo $cfg['appname']; ?>.list_all();">List Items & Students </a></td>
	    		</tr>
	    		
	    		<tr>
	    			<td colspan="4">
	    			
					<div class="easyui-accordion"  style="width:680px;height:300px" fit="true">
						
					<div title="Invoicable Items" style="overflow:auto;padding:10px;">
						<div id="<?php echo ui::fi('div_list_items'); ?>"></div>
					</div>
					
					 <div title="Students List"  style="padding:10px;">
					  <div id="<?php echo ui::fi('div_list_students'); ?>"></div>
					 </div>
				   </div>
	    			
	    		 </td>
	    		</tr>
	    	
	    	   <tr>
	    			<td >&nbsp;</td>
	    			<td colspan="2">
	    			<a href="javascript:void(0)" class="easyui-linkbutton"  onclick="<?php echo $cfg['appname']; ?>.invoice();"><i class="fa fa-cogs"></i> Invoice Students</a>
	    			|
	    			<a href="javascript:void(0)" class="easyui-linkbutton"  onclick="<?php echo $cfg['appname']; ?>.clearForm()"><i class="fa fa-refresh"></i> Reset</a>
	    			</td>
	    		</tr>
	    			
	    	</table>
 
     </form>

	<script>
		
	 var <?php echo $cfg['appname']; ?> = {
		clearForm:function (){
			$('#<?php echo ui::fi('ff'); ?>').form('clear');
		},
		list_all:function (){
		 this.list_items();
		 this.list_students();
		},
		list_items:function (){
		 ui.cc('list_items',$('#<?php echo ui::fi('ff'); ?>').serialize() + '&modvars=<?php echo $vars; ?>','<?php echo ui::fi('div_list_items'); ?>');
		},
		list_students:function (){
		 var form = $('#<?php echo ui::fi('form'); ?>').val();	
		 var term = $('#<?php echo ui::fi('term'); ?>').val();	
		 var year = $('#<?php echo ui::fi('year'); ?>').val();	
		 var invdesc = 'Fees Invoice Form '+form+' Term '+term+' - '+year;	
		 $('#<?php echo ui::fi('invdesc'); ?>').val(invdesc);
		 ui.cc('list_students',$('#<?php echo ui::fi('ff'); ?>').serialize() + '&modvars=<?php echo $vars; ?>','<?php echo ui::fi('div_list_students'); ?>');
		},
		toggle_student:function(){
			//var checked = $('#<?php echo ui::fi('inv_checkall'); ?>').prop("checked");
            $(".inv_students").prop('checked', $('#<?php echo ui::fi('inv_checkall'); ?>').prop("checked"));
		},
		invoice:function (){
			var validate =  $('#<?php echo ui::fi('ff'); ?>').form('validate');
			if(!validate){
				$.messager.alert('Error','Fill In All Required Fields','error');
			 return;	
			}else{
			 $.messager.confirm('Invoicing...', "Invoice Selected students now?", function(r) {
 	 	     if (r){
			 $.messager.progress();
			 var fdata = $('#<?php echo ui::fi('ff'); ?>').serialize() + '&modvars=<?php echo $vars; ?>&function=save';
		     $.post('./endpoints/crud/', fdata, function(data) {
		     $.messager.progress('close');
             if (data.success === 1) {
                $.messager.show({title: 'Success', msg: data.message});
              } else {
                $.messager.alert('Error',data.message,'error');
             }
            }, "json");
		   }
		  });
		 }
		},
	   }
		
	</script>
