<?php

/**
* auto created config file for /modules/finance/sf/transactions/receipting/approve
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-01-06 17:10:22
*/

  class approve{
	private $id;
	private $datasrc;
	private $primary_key;
	private $admno;
	
	public function __construct(){
		global $cfg;
		
		$this->id           = filter_input(INPUT_POST , 'id');
		$this->datasrc      = valueof($cfg,'datasrc');
		$this->primary_key  = valueof($cfg,'pkcol');
	}
	
	public function data(){
		global $db;
		 //$db->debug=1;
		
	$page   = isset($_POST['page']) ? intval($_POST['page']) : 1;
    $rows   = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
    $sort   = isset($_POST['sort']) ? strval($_POST['sort']) : 'RECEIPTNO';
    $order  = isset($_POST['order']) ? strval($_POST['order']) : 'asc';
    $offset = ($page-1)*$rows;
 		
    //data sorting
    
    $sortbys = array();
    
    if( isset($_POST['sort']) && !empty($_POST['sort']) ) {
	 $sort_cols_array  = explode(',', $_POST['sort']);
	 $sort_order_array = explode(',', $_POST['order']);
	 
	 if(sizeof($sort_cols_array)>0){
		foreach($sort_cols_array as $sort_col_index => $sort_col_name){
		  
		 $sort_col_name   = isset($cfg['columns'][$sort_col_name]['dbcol']) ? strtoupper($cfg['columns'][$sort_col_name]['dbcol']) : $sort_col_name;
		 $sort_col_order  = valueof($sort_order_array, $sort_col_index, 'asc');
		 $sortbys[]       = "{$sort_col_name} {$sort_col_order}";
		}
	 }
	 
	 if(sizeof($sortbys)>0){
	  $sortby = ' order by ' .implode(',', $sortbys);
	 }
	 
   }else{
	  $sortby =  ' order by RECEIPTNO asc';  
   }
   
    $filters_conditions_str =  '';
    
    /**
     * @ekariz 22OCT15 
     */
     
    if(isset($_POST['filterRules'])) {
	 $replace         = array("'");
	 $filterRules     = $_POST['filterRules'];
	 $filterRules_obj = json_decode($filterRules);
	 $filters_conditions = array();
	 
	 foreach($filterRules_obj as $filterRule){
	  $filterRuleCol = @$filterRule->field;
	  $filterRuleOpr = @$filterRule->op;
	  $filterRuleVal = str_replace( $replace, "\'", @$filterRule->value );
	  
	  $filterRuleDBCol = $filterRuleCol;
	  
	  if(!is_null($filterRuleDBCol)){
	  
	   switch($filterRuleOpr){
         case 'none' : $filters_conditions[]="{$filterRuleDBCol} is not null";break;
         case 'contains' : $filters_conditions[]="{$filterRuleDBCol} like '%{$filterRuleVal}%'";break;
         case 'equal' : $filters_conditions[]="{$filterRuleDBCol} = '{$filterRuleVal}'";break;
         case 'notequal' : $filters_conditions[]="{$filterRuleDBCol} != '{$filterRuleVal}'";break;
         case 'beginwith' : $filters_conditions[]="{$filterRuleDBCol} like '{$filterRuleVal}%'";break;
         case 'endwith' : $filters_conditions[]="{$filterRuleDBCol} like '%{$filterRuleVal}'";break;
         case 'less' : $filters_conditions[]="{$filterRuleDBCol} < '{$filterRuleVal}'";break;
         case 'lessorequal' : $filters_conditions[]="{$filterRuleDBCol} <= '{$filterRuleVal}'";break;
         case 'greater' : $filters_conditions[]="{$filterRuleDBCol} > '{$filterRuleVal}'";break;
         case 'greaterorequal' : $filters_conditions[]="{$filterRuleDBCol} >= '{$filterRuleVal}'";break;
        }
        
	  }
        
	 }
	 
	 if(sizeof($filters_conditions)>0){
		$filters_conditions_str = 'and ('. implode(' and ', $filters_conditions). ')';
	 }
	}
	
    $sql_where = ' where (APPROVED is null or APPROVED=0) and (PROCESSED is null or PROCESSED=0) '. $filters_conditions_str;
    
	$numRecords = $db->GetOne("select count(*) from VIEWSFRCTS {$sql_where} ");

	$result["total"] = $numRecords;
	
	$rs = $db->SelectLimit("
	select RECEIPTNO , BATCHID , ADMNO , FULLNAME , YEARCODE , TERMCODE , FORMCODE , DATERECEIPT , PYMDCODE , 
	PYMDDESC , DOCUMENTNO , DOCUMENTDATE , DOCUMENTAMOUNT
	from VIEWSFRCTS  {$sql_where}  {$sortby}", $rows, $offset);
	
	$items = array();
	
	if($rs){
	 while(!$rs->EOF){
		 
	  $amount                        = valueof($rs->fields,'DOCUMENTAMOUNT');
	  $rs->fields['DOCUMENTAMOUNT'] = number_format( $amount ,2);
	  
	  array_push($items, $rs->fields);
	  $rs->MoveNext();
	 }
	}
	
	$result["rows"] = $items;

	echo header('Content-type : text/json');
	echo json_encode($result);
     
 }
	
	public function save(){
		global $db;
		 //print_pre($_POST);
	   //$db->debug=1;
     
		if(!isset($_POST['receiptnos']) || (isset($_POST['receiptnos']) && count($_POST['receiptnos'])==0)){
		  return json_response(0,'Select Receipt(s) to Approve');
		}
		
	 $user     = new user(); 
	 
     foreach ($_POST['receiptnos'] as $receiptno){
      
      $receipt_header = $db->GetRow("
		select R.RECEIPTNO , R.BATCHID , R.ADMNO , 
		P.FULLNAME , R.YEARCODE , R.TERMCODE , R.FORMCODE , R.DATERECEIPT , 
		R.PYMDCODE , M.PYMDDESC , R.DOCUMENTNO , R.DOCUMENTDATE , R.DOCUMENTAMOUNT , R.BKCODE, R.FYRPRCODE
		from FINRCTHD R
		inner join SASTUDENTS P ON R.ADMNO = P.ADMNO
		inner join FINPYMD M ON M.PYMDCODE = R.PYMDCODE
		where R.RECEIPTNO='{$receiptno}'
      ");
      
        $batchid         = valueof($receipt_header, 'BATCHID');
        $fullname        = valueof($receipt_header, 'FULLNAME');
        $admno           = valueof($receipt_header, 'ADMNO');
        $accountno       = valueof($receipt_header, 'ACCOUNTNO');
        $yearcode        = valueof($receipt_header, 'YEARCODE');
        $termcode        = valueof($receipt_header, 'TERMCODE');
        $formcode        = valueof($receipt_header, 'FORMCODE');
        $datereceipt     = valueof($receipt_header, 'DATERECEIPT');
        $pymdcode        = valueof($receipt_header, 'PYMDCODE');
        $pymddesc        = valueof($receipt_header, 'PYMDDESC',$pymdcode);
        $documentno      = valueof($receipt_header, 'DOCUMENTNO');
        $documentdate    = valueof($receipt_header, 'DOCUMENTDATE');
        $documentamount  = valueof($receipt_header, 'DOCUMENTAMOUNT');
        $documentamount  = $documentamount>0 ? ($documentamount*-1 ) : $documentamount;
        $bkcode          = valueof($receipt_header, 'BKCODE');
        $fyrprcode       = valueof($receipt_header, 'FYRPRCODE');
        $doctype         = 'REC';
        
       // echo "\$admno={$admno} <br>";
       // echo "\$yearcode={$yearcode} <br>";
       // echo "\$termcode={$termcode} <br>";
       // echo "\$formcode={$formcode} <br>";
         // exit();
        
     if(count($receipt_header)>0) {
     		
        $statement_header = new ADODB_Active_Record('FINSTH', array('ADMNO','DOCNO'));
    
        $statement_header->Load("
         ADMNO='".$admno."' 
         AND DOCNO='".$receiptno."' 
        ");
      
       $statement_header_saved = true;
    
      if(empty($statement_header->_original)) {
      	
       $statement_header->id         = generateID( $statement_header->_tableat );
       $statement_header->admno      = $admno;
       $statement_header->yearcode   = $yearcode;
       $statement_header->termcode   = $termcode;
       $statement_header->formcode   = $formcode;
       $statement_header->batchid    = $batchid;
       $statement_header->entryid    = 1;
       $statement_header->srccode    = $doctype;
       $statement_header->amount     = $documentamount;
       $statement_header->auditdate  = $documentdate;
       $statement_header->docno      = $receiptno;
       $statement_header->docdesc    = "FEES PAYMENT {$documentno}";
       $statement_header->doctype    = $doctype;
       $statement_header->accountdebit = 0;
       $statement_header->accountbank  = 0;
       $statement_header->bkcode       = $bkcode;
       $statement_header->fyrprcode    = $fyrprcode;
       $statement_header->audituser    = $user->userid;
       $statement_header->audittime    = time();
     
       //print_pre($statement_header);
      // exit();
       
        if(!$statement_header->Save()){
     	   $statement_header_saved = false;
        }
          
      }//header save once
         
         
//         echo "\$statement_header_saved={$statement_header_saved} <br>";
//         $db->debug=1;
         
         if($statement_header_saved){
         
                $receipt_detail_lines  = $db->GetAssoc("
                SELECT * FROM FINRCTDT
                WHERE ADMNO='".$admno."' 
                AND RECEIPTNO='".$receiptno."'
               ");
      
                $receipt_detail_lines_count = count($receipt_detail_lines);
                $receipt_detail_lines_processed = 0;
//                print_pre($receipt_detail_lines);
//                exit();
                
               if(count($receipt_detail_lines)>0){
                 foreach ($receipt_detail_lines as $receipt_detail_line){	
               
                	// print_pre($receipt_detail_line);
//                 	exit();
                 	
                //--------------------------------------------------------------------------------------------------------------------
//            
                $statement_line = new ADODB_Active_Record('FINSTD', array('ADMNO', 'DOCNO','FITEMCODE'));
    	
                $statement_line->Load("
                ADMNO='".$admno."' 
                AND DOCNO='".$receiptno ."' 
                AND FITEMCODE='".valueof($receipt_detail_line, 'FITEMCODE')."' 
               ");
      
//                print_pre($statement_line);
                
               $statement_line_saved = true;
    
                if(empty($statement_line->_original)){
                 	
//                 $statement_line->id         = generateID( $statement_line->_tableat );
                 $statement_line->admno      = valueof($receipt_detail_line, 'ADMNO');
                 $statement_line->yearcode   = $yearcode;
                 $statement_line->termcode   = $termcode;
                 $statement_line->formcode   = $formcode;
                 $statement_line->batchid    = $batchid;
                 $statement_line->entryid    = generateID( $statement_line->_tableat, 'ENTRYID' );
                 $statement_line->fitemcode  = valueof($receipt_detail_line, 'FITEMCODE');
                 $statement_line->fitemname  = valueof($receipt_detail_line, 'FITEMNAME');
                 $statement_line->docno      = $statement_header->docno;
                 $statement_line->docdesc    = $statement_header->docdesc;
                 $statement_line->amount     = valueof($receipt_detail_line, 'AMOUNT');
                 $statement_line->doctype    = $doctype;
                 $statement_line->audituser  = $user->userid;
                 $statement_line->auditdate  = date('Y-m-d');
     
                 //print_pre($statement_line);
//                 exit();
                 
                 if($statement_line->Save()){
                 	
                  $db->Execute("UPDATE FINRCTDT SET PROCESSED=1 
     	              WHERE ADMNO='".$admno."' 
                    AND RECEIPTNO='".$receiptno ."' 
                    AND FITEMCODE='".valueof($receipt_detail_line, 'FITEMCODE')."' 
                   ");
                  
                 }else{	
     	            $statement_line_saved = false;
                 }
     
//                 exit();
                }//each line
                  
                 if($statement_line_saved){
                 	++$receipt_detail_lines_processed;
                 }
                 
                //--------------------------------------------------------------------------------------------------------------------
              }//each line
              
            }//if count dtls
            
            if($receipt_detail_lines_processed == $receipt_detail_lines_count ){
             $db->Execute("UPDATE FINRCTHD SET APPROVED=1, PROCESSED=1 WHERE RECEIPTNO='".$receiptno ."' ");
            }
//            exit();
            
          }//if header saved
     	}
     }
		  return json_response(1,'processed');
		  json_response(0,'failed');	
  }

}
