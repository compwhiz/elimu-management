<?php
/**
* auto created config file for /modules/finance/sf/transactions/receipting/receipt
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-01-06 17:09:46
*/

define('MAKE_FIELDS_UNIQUE' , true);

$scriptname  = @end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'Receipt';//user-readable formart
 $cfg['appname']       = 'student_receipt';//lower cased one word
 $cfg['datasrc']       = 'FINRCTDT';//where to get data
 $cfg['datatbl']       = 'FINRCTDT';//base data src [for updates & deletes]
 $cfg['form_width']    = 400;
 $cfg['form_height']   = 200;
 $cfg['window_width']    = 500;
 $cfg['window_height']   = 400;
 
 $cfg['pkcol']         = 'RECEIPTNO';//the primary key
 
 $cfg['tblbns']['chk_button_add']    = 1;
 $cfg['tblbns']['chk_button_edit']   = 1;
 $cfg['tblbns']['chk_button_delete'] = 1;
 $cfg['tblbns']['chk_button_import'] = 1;
 $cfg['tblbns']['chk_button_export'] = 1;
 

$cfg['columns']['q30ei'] = array(
                      'dbcol'=>'HEADERID',
                      'title'=>'Headerid',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['apmng'] = array(
                      'dbcol'=>'BATCHID',
                      'title'=>'Batchid',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['y0exm'] = array(
                      'dbcol'=>'RECEIPTNO',
                      'title'=>'Receiptno',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['jenoh'] = array(
                      'dbcol'=>'AMOUNT',
                      'title'=>'Amount',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'numberbox',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['bgp6v'] = array(
                      'dbcol'=>'FITEMCODE',
                      'title'=>'Code',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['je4im'] = array(
                      'dbcol'=>'FITEMNAME',
                      'title'=>'Name',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['9wco2'] = array(
                      'dbcol'=>'PROCESSED',
                      'title'=>'Processed',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['tzvlo'] = array(
                      'dbcol'=>'ADMNO',
                      'title'=>'Admno',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['dmrtk'] = array(
                      'dbcol'=>'YEARCODE',
                      'title'=>'Code',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['sxung'] = array(
                      'dbcol'=>'TERMCODE',
                      'title'=>'Code',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['8avyw'] = array(
                      'dbcol'=>'FORMCODE',
                      'title'=>'Code',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      
                      
$combogrid_array   = array();
$combogrid_array['find_admno']['columns']['ADMNO']      = array( 'field'=>'ADMNO', 'title'=>'Admission No', 'width'=> 100 , 'isIdField' => true );
$combogrid_array['find_admno']['columns']['FULLNAME']   = array( 'field'=>'FULLNAME', 'title'=>'Full Name', 'width'=> 200, 'isTextField'=>true);
$combogrid_array['find_admno']['columns']['STREAMCODE'] = array( 'field'=>'STREAMCODE', 'title'=>'Stream', 'width'=>80);
$combogrid_array['find_admno']['source'] ='VIEWSP';
	
  
