<?php

/**
* auto created config file for /modules/finance/sf/transactions/receipting/receipt
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-01-06 17:09:46
*/

 
  class receipt {
	private $id;
	private $datasrc;
	private $primary_key;
	private $admno;
	
	public function __construct(){
		global $cfg;
		
		$this->id           = filter_input(INPUT_POST , 'id');
		$this->datasrc      = valueof($cfg,'datasrc');
		$this->primary_key  = valueof($cfg,'pkcol');
	}
	
	public function data_precheck(){
	  global $db;
	  
        $admno   = filter_input(INPUT_POST, 'admno');
        $profile = $db->CacheGetOne(1200,"SELECT ADMNO FROM SASTUDENTS WHERE ADMNO='{$admno}' ");
        
        if(!empty($profile)){
		  return json_response(1,"Student  {$admno}  Found ");	
        }else{
          return json_response(0,"Student '{$admno}' Not Found ");	
        }
        	
	}
	
	public function data(){
		global $db;
		
		$data     = array();
        $admno    = filter_input(INPUT_GET, 'admno');
        $student  = new student($admno,0);
        
        if(empty($student->admno)){
          return json_response(0,"Student \'{$admno}\' Not Found ");	
        }
          
		$data['admno']       = $student->admno;
		$data['year']        = date('Y');
		$data['form']        = $student->streamcode;
        $data['formcode']    = $student->formcode;
		$data['admdate']     = $student->admdate;
		$data['mobilephone'] = $student->mobilephone;
		
	    if(defined('MAKE_FIELDS_UNIQUE') && defined('MNUID')) {
		 foreach($data as $k=>$v){
		  $field_id = "{$k}_".MNUID; 
		  unset($data[$k]);
		  $data[$field_id] = $v;
		 }
	    }

		return json_encode($data);
	}
	
	public function save(){
		global $db,$cfg;
		
		//print_pre($_POST);
		//$db->debug=1;
		
	  $user     = new user(); 
      $itemDef  = $db->GetOne("select FITEMCODE from FINITEMS where ISTUITION=1");
    
      if(!$itemDef){
    	 return json_response(0,'Error: Setup & mark default tuition fees item');
      }
      
      $formcode    = filter_input(INPUT_POST , ui::fi('form'));
	  $admno       = filter_input(INPUT_POST , ui::fi('find_admno'));
      $yearcode    = filter_input(INPUT_POST , ui::fi('year'));
      $termcode    = filter_input(INPUT_POST , ui::fi('term'));
      $paybank     = filter_input(INPUT_POST , ui::fi('paybank'));
      $paymode     = filter_input(INPUT_POST , ui::fi('paymode'));
      $paydate     = filter_input(INPUT_POST , ui::fi('paydate'));
      $payref      = filter_input(INPUT_POST , ui::fi('payref'));
      $payamount   = filter_input(INPUT_POST , ui::fi('payamount', FILTER_VALIDATE_FLOAT));
      $rct_number  = self::get_rct_number( $paybank );
      $fyear       = self::get_fyear();
      $student     = new student($admno, 0);
      
      if(strlen($formcode)>1){
       $formcode = substr($formcode,0,1);
      }
      
      //echo "\$yearcode={$yearcode} <br>";
      //echo "\$termcode={$termcode} <br>";
      //echo "\$formcode={$formcode} <br>";
      //echo "\$paybank={$paybank} <br>";
      //echo "\$paymode={$paymode} <br>";
      //echo "\$paydate={$paydate} <br>";
      //echo "\$payref={$payref} <br>";
      //echo "\$payamount={$payamount} <br>";
      //echo "\$rct_number={$rct_number} <br>";
      //echo "\$fyear={$fyear} <br>";
      //exit();
      
      $items    = $db->CacheGetAssoc(1200,"SELECT FITEMCODE ,FITEMNAME FROM FINITEMS ");
      $bankac   = $db->GetOne("SELECT ACCOUNTBANK FROM FINBK WHERE BKCODE='{$paybank}' ");
    
      $rct_header= new ADODB_Active_Record('FINRCTHD', array('RECEIPTNO'));
    
      $rct_header->Load("
      RECEIPTNO='{$rct_number}' 
     ");
      
     $rct_header_saved = true;
    
    if(empty($rct_header->_original)){
     $rct_header->id              =  generateID( $rct_header->_tableat );// 	
     $rct_header->batchid         =  date('Ymd');
     $rct_header->receiptno       =  $rct_number;
     $rct_header->admno           =  $student->admno;
     $rct_header->yearcode        =  $yearcode;
     $rct_header->termcode        =  $termcode;
     $rct_header->formcode        =  $formcode;
     $rct_header->datereceipt     =  date('Y-m-d');;
     $rct_header->pymdcode        =  $paymode;
    }
    
     $rct_header->documentno      =  $payref;
     $rct_header->documentdate    =  $paydate;
     $rct_header->amount          =  $payamount;
     $rct_header->documentamount  =  $payamount;
     
     $rct_header->bkcode          =  $paybank;
     $rct_header->accountbank     =  $bankac;
     $rct_header->fyrprcode       =  $fyear;
     
     $rct_header->auditdate       =  date('Y-m-d');
     $rct_header->audittime       =  time();
     $rct_header->audituser       =  $user->userid;
     
      if(!$rct_header->Save()){
     	 $rct_header_saved = false;
      }
     
         if(!$rct_header_saved){
      	  return json_response(0,'saving of receipt failed');
         }
    
                $itemcode  = $itemDef;
                $rct_detail_lines_saved = 0;
                
                $rct_detail_line= new ADODB_Active_Record('FINRCTDT', array('RECEIPTNO','FITEMCODE'));
    
                $itemname    = valueof($items , $itemcode , $itemcode);
                
                $rct_detail_line->Load("
                RECEIPTNO='{$rct_number}' 
                AND FITEMCODE='{$itemcode}' 
               ");
      
                $rct_detail_line_saved = true;
    
                if(empty($rct_detail_line->_original)){
                 	
                 $rct_detail_line->id           = generateID( $rct_detail_line->_tableat );// 	
                 $rct_detail_line->headerid     = $rct_header->id;
                 $rct_detail_line->batchid      = $rct_header->batchid;
                 $rct_detail_line->receiptno    = $rct_number;
                 $rct_detail_line->yearcode     = $termcode;
                 $rct_detail_line->termcode     = $termcode;
                 $rct_detail_line->formcode     = $formcode;
                 $rct_detail_line->admno        = $student->admno;
                 
                 $rct_detail_line->fitemcode    = $itemcode;
                 $rct_detail_line->fitemname    = $itemname;
                 $rct_detail_line->amount       = $payamount;
                 
                 $rct_detail_line->audituser    =  $user->userid;
                 $rct_detail_line->audittime    =  time();
     
                 if(!$rct_detail_line->Save()){
     	            $rct_detail_line_saved = false;
                 }
     
                 if($rct_detail_line_saved){
                 	++$rct_detail_lines_saved;
                 }else{
                 	self::roll_back( $rct_number, $student->accountno );
                 	return json_response(0,'saving of details failed');
                 }
                 
               }
               
               
        self::update_rct_number( $paybank );
    
        if($rct_detail_lines_saved){
         
         return  json_encode(
	        array(
	         'success' => 1,
	         'message' => "student '{$student->admno}' receipted '{$rct_number}'",
	         'receiptno' => $rct_number,
	        )
	       );
	     
        }else{
         return json_response(0,'receipting Failed');
        }

  }
	
    private function roll_back( $receiptno, $accountno ){
    	global $db;
    	// $db->debug=1;
    	$delh = $db->Execute("DELETE FROM FINRCTHD WHERE RECEIPTNO='{$receiptno}' AND ACCOUNTNO='{$accountno}' ");
    	
    	if($delh){
    	 $deld = $db->Execute("DELETE FROM FINRCTDT WHERE RECEIPTNO='{$receiptno}' AND ACCOUNTNO='{$accountno}' ");
    	 if($deld){	
    	  return  true;
    	 }
    	}
    	
    	return  false;
    }
    
    private function get_fyear(){
  	 
 	  $periods      = array();
 	  $periods[7] = 1;
 	  $periods[8] = 2;
 	  $periods[9] = 3;
 	  $periods[10] = 4;
 	  $periods[11] = 5;
 	  $periods[12] = 6;
 	  $periods[1] = 7;
 	  $periods[2] = 8;
 	  $periods[3] = 9;
 	  $periods[4] = 10;
 	  $periods[5] = 11;
 	  $periods[6] = 12;
 	 
      $year        =  date('Y');
  	  $year_prev   =  date('Y')-1;
  	  $year_next   =  date('Y')+1;
  	  
 	  $month  =  date('n');
 	  $day	  =  date('d');
 	  $fiscal_period =  $periods[$month];
 	  
      if($month>=7){
 	  	$fiscal_year   = "{$year_next}";
 	  }else{
 	  	$fiscal_year  = "{$year}";
 	  }
 	  
// 	  echo "\$month={$month} <br>";
// 	  echo "\$day={$day} <br>";
// 	  echo "\$fiscal_period={$fiscal_period} <br>";
// 	  echo "\$fiscal_year={$fiscal_year} <br>";
 	  
      return  	$fiscal_year .'-' . $fiscal_period;
        
  }
    
    private function get_rct_number($bkcode){
    	global $db;
    	
    	$prefix = $db->GetOne("select NAMESHORT from  FINBK where BKCODE='{$bkcode}' ");
    	$prefix = !empty($prefix) ? $prefix : $bkcode;
    	$appno = $db->GetOne("SELECT MAX(NXTNO)  FROM FINDOCSRC WHERE BKCODE='{$bkcode}' ");
    	
        if(empty($appno)){
          if($db->Execute("INSERT INTO FINDOCSRC (BKCODE,NXTNO) VALUES('{$bkcode}',1)")){
          	$appno = 1;
          }
        }
        
//        $appno_new = $appno+1;
        
//		$db->Execute("UPDATE FINDOCSRC SET NXTNO={$appno_new} WHERE BKCODE='{$bkcode}' ");

	    return   generateUniqueCode($prefix, $appno,6);
	    
    }
    
    private function update_rct_number($bkcode){
    	global $db;
    	
		$update  = $db->Execute("UPDATE FINDOCSRC SET NXTNO=(NXTNO+1) WHERE BKCODE='{$bkcode}' ");

		if($update) {
		 return true;
		}
		
		return false;
	    
    }
    
    public function delete(){
		global $db, $cfg;
		
		$id  = filter_input(INPUT_POST , 'id');
		$del = $db->Execute("UPDATE FINFSTR SET ACTIVE=0 WHERE ID={$id}");
		
		return  self::list_items();
		
		if($del){
		 json_response(1,'deleted');
		}else{
		  json_response(0,'failed');	
		}
		
	}
	
	
}
