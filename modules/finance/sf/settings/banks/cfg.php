<?php
/**
* auto created config file for /modules/finance/sf/settings/banks
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-01-06 16:34:52
*/

$scriptname  = @end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'Banks';//user-readable formart
 $cfg['appname']       = 'banks';//lower cased one word
 $cfg['datasrc']       = 'FINBK';//where to get data
 $cfg['datatbl']       = 'FINBK';//base data src [for updates & deletes]
 $cfg['form_width']    = 400;
 $cfg['form_height']   = 300;
 $cfg['window_width']    = 550;
 $cfg['window_height']   = 400;
 
 $cfg['pkcol']         = 'BKCODE';//the primary key
 
 $cfg['tblbns']['chk_button_add']    = 1;
 $cfg['tblbns']['chk_button_edit']   = 1;
 $cfg['tblbns']['chk_button_delete'] = 1;
 $cfg['tblbns']['chk_button_import'] = 1;
 $cfg['tblbns']['chk_button_export'] = 1;
 

$cfg['columns']['kr4fb'] = array(
                      'dbcol'=>'BKCODE',
                      'title'=>'Bank Code',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['3ptgn'] = array(
                      'dbcol'=>'BKNAME',
                      'title'=>'Bank Name',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['hgcdn'] = array(
                      'dbcol'=>'NAMESHORT',
                      'title'=>'Short Name',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['uf8vq'] = array(
                      'dbcol'=>'ACCOUNTBANK',
                      'title'=>'Bank A/C',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['cf2sa'] = array(
                      'dbcol'=>'ACCOUNTGL',
                      'title'=>'GL A/C',
                      'width'=>100,
                      'sortable'=> true,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'combogrid',
                      'selectsrc'=> 'FINACCT|ACCOUNTNO|ACCOUNTNAME|ACCOUNTNO',
                      );
                      



                      
$cfg['columns']['oip8f'] = array(
                      'dbcol'=>'CRCODE',
                      'title'=>'Currency',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      $combogrid_array   = array();
	 
$combogrid_array['cf2sa']['columns']['ACCOUNTNO']  = array( 'field'=>'ACCOUNTNO', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['cf2sa']['columns']['ACCOUNTNAME']  = array( 'field'=>'ACCOUNTNAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['cf2sa']['source'] ='FINACCT';
  