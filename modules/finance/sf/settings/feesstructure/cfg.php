<?php
/**
* auto created config file for /modules/finance/sf/settings/feesstructure
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-03-07 06:59:15
*/

$scriptname  = @end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'Fees Structure';//user-readable formart
 $cfg['appname']       = 'feesstructure';//lower cased one word
 $cfg['datasrc']       = 'VIEWFS';//where to get data
 $cfg['datatbl']       = 'FINFSTR';//base data src [for updates & deletes]
 $cfg['form_width']    = 450;
 $cfg['form_height']   = 300;
 $cfg['window_width']    = 600;
 $cfg['window_height']   = 450;
 
 $cfg['pkcol']         = 'YEARCODE';//the primary key
 
 $cfg['tblbns']['chk_button_add']    = 1;
 $cfg['tblbns']['chk_button_edit']   = 1;
 $cfg['tblbns']['chk_button_delete'] = 1;
 $cfg['tblbns']['chk_button_import'] = 1;
 $cfg['tblbns']['chk_button_export'] = 1;
 

$cfg['columns']['h2f7p'] = array(
                      'dbcol'=>'YEARCODE',
                      'title'=>'Year',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'combogrid',
                      'selectsrc'=> 'SAYEAR|YEARCODE|YEARNAME|YEARCODE',
                      );
                      



                      
$cfg['columns']['jvzgg'] = array(
                      'dbcol'=>'TERMCODE',
                      'title'=>'Term',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>3,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'SATERMS|TERMCODE|TERMNAME|TERMCODE',
                      );
                      



                      
$cfg['columns']['cuxrw'] = array(
                      'dbcol'=>'TERMNAME',
                      'title'=>'Term',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>2,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['pxxit'] = array(
                      'dbcol'=>'FORMCODE',
                      'title'=>'Form',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>3,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'SAFORMS|FORMCODE|FORMNAME|FORMCODE',
                      );
                      



                      
$cfg['columns']['vmocz'] = array(
                      'dbcol'=>'FORMNAME',
                      'title'=>'Form',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>2,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['ftvpk'] = array(
                      'dbcol'=>'FITEMCODE',
                      'title'=>'Item',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>3,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'combogrid',
                      'selectsrc'=> 'FINITEMS|FITEMCODE|FITEMNAME|FITEMCODE',
                      );
                      



                      
$cfg['columns']['hpmj0'] = array(
                      'dbcol'=>'FITEMNAME',
                      'title'=>'Item',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>2,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['4pwys'] = array(
                      'dbcol'=>'MUCODE',
                      'title'=>'U.O.M',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>4,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['vmh3f'] = array(
                      'dbcol'=>'AMOUNT',
                      'title'=>'Amount',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'numberbox',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['xh1gy'] = array(
                      'dbcol'=>'ACCTGL',
                      'title'=>'Acctgl',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>4,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['mpkyl'] = array(
                      'dbcol'=>'ISTUITION',
                      'title'=>'Istuition',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>4,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> '',
                      'selectsrc'=> '',
                      );
                      



                      $combogrid_array   = array();
	 
$combogrid_array['h2f7p']['columns']['YEARCODE']  = array( 'field'=>'YEARCODE', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['h2f7p']['columns']['YEARNAME']  = array( 'field'=>'YEARNAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['h2f7p']['source'] ='SAYEAR';
	 
$combogrid_array['jvzgg']['columns']['TERMCODE']  = array( 'field'=>'TERMCODE', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['jvzgg']['columns']['TERMNAME']  = array( 'field'=>'TERMNAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['jvzgg']['source'] ='SATERMS';
	 
$combogrid_array['pxxit']['columns']['FORMCODE']  = array( 'field'=>'FORMCODE', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['pxxit']['columns']['FORMNAME']  = array( 'field'=>'FORMNAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['pxxit']['source'] ='SAFORMS';
	 
$combogrid_array['ftvpk']['columns']['FITEMCODE']  = array( 'field'=>'FITEMCODE', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['ftvpk']['columns']['FITEMNAME']  = array( 'field'=>'FITEMNAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['ftvpk']['source'] ='FINITEMS';
  