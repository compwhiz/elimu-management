<?php
/**
* auto created config file for /modules/finance/sf/settings/feesitems
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-03-07 06:50:29
*/

$scriptname  = @end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'Fees Items';//user-readable formart
 $cfg['appname']       = 'feesitems';//lower cased one word
 $cfg['datasrc']       = 'FINITEMS';//where to get data
 $cfg['datatbl']       = 'FINITEMS';//base data src [for updates & deletes]
 $cfg['form_width']    = 450;
 $cfg['form_height']   = 300;
 $cfg['window_width']    = 500;
 $cfg['window_height']   = 400;
 
 $cfg['pkcol']         = 'FITEMCODE';//the primary key
 
 $cfg['tblbns']['chk_button_add']    = 1;
 $cfg['tblbns']['chk_button_edit']   = 1;
 $cfg['tblbns']['chk_button_delete'] = 1;
 $cfg['tblbns']['chk_button_import'] = 1;
 $cfg['tblbns']['chk_button_export'] = 1;
 

$cfg['columns']['5gquh'] = array(
                      'dbcol'=>'FITEMCODE',
                      'title'=>'Item Code',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['mmzf3'] = array(
                      'dbcol'=>'FITEMNAME',
                      'title'=>'Item Name',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'text',
                      'selectsrc'=> '',
                      );
                      



                      
$cfg['columns']['fqobg'] = array(
                      'dbcol'=>'MUCODE',
                      'title'=>'U.O.M',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'select',
                      'selectsrc'=> 'FINMU|MUCODE|MUNAME|MUCODE',
                      );
                      



                      
$cfg['columns']['g0gtk'] = array(
                      'dbcol'=>'ACCTGL',
                      'title'=>'GL A/C',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'combogrid',
                      'selectsrc'=> 'FINACCT|ACCOUNTNO|ACCOUNTNAME|ACCOUNTNO',
                      );
                      



                      
$cfg['columns']['36tsu'] = array(
                      'dbcol'=>'ISTUITION',
                      'title'=>'Is Tuition',
                      'width'=>100,
                      'sortable'=> false,
                      'import'=>1,
                      'visible'=>1,
                      'colalign'=>'left',
                      'validation_class'=> 'easyui-validatebox',
                      'required'=> 1,
                      'validType'=> '',
                      'inputType'=> 'checkbox',
                      'selectsrc'=> '',
                      );
                      



                      $combogrid_array   = array();
	 
$combogrid_array['fqobg']['columns']['MUCODE']  = array( 'field'=>'MUCODE', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['fqobg']['columns']['MUNAME']  = array( 'field'=>'MUNAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['fqobg']['source'] ='FINMU';
	 
$combogrid_array['g0gtk']['columns']['ACCOUNTNO']  = array( 'field'=>'ACCOUNTNO', 'title'=>'Code', 'width'=> 80, 'isIdField' => true );
$combogrid_array['g0gtk']['columns']['ACCOUNTNAME']  = array( 'field'=>'ACCOUNTNAME', 'title'=>'Name', 'width'=> 150, 'isTextField'=>true);
$combogrid_array['g0gtk']['source'] ='FINACCT';
  