<?php

$school      = new school(120);
$dateNow     = textDate(null, true);

$admno       = filter_input(INPUT_GET , ui::fi('find_admno'));
$student     = new student($admno);
    
 ?>
 <html>
  <head>
   <title>Fees Statement</title>
  <script language="JavaScript" type="text/javascript">
    setTimeout("window.print();", 10000);
</script>
<style>
	
    body {  
	 padding : 0px;
	 margin : 2px;
	 font-size:9px;
    }
    
    .header1{
	 font-size:16px;
	 font-weight:bold;
	}
	
    .header2{
	 font-size:14px;
	 font-weight:bold;
	}
	
    .header3{
	 font-size:12px;
	 font-weight:bold;
	}
	
    table.top {
        font-family:Verdana;
        font-size:12px;
        empty-cells: show;
        border:1px solid #000;
        border-collapse:collapse;
        border-spacing: 0.5rem;
        empty-cells:show;
        vertical-align: top;
    }
    
     table.top td{
       font-family:Verdana;
       font-size:14px;
       vertical-align: top;
     }
     
   table.plain {
        font-family:Verdana;
        font-size:12px;
        empty-cells: show;
        border-collapse:collapse;
        border-spacing: 0.5rem;
        empty-cells:show;
        vertical-align: middle;
    }

   table.summary {
        font-family:Verdana;
        font-size:12px;
        empty-cells: show;
        border:1px solid #000;
        border-collapse:collapse;
        border-spacing: 0.5rem;
        empty-cells:show;
    }

    table.summary td {
        border:1px solid #ccc;
    }
    
    td.grid {
      padding:2px;
    }
    
    td.abottom {
        vertical-align:bottom;
        font-size:9px;
    }

    td.bold {
        font-weight:bolder;
    }

    td.fade {
        color:#555;
    }

    span.title{
     font-size:14px;
     font-weight:bold;
    }
    
    @media all {
        .page-break  { display: none; }
    }

    @media print {
        .page-break  { 
            display: block; 
            page-break-before: always;
            margin:0px;
            padding:0px;
        }
    }

    @media screen {
        .page-break  { 
            display: block; 
            page-break-before: always;
            margin:5px;
            padding:5px;
        }
    }

</style>

</head>
 <body>
 <?php
			
    $records = $db->GetArray("SELECT * FROM FINSTH  WHERE ADMNO='{$admno}' ORDER BY AUDITDATE ");
    
    $lines   = array();
    $balance = 0;
    foreach ($records as $itemcode=>$item){
		 	
		  $yearcode     = valueof($item, 'YEARCODE');
		  $formcode     = valueof($item, 'FORMCODE');
		  $termcode     = valueof($item, 'TERMCODE');
		  $entryid      = valueof($item, 'ENTRYID');
		  $doctype      = valueof($item, 'DOCTYPE');
		  $docno        = valueof($item, 'DOCNO');
		  $docdesc      = valueof($item, 'DOCDESC');
		  $amount       = valueof($item, 'AMOUNT');
		  $amount       = $amount*1;
		  $auditdate    = valueof($item, 'AUDITDATE');
		  $auditdate    = date('d/m/Y', strtotime($auditdate));
		  
		  $balance      = ($balance + $amount);
		  
		  switch($doctype){
			  case 'INV':
			  case 'DR':
			   $dr = $amount;
			   $cr = 0;
			  break;
			  case 'REC':
			  case 'CR':
			   $dr = 0;
			   $cr = $amount*-1;
			  break;
		  }
		  
		  $lines[$formcode][$termcode][] = array
		  (
		    'yearcode' => $yearcode,
		    'docno'    => $docno,
		    'docdesc'  => $docdesc,
		    'doctype'  => $doctype,
		    'docdate'  => $auditdate,
		    'dr'       => $dr,
		    'cr'       => $cr,
		    'balance'  => $balance,
		  );
		 	
	}
	
	$balance_dsp = number_format($balance,2);
	
     echo "<table class=\"summary\"  cellpadding=\"5\" cellspacing=\"5\" width=\"100%\" >";
     
      echo "<tr>";
	   echo "<td colspan=\"6\" class=\"header1\" align=\"center\" >{$school->name}</td>";
	  echo "</tr>";
	  
      echo "<tr>";
	   echo "<td colspan=\"6\" class=\"header3\" align=\"center\">FEES STATEMENT AS AT {$dateNow}</td>";
	  echo "</tr>";
			 
		  echo "<tr>";
		   echo "
		   <td align=\"left\" colspan=\"6\">
		   
		     <table  class=\"summary\"  cellpadding=\"2\" cellspacing=\"0\" width=\"50%\">
		      <tr>
		       <td class=\"\">Student No</td><td  class=\"\"><b>{$student->admno}</b></td>
		       </tr>
		       <tr>
		       <td  class=\"\">Student Name</td><td  class=\"\"><b>{$student->fullname}</b></td>
		      </tr>
		       <tr>
		       <td  class=\"\">Current Stream</td><td  class=\"\"><b>{$student->streamcode}</b></td>
		      </tr>
		      <tr >
		       <td  class=\"\">Balance</td><td  class=\"\"><b>{$balance_dsp}</b></td>
		      </tr>
		     </table>
		     
		   </td>
		   ";
		  echo "</tr>";
		  
		  
     if(count($lines)>0){
		
		 $total = 0;
		 
		 foreach ($lines as $formcode=>$form_items){
		  foreach ($form_items as $termcode=>$term_items){
			  
			 echo "<tr>";
			  echo "<td colspan=\"6\">FORM: {$formcode} TERM: {$termcode}</td>";
			 echo "</tr>";
			 
			 echo "
			  <tr>
			   <td class=\"bold\">Dated</td>
			   <td class=\"bold\">Doc No</td>
			   <td class=\"bold\">Description</td>
			   <td class=\"bold\">Dedit (DR)</td>
			   <td class=\"bold\">Credit (CR)</td>
			   <td class=\"bold\">Balance</td>
			  </tr>
			  ";
		 
		 
		   $term_totals = array( 'cr' => 0 ,'dr' => 0 ,'balance' => 0 );
		     
		   foreach ($term_items as $items){
		 
		     $docdate      = valueof($items, 'docdate');
		     $docno        = valueof($items, 'docno');
		     $docdesc      = valueof($items, 'docdesc');
		     $dr           = valueof($items, 'dr',0,'number_format' );
		     $cr           = valueof($items, 'cr',0,'number_format' );
		     $balance      = valueof($items, 'balance',0,'number_format' );
		     $balance_raw  = valueof($items, 'balance');
		     
		     
		     $term_totals['dr']      += valueof($items, 'dr');
		     $term_totals['cr']      += valueof($items, 'cr');
		     
		     //echo "1.\$term_totals['balance']={$term_totals['balance']} <br>";
		     //echo "2.\$balance_raw={$balance_raw} <br>";
		     $term_totals['balance'] = valueof($items, 'balance');
		     //echo "3.\$term_totals['balance']={$term_totals['balance']} <hr>";
		     
		    echo "<tr>
		     <td class=\"\">{$docdate}</td>
		     <td class=\"\">{$docno}</td>
		     <td class=\"\">{$docdesc}</td>
		     <td class=\"\">{$dr}</td>
		     <td class=\"\">{$cr}</td>
		     <td class=\"\">{$balance}</td>
		    </tr>
		     ";
		  
		   }
		   
		     $dr_total        = valueof($term_totals, 'dr',0,'number_format' );
		     $cr_total        = valueof($term_totals, 'cr',0,'number_format' );
		     $balance_total   = valueof($term_totals, 'balance',0,'number_format' );
		     
		    echo "<tr>
		     <td colspan=\"3\" align=\"center\" class=\"bold\">TOTALS</td>
		     <td class=\"bold\">{$dr_total}</td>
		     <td class=\"bold\">{$cr_total}</td>
		     <td class=\"bold\">{$balance_total}</td>
		    </tr>
		     ";
		     
		     echo "<tr>
		     <td colspan=\"6\" align=\"left\" class=\"fade\">CLOSING BALANCE FOR F{$formcode}T{$termcode} : {$balance_total}</td>
		    </tr>
		     ";
		  }
		 }
		 
		
		 
     }else{
		 echo "<tr>";
		  echo "<td colspan=\"5\">No Items</td>";
		 echo "</tr>";
     }
		
     echo "</table>";
 ?>
 </body>
  </html>
 
