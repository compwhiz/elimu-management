<?php

/**
* auto created config file for /modules/finance/sf/reports/feesstatement
* @author kenmsh@gmail.com
* 
* @version 2.0
* @since 2016-01-06 16:17:43
*/

define('MAKE_FIELDS_UNIQUE' , true);

$scriptname  = @end(explode('/',$_SERVER['PHP_SELF']));
$scriptpath  = str_replace($scriptname,'',$_SERVER['PHP_SELF']);
$root        = $_SERVER['DOCUMENT_ROOT'].$scriptpath;
$delimeter  = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? '\\' : '/'; 

define('BASEPATH',$root);
define('DIR',     dirname( __FILE__ ) . $delimeter); 
    
$break   = explode($delimeter, DIR);

define('CLASSFILE',  $break[count($break) - 2] ); 

 $cfg                  = array();
 $cfg['apptitle']      = 'Fees Statement';//user-readable formart
 $cfg['appname']       = 'feesstatement';//lower cased
 $cfg['datasrc']       = 'ACSP';//where to get data
 $cfg['datatbl']       = 'ACSP';//base data src [for updates & deletes]
 $cfg['form_width']    = 400;
 $cfg['form_height']   = 280;

$combogrid_array   = array();
$combogrid_array['find_admno']['columns']['ADMNO']      = array( 'field'=>'ADMNO', 'title'=>'Admission No', 'width'=> 100 , 'isIdField' => true );
$combogrid_array['find_admno']['columns']['FULLNAME']   = array( 'field'=>'FULLNAME', 'title'=>'Full Name', 'width'=> 200, 'isTextField'=>true);
$combogrid_array['find_admno']['columns']['STREAMCODE'] = array( 'field'=>'STREAMCODE', 'title'=>'Stream', 'width'=>80);
$combogrid_array['find_admno']['source'] ='VIEWSP';
	
