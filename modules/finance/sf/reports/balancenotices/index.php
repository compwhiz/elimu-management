<?php

/**
* auto created index file for modules/students/exams/reports/reportforms
* @author kenmsh@gmail.com
*
* @version 2.0
* @since 2016-01-12 09:11:55
*/

 $forms = $db->GetAssoc('SELECT FORMCODE,FORMNAME FROM SAFORMS ORDER BY FORMCODE');
 $streams_data = $db->GetAssoc('SELECT STREAMCODE,STREAMNAME,FORMCODE FROM SASTREAMS');
 $years = $db->GetAssoc('SELECT YEARCODE,YEARNAME FROM SAYEAR ORDER BY YEARCODE');
 $terms = $db->GetAssoc('SELECT TERMCODE,TERMNAME FROM SATERMS ORDER BY TERMCODE');
 $exams = $db->GetAssoc('SELECT TERMCODE,TERMNAME FROM SATERMS ORDER BY TERMCODE');

 $school = new school();
 
 $forms  = array();
 if(isset($streams_data)){
  if(sizeof($streams_data)>0){	
   foreach ($streams_data as $streamcode=>$stream_data){
   	$streamname = valueof($stream_data, 'STREAMNAME');
   	$formcode   = valueof($stream_data, 'FORMCODE');
   	$forms[$formcode] = "Form {$formcode}";
   	$forms[$streamcode] = "Form {$formcode}-{$streamname}";
   }
  }
 }
?>
        <form id="<?php echo ui::fi('ff'); ?>" method="post" novalidate>
		<table cellpadding="2" cellspacing="0" width="100%">
		
			<tr>
				<td  width="130px">Year:</td>
				<td>
					<?php echo ui::form_select_fromArray('year', $years, $school->active_yearcode," onchange=\"{$cfg['appname']}.list_students();\" ");  ?>
				</td>
			</tr>
			
			<tr>
				<td>Term:</td>
				<td>
					<?php echo ui::form_select_fromArray('term', $terms, $school->active_termcode,"onchange=\"{$cfg['appname']}.list_students();\"");  ?>
				</td>
			</tr>
			
			<tr>
				<td>Form</td>
				<td>
				<?php echo ui::form_select_fromArray('form', $forms,'',"onchange=\"{$cfg['appname']}.list_students();\"");  ?>
				</td>
			</tr>
			
			<tr>
				<td>Student *(Optional):</td>
				<td>
					<?php echo ui::form_input( 'text', 'find_admno', 20, '', '', '', '', '', '' , ""); ?>
				</td>
			</tr>
			
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			
			<tr>
				<td>&nbsp;</td>
				<td>
				 <a href="javascript:void(0)" class="easyui-linkbutton"  onclick="<?php echo $cfg['appname']; ?>.print_report();"><i class="fa fa-print"></i> Open Report</a>
				 &nbsp;
				 <a href="javascript:void(0)" class="easyui-linkbutton"  onclick="<?php echo $cfg['appname']; ?>.clearForm()"><i class="fa fa-undo"></i> Reset</a>
				</td>
			</tr>
			
		</table>
     </form>
     <script>
		
	  var <?php echo $cfg['appname']; ?> = {
		clearForm:function (){
			$('#<?php echo ui::fi('ff'); ?>').form('clear');
			$('#<?php echo ui::fi('find_admno'); ?>').val();
			$('.combo-text').val();
		},
		list_students:function (){
		 var form = $('#<?php echo ui::fi('form'); ?>').val();
  		   
  		 $('#<?php echo ui::fi('find_admno'); ?>').combogrid({
			panelWidth:300,
			idField:'code',
			textField:'name',
			url:'./endpoints/crud/',
			mode:'remote',
			delay: 100,
			queryParams:{ modvars:'<?php echo $vars; ?>',function:'data_students', form: form },
			columns:[[
				{field:'code',title:'Admission No',width:120},
				{field:'name',title:'Full Name',width:150},
				{field:'stream',title:'Stream',width:100}
			]]
		   });
		 
		},
		loadProfile :function(){
		  var find_admno = $('#<?php echo ui::fi('find_admno'); ?>').combogrid('getValue');
		  $.post('./endpoints/crud/',  'modvars=<?php echo $vars; ?>&function=get_profile&admno='+find_admno,function( html ){
  		   $('#<?php echo ui::fi('div_stage'); ?>').html(html);
  		  });
		},
        print_report:function (){
         var fdata = $('#<?php echo ui::fi('ff'); ?>').serialize();
  	     var w=window.open('./endpoints/print/?modvars=<?php echo $vars; ?>&'+fdata,'p','height=800,width=830,toolbar=no,menubar=no,directories=no,location=no,scrollbars=yes,status=no,resizable=no,fullscreen=no,top=0,left=0');
          w.focus();
        },
	  }
        
    <?php
      echo ui::ComboGrid($combogrid_array,'find_admno');
	?>
			
	</script>
