<?php

//print_pre($_GET);//remove
//$db->debug=1;//remove 


$school      = new school(120);
$dateNow     = textDate(null, true);
$dateY       = date('Y');
$admno       = filter_input(INPUT_GET , ui::fi('find_admno'));
$yearcode    = filter_input(INPUT_GET , ui::fi('year'));
$termcode    = filter_input(INPUT_GET , ui::fi('term'));
$formcode    = filter_input(INPUT_GET , ui::fi('form'));
$fsCol       = (strlen($formcode)==1) ? 'FORMCODE' : 'STREAMCODE';
$sql_extra   = '';

if(!empty($admno)){
$sql_extra = " and ADMNO='{$admno}'";
}

$principal = $db->GetRow("
SELECT C.SLTCODE,T.TCHNO, C.FULLNAME FROM SAPRC T
INNER JOIN SATEACHERS C ON C.TCHNO = T.TCHNO
WHERE T.YEAREND>={$dateY} AND T.YEAREND>={$dateY}
");

if(empty($principal)){
	die("Missing Set Up for Current Serving Principal");
}

$principal_name       = valueof($principal,'SLTCODE') .'. '. valueof($principal,'FULLNAME');
$principal_signature  = valueof($principal,'TCHNO').'.jpg';
$principal_signature_default   = 'default.jpg';
$signature           = ROOT.'public/signatures/'.$principal_signature;
$signature_default   = ROOT.'public/signatures/'.$principal_signature_default;
$rand                = rand(1,100);

 if(file_exists($signature) && is_readable($signature)){
	$signature = '<img src="../../public/signatures/'.valueof($principal,'TCHNO').'.jpg?cache_reload='.$rand.'" border="0">';
 }else{
	$signature = '<img src="../../public/signatures/default.jpg?cache_reload='.$rand.'" border="0">';
 }


$students = $db->Execute("
SELECT ADMNO,FULLNAME,STREAMNAME,GNDCODE
 FROM VIEWSP 
 WHERE {$fsCol}='{$formcode}'
 {$sql_extra}
 order by ADMNO
");


if(!$students){
 die("No Student To Display");
}

if($students->RecordCount()==0){
 die("No Student To Display");
}

 ?>
 <html>
  <head>
   <title>Fees Balance Notifications</title>
  <script language="JavaScript" type="text/javascript">
    setTimeout("window.print();", 10000);
</script>
<style>
	
    body {  
	 padding : 0px;
	 margin : 2px;
	 font-size:9px;
    }
    
    .header1{
	 font-size:18px;
	 font-weight:bold;
	}
	
    .header2{
	 font-size:14px;
	 font-weight:bold;
	}
	
    .header3{
	 font-size:12px;
	 font-weight:bold;
	}
	
    table.top {
        font-family:Verdana;
        font-size:12px;
        empty-cells: show;
        border:1px solid #000;
        border-collapse:collapse;
        border-spacing: 0.5rem;
        empty-cells:show;
        vertical-align: top;
    }
    
     table.top td{
       font-family:Verdana;
       font-size:14px;
       vertical-align: top;
     }
     
   table.plain {
        font-family:Verdana;
        font-size:12px;
        empty-cells: show;
        border-collapse:collapse;
        border-spacing: 0.5rem;
        empty-cells:show;
        vertical-align: middle;
    }

   table.summary {
        font-family:Verdana;
        font-size:12px;
        empty-cells: show;
        border:1px solid #fff;
        border-collapse:collapse;
        border-spacing: 0.5rem;
        empty-cells:show;
    }

    table.summary td {
        border:1px solid #fff;
    }
    
    table.summary td.bottom {
        border-bottom:2px solid #000;
    }
    
    table.summary td.bottom-dashed {
        border-bottom:2px dotted #000;
    }
    
    td.grid {
      padding:2px;
    }
    
    td.abottom {
        vertical-align:bottom;
        font-size:9px;
    }

    td.bold {
        font-weight:bolder;
    }

    td.fade {
        color:#555;
    }

    span.title{
     font-size:14px;
     font-weight:bold;
    }
    
    @media all {
        .page-break  { display: none; }
    }

    @media print {
        .page-break  { 
            display: block; 
            page-break-before: always;
            margin:0px;
            padding:0px;
        }
    }

    @media screen {
        .page-break  { 
            display: block; 
            page-break-before: always;
            margin:5px;
            padding:5px;
        }
    }

</style>

</head>
 <body>
 <?php

    $banks_str     =  array();
    $banks         = $db->GetAssoc("select BKNAME,ACCOUNTBANK from FINBK ");

    if(sizeof($banks)>0){
	 foreach($banks as $bankname => $accountno){
		 $banks_str[] = " <b>{$bankname} A/C {$accountno}</b> ";
	 }
	}

	$banks_str     = implode(' or ', $banks_str);

	foreach($students as $student){
		
	$admno         = valueof($student, 'ADMNO'); 
	$fullname      = valueof($student, 'FULLNAME'); 
	$streamname    = valueof($student, 'STREAMNAME'); 
	$gndcode       = valueof($student, 'GNDCODE'); 
	$relationship  = strtolower($gndcode)=='f' ? 'daughter' : 'son';
    $balance_raw   = $db->GetOne("SELECT SUM(AMOUNT) FROM FINSTH WHERE ADMNO='{$admno}' ");
    $balance       = $balance_raw<0 ? $balance_raw*-1 : $balance_raw;
	$balance_dsp   = number_format($balance,2);
	$pay_str       = $balance_raw>0 ? " is expected to pay  Kshs. <b>{$balance_dsp}</b> being TERM {$termcode} fees and any unpaid fee balance" : " has an overpayment of Kshs. <b>{$balance_dsp}</b>";
	
     echo "<table class=\"summary\"  cellpadding=\"5\" cellspacing=\"5\" width=\"100%\" >";
     
      echo "<tr>";
	   echo "<td colspan=\"6\" class=\"header1\" align=\"center\" >{$school->name}</td>";
	  echo "</tr>";
	  
      echo "<tr>";
	   echo "<td colspan=\"6\" class=\"header2\" align=\"center\" >P.O BOX {$school->address} , email: {$school->email}</td>";
	  echo "</tr>";
	  
      echo "<tr>";
	   echo "<td colspan=\"6\" class=\"header3 bottom\" align=\"left\">FEES BALANCE NOTIFICATION FOR TERM {$termcode}, {$yearcode} AS AT {$dateNow}</td>";
	  echo "</tr>";
			 
	  
      echo "<tr>";
	   echo "<td colspan=\"6\" class=\"header4 bottom-dashed\" align=\"left\"><b>NAME:</b>&nbsp;&nbsp;&nbsp;{$fullname} &nbsp;&nbsp;&nbsp; <b>ADMNO:</b>&nbsp;&nbsp;&nbsp;{$admno}   &nbsp;&nbsp;&nbsp;<b>CLASS:</b>&nbsp;&nbsp;&nbsp;{$streamname} </td>";
	  echo "</tr>";
			 
		  echo "<tr>";
		   echo "
		   <td align=\"left\" colspan=\"6\">
		   
		     <table  class=\"summary\"  cellpadding=\"2\" cellspacing=\"0\" width=\"100%\">
		     
		      <tr>
		       <td colspan=\"2\" align=\"left\" style=\"padding:10px\">
		       <b>Dear Parent/Guardian</b> <br><br>
		       Your {$relationship} {$pay_str} .<br>
		       Attached find the fees statement for your perusal.<br>
		       The school accounts are {$banks_str}. <br>
		       Contact No {$school->telephone} <br>
		       
		       <br><br>
		       
		       {$signature} <br><br>
		        
		        <span style=\"border-top:1px solid black\">{$principal_name}</span><br><br>
		        <span style=\"border-bottom:2px solid black\"><b>PRINCIPAL (PTA/BOG SECRETARY)</b></span>
		       
		       </td>
		      </tr>
		      
		      
		      
		     </table>
		     
		   </td>
		   ";
		  echo "</tr>";
		  
     echo "</table>";
     echo " \r\n<div class=\"page-break\"></div>  \r\n";

   }
    
 ?>
 </body>
  </html>
 
