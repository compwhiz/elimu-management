<?php

/**
* auto created config file for /modules/finance/sf/reports/balancenotices
* @author kenmsh@gmail.com
* 
* @version 2.0
* @since 2016-01-06 16:17:43
*/

  class balancenotices{
	private $admno;
	
	public function __construct(){
		global $cfg;
		
		$this->id           = filter_input(INPUT_POST , 'id');
		$this->admno        = filter_input(INPUT_POST , 'admno');
	}
	
	
	public function get_profile(){
		global $db,$cfg;
		
         $student    = new student( $this->admno );
         
         if(!empty($student->fullname)){
			 
			 $balance = $db->GetOne("SELECT SUM(AMOUNT) FROM FINSTH WHERE ADMNO='{$this->admno}' ");
			 $success = 1;
			 $message = 'ok';
			 
			 $data['admno']       =  $student->admno;
			 $data['fullname']    =  $student->fullname;
			 $data['streamname']  =  $student->streamcode;
			 $data['formname']    =  $student->formcode;
			 $data['balance']     =  number_format($balance,2);
			 
	     }else{
		     $success = 1;
			 $message = 'Student Not Found';
	     }
	     
          $data['success']    =  $success;
          $data['message']    =  $message;
         
         return json_encode($data);
	}
	
	
 public function data_students(){
		global $db, $cfg;
	$q         =  filter_input(INPUT_POST , 'q');
	$q         =  trim($q);
	$formcode  =  filter_input(INPUT_POST , 'form', FILTER_SANITIZE_STRING);
	$eqCol     =  (strlen($formcode)==1) ? 'FORMCODE' : 'STREAMCODE';
	
	$sql_where =  !empty($q) ?  "  and ( ADMNO like '%{$q}%' or FULLNAME like '%{$q}%' )" : '';
	$rs        =  $db->SelectLimit("select ADMNO CODE,FULLNAME NAME, STREAMNAME STREAM  from VIEWSP where {$eqCol}='{$formcode}' {$sql_where} ",10);
	$json_cols = array('code','name','stream');
	$str = '';
	if($rs){
		$numRecs = $rs->RecordCount();
		$numCols = count($json_cols);
		if($numRecs>0){
			$count = 1;
			$count_cols = 0;

			$str = '';
			while (!$rs->EOF){
				
				 $str .= "{";
				 
				 $count_cols = 1;
				 foreach ($json_cols as $json_col ){
					$db_col = strtoupper($json_col);
					$json_col_value = isset($rs->fields[$db_col]) ? $rs->fields[$db_col] : '';
					$str .= "\"{$json_col}\":\"{$json_col_value}\"";
					$str .=  $count_cols<$numCols ? ',' : '';
					
					++$count_cols;
				 }
			 
				 $str .= "}";
				 
				$str .=  $count <$numRecs ? ",\r\n" : '';			 
				
			 ++$count;	
			 $rs->MoveNext();
			}
		}
	}
 
	 header('Content-type: text/json');
	 
	 echo '['.$str.']';
	 
  }
 
	
}
