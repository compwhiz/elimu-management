<?php
require_once('./init.php');
require_once('./config/db.php');
include_once('./include/combine.php');

//if (!isset($_SESSION['user'])) {
//    require_once('login.php');
//    die;
//}

$user = new user();
$sql = 'SELECT USERID,USERNAME,PASSWORD from users';
$users = $db->GetArray($sql);
?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Change Password</title>
    <link rel="stylesheet" href="/public/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
</head>
<body class="mb-5">


<div class="container mt-5" id="app">
    <div>
        <div class="alert alert-info" role="alert" v-if="showpass">
            <strong>Password changed succesfully</strong>
        </div>

        <form class="form">
            <div class="form-group">
                <label for="username">User Name </label>
                <input
                        disabled
                        value="<?php echo $user->username ?>"
                        type="text"
                        name="username" id="username" class="form-control" placeholder="User Name "
                        aria-describedby="Username">
            </div>
            <div class="form-group">
                <label for="password ">New Password </label>
                <input type="text" name="password " id="password " class="form-control"
                       aria-describedby="password" v-model="newpassword">
            </div>
            <button type="button" class="btn btn-primary" @click.prevent="changepassword">Change Password
            </button>
        </form>

    </div>

  

</div>


<script src="/public/js/jquery.js"></script>
<script src="/public/js/tether.js"></script>
<script src="/public/js/bootstrap.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.16/vue.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>

<script>
    $(document).ready(function () {
        $('#example').DataTable();
    });
    let app = new Vue({
        data: {
            userid: <?php echo "\"$user->userid\"" ?>,
            newpassword: '',
            teacherpassword:'',
            showpass: false,
        },
        el: '#app',
        methods: {
            changepassword() {
                axios.post('./endpoints/changepassword/', this.$data).then(response => {

                    this.newpassword = '';
                    this.showpass = true
                }).catch(

                );
            }
        }
    })
</script>
</body>
</html>